<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => 'web'], function () {
    // Softdelete table row
    Route::get('/dashboard/{id}/{model}', 'Controller@delete')->name('delete.submit');
    // Dashboard route
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/fees-dashboard', 'HomeController@feesCollection')->name('fees-dashboard');
    /***************************** Master routes  ****************************************************************************/
    // School route
    Route::get('/school-list', 'SchoolController@index')->name('school-list')->middleware('can:school-list');
    Route::get('/school-add', 'SchoolController@add')->name('school-add')->middleware('can:school-add');
    Route::match(['get', 'post'], '/school-edit/{id?}', 'SchoolController@add')->name('school-edit')->middleware('can:school-edit');
    // Medium / College Stream route
    Route::get('/medium-list', 'MediumController@index')->name('medium-list')->middleware('can:medium-list');
    Route::get('/medium-add', 'MediumController@add')->name('medium-add')->middleware('can:medium-add');
    Route::match(['get', 'post'], '/medium-edit/{id?}', 'MediumController@add')->name('medium-edit')->middleware('can:medium-edit');
    // Academic Year route
    Route::get('/academic-year-list', 'AcademicYearController@index')->name('academic-year-list')->middleware('can:academic-year-list');
    Route::get('/academic-year-add', 'AcademicYearController@add')->name('academic-year-add')->middleware('can:academic-year-add');
    Route::match(['get', 'post'], '/academic-year-edit/{id?}', 'AcademicYearController@add')->name('academic-year-edit')->middleware('can:academic-year-edit');
    // Class route
    Route::get('/class-list', 'ClassListController@index')->name('class-list')->middleware('can:class-list');
    Route::get('/class-add', 'ClassListController@add')->name('class-add')->middleware('can:class-add');
    Route::match(['get', 'post'], '/class-edit/{id?}', 'ClassListController@add')->name('class-edit')->middleware('can:class-edit');
    // Designation route
    Route::get('/designation-list', 'DesignationController@index')->name('designation-list')->middleware('can:designation-list');
    Route::get('/designation-add', 'DesignationController@add')->name('designation-add')->middleware('can:designation-add');
    Route::match(['get', 'post'], '/designation-edit/{id?}', 'DesignationController@add')->name('designation-edit')->middleware('can:designation-edit');
    // Document route
    Route::get('/document-list', 'DocumentController@index')->name('document-list');
    Route::get('/document-add', 'DocumentController@add')->name('document-add');
    Route::match(['get', 'post'], '/document-edit/{id?}', 'DocumentController@add')->name('document-edit');
    // Category route
    Route::get('/category-list', 'CategoryController@index')->name('category-list');
    Route::get('/category-add', 'CategoryController@add')->name('category-add');
    Route::match(['get', 'post'], '/category-edit/{id?}', 'CategoryController@add')->name('category-edit');
    // Admission Type route
    Route::get('/admissiontype-list', 'AdmissionTypeController@index')->name('admissiontype-list');
    Route::get('/admissiontype-add', 'AdmissionTypeController@add')->name('admissiontype-add');
    Route::match(['get', 'post'], '/admissiontype-edit/{id?}', 'AdmissionTypeController@add')->name('admissiontype-edit');
    // Scholarship Type route
    Route::get('/scholarshiptype-list', 'ScholarshipTypeController@index')->name('scholarshiptype-list');
    Route::get('/scholarshiptype-add', 'ScholarshipTypeController@add')->name('scholarshiptype-add');
    Route::match(['get', 'post'], '/scholarshiptype-edit/{id?}', 'ScholarshipTypeController@add')->name('scholarshiptype-edit');
    // FeesHead route
    Route::get('/fees-head-list', 'FeesHeadController@index')->name('fees-head-list');
    Route::get('/fees-head-add', 'FeesHeadController@add')->name('fees-head-add');
    Route::match(['get', 'post'], '/fees-head-edit/{id?}', 'FeesHeadController@add')->name('fees-head-edit');
    // ClassMaster route
    Route::get('/class-master-list', 'ClassMasterController@index')->name('class-master-list');
    Route::get('/class-master-add', 'ClassMasterController@add')->name('class-master-add');
    Route::match(['get', 'post'], '/class-master-edit/{id?}', 'ClassMasterController@add')->name('class-master-edit');
    /***************************** Fees Management routes  ****************************************************************************/
    // Class Fees route
    Route::get('/classfees-list', 'ClassYearlyFeesController@index')->name('classfees-list');
    Route::get('/classfees-add/{id}', 'ClassYearlyFeesController@add')->name('classfees-add');
    Route::get('/classfees-delete/{id}', 'ClassYearlyFeesController@deleteFeeHead')->name('classfees-delete');
    Route::match(['get', 'post'],'/classfees-edit/{id}', 'ClassYearlyFeesController@add')->name('classfees-edit');

    Route::get('/student-list/{school_id?}', 'ClassYearlyFeesController@studentList')->name('student-list');
    Route::match(['get', 'post'],'/feescollection/{id}', 'ClassYearlyFeesController@feesCollection')->name('feescollection');
    Route::get('printpayrecipt/{id}', 'ClassYearlyFeesController@printFeeRecipt')->name('printpayrecipt');
    Route::get('deleteReceipt/{id}', 'ClassYearlyFeesController@deleteFeeRecipt')->name('deleteReceipt');
    /***************************** Student Management routes  ****************************************************************************/
    // Admission route
    Route::get('/admission-list', 'AdmissionController@index')->name('admission-list')->middleware('can:admission-list');
    Route::get('/admission-add', 'AdmissionController@add')->name('admission-add')->middleware('can:admission-add');
    Route::match(['get', 'post'], '/admission-edit/{id?}', 'AdmissionController@add')->name('admission-edit')->middleware('can:admission-edit');

    /******************************************* General Setting routes *******************************************/
        //Profile route
        Route::match(['get', 'post'], '/my-profile', 'GeneralSettingController@profile')->name('my-profile');
        //Change Password route
        Route::match(['get', 'post'], '/change-password', 'GeneralSettingController@changePassword')->name('change-password');
    /******************************************* System Setting routes *******************************************/
        //User route
        Route::get('/user-list', 'UserController@index')->name('user-list')->middleware('can:user-list');
        Route::get('/user-add', 'UserController@add')->name('user-add')->middleware('can:user-add');
        Route::match(['get', 'post'], '/user-edit/{id?}', 'UserController@add')->name('user-edit')->middleware('can:user-edit');
        //Role route
        Route::get('/role-list', 'RoleController@index')->name('role-list')->middleware('can:role-list');
        Route::get('/role-add', 'RoleController@add')->name('role-add')->middleware('can:role-add');
        Route::match(['get', 'post'], '/role-edit/{id?}', 'RoleController@add')->name('role-edit')->middleware('can:role-edit');
    /******************************************* Class Room Management routes *******************************************/
        Route::match(['get', 'post'],'/class-room-list', 'ClassRoomController@index')->name('class-room-list');
        Route::get('class-room-excel',array('as'=>'class-room-excel','uses'=>'ClassRoomController@classRoomExcelExport'));
        Route::match(['get', 'post'],'/student-class-list', 'ClassRoomController@studentPromote')->name('student-class-list');
        Route::get('printclasslist/{school?}/{class?}/{section?}/{scholarship?}', 'ClassRoomController@printClassList')->name('printclasslist');

        Route::get('/class-teacher-list', 'ClassRoomController@teacherList')->name('class-teacher-list');
        Route::match(['get', 'post'],'/class-teacher-add', 'ClassRoomController@teacherAdd')->name('class-teacher-add');
        Route::match(['get', 'post'],'class-teacher-edit/{id?}', 'ClassRoomController@teacherAdd')->name('class-teacher-edit');
        Route::get('print-teacher-list', 'ClassRoomController@printClassTeacher')->name('print-teacher-list');
    /******************************************* Exam Management routes *******************************************/
        // Subject route
        Route::get('/subject-list', 'SubjectController@index')->name('subject-list');
        Route::get('/subject-add', 'SubjectController@add')->name('subject-add');
        Route::match(['get', 'post'], '/subject-edit/{id?}', 'SubjectController@add')->name('subject-edit');
        // Test route
        Route::get('/test-list', 'TestController@index')->name('test-list');
        Route::get('/test-add', 'TestController@add')->name('test-add');
        Route::match(['get', 'post'], '/test-edit/{id?}', 'TestController@add')->name('test-edit');
        Route::get('/class-test-subject', 'ClassTestController@index')->name('class-test-subject');
        Route::match(['get', 'post'], '/assign-class-subject/{classId?}', 'ClassTestController@subjectAssign')->name('assign-class-subject');
        Route::match(['get', 'post'], '/assign-class-test/{classId?}', 'ClassTestController@testAssign')->name('assign-class-test');

    /******************************************* Reports routes *******************************************/
        Route::match(['get', 'post'],'/generalregister', 'ReportController@generalRegister')->name('generalregister')->middleware('can:generalregister');
        Route::get('printgeneralregister/{type}/{year}/{school}', 'ReportController@printGeneralRegister')->name('printgeneralregister')->middleware('can:printgeneralregister');
        //Bonafied Certificate
        Route::get('/bonafied-certificate-list', 'ReportController@index')->name('bonafied-list')->middleware('can:bonafied-list');
        Route::post('/bonafied-certificate', 'ReportController@bonafiedAdd')->name('bonafiedcertificate')->middleware('can:bonafiedcertificate');
        Route::match(['get', 'post'],'printbonafied/{id}', 'ReportController@printBonafied')->name('printbonafied')->middleware('can:printbonafied');
        //;Leaving Certificate
        Route::match(['get', 'post'],'/leaving-certificate', 'ReportController@leavingAdd')->name('leaving-certificate')->middleware('can:leaving-certificate');
        Route::match(['get', 'post'],'printleaving/{id}', 'ReportController@printLeaving')->name('printleaving')->middleware('can:printleaving');
        //Character Certificate
        Route::get('/character-certificate-list', 'ReportController@characterList')->name('character-list')->middleware('can:character-list');
        Route::post('/character-certificate', 'ReportController@characterAdd')->name('character-certificate')->middleware('can:character-certificate');
        Route::match(['get', 'post'],'printcharacter/{id}', 'ReportController@printCharacter')->name('printcharacter')->middleware('can:printcharacter');
        //Attempt Certificate
        Route::get('/attempt-certificate-list', 'ReportController@attemptList')->name('attempt-list')->middleware('can:attempt-list');
        Route::post('/attempt-certificate', 'ReportController@attemptAdd')->name('attempt-certificate')->middleware('can:attempt-certificate');
        Route::match(['get', 'post'],'printattempt/{id}', 'ReportController@printAttempt')->name('printattempt')->middleware('can:printattempt');
    /******************************************* Attendance Management routes *******************************************/
        //Attendance route
        Route::match(['get','post'],'/student-attendance-list', 'StudentAttendanceController@index')->name('student-attendance-list');
		Route::match(['get','post'], '/student-attendance-add', 'StudentAttendanceController@studentAttendance')->name('student-attendance-add');
		Route::match(['get','post'], '/view-attendance/{attenid?}', 'StudentAttendanceController@viewAttendance')->name('view-attendance');
        Route::match(['get','post'],'/attendance-status/{id?}', 'StudentAttendanceController@attendanceChangeStatus')->name('attendance-status');
        Route::match(['get','post'], '/delete-attendance/{attenid?}', 'StudentAttendanceController@deleteAttendance')->name('delete-attendance');
    /******************************************* Staff Management routes *******************************************/
        //staff route
        Route::get('/employee-list', 'EmployeeController@index')->name('employee-list');
        Route::get('/employee-add', 'EmployeeController@add')->name('employee-add');
        Route::match(['get', 'post'], '/employee-edit/{id?}', 'EmployeeController@add')->name('employee-edit');
    /******************************************* Fees Report routes *******************************************/
        //Fees Report
        Route::match(['get', 'post'],'/class-fees-report', 'FeesReportController@index')->name('class-fees-report');
        Route::match(['get', 'post'],'/daily-fees-report', 'FeesReportController@dailyFeesReport')->name('daily-fees-report');
        //Monthly Fees Report
        Route::match(['get', 'post'],'/total-fees-report', 'FeesReportController@total_fees_report')->name('total-fees-report');
 });
