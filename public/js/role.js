// Move This Code in Common Js file Start
$(function () {
    // setTimeout(() => {
    //     $("#flashDivId").click();
    // }, 3000);
});
// End

function checkAllChild(that){
    $that = $(that);
    var parentDiv = $that.closest('.subdiv');
    $(parentDiv).find(':checkbox').each(function(){
        if($that.is(':checked')){
            $(this).prop('checked', true);
        }else{
            $(this).prop('checked', false);
        }
    });
}

function checkForParent(checkbox){
    $checkbox = $(checkbox);
    var parentDiv = $checkbox.closest('.subdiv');
    var parentCheckBox = parentDiv.find('.parentCheckBox');
    var checkedLength = parseInt($(parentDiv).find('.childCheckBox:checked').length);
    var checkboxlength = parseInt($(parentDiv).find('.childCheckBox:checkbox').length);
    if(!$checkbox.is(':checked')){
        $(parentCheckBox).prop('checked', false);
    }else if(checkedLength == checkboxlength){
        $(parentCheckBox).prop('checked', true);
    }
}