$('#SchoolId').change(function(){
    var SchoolId = $(this).val();
    $('#TeacherId option[value=""]').prop('selected', true);
    var teacherEleme = $("[name='teacher_id'] option");
    $.each(teacherEleme, function(i, opt){
        if(i > 0){
            if($(opt).attr('data-school') == SchoolId){
                $(opt).removeClass('hidden');
            }else{
                $(opt).addClass('hidden');
            }
        }
    });
    $("#TeacherId").trigger('change');
});