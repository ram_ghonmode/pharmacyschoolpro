$(document).ready(function () {
    $('#admissionDetailFormId').validate({ 
        errorClass: 'errors',
        rules: {
            school_id: {
                required: true
            },
            fees_type: {
                required: true
            },
            admission_type: {
                required: true
            },
            gr_number: {
                required: true
            },
            student_id: {
                required: true
            },
            first_name: {
                required: true
            },
            middle_name: {
                required: true
            },
            last_name: {
                required: true
            },
            father_first_name: {
                required: true
            },
            father_last_name: {
                required: true
            },
            mother_name: {
                required: true
            },
            gender: {
                required: true
            },
            admission_date: {
                required: true
            },
            academic_year: {
                required: true
            },
            admit_to: {
                required: true
            },
            current_class: {
                required: true
            },
            section: {
                required: false
            }, 
            medium: {
                required: false
            },
            dob: {
                required: false
            },
            age: {
                required: false,
                number: true,
            },
            country: {
                required: false
            },
            state: {
                required: false
            },
            district: {
                required: false
            },
            tahsil: {
                required: false
            },
            mobile_no: {
                required: false,
                number: true,
            },
        },
        messages: {
            school_id: {
                required: "School is required."
            },
            fees_type: {
                required: "Fees type is required."
            },
            admission_type: {
                required: "Admission type is required."
            },
            gr_number: {
                required: "General no. is required."
            },
            student_id: {
                required: "Student Id is required."
            },
            first_name: {
                required: "First name is required."
            },
            middle_name: {
                required: "Middle name is required."
            },
            last_name: {
                required: "Last name is required."
            },
            father_first_name: {
                required: "Father first name is required."
            },
            father_last_name: {
                required: "Father last name is required."
            },
            mother_name: {
                required: "Mother name is required."
            },
            gender: {
                required: "Gender is required."
            },
            admission_date: {
                required: "Admission date is required."
            },
            academic_year: {
                required: "Academic year is required."
            },
            admit_to: {
                required: "Admit to is required."
            },
            current_class: {
                required: "Current class is required."
            },
            medium: {
                required: "Medium is required."
            },
            section: {
                required: "Section is required."
            },
            dob: {
                required: "DOB is required."
            },
            age: {
                number: "Age must be digits."
            },
            country: {
                required: "Country is required."
            },
            state: {
                required: "State is required."
            },
            district: {
                required: "District is required."
            },
            tahsil: {
                required: "Tahsil is required."
            },
            mobile_no: {
                required: "Contact no. is required.",
                number: "Contact no. must be digits."
            },
        }
    });
});