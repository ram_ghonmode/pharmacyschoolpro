<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gr_number')->nullable();
            $table->string('admission_in');
            $table->string('fullname')->nullable();
            $table->string('current_class')->nullable();
            $table->date('current_date')->nullable();
            $table->date('admission_date')->nullable();
            $table->date('last_date')->nullable();
            $table->date('dob')->nullable();
            $table->string('section')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_certificates');
    }
}
