<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBonafiedCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonafied_certificates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gr_number')->nullable();
            $table->string('admission_in')->nullable();
            $table->string('fullname',500)->nullable();
            $table->date('current_date')->nullable();
            $table->string('current_class')->nullable();
            $table->string('current_year')->nullable();
            $table->date('dob')->nullable();
            $table->string('caste')->nullable();
            $table->string('category')->nullable();
            $table->string('aadhar_no')->nullable();
            $table->string('section')->nullable();
            $table->string('student_id')->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonafied_certificates');
    }
}
