<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeesConcessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees_concessions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('remark')->nullable();
            $table->string('student_id')->nullable();
            $table->string('gr_number')->nullable();
            $table->string('current_class')->nullable();
            $table->string('section')->nullable();
            $table->string('school_id')->nullable();
            $table->string('amount')->nullable();
            $table->date('date')->nullable();
            $table->string('academic_year')->nullable();
            $table->enum('admission_in',['College'])->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees_concessions');
    }
}
