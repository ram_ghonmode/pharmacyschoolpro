<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentYearlyFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_yearly_fees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('admission_id')->nullable();
            $table->string('gr_number')->nullable();
            $table->string('receipt_no')->nullable();
            $table->string('current_class')->nullable();
            $table->string('section')->nullable();
            $table->string('school_id')->nullable();
            $table->enum('fees_type',['External','Internal'])->nullable();
            $table->string('total_amount')->nullable();
            $table->string('amount')->nullable();
            $table->date('date')->nullable();
            $table->string('academic_year')->nullable();
            $table->enum('admission_in',['Pre-Primary','School', 'College'])->nullable();
            $table->enum('pay_type',['Cheque','Cash','DD'])->nullable();
            $table->string('draft_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_yearly_fees');
    }
}
