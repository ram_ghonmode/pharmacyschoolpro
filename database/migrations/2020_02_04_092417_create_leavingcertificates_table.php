<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavingcertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leavingcertificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gr_number')->nullable();
            $table->string('admission_id')->nullable();
            $table->string('admission_in');
            $table->string('serial_no')->nullable();
            $table->string('result')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('class')->nullable();
            $table->mediumText('dictinction_text')->nullable();
            $table->string('conduct')->nullable();
            $table->string('progress')->nullable();
            $table->string('leaving_reason')->nullable();
            $table->date('leaving_date')->nullable();
            $table->enum('leaving_type', ['school','college'])->default('school');
            $table->enum('status', ['ORIGINAL','DUPLICATE'])->default('ORIGINAL');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leavingcertificates');
    }
}
