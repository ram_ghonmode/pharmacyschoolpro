<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->nullable();
            $table->string('name')->nullable();
            $table->string('email_id')->nullable();
            $table->string('number')->nullable();
            $table->string('alt_num')->nullable();
            $table->string('dob')->nullable();
            $table->integer('designation_id')->nullable();
            $table->string('p_handicap')->nullable();
            $table->string('handicap')->nullable();
            $table->string('class')->nullable();
            $table->enum('gender', ['Male','Female'])->nullable();
            $table->enum('marital_status', ['Married','Unmarried'])->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('pin_code')->nullable();
            $table->string('state')->nullable();
            $table->string('nationality')->nullable();
            $table->string('caste')->nullable();
            $table->string('category')->nullable();
            $table->string('religion')->nullable();
            $table->string('working_status')->nullable();
            $table->string('aadhar')->nullable();
            $table->string('pan_num')->nullable();
            $table->string('pay_band')->nullable();
            $table->string('doj')->nullable();
            $table->string('dor')->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
