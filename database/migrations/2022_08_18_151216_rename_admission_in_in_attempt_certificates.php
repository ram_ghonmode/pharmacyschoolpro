<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameAdmissionInInAttemptCertificates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attempt_certificates', function (Blueprint $table) {
            $table->renameColumn('admission_type','admission_in');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attempt_certificates', function (Blueprint $table) {
                $table->renameColumn('admission_in','admission_type');

        });
    }
}
