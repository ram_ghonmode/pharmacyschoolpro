<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('institute_name')->nullable();
            $table->string('school_name');
            $table->string('school_name_marathi')->nullable();
            $table->string('academic_year');
            $table->string('session');
            $table->string('address')->nullable();
            $table->integer('country_id');
            $table->string('state_id');
            $table->string('city_id');
            $table->string('tahsil_id')->nullable();
            $table->string('pin_code')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('fax_no')->nullable();
            $table->string('recognition_no')->nullable();
            $table->string('index_no')->nullable();
            $table->string('website_url')->nullable();
            $table->string('email')->nullable();
            $table->string('udise_no')->nullable();
            $table->string('board')->nullable();
            $table->string('establishment_year')->nullable();
            $table->date('granted_from_date')->nullable();
            $table->date('granted_end_date')->nullable();
            $table->string('logo')->nullable();
            $table->string('principal_name')->nullable();
            $table->string('principal_mob_no')->nullable();
            $table->string('sport_teacher_name')->nullable();
            $table->string('sport_teacher_mob_no')->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
