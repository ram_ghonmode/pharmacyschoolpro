<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentGeneralInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_general_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('admission_id');
            $table->string('gr_number');
            $table->string('student_id');
            $table->string('blood_group')->nullable();
            $table->string('height_feet')->nullable();
            $table->string('height_inch')->nullable();
            $table->string('weight')->nullable();
            $table->string('handicapped')->nullable();
            $table->string('last_school_attended')->nullable();
            $table->date('leaving_date')->nullable();
            $table->string('reason_of_leaving')->nullable();
            $table->string('leaving_class')->nullable();
            $table->string('pim_1')->nullable();
            $table->string('pim_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_general_infos');
    }
}
