<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gr_number');
            $table->string('student_id');
            $table->integer('school_id');
            $table->integer('scholarship_type_id')->nullable();
            $table->integer('admission_type_id')->nullable();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('fullname_in_hindi')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('gender')->nullable();
            $table->date('dob')->nullable();
            $table->string('age')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('admission_date')->nullable();
            $table->string('academic_year')->nullable();
            $table->string('admit_to')->nullable();
            $table->string('current_class')->nullable();
            $table->string('section')->nullable();
            $table->string('admission_in')->nullable();
            $table->string('college_stream')->nullable();
            $table->string('medium')->nullable();
            $table->string('nationality')->nullable();
            $table->string('domicile')->nullable();
            $table->string('mother_tounge')->nullable();
            $table->string('caste')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('document')->nullable();
            $table->string('religion')->nullable();
            $table->string('admission_status')->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
