<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttemptCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attempt_certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('gr_number')->nullable();
            $table->string('admission_type');
            $table->string('fullname')->nullable();
            $table->string('dictinction_text')->nullable();
            $table->date('current_date')->nullable();
            $table->date('admission_date')->nullable();
            $table->string('result')->nullable();
            $table->string('month')->nullable();
            $table->string('class')->nullable();
            $table->string('year')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attempt_certificates');
    }
}
