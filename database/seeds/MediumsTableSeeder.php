<?php

use Illuminate\Database\Seeder;

class MediumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medium_streams')->insert([
            [
                'name' => 'English',
                'type' => 'Medium'
            ]
        ]);
    }
}
