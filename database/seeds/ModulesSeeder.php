<?php

use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'name' => 'System setting',
                'parent' => 0,
            ],
            [
                'name' => 'User',
                'parent' => 1,
            ],
            [
                'name' => 'Role',
                'parent' => 1,
            ],
            [
                'name' => 'Master',
                'parent' => 0,
            ],
            [
                'name' => 'School',
                'parent' => 4,
            ],
            [
                'name' => 'Medium / College Stream',
                'parent' => 4,
            ],
            [
                'name' => 'Academic Year',
                'parent' => 4,
            ],
            [
                'name' => 'Class',
                'parent' => 4,
            ],
            [
                'name' => 'Designation',
                'parent' => 4,
            ],
            [
                'name' => 'Allowance',
                'parent' => 4,
            ],
            [
                'name' => 'Deduction',
                'parent' => 4,
            ],
            [
                'name' => 'Account',
                'parent' => 4,
            ],
            [
                'name' => 'Fees Head',
                'parent' => 4,
            ],
            [
                'name' => 'Leave Type',
                'parent' => 4,
            ],
            [
                'name' => 'Student Management',
                'parent' => 0,
            ],
            [
                'name' => 'Admission',
                'parent' => 15,
            ],
            [
                'name' => 'Class Room Management',
                'parent' => 0,
            ],
            [
                'name' => 'Class',
                'parent' => 17,
            ],
            [
                'name' => 'Teacher Allotment',
                'parent' => 17,
            ],
            [
                'name' => 'Attendance Management',
                'parent' => 0,
            ],
            [
                'name' => 'Attendance',
                'parent' => 20,
            ],
            [
                'name' => 'Exam Management',
                'parent' => 0,
            ],
            [
                'name' => 'Subject',
                'parent' => 22,
            ],
            [
                'name' => 'Test',
                'parent' => 22,
            ],
            [
                'name' => 'Class Test',
                'parent' => 22,
            ],
            [
                'name' => 'Student Report',
                'parent' => 0,
            ],
            [
                'name' => 'General',
                'parent' => 26,
            ],
            [
                'name' => 'Leaving',
                'parent' => 26,
            ],
            [
                'name' => 'Bonafide',
                'parent' => 26,
            ],
            [
                'name' => 'Character',
                'parent' => 26,
            ],
            [
                'name' => 'Attempt',
                'parent' => 26,
            ],
            [
                'name' => 'Staff Management',
                'parent' => 0,
            ],
            [
                'name' => 'Staff',
                'parent' => 32,
            ],
        ]);
    }
}
