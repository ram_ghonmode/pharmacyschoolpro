<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin',
                'slug' => 'admin',
                'permissions' => json_encode([
                    'user-list' => true,
                    'user-add' => true,
                    'user-edit' => true,
                    'user-delete' => true,
                    'role-list' => true,
                    'role-add' => true,
                    'role-edit' => true,
                    'role-delete' => true,
                    'role-permissions' => true,
                    'school-list' => true,
                    'school-add' => true,
                    'school-edit' => true,
                    'school-delete' => true,
                    'medium-list' => true,
                    'medium-add' => true,
                    'medium-edit' => true,
                    'medium-delete' => true,
                    'academic-year-list' => true,
                    'academic-year-add' => true,
                    'academic-year-edit' => true,
                    'academic-year-delete' => true,
                    'class-list' => true,
                    'class-add' => true,
                    'class-edit' => true,
                    'class-delete' => true,
                    'designation-list' => true,
                    'designation-add' => true,
                    'designation-edit' => true,
                    'designation-delete' => true,
                    'allowance-list' => true,
                    'allowance-add' => true,
                    'allowance-edit' => true,
                    'allowance-delete' => true,
                    'deduction-list' => true,
                    'deduction-add' => true,
                    'deduction-edit' => true,
                    'deduction-delete' => true,
                    'account-list' => true,
                    'account-add' => true,
                    'account-edit' => true,
                    'account-delete' => true,
                    'fees-head-list' => true,
                    'fees-head-add' => true,
                    'fees-head-edit' => true,
                    'fees-head-delete' => true,
                    'leave-type-list' => true,
                    'leave-type-add' => true,
                    'leave-type-edit' => true,
                    'leave-type-delete' => true,
                    'admission-list' => true,
                    'admission-add' => true,
                    'admission-edit' => true,
                    'admission-view' => true,
                    'admission-delete' => true,
                    'class-room-list' => true,
                    'printclasslist' => true,
                    'class-teacher-list' => true,
                    'class-teacher-add' => true,
                    'class-teacher-edit' => true,
                    'class-teacher-delete' => true,
                    'print-teacher-list' => true,
                    'student-attendance-list' => true,
                    'student-attendance-add' => true,
                    'attendance-status' => true,
                    'delete-attendance' => true,
                    'subject-list' => true,
                    'subject-add' => true,
                    'subject-edit' => true,
                    'subject-delete' => true,
                    'test-list' => true,
                    'test-add' => true,
                    'test-edit' => true,
                    'test-delete' => true,
                    'class-test-subject' => true,
                    'assign-class-subject' => true,
                    'assign-class-test' => true,
                    'generalregister'  => true,
                    'printgeneralregister'=> true,
                    'leaving-certificate' => true,
                    'printleaving'=> true,
                    'bonafied-list' => true,
                    'bonafiedcertificate'=> true,
                    'printbonafied'=> true,
                    'character-list'=> true,
                    'character-certificate'=> true,
                    'printcharacter'=> true,
                    'attempt-list'=> true,
                    'attempt-certificate'=> true,
                    'printattempt'=> true,
                    'employee-list' => true,
                    'employee-add' => true,
                    'employee-edit' => true,
                    'employee-delete' => true,
                ]),
            ],
            [
                'name' => 'Supervisor',
                'slug' => 'supervisor',
                'permissions' => json_encode([

                ]),
            ],
            [
                'name' => 'Clerk',
                'slug' => 'clerk',
                'permissions' => json_encode([

                ]),
            ],
            [
                'name' => 'Accountant',
                'slug' => 'accountant',
                'permissions' => json_encode([

                ]),
            ],
            [
                'name' => 'Teacher',
                'slug' => 'teacher',
                'permissions' => json_encode([

                ]),
            ]

        ]);
    }
}
