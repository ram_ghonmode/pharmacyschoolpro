<?php

use Illuminate\Database\Seeder;

class AcademicYesrsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('academic_years')->insert([
            [
                'academic_year' => '2010-11'
            ],
            [
                'academic_year' => '2011-12'
            ],
            [
                'academic_year' => '2012-13'
            ],
            [
                'academic_year' => '2013-14'
            ],
            [
                'academic_year' => '2014-15'
            ],
            [
                'academic_year' => '2015-16'
            ],
            [
                'academic_year' => '2016-17'
            ],
            [
                'academic_year' => '2017-18'
            ],
            [
                'academic_year' => '2018-19'
            ],
            [
                'academic_year' => '2019-20'
            ],
            [
                'academic_year' => '2020-21'
            ],
            [
                'academic_year' => '2021-22'
            ],
            [
                'academic_year' => '2022-23'
            ]
        ]);
    }
}
