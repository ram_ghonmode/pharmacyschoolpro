<?php

use Illuminate\Database\Seeder;

class SchoolUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('school_users')->insert([
            [
                'school_id' => 1,
                'user_id' => 1
            ]
        ]);
    }
}
