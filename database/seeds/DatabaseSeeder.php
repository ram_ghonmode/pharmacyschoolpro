<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            CountryTableSeeder::class,
            StateTableSeeder::class,
            CityTableSeeder::class,
            TahsilTableSeeder::class,
            RolesSeeder::class,
            RoleUserSeeder::class,
            ModulesSeeder::class,
            MethodsSeeder::class,
            SchoolUserSeeder::class,
            ClassesTableSeeder::class,
            MediumsTableSeeder::class,
            AcademicYesrsTableSeeder::class,
        ]);
    }
}
