<?php

use Illuminate\Database\Seeder;

class TahsilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tahsils')->insert([
            'tahsil_name' => 'Wardha',
            'city_id'   => 1,
            'state_id'  => 1,
            'country_id'=> 1,
        ]);
    }
}
