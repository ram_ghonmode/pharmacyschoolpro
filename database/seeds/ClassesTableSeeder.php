<?php

use Illuminate\Database\Seeder;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('class_lists')->insert([
            [
                'class_name' => '1st Year'
            ],
            [
                'class_name' => '2st Year'
            ]
        ]);
    }
}
