<?php

use Illuminate\Database\Seeder;

class MethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('methods')->insert([
            [
                'module_id' => 2,
                'route_name' => 'user-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 2,
                'route_name' => 'user-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 2,
                'route_name' => 'user-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 2,
                'route_name' => 'user-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-permissions',
                'display_name' => 'Permissions',
                'published' => false,
            ],
            [
                'module_id' => 5,
                'route_name' => 'school-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 5,
                'route_name' => 'school-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 5,
                'route_name' => 'school-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 5,
                'route_name' => 'school-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 6,
                'route_name' => 'medium-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 6,
                'route_name' => 'medium-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 6,
                'route_name' => 'medium-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 6,
                'route_name' => 'medium-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 7,
                'route_name' => 'academic-year-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 7,
                'route_name' => 'academic-year-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 7,
                'route_name' => 'academic-year-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 7,
                'route_name' => 'academic-year-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 8,
                'route_name' => 'class-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 8,
                'route_name' => 'class-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 8,
                'route_name' => 'class-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 8,
                'route_name' => 'class-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 9,
                'route_name' => 'designation-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 9,
                'route_name' => 'designation-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 9,
                'route_name' => 'designation-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 9,
                'route_name' => 'designation-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 10,
                'route_name' => 'allowance-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 10,
                'route_name' => 'allowance-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 10,
                'route_name' => 'allowance-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 10,
                'route_name' => 'allowance-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 11,
                'route_name' => 'deduction-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 11,
                'route_name' => 'deduction-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 11,
                'route_name' => 'deduction-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 11,
                'route_name' => 'deduction-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 12,
                'route_name' => 'account-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 12,
                'route_name' => 'account-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 12,
                'route_name' => 'account-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 12,
                'route_name' => 'account-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 13,
                'route_name' => 'fees-head-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 13,
                'route_name' => 'fees-head-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 13,
                'route_name' => 'fees-head-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 13,
                'route_name' => 'fees-head-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 14,
                'route_name' => 'leave-type-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 14,
                'route_name' => 'leave-type-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 14,
                'route_name' => 'leave-type-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 14,
                'route_name' => 'leave-type-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 16,
                'route_name' => 'admission-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 16,
                'route_name' => 'admission-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 16,
                'route_name' => 'admission-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 16,
                'route_name' => 'admission-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 18,
                'route_name' => 'class-room-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 19,
                'route_name' => 'class-teacher-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 19,
                'route_name' => 'class-teacher-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 19,
                'route_name' => 'class-teacher-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 19,
                'route_name' => 'class-teacher-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 21,
                'route_name' => 'student-attendance-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 21,
                'route_name' => 'student-attendance-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 21,
                'route_name' => 'attendance-status',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 21,
                'route_name' => 'delete-attendance',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 23,
                'route_name' => 'subject-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 23,
                'route_name' => 'subject-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 23,
                'route_name' => 'subject-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 23,
                'route_name' => 'subject-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 24,
                'route_name' => 'test-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 24,
                'route_name' => 'test-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 24,
                'route_name' => 'test-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 24,
                'route_name' => 'test-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
            [
                'module_id' => 25,
                'route_name' => 'class-test-subject',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 25,
                'route_name' => 'assign-class-subject',
                'display_name' => 'Subject',
                'published' => true,
            ],
            [
                'module_id' => 25,
                'route_name' => 'assign-class-test',
                'display_name' => 'Test',
                'published' => true,
            ],
            [
                'module_id' => 27,
                'route_name' => 'generalregister',
                'display_name' => 'List',
                'published' => true,
            ],
            // [
            //     'module_id' => 27,
            //     'route_name' => 'generalregcertificate',
            //     'display_name' => 'Add',
            //     'published' => true,
            // ],
            [
                'module_id' => 27,
                'route_name' => 'printgeneralregister',
                'display_name' => 'Print',
                'published' => false,
            ],
            [
                'module_id' => 28,
                'route_name' => 'leaving-certificate',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 28,
                'route_name' => 'leaving-certificate',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 28,
                'route_name' => 'printleaving',
                'display_name' => 'Print',
                'published' => false,
            ],
            [
                'module_id' => 29,
                'route_name' => 'bonafied-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 29,
                'route_name' => 'bonafiedcertificate',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 29,
                'route_name' => 'printbonafied',
                'display_name' => 'Print',
                'published' => false,
            ],
            [
                'module_id' => 30,
                'route_name' => 'character-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 30,
                'route_name' => 'character-certificate',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 30,
                'route_name' => 'printcharacter',
                'display_name' => 'Print',
                'published' => false,
            ],
            [
                'module_id' => 31,
                'route_name' => 'attempt-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 31,
                'route_name' => 'attempt-certificate',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 31,
                'route_name' => 'printattempt',
                'display_name' => 'Print',
                'published' => false,
            ],
            [
                'module_id' => 33,
                'route_name' => 'employee-list',
                'display_name' => 'List',
                'published' => true,
            ],
            [
                'module_id' => 33,
                'route_name' => 'employee-add',
                'display_name' => 'Add',
                'published' => true,
            ],
            [
                'module_id' => 33,
                'route_name' => 'employee-edit',
                'display_name' => 'Edit',
                'published' => true,
            ],
            [
                'module_id' => 33,
                'route_name' => 'employee-delete',
                'display_name' => 'Delete',
                'published' => false,
            ],
        ]);
    }
}









