@extends('layouts.dash')   
@section('title', 'Class Test And Subject')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Class Test And Subject </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>SR.NO.</th>
                                <th>CLASS</th>
                                <th>ASSIGN SUBJECTS</th>
                                <th>ASSIGN TESTS</th>
                                <th>ADD SUBJECT</th>
                                <th>ADD TEST</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classes) && count($classes) > 0)
                                <?php $count = 1; ?>
                                @foreach($classes as $key => $val)
                                    <?php 
                                        $classId = Controller::cryptString($key, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $val->class_name }}</td>
                                        @php
                                            $allSubject = "";
                                            if(!empty($assignSub)){
                                                foreach($assignSub as $key => $sub){
                                                    if($sub->class_id == $val->id){
                                                        $allSubject .= " ".$sub->subjects->subject_name.',';
                                                    }
                                                }
                                                $allSubject = rtrim($allSubject, ',');
                                            }
                                        @endphp
                                        <td>{{ !empty($allSubject) ? $allSubject : '' }}</td>
                                        @php
                                            $allTest = "";
                                            if(!empty($assignTest)){
                                                foreach($assignTest as $key => $test){
                                                    if($test->class_id == $val->id){
                                                        $allTest .= " ".$test->tests->test_name.',';
                                                    }
                                                }
                                                $allTest = rtrim($allTest, ',');
                                            }
                                        @endphp
                                        <td>{{ !empty($allTest) ? $allTest : '' }}</td>
                                        <td> 
                                            <a href="{{ route('assign-class-subject', ['classId' => $val->id]) }}" target="_blank" class="btn btn-primary btn-xs waves-effect"  title="Add/Edit/View">
                                                Add
                                            </a>
                                        </td>
                                        <td> 
                                            <a href="{{ route('assign-class-test', ['classId' => $val->id]) }}" target="_blank" class="btn btn-primary btn-xs waves-effect" title="Add/Edit/View" >
                                                Add
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection