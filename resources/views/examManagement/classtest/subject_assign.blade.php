@extends('layouts.dash')    
@section('title', 'Assign Subject')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Assign Subject </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('class-test-subject') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('assign-class-subject', ['classId' => Request::route('classId')]) }}" method="POST" autocomplete="off">
                    @csrf
                        <input type="hidden" name="class_id" class="form-control" value="{{ Request::route('classId')}}"/>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Class Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" value="{{ $className->class_name ?? '' }}" readonly />
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Subject <?php echo $reqLabel; ?></label>
                                <select name="subject_id[]" class="form-control">
                                    <option value="">Select subject</option>
                                    @foreach($subjects as $key => $val)
                                        <option value="{{ $key }}" >{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-header">
                    <h3 class="page-title"> Subject List </h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Subject</th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classSubjects) && count($classSubjects) > 0)
                                @foreach($classSubjects as $key => $subject)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ !empty($subject->subjects->subject_name) ? $subject->subjects->subject_name : '' }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ url('/dashboard/'.$subject->id.'/class_subjects')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection