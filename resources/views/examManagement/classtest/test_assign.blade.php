@extends('layouts.dash')    
@section('title', 'Assign Test')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Assign Test </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('class-test-subject') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('assign-class-test', ['classId' => Request::route('classId')]) }}" method="POST" autocomplete="off">
                    @csrf
                        <input type="hidden" name="class_id" class="form-control" value="{{ Request::route('classId')}}"/>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Class Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" value="{{ $className->class_name ?? '' }}" readonly />
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Test <?php echo $reqLabel; ?></label>
                                <select name="test_id[]" class="form-control">
                                    <option value="">Select test</option>
                                    @foreach($tests as $key => $val)
                                        <option value="{{ $key }}" >{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-header">
                    <h3 class="page-title"> Test List </h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Test</th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classTests) && count($classTests) > 0)
                                @foreach($classTests as $key => $test)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ !empty($test->tests->test_name) ? $test->tests->test_name : '' }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ url('/dashboard/'.$test->id.'/class_tests')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection