@extends('layouts.dash')    
@section('title', 'Test')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Test </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('test-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('test-edit', ['id' => isset($test->id) ? Controller::cryptString($test->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Test Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter test name" name="test_name" value="{{ old('test_name', isset($test->test_name) ? $test->test_name : '' ) }}">
                                @if ($errors->has('test_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('test_name') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection