@extends('layouts.dash')   
@section('title', 'Test')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Test </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('test-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Test Name </th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($tests) && count($tests) > 0)
                            <?php  $count = $tests->firstItem(); ?>
                                @foreach($tests as $key => $test)
                                    <?php 
                                        $encyId = Controller::cryptString($test->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $test->test_name }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('test-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$test->id.'/tests')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $tests->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection