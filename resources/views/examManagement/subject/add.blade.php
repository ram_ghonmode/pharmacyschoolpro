@extends('layouts.dash')    
@section('title', 'Subject')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Subject </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('subject-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('subject-edit', ['id' => isset($subject->id) ? Controller::cryptString($subject->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Subject Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter subject name" name="subject_name" value="{{ old('subject_name', isset($subject->subject_name) ? $subject->subject_name : '' ) }}">
                                @if ($errors->has('subject_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('subject_name') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection