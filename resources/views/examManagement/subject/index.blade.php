@extends('layouts.dash')   
@section('title', 'Test')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Test </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('subject-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Subject Name </th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($subjects) && count($subjects) > 0)
                            <?php  $count = $subjects->firstItem(); ?>
                                @foreach($subjects as $key => $subject)
                                    <?php 
                                        $encyId = Controller::cryptString($subject->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $subject->subject_name }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('subject-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$subject->id.'/subjects')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $subjects->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection