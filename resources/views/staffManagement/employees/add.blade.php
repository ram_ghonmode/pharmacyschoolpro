@extends('layouts.dash')    
@section('title', 'Staff')
@section('content')   
<?php 
    use App\Http\Controllers\Controller; 
    $categories = ['Open', 'OBC', 'SC', 'ST', 'NT', 'SBC', 'VJ', 'NT-B', 'NT-C', 'NT-D'];  
    $religiones = ['Hinduism','Islam','Christanity','Sikhism','Buddhism','Jainism','Other Religion']; 
    $statuses =['Active','Suspended','Retired','Terminated','Fired']; 
    $banks =['SBI','PNB','BOI','Axis Bank','Allahabad Bank','ICICI Bank','Union Bank']; 
    $classes =['Class I','Class II','Class III(A)','Class III(B)','Class IV']; 
    $prevRoleId = !empty($employee->roles[0]) ? $employee->roles[0]->id : NULL;
    $reqLabel = '<sup class="text-danger">*</sup>'; 
?> 

    <div class="page-header">
        <h3 class="page-title"> Staff </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('employee-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('employee-edit', ['id' => isset($employee->id) ? Controller::cryptString($employee->id, 'e') : '']) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                        <div class="row clearfix">
                            <div class="col-sm-3 upload-img-div text-center">
                            <?php  ?> 
                                <?php $scr = isset($employee->users->user_img_url) ? $employee->users->user_img_url : asset("public/Stellar-master/images/brokanupload-img.png");?>
                                <img class="upload-img" id="uploadimgId" onerror="this.src='{{ $scr }}';" src="" alt="your image" style="width:200px;height:200px" />
                                <br>
                                <input type='file' name="user_img_url" id="computerupload" onchange="readURL(this);"  value="{{ old('user_img_url', isset($employee->users->user_img_url) ? $employee->users->user_img_url : '' ) }}"/>
                                <label class="btn btn-primary" for="computerupload" style="padding: 0.5rem 1rem; margin-top:8px;" >Upload</label>
                                <button class="btn btn-secondary" id="resetupload" type="button" style="padding: 0.5rem 1rem;">Reset</button><br>
                                @if ($errors->has('user_img_url'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('user_img_url') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-9">
                                <div class="row clearfix">
                                    <div class="col-sm-6 form-group">
                                        <label for="">School <?php echo $reqLabel; ?></label>
                                        <select class="form-control" name="school_id">
                                            <option value="">Select school</option>
                                            @foreach($data['schools'] as $id => $name)
                                                <option value="{{ $id  }}" @if(old('school_id', isset($employee->school_id) ? $employee->school_id : '') == $id ) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('school_id'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('school_id') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label for="">Name <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" placeholder="Enter full name" name="name" value="{{ old('name', isset($employee->name) ? $employee->name : '' ) }}">
                                        @if ($errors->has('name'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('name') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-3 form-group">
                                        <label for="">Email <?php echo $reqLabel; ?></label>
                                        <input type="email" class="form-control" placeholder="Enter email" name="email_id" value="{{ old('email_id', isset($employee->email_id) ? $employee->email_id : '' ) }}">
                                        @if ($errors->has('email_id'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('email_id') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Contact Number. <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" placeholder="Enter contact number" name="number" value="{{ old('number', isset($employee->number) ? $employee->number : '' ) }}">
                                        @if ($errors->has('number'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('number') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Alternate Contact Number </label>
                                        <input type="text" class="form-control" placeholder="Enter alternate number" name="alt_num" value="{{ old('alt_num', isset($employee->alt_num) ? $employee->alt_num : '' ) }}">
                                        @if ($errors->has('alt_num'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('alt_num') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Date Of Birth <?php echo $reqLabel; ?></label>
                                        <div id="datepicker-popup" class="input-group date datepicker" style="padding:0">
                                            <input type="text" name="dob" value="{{ !empty($employee->dob) ? date('d-m-Y', strtotime($employee->dob)) : old('dob') }}" class="datepicker form-control" placeholder="Date of birth">
                                            <span class="input-group-addon input-group-append border-left">
                                                <span class="icon-calendar input-group-text"></span>
                                            </span>
                                        </div>
                                        @if ($errors->has('dob'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('dob') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row clearfix" >
                                    <div class="col-sm-3 form-group">
                                        <label for="">Physically Handicapped<?php echo $reqLabel; ?></label>
                                        <select name="p_handicap" class="form-control">
                                            <option value="">Physically handicapped</option>
                                            <option value="Yes"  @if(old('p_handicap', isset($employee->p_handicap) ? $employee->p_handicap : '') == 'Yes') selected @endif>Yes</option>
                                            <option value="No"  @if(old('p_handicap', isset($employee->p_handicap) ? $employee->p_handicap : '') == 'No') selected @endif>No</option>
                                        </select>
                                        @if ($errors->has('p_handicap'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('p_handicap') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Handicapped % </label>
                                        <input type="text" class="form-control" placeholder="Enter handicap" name="handicap" value="{{ old('handicap', isset($employee->handicap) ? $employee->handicap : '' ) }}">
                                        @if ($errors->has('handicap'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('handicap') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Class</label>
                                        <select name="class" class="form-control">
                                            <option value="">Class</option>
                                            @foreach($classes as $k => $class)
                                                <option value="{{ $class }}"  @if(old('class', isset($employee->class) ? $employee->class : '') == $class) selected @endif>{{ $class }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Designation<?php echo $reqLabel; ?></label>
                                        <select name="designation_id" class="form-control">
                                            <option value="">Select designation</option>
                                            @foreach($data['designations'] as $id => $name)
                                                <option value="{{ $id  }}" @if(old('designation_id', isset($employee->designation_id) ? $employee->designation_id : '') == $id ) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('designation_id'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('designation_id') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-6 m-b-0 admission-rdio">
                                        <div class="demo-radio-button">
                                            <label class="label-radio">Gender &nbsp;&nbsp;</label>
                                            <div class="radio-group">
                                                <input name="gender" type="radio" value="Male" @if(old('gender', isset($employee->gender) ? $employee->gender : '') == 'Male') checked @endif class="with-gap radio-col-indigo" checked />
                                                <label for="maleId">Male</label>
                                            </div>
                                            <div class="radio-group">
                                                <input name="gender" type="radio" value="Female" @if(old('gender', isset($employee->gender) ? $employee->gender : '') == 'Female') checked @endif class="with-gap radio-col-indigo" />
                                                <label for="femaleId">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 m-b-0 admission-rdio">
                                        <div class="demo-radio-button">
                                            <label class="label-radio">Marital Status &nbsp;&nbsp;</label>
                                            <div class="radio-group ">
                                                <input name="marital_status" type="radio" value="married" @if(old('marital_status', isset($employee->marital_status) ? $employee->marital_status : '') == 'married') checked @endif class="with-gap radio-col-indigo" checked />
                                                <label for="marriedId">Married</label>
                                            </div>
                                            <div class="radio-group">
                                                <input name="marital_status" type="radio" value="unmarried" @if(old('marital_status', isset($employee->marital_status) ? $employee->marital_status : '') == 'unmarried') checked @endif class="with-gap radio-col-indigo" />
                                                <label for="unmarriedId">Unmarried</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix" style="margin-top:5px">
                            <div class="form-group col-sm-4">
                                <label for="">Address</label>
                                <input type="text" class="form-control" placeholder="Enter address" name="address" value="{{ old('address', isset($employee->address) ? $employee->address : '' ) }}">
                                @if ($errors->has('address'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('address') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">City</label>
                                <input type="text" class="form-control" placeholder="Enter city" name="city" value="{{ old('city', isset($employee->city) ? $employee->city : '' ) }}">
                                @if ($errors->has('city'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('city') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Pin Code</label>
                                <input type="text" class="form-control" placeholder="Enter pin code" name="pin_code" value="{{ old('pin_code', isset($employee->pin_code) ? $employee->pin_code : '' ) }}">
                                @if ($errors->has('pin_code'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('pin_code') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-4">
                                <label for="">State</label>
                                <input type="text" class="form-control" placeholder="Enter state" name="state" value="{{ old('state', isset($employee->state) ? $employee->state : '' ) }}">
                                @if ($errors->has('state'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('state') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Nationality</label>
                                <input type="text" class="form-control" placeholder="Enter nationality" name="nationality" value="{{ old('nationality', isset($employee->nationality) ? $employee->nationality : '' ) }}">
                                @if ($errors->has('nationality'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('nationality') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Caste</label>
                                <input type="text" class="form-control" placeholder="Enter caste" name="caste" value="{{ old('caste', isset($employee->caste) ? $employee->caste : '' ) }}">
                                @if ($errors->has('caste'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('caste') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                           <div class="form-group col-sm-4">
                               <select name="category" class="form-control">
                                    <option value="">Select category</option>
                                    @foreach($categories as $k => $category)
                                        <option value="{{ $category }}"  @if(old('category', isset($employee->category) ? $employee->category : '') == $category) selected @endif>{{ $category }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <select name="religion" class="form-control">
                                    <option value="">Select religion</option>
                                    @foreach($religiones as $k => $religion)
                                        <option value="{{ $religion }}"  @if(old('religion', isset($employee->religion) ? $employee->religion : '') == $religion) selected @endif>{{ $religion }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <select name="working_status" class="form-control">
                                    <option value="">Status</option>
                                    @foreach($statuses as $k => $status)
                                        <option value="{{ $status }}"  @if(old('status', isset($employee->status) ? $employee->status : '') == $status) selected @endif>{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-4">
                                <label for="">ID Details (Aadhar)</label>
                                <input type="text" class="form-control" placeholder="Enter aadhar details" name="aadhar" value="{{ old('aadhar', isset($employee->aadhar) ? $employee->aadhar : '' ) }}">
                                @if ($errors->has('aadhar'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('aadhar') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">ID Details (PAN)</label>
                                <input type="text" class="form-control" placeholder="Enter pan details" name="pan_num" value="{{ old('pan_num', isset($employee->pan_num) ? $employee->pan_num : '' ) }}">
                                @if ($errors->has('pan_num'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('pan_num') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Pay Band</label>
                                <input type="text" class="form-control" placeholder="Enter pay band" name="pay_band" value="{{ old('pay_band', isset($employee->pay_band) ? $employee->pay_band : '' ) }}">
                                @if ($errors->has('pay_band'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('pay_band') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <div class="col-sm-3 form-group">
                                <label for="">Date Of Joining <?php echo $reqLabel; ?></label>
                                <div id="datepicker-popup" class="input-group date datepicker" style="padding:0">
                                    <input type="text" name="doj" value="{{ old('doj', isset($employee->doj) ? date('d-m-Y', strtotime($employee->doj)) : '' ) }}" class="datepicker form-control" placeholder="Date of joining">
                                    <span class="input-group-addon input-group-append border-left">
                                        <span class="icon-calendar input-group-text"></span>
                                    </span>
                                </div>
                                @if ($errors->has('doj'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('doj') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Date Of Retirement </label>
                                <div id="datepicker-popup" class="input-group date datepicker" style="padding:0">
                                    <input type="text" name="dor" value="{{ old('dor', isset($employee->dor) ? date('d-m-Y', strtotime($employee->dor)) : '' ) }}" class="datepicker form-control" placeholder="Date of retirement">
                                    <span class="input-group-addon input-group-append border-left">
                                        <span class="icon-calendar input-group-text"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <h4 class="card-inside-title" style="padding: 10px;">Staff Bank Details</h2>
                        <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                                <label class="form-label">Bank Name</label>
                                <input type="text" class="form-control" placeholder="Enter bank name" name="bank_name" value="{{ old('bank_name', isset($employee->bankdetail->bank_name) ? $employee->bankdetail->bank_name : '' ) }}">
                                @if ($errors->has('bank_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('bank_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label class="form-label">A\C Number</label>
                                <input type="text" name="ac_num" value="{{ old('ac_num', isset($employee->bankdetail->ac_num) ? $employee->bankdetail->ac_num : '' ) }}" class="form-control" placeholder="Enter A\C number" >
                            </div>
                            <div class="col-sm-3 form-group">
                                <label class="form-label">A\C Holder Name</label>
                                <input type="text" name="ac_name" value="{{ old('ac_name', isset($employee->bankdetail->ac_name) ? $employee->bankdetail->ac_name : '' ) }}" class="form-control" placeholder="Enter A\C holder name" >
                            </div>
                            <div class="col-sm-3 form-group">
                                <label class="form-label">Bank IFSC Code</label>
                                <input type="text" name="ifsc_code" value="{{ old('ifsc_code', isset($employee->bankdetail->ifsc_code) ? $employee->bankdetail->ifsc_code : '' ) }}" class="form-control" placeholder="Enter bank IFSC code" >
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                            <label class="form-label">P\F</label>
                                <select name="select_pf" class="form-control">
                                    <option value="">Select PF</option>
                                    <option value="GPF" @if(old('select_pf', isset($employee->bankdetail->select_pf) ? $employee->bankdetail->select_pf : '') == 'GPF') selected @endif>GPF</option>
                                    <option value="DCPS-Regular" @if(old('select_pf', isset($employee->bankdetail->select_pf) ? $employee->bankdetail->select_pf : '') == 'DCPS-Regular') selected @endif>DCPS-Regular</option>
                                    <option value="DCPS-Delay" @if(old('select_pf', isset($employee->bankdetail->select_pf) ? $employee->bankdetail->select_pf : '') == 'DCPS-Delay') selected @endif>DCPS-Delay</option>
                                </select>
                            </div>
                            <div class="col-sm-3 form-group">
                                <label class="form-label">P\F Number</label>
                                <input type="text" name="pf_acc" value="{{ old('pf_acc', isset($employee->bankdetail->pf_acc) ? $employee->bankdetail->pf_acc : '' ) }}" class="form-control" placeholder="Enter P\F number">
                            </div>
                        </div>
                        <h4 class="card-inside-title" style="padding: 10px;">Login Credentials</h2>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label class="form-label">Email Id <?php echo $reqLabel; ?></label>
                                <input type="email" name="email" value="{{ old('email', isset($employee->users->email) ? $employee->users->email : '' ) }}" class="form-control" placeholder="Enter email"/>
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('email') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label>Role <?php echo $reqLabel; ?></label>
                                <select name="role_id" id="" class="form-control">
                                    <option value="">Select role</option>
                                    @foreach($data['roles'] as $id => $name)
                                        <option value="{{ $id }}" @if(old('role_id', $prevRoleId) == $id) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role_id'))
                                    <span class="text-danger" role="alert">
                                        <small>{{ $errors->first('role_id') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

                                            
                                        