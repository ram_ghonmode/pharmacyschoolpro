@extends('layouts.dash')   
@section('title', 'Staff List')
@section('content')
<?php use App\Http\Controllers\Controller; ?> 
<style>
    container--default .select2-selection--single select.select2-search__field, select.typeahead, select.tt-query, select.tt-hint
    {
        padding: -3.5625rem 0.75rem !important;
    }
</style> 
    <div class="page-header">
        <h3 class="page-title"> Staff  List </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('employee-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table id="student-listing" class="table table-bordered table-responsive" width="100%">
                        <thead>
                            <tr>
                                <th width="5%">Sr. No.</th>
                                <th>Full Name</th>
                                <th>Designation</th>
                                <th>Login ID</th>
                                <th>Contact No.</th>
                                <th>Gender</th>
                                <th>Category</th>
                                <th>Joining Date</th>
                                <th>Retirement Date</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($employees) && count($employees) > 0)
                                @foreach($employees as $key => $employee)
                                    <?php 
                                        $encyId = Controller::cryptString($employee->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $employee->name }}</td>
                                        <td>{{ $employee->designations->designation_name ?? '' }}</td>
                                        <td>{{ $employee->users->email ?? '' }}</td>
                                        <td>{{ $employee->number ?? '' }}</td>
                                        <td>{{ $employee->gender ?? ''}}</td>
                                        <td>{{ $employee->category ?? ''}}</td>
                                        <td>{{ $employee->doj ?? '' }}</td>
                                        <td>{{ $employee->dor ?? '' }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('employee-edit', ['id' => $encyId]) }}">Edit</a>
                                                   &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$employee->id.'/employees')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection