@extends('layouts.dash')   
@section('title', 'Designation')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Designation </h3>
        @can('designation-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('designation-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Designation Name </th>
                                @if(Gate::check('designation-edit') || Gate::check('designation-delete'))
                                    <th width="18%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($designations) && count($designations) > 0)
                            <?php  $count = $designations->firstItem(); ?>
                                @foreach($designations as $key => $designation)
                                    <?php 
                                        $encyId = Controller::cryptString($designation->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $designation->designation_name }}</td>
                                        @if(Gate::check('class-edit') || Gate::check('class-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('designation-edit')
                                                        <a href="{{ route('designation-edit', ['id' => $encyId]) }}">Edit</a>
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('designation-delete')
                                                        <a href="{{ url('/dashboard/'.$designation->id.'/designations')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif       
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $designations->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection