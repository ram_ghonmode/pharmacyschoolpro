@extends('layouts.dash')    
@section('title', 'Designation')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Designation </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('designation-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('designation-edit', ['id' => isset($designation->id) ? Controller::cryptString($designation->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Designation Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter designation name" name="designation_name" value="{{ old('designation', isset($designation->designation_name) ? $designation->designation_name : '' ) }}">
                                @if ($errors->has('designation_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('designation_name') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection