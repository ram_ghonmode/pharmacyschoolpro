@extends('layouts.print')
@section('content')
    <style>
        .float-div{float: left;}
        .clear-both{clear: both;}
        .float-right{float: right;}
        .text-center{text-align: center;}
        .footer-div{padding-top: 90px;}
    </style>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    @php
                        $classNqty = [];      
                        $instituteName = '';
                        $stamp = '';
                        if(!empty($data['type'])){
                            if($data['type'] == 'School'){
                                $instituteName = $data['instName']->school_name;
                                $stamp = "Head Master";
                            }elseif($data['type'] == 'College'){
                                $instituteName = $data['instName']->school_name;
                                $stamp = "Principal";
                            }
                        }
                    @endphp
                    <table border="1" >
                        <thead>
                        <h2>GENERAL REGISTER</h2>
                        <p>Name of the School <span style="min-width: 450px;border-bottom: 1px solid #000;display: inline-block;margin: 0 10px;">{{ $instituteName }}</span>For the Year : {{ !empty($data['year']['academic_year']) ? $data['year']['academic_year'] : 2015 .'-'. 2016 }} </p>
                        <p style="text-align: center;">[For Admission & Withdrawal Of Student] (In Chapter 11 Section 11) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Vide Rule 83-A.1. Under Chapter Chapter 11 Section 4 APPENDIX EIGHTEEN]</p>
                       
                            <tr>
                                <th>General <br> Register <br> No.</th>
                                <th>Student Name Student- I.D. / U.I.D/</th>
                                <th class="rotate-text">Nationality</th>
                                <th class="rotate-text">Mother tongue</th>
                                <th class="rotate-text">Religion</th>
                                <th class="rotate-text">Caste</th>
                                <th class="rotate-text">Sub Caste</th>
                                <th>Place of <br> Birth</th>
                                <th>Date of Birth <br>(In Figure <br>/ In Words)</th>
                                <th>Last School <br>Attended <br>& Class</th>
                                <th>Date of <br>Admission</th>
                                <th>From which <br> Class and <br> when was <br> studied</th>
                                <th>Progress <br> in the <br> study</th>
                                <th class="rotate-text">Conduct</th>
                                <th>Date & Sign <br> of Parent <br> from issue</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                @for ($i = 1; $i <= 15; $i++)
                                    <td>{{ $i }}</td>
                                @endfor
                            </tr>
                            @if(!empty($allGenRegData))
                                @foreach ($allGenRegData as $regisData)
                                    @php 
                                        if(array_key_exists($regisData->admitToClasses->class_name,$classNqty)){
                                            $classNqty[$regisData->admitToClasses->class_name] += 1;
                                        }else{
                                            $classNqty[$regisData->admitToClasses->class_name] = 1;
                                        }
                                    @endphp
                                    <div>
                                        <tr>
                                            <td>{{ $regisData->gr_number }}</td>
                                            <td>{{ $regisData->first_name .' '.  $regisData->middle_name .' '.  $regisData->last_name }}</td>
                                            <td rowspan="3">{{ $regisData->nationality }}</td>
                                            <td rowspan="3">{{ $regisData->mother_tounge }}</td>
                                            <td rowspan="3">{{ $regisData->religion }}</td>
                                            <td rowspan="3">{{ $regisData->caste }}</td>
                                            <td rowspan="3">{{ $regisData->category }}</td>
                                            <td rowspan="3">{{ $regisData->place_of_birth }}</td>
                                            <td>{{ \Carbon\Carbon::parse($regisData->dob)->format('d/m/Y') }}</td>
                                            <td rowspan="3">{{ $regisData->studentgeneralinfos->last_school_attended . ' / '. $regisData->studentgeneralinfos->leaving_class }}</td>
                                            <td>{{ \Carbon\Carbon::parse($regisData->admission_date)->format('d/m/Y') }}</td>
                                            <td>{{ $regisData->admitToClasses->class_name }}</td>
                                            <td>{{ !empty($regisData->leavingcertificates->progress) ? $regisData->leavingcertificates->progress : null }}</td>
                                            <td>{{ !empty($regisData->leavingcertificates->conduct) ? $regisData->leavingcertificates->conduct : null }}</td>
                                            <!-- <td></td> -->
                                            <td rowspan="4"></td>
                                            <!-- <td></td> -->
                                        </tr>
                                        
                                            <?php
                                              $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                              $dobStr = \Carbon\Carbon::parse($regisData->dob)->format('d/M/Y');
                                              $dobArr = explode("/",$dobStr);
                                             
                                          ?>
                                        
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td rowspan="3" width="80px;">{{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Mother's <br> Name</td>
                                            <td>{{ $regisData->mother_name }}</td>
                                            <td></td>
                                            <td>{{ \Carbon\Carbon::parse($regisData->admission_date)->format('d/m/Y') }}</td>
                                            <td></td>
                                            <td></td>
                                            
                                        </tr>
                                        <tr class="uidno-tr">
                                            <td>Student <br> U.I.D No</td>
                                            <td colspan="2" style="padding: 0;">
                                                <table border="1" class="uidno-table">
                                                    <td style="text-align:left;">{{ $regisData->student_id }}</td>
                                                </table>
                                            </td>
                                            <td>Adhar <br>Card No.</td>
                                            <td colspan="4" style="padding: 0;">
                                                <table border="1" class="uidno-table">
                                                    <td style="text-align:left;">{{ $regisData->studentbankinfos->aadhar_no }}</td>
                                                </table>
                                            </td>
                                            <td>Reason of leaving <br> school</td>
                                            <td colspan="4" style="padding: 0;">
                                                <table border="1" class="uidno-table">
                                                    <td style="text-align:left;">{{ !empty($regisData->leavingcertificates->leaving_reason) ? $regisData->leavingcertificates->leaving_reason : null }}</td>
                                                </table>
                                            </td>
                                            
                                        </tr>
                                    </div>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive col-md-12">                       
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <tr>
                                    <th width="10%">SR.NO</th>
                                    <th>CLASS NAME</th>
                                    <th>ADMISSIONS</th>                                           
                                </tr>
                            </thead>
                            <tbody>   
                            @if(!empty($allGenRegData))
                            @php 
                                ksort($classNqty);
                            @endphp
                                @foreach($classNqty as $key => $value)  
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>                                          
                                        <td> {{ $key }} </td>
                                        <td>{{ $value }}</td>
                                    </tr>     
                                @endforeach
                            @endif                                                                  
                            </tbody>     
                        </table>        
                    </div>
                </div>
            </div>
            <div class="footer-div">
                <div class="float-right">
                    <p class="text-center">{{$stamp}}</p>
                    <p>{{ $instituteName }}.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
