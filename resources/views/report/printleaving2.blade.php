@extends('layouts.print')

@section('content')
<?php 
    function numberTowords($num){
        $ones = array(
            0 =>"ZERO",
            1 => "ONE",
            2 => "TWO",
            3 => "THREE",
            4 => "FOUR",
            5 => "FIVE",
            6 => "SIX",
            7 => "SEVEN",
            8 => "EIGHT",
            9 => "NINE",
            10 => "TEN",
            11 => "ELEVEN",
            12 => "TWELVE",
            13 => "THIRTEEN",
            14 => "FOURTEEN",
            15 => "FIFTEEN",
            16 => "SIXTEEN",
            17 => "SEVENTEEN",
            18 => "EIGHTEEN",
            19 => "NINETEEN",
            "014" => "FOURTEEN"
        );
        $tens = array(
            0 => "ZERO",
            1 => "TEN",
            2 => "TWENTY",
            3 => "THIRTY",
            4 => "FORTY",
            5 => "FIFTY",
            6 => "SIXTY",
            7 => "SEVENTY",
            8 => "EIGHTY",
            9 => "NINETY"
        );
        $hundreds = array(
            "HUNDRED",
            "THOUSAND",
            "MILLION",
            "BILLION",
            "TRILLION",
            "QUARDRILLION"
        ); //limit t quadrillion
        $num = number_format($num,2,".",",");
        $num_arr = explode(".",$num);
        $wholenum = $num_arr[0];
        $decnum = $num_arr[1];
        $whole_arr = array_reverse(explode(",",$wholenum));
        krsort($whole_arr,1);
        $rettxt = "";
        foreach($whole_arr as $key => $i){
            while(substr($i,0,1)=="0")
            $i=substr($i,1,5);
            if(!empty($i) && $i < 20){
                //echo "getting:".$i;
                $rettxt .= $ones[$i];
            }elseif(!empty($i) && $i < 100){
                if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
                if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
            }else{
                if(!empty($i) && substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
                if(!empty($i) && substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
                if(!empty($i) && substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
            }
            if($key > 0){
                $rettxt .= " ".$hundreds[$key]." ";
            }
        }
        if($decnum > 0){
            $rettxt .= " and ";
            if($decnum < 20){
                $rettxt .= $ones[$decnum];
            }elseif($decnum < 100){
                $rettxt .= $tens[substr($decnum,0,1)];
                $rettxt .= " ".$ones[substr($decnum,1,1)];
            }
        }
        return $rettxt;
    }
?>
 <?php
    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
    $dobStr = \Carbon\Carbon::parse($leavingData->admission->dob)->format('d/m/Y');
    $dobArr = explode("/",$dobStr);

    $year = $dobArr[2];
    $month = $dobArr[1];
    $day  = $dobArr[0];
    $birth_day = numberTowords($day);
    $birth_year = numberTowords($year);
    $monthNum = $month;
    $dateObj = DateTime::createFromFormat('!m', $monthNum);//Convert the number into month name
    $monthName = strtoupper($dateObj->format('F'));
?>
<?php
    $distinction_text = null;
    if($leavingData->class == 'DISTINCTION' && !empty($leavingData->dictinction_text)){
        $distinction_text = '  DISTINCTION IN - '. $leavingData->dictinction_text;
    }elseif (!empty($leavingData->class) && !empty($leavingData->dictinction_text)) {
        $distinction_text = 'IN '. $leavingData->class . '  DIST. IN - '. $leavingData->dictinction_text;
    }elseif(!empty($leavingData->class)){
        $distinction_text = 'IN '. $leavingData->class;
    }
?>
<?php

    if($leavingData->month == 'Regular Leaving'){
    $previousMonthNYear = date("Y");
    }else{
        $previousMonthNYear = date("Y",strtotime("-1 year"));
    }
?>

@if($leavingData->admission->current_class == "D Pharm I Year" || $leavingData->admission->current_class == "D Pharm II Year")
    <?php
        $schoolOrCollege = 'Collage';
        $schOrClgName = $leavingData->admission->school->school_name;
        $udiseno = $leavingData->admission->school->udise_no;
        $indexno = $leavingData->admission->school->index_no;
        $recognitionNo = $leavingData->admission->school->recognition_no;
        $classArray = []; $k = 1;  $regular_leaving = null;
        foreach ($classList as $key => $value) {
            $classArray[$k] = $value;
            ++$k;
        }
        $arrKey = array_search($leavingData->admission->current_class, $classArray);
        $cls = $classArray[--$arrKey];
    ?>
    @if($leavingData->admission->current_class != $leavingData->admission->admit_to)
        <?php
            if($leavingData['month'] == 'Regular Leaving'){
                $regular_leaving = ' Std '. $leavingData['result'].' Regular Examination '.$leavingData['year'];
            }else{
            $regular_leaving =$leavingData['result'] .'  '. $leavingData['month'] .'  '. $leavingData['year'];
            }
        ?>
    @endif    
    @else
    <?php $curClass = substr($leavingData['admission']['current_class'], 0, -2);?>
    @if($curClass <= 10)
        <?php
            $schoolOrCollege = 'College';
            $schOrClgName = !empty($leavingData['admission']['school']['school_name']) ? $leavingData['admission']['school']['school_name'] : '';
            $udiseno = !empty($leavingData['admission']['school']['udise_no']) ? $leavingData['admission']['school']['udise_no'] : '';
            $indexno = !empty($leavingData['admission']['school']['index_no']) ? $leavingData['admission']['school']['index_no'] : '';
            $recognitionNo = !empty($leavingData['admission']['school']['recognition_no']) ? $leavingData['admission']['school']['recognition_no'] : '';
        ?>
    @else
        <?php
            $schoolOrCollege = 'College';
            $schOrClgName = !empty($leavingData['admission']['school']['school_name']) ? $leavingData['admission']['school']['school_name'] : '';
            $udiseno = !empty($leavingData['admission']['school']['udise_no']) ? $leavingData['admission']['school']['udise_no'] : '';
            $indexno = !empty($leavingData['admission']['school']['index_no']) ? $leavingData['admission']['school']['index_no'] : '';
            $recognitionNo = !empty($leavingData['admission']['school']['recognition_no']) ? $leavingData['admission']['school']['recognition_no'] : '';
        ?>
    @endif
        <?php
            $classArray = []; $k = 1;
            foreach ($classList as $key => $value) {
                $classArray[$k] = $value;
                ++$k;
            }
            $arrKey = array_search($leavingData['admission']['current_class'], $classArray);
            // $cls = $classArray[--$arrKey];
            $regular_leaving = null;
        ?>
        @if($leavingData['admission']['current_class'] != $leavingData['admission']['admit_to'])
            <?php
                if($leavingData['month'] == 'Regular Leaving'){
                    $regular_leaving = ' Std '. $leavingData['result'].' Regular Examination '.$leavingData['year'];
                }else{
                $regular_leaving = $leavingData['result'] .'  '. $leavingData['month'] .'  '. $leavingData['year'];
                }
            ?>
        @endif   
    @endif

    <?php 

        $aadhar = str_split($leavingData['admission']['studentbankinfos']['aadhar_no']);
    ?>
    <?php 
        if ($leavingData->admission->current_class == 1) {
            $class = "D Pharm I Year";
        }else{
            $class = "D Pharm II Year";
        }
    ?>
<div class="row" style="page-break-after: always;position: relative;">
    <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top: 500px;"> <canvas  class="can" id="demo" style="opacity: 0.1;"></canvas></div>
        <div class="col-sm-12" style="padding:0 35px; font-size: 40px;line-height: 35px;">
            <div class="image mx-4">
                <center>
                    <div class="row">
                        <div class="col-1">
                            <img src='{{ asset("public/images/logo.png") }}' class="img-responsive pull-right" width="150%">
                        </div>
                        <div class="col-11">
                            <span style="font-size: 22px;"><b>{{ $leavingData['admission']['school']['institute_name']}}</b></span><br>
                            <span style="font-size: 27px;"><b>{{ $leavingData['admission']['school']['school_name'] }}</b></span><br>
                            <span style="font-size: 21px;"><b>{{ $leavingData['admission']['school']['address'] }} Dist. {{ $leavingData['admission']['school']['tahsil']['tahsil_name']}} (M.S){{ $leavingData['admission']['school']['phone_no'] }}</b></span>
                            <div class="row justify-content-between">
                                <div class="col"><span style="font-size:22px;">MSBTE CODE -0741</span></div>
                                <div class="col"><span style="font-size:22px;">DTE CODE -4277</span></div>
                            </div>
                            <h2><u>Leaving Certificate</u></h2>
                            <span  style="font-size: 20px;">( Govt. Recognised )</span>
                        </div>
                    </div>
                </center>
                <div class="row justify-content-between ">
                    <div class="col-3">
                        <span style="font-size:22px;">Serial No: </span>
                        <span style="font-size:22px;"><b>{{ $leavingData->serial_no }}</b></span>
                    </div>
                    <div class="col-5">
                        <span style="font-size:22px;">General Register No: </span>
                        <span style="font-size:22px;"><b>{{ $leavingData->gr_number }}</b></span>
                    </div>
                </div>
                <span style="font-size:22px; margin-left:50px ">
                    No change any entry in this certificate shall be made except by the authority issuing and any infragmennt of this requirment is liable to involve the imposition of a penalty even that of rustication.
                </span>
                <div class="row">
                    <div class="col">
                        <table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr>
                                <td>
                                    <span style="font-size:22px;">MSBTE Enrollment No.</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['student_id']}}</b></span>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <span style="font-size:22px;">Name of Student(in full)</span>
                                </td>
                                <td>
                                    <span>:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['first_name'] }} {{ $leavingData['admission']['guardianinfos']['father_first_name'] }} {{ $leavingData['admission']['guardianinfos']['father_last_name'] }}</b></span>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <span style="font-size:22px;">Mother Name</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['mother_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Race & Caste</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['religion'] }} ({{ $leavingData['admission']['caste'] }})</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Category</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['categories']['category_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Nationality</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">{{ $leavingData['admission']['nationality'] }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Palce of Birth (Village / City)</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['place_of_birth'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Date of Birth</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b><span>{{ $dobStr }} ({{ $birth_day .' '. $monthName .' '. $birth_year }})</span></b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Last School/Collage Attended</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    @if(!empty($leavingData['admission']['studentgeneralinfos']['last_school_attended']))
                                        <span style="font-size:22px;"><b>{{ $leavingData['admission']['studentgeneralinfos']['last_school_attended'] }} - std {{ $leavingData['admission']['studentgeneralinfos']['leaving_class'] }}</b></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Progress</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData['progress'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Conduct</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData['conduct'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Admission Date</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ \Carbon\Carbon::parse($leavingData['admission']['admission_date'])->format('d/m/Y') }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Cource & Year in Which Studying</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ 'May '. $previousMonthNYear .' in class '. $class }} </b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Leaving Date</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ \Carbon\Carbon::parse($leavingData->leaving_date)->format('d/m/Y') }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Reason for Leaving</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData->leaving_reason }}</b></span>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <span style="font-size:22px;">Remarks</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    
                                    <span style="font-size:22px;"><b>He/She Has {{$class}} in  Examination in {{$leavingData->month}} {{$previousMonthNYear}} With {{$distinction_text}}</b></span>
                                </td>
                            </tr>
                        </table>
                        <center>
                            <p style="font-size:22px; margin-top: 20px ">Certified that the above information is in accordance with the Institute General Register No. {{ $leavingData->gr_number }}</p>
                        </center>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td>
                            <span style="font-size:22px;">Seal</span>
                        </td>
                    </tr>
                    <tr class="text-center">
                        <td width="50%" style="text-align: center;">
                            <p><b>_______________</b></p>
                            <span style="font-size:22px;">Clerk</span>
                        </td>
                        <td width="50%" style="padding-top:0px;">
                            <p style="text-align: center;"><b>_______________</b></p>
                            <span style="font-size:22px;">Principal</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <span style="font-size:22px;">Date: </span>
                            <span style="font-size:22px;"><b>{{ date('d', strtotime($leavingData->leaving_date)) }}/{{ date('m', strtotime($leavingData->leaving_date)) }}/{{ date('Y', strtotime($leavingData->leaving_date)) }}</b></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <span style="font-size:22px;">Place : </span>
                            <span style="font-size:22px;"><b>Pulgaon</b></span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

<!-- 2nd page of certificate -->
<div class="row mx-4">
    <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top: 500px;"> <canvas  class="can" id="demo2" style="opacity: 0.1; z-index: 1;"></canvas></div>
    <div class="col-sm-12" style="padding:0 35px; font-size: 40px;line-height: 37px;">
        <div class="image">
            <center>
                    <div class="row">
                        <div class="col-1">
                            <img src='{{ asset("public/images/logo.png") }}' class="img-responsive pull-right" width="150%">
                        </div>
                        <div class="col-11">
                            <span style="font-size: 22px;"><b>{{ $leavingData['admission']['school']['institute_name']}}</b></span><br>
                            <span style="font-size: 27px;"><b>{{ $leavingData['admission']['school']['school_name'] }}</b></span><br>
                            <span style="font-size: 21px;"><b>{{ $leavingData['admission']['school']['address'] }} Dist. {{ $leavingData['admission']['school']['tahsil']['tahsil_name']}} (M.S){{ $leavingData['admission']['school']['phone_no'] }}</b></span>
                            <div class="row justify-content-between">
                                <div class="col"><span style="font-size:22px;">MSBTE CODE -0741</span></div>
                                <div class="col"><span style="font-size:22px;">DTE CODE -4277</span></div>
                            </div>
                            <h2><u>Leaving Certificate</u></h2>
                            <span  style="font-size: 17px;">( Govt. Recognised )</span>
                        </div>
                    </div>
                </center>
                <div class="row justify-content-between ">
                    <div class="col-3">
                        <span style="font-size:22px;">Serial No: </span>
                        <span style="font-size:22px;"><b>{{ $leavingData->serial_no }}</b></span>
                    </div>
                    <div class="col-5">
                        <span style="font-size:22px;">General Register No: </span>
                        <span style="font-size:22px;"><b>{{ $leavingData->gr_number }}</b></span>
                    </div>
                </div>
                <span style="font-size:22px; margin-left:50px ">
                    No change any entry in this certificate shall be made except by the authority issuing and any infragmennt of this requirment is liable to involve the imposition of a penalty even that of rustication.
                </span>
                <div class="row">
                    <div class="col">
                        <table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr>
                                <td>
                                    <span style="font-size:22px;">MSBTE Enrollment No.</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['student_id']}}</b></span>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <span style="font-size:22px;">Name of Student(in full)</span>
                                </td>
                                <td>
                                    <span>:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['first_name'] }} {{ $leavingData['admission']['guardianinfos']['father_first_name'] }} {{ $leavingData['admission']['guardianinfos']['father_last_name'] }}</b></span>
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    <span style="font-size:22px;">Mother Name</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['mother_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Race & Caste</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['religion'] }} ({{ $leavingData['admission']['caste'] }})</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Category</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['categories']['category_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Nationality</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">{{ $leavingData['admission']['nationality'] }}</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Palce of Birth (Village / City)</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b>{{ $leavingData['admission']['place_of_birth'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Date of Birth</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    <span style="font-size:22px;"><b><span>{{ $dobStr }} ({{ $birth_day .' '. $monthName .' '. $birth_year }})</span></b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Last School/Collage Attended</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td >
                                    @if(!empty($leavingData['admission']['studentgeneralinfos']['last_school_attended']))
                                        <span style="font-size:22px;"><b>{{ $leavingData['admission']['studentgeneralinfos']['last_school_attended'] }} - std {{ $leavingData['admission']['studentgeneralinfos']['leaving_class'] }}</b></span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Progress and Conduct</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData['progress'] }} ({{ $leavingData['conduct'] }})</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Admission Date</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ \Carbon\Carbon::parse($leavingData['admission']['admission_date'])->format('d/m/Y') }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Cource & Year in Which Studying</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ 'May '. $previousMonthNYear .' in class '. $class }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Leaving Date</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ \Carbon\Carbon::parse($leavingData->leaving_date)->format('d/m/Y') }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-size:22px;">Reason for Leaving</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>{{ $leavingData->leaving_reason }}</b></span>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <span style="font-size:22px;">Remarks</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;">:</span>
                                </td>
                                <td>
                                    <span style="font-size:22px;"><b>He/She Has {{$class}} in  Examination in {{$leavingData->month}} {{$previousMonthNYear}} With {{$distinction_text}}</b></span>
                                </td>
                            </tr>
                        </table>
                        <center>
                            <p style="font-size:22px; margin-top: 20px ">Certified that the above information is in accordance with the Institute General Register No. {{ $leavingData->gr_number }}</p>
                        </center>
                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr class="text-center">
                        <td width="50%" style="text-align: center;">
                            <p><b>_______________</b></p>
                            <span style="font-size:22px;">Clerk</span>
                        </td>
                        <td width="50%" style="padding-top:0px;">
                            <p style="text-align: center;"><b>_______________</b></p>
                            <span style="font-size:22px;">Principal</span><br>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <span style="font-size:22px;">Date: </span>
                            <span style="font-size:22px;"><b>{{ date('d', strtotime($leavingData->leaving_date)) }}/{{ date('m', strtotime($leavingData->leaving_date)) }}/{{ date('Y', strtotime($leavingData->leaving_date)) }}</b></span>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="3">
                            <span style="font-size:22px;">Place : </span>
                            <span style="font-size:22px;"><b>Pulgaon</b></span>
                        </td>
                    </tr>
                </table>
        </div>
        
    </div>
</div>
<script>
        // (B) IMAGES + CANVAS
    var iBack = new Image(),
        iMark = new Image(),
        iLoaded = 0,
        canvas = document.getElementById("demo"),
        ctx = canvas.getContext("2d");
     
    // (C) WATERMARK
    function cmark () { 
        iLoaded++; 
        if (iLoaded==2) {        
          // (C2) ADD WATERMARK
            canvas.width = iBack.naturalWidth;
            canvas.height = iBack.naturalHeight;
            ctx.drawImage(iMark, 0, 0, iMark.naturalWidth, iMark.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack.onload = cmark;
    iMark.onload = cmark;
    iBack.src = '{{ asset("public/images/logo.png") }}';
    iMark.src = '{{ asset("public/images/logo.png") }}';
/*for 2nd water mark*/
            // (B) IMAGES + CANVAS
    var iBack2 = new Image(),
        iMark2 = new Image(),
        iLoaded2 = 0,
        canvas2 = document.getElementById("demo2"),
        ctx2 = canvas2.getContext("2d");
     
    // (C) WATERMARK
    function cmark2 () { 
        iLoaded2++; 
        if (iLoaded2==2) {        
          // (C2) ADD WATERMARK
            canvas2.width = iBack2.naturalWidth;
            canvas2.height = iBack2.naturalHeight;
            ctx2.drawImage(iMark2, 0, 0, iMark2.naturalWidth, iMark2.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack2.onload = cmark2;
    iMark2.onload = cmark2;
    iBack2.src = '{{ asset("public/images/logo.png") }}';
    iMark2.src = '{{ asset("public/images/logo.png") }}';
        
</script>        
@endsection

