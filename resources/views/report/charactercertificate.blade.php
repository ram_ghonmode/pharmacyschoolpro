@extends('layouts.dash')   
@section('title', 'Character List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Character Certificate </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" action="{{ route('character-list') }}">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-sm-5">
                                <div class="demo-radio-button">
                                    <div class="radio-group">
                                        <input name="admission_in" type="radio" id="atcollegeId" value="college" class="with-gap radio-col-indigo" @if(!empty($data['admission_in']) && $data['admission_in'] == 'college') checked @endif />
                                        <label for="atcollegeId">College</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line" >
                                        <input type="text" name="gr_number" value="{{ !empty($data['gr_number']) ? $data['gr_number'] : old('gr_number') }}" style="border: none;" placeholder="G.R No." required  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-sm btn-info">Generate</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            @if(!empty($characterData ) && !empty($leavingData ))
                <div class="row clearfix">
                    <form method="POST" action="{{ route('character-certificate') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body"> 
                                    @php
                                        $instituteName = '';
                                        $instname ='';
                                        $stamp = '';
                                        if(!empty($data['admission_in'])){
                                            if($data['admission_in'] == 'school' || $data['admission_in'] == 'pre-primary'){
                                                $instituteName = $characterData->school->school_name;
                                                $instname = "school"; $stamp = "Head Master";
                                            }elseif($data['admission_in'] == 'college'){
                                                $instituteName = $characterData->school->school_name;
                                                $instname = "college"; $stamp = "Principal";
                                            }
                                        }
                                    @endphp  
                                    @php
                                        $genderinfo = '';
                                        $geninfo = '';
                                            if($characterData['gender'] == 'Female'){
                                                $genderinfo = "she"; $geninfo = "her";
                                            }elseif($characterData['gender'] == 'Male'){
                                                $genderinfo ="he"; $geninfo = "his";
                                            }
                                        
                                    @endphp  
                                                        
                                    <div class="border-class">
                                        
                                        <div class="heading-div">
                                            <h1 style="margin-bottom: 50px; margin-top: 20px;">{{ $instituteName }}</h1>
                                            <div class="" style="margin:0">
                                                <img style="width:100px" src='{{ asset("public/images/logo.png") }}'>
                                                <h3 style="padding-top: 10px;" ><u>CHARACTER CERTIFICATE</u></h3>
                                            </div>
                                            
                                            <input type="hidden" name="gr_number"  value="{{ !empty($characterData['gr_number']) ? $characterData['gr_number'] : old('gr_number') }}">
                                            <input type="hidden" name="admission_in"  value="{{ !empty($characterData['admission_in']) ? $characterData['admission_in'] : old('admission_in') }}">
                                        </div>
                                        
                                        <!-- <div style="padding-top: 120px; text-align: center;"><h3  ><u>CHARACTER CERTIFICATE</u></h3></div> -->

                                        <div class="clear-both"></div>
                                        <div class="header-div" style="padding:30px 30px;">
                                            <?php
                                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                $dobStr = \Carbon\Carbon::parse($characterData->dob)->format('d/M/Y');
                                                $dobArr = explode("/",$dobStr);
                                            ?>

                                            <p style="text-indent: 70px; text-align: justify;">This is to certify that Shri/Smt./Ku <b>{{ $characterData->first_name.' '.  $characterData->middle_name .' '. $characterData->last_name }}</b> is/ was a bonafied student of this college during the term <b> {{ date('d-m-Y',strtotime($characterData->admission_date)) }}</b> to <b>{{ date('d-m-Y',strtotime($leavingData->leaving_date)) }},</b> <!-- {{$instname}}, from<b> {{ date('d-m-Y',strtotime($characterData->admission_date)) }}</b> to <b>{{ date('d-m-Y',strtotime($leavingData->leaving_date)) }},</b> and was studing in standard <b>{{ !empty($characterData->classes->class_name) ? $characterData->classes->class_name : '' }} {{ $characterData->section }} --></b>.</p>
                                            <p style="text-align: justify;">He / She has passed his final year <b>{{ !empty($characterData->classes->class_name) ? $characterData->classes->class_name : '' }} </b>section examination held in the month of<b>-----</b></p>
                                            <!-- <p style="text-align: justify;">As per our {{$instname}} record register {{$geninfo}} date of birth is <b>{{ date('d-m-Y',strtotime($characterData->dob)) }}</b> (in words) {{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }}.</p> -->
                                            <p style="text-align: justify;">As far as my knowledge is concerned he/she posseses a good moral charcter. I wish him every success in his/her future life.</p>
                                            <!-- <p style="text-align: justify;">So far as known to us, {{$genderinfo}} bears a good moral character and conduct.</p> -->
                                        </div>
                                            
                                        <div class="footer-div">
                                            <div class="float-div">
                                                <!-- <p>Place: {{ $characterData->school->city->city_name }}</p> -->
                                                <p>Date: {{ date('d-m-Y') }}</p>
                                            </div>
                                            <div class="float-right">
                                                <p class="text-center">{{$stamp}}</p>
                                                <p>{{ $instituteName }}.</p>
                                            </div>
                                            <div class="clear-both"></div>
                                        </div>
                                    </div>   
                                    @can('leaving-certificate')                 
                                        <div class="clearfix">    
                                            <button type="submit" class="btn btn-sm btn-info waves-effect" style="float:right">GENERATE</button> 
                                        </div> 
                                    @endcan                   
                                </div>
                            </div>
                        </div>
                    </form>    
                </div>
            @endif
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered reporttable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>GR No.</th>
                                <th>Admission Type</th>
                                <th>Full Name</th>
                                <th>ADMISSION DATE</th>
                                <th>Date</th>
                                @can('printcharacter') 
                                    <th class="text-center">Action_List</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($CharacterCertificate))
                                @foreach($CharacterCertificate as $key => $charCert)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $charCert->gr_number }}</td>
                                        <td>{{ $charCert->admission_in ?? '' }}</td>
                                        <td>{{ $charCert->fullname }}</td>
                                        <td>{{ date('d-m-Y',strtotime($charCert->admission_date)) }}</td>
                                        <td>{{ date('d-m-Y',strtotime($charCert->current_date)) }}</td>
                                        @can('printcharacter') 
                                            <td class="text-center">
                                                <a href='{{ url("printcharacter/".Crypt::encrypt($charCert->id)) }}' target="_blank" title="Print">
                                                    <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                        <i class="icon-printer menu-icon"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection