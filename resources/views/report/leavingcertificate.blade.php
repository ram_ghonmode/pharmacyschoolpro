@extends('layouts.dash')   
@section('title', 'Leaving List')
@section('content')  
<style>
    select.form-control{outline: none;border-bottom: 1px solid #cbced3;}
    .form-group {margin-bottom: 0.5rem;}
</style>
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Leaving Certificate </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card" style="position: relative;">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('leaving-certificate') }}"  id="leavingCertifFormId">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <select name="admission_in" class="form-control show-tick">
                                           <!--  <option value="">Admission Type</option> -->
                                            <!-- <option value="Pre-Primary" @if(old('admission_in', isset($data['admission_in']) ? $data['admission_in'] : '') == "Pre-Primary") selected @endif>Pre-Primary</option>
                                            <option value="School" @if(old('admission_in', isset($data['admission_in']) ? $data['admission_in'] : '') == "School" ) selected @endif>School</option> -->
                                            <option value="College" @if(old('admission_in', isset($data['admission_in']) ? $data['admission_in'] : '') == "College" ) selected @endif>College</option>
                                        </select>
                                    </div>
                                </div>
                                @if ($errors->has('admission_in'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('admission_in') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line" >
                                        <input type="text" name="gr_number" value="{{ !empty($data['gr_number']) ? $data['gr_number'] : old('gr_number') }}" class="form-control" style="border: none;" placeholder="G.R No."/>
                                    </div>
                                    @if ($errors->has('gr_number'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('gr_number') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-2">
                                <select name="result" id="resultId" class="form-control text-uppercase">
                                    <option value="">Result</option>
                                    <option value="PASS" @if(old('result', isset($data['result']) ? $data['result'] : '') == 'PASS') selected @endif>PASS</option>
                                    <option value="FAIL" @if(old('result', isset($data['result']) ? $data['result'] : '') == 'FAIL') selected @endif>FAIL</option>
                                    <option value="Promoted with ATKT" @if(old('result', isset($data['result']) ? $data['result'] : '') == 'Promoted for 11th standard with ATKT') selected @endif>Promoted</option>
                                    <option value="Eligible for skill development program" @if(old('result', isset($data['result']) ? $data['result'] : '') == 'Eligible for skill development program') selected @endif>Skill Development</option>
                                    <option value="Eligible for re-examination" @if(old('result', isset($data['result']) ? $data['result'] : '') == 'Eligible for re-examination') selected @endif>Re-Examination</option>
                                </select>
                                @if ($errors->has('result'))
                                    <span class="invalid-feedback text-danger">
                                        <small>{{ $errors->first('result') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line" >
                                        <input type="text" name="progress" value="Satisfactory" class="form-control" style="border: none;" placeholder="Progress"/>
                                    </div>
                                </div>
                                @if ($errors->has('progress'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('progress') }}</small>
                                    </span>
                                @endif
                            </div>
                            <?php $monthArr = ['Summer','Winter','Regular Leaving']; ?>
                            <div class="col-md-2">
                                <select name="month" class="form-control">
                                    <option value=""> Month</option>
                                    @foreach ($monthArr as $mkey => $month)
                                        <option value="{{ $month }}" @if(old('month',isset($data['month']) ? $data['month'] : '') == $month) selected @endif>{{ $month }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('month'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('month') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <select name="year" id="yearId" class="form-control text-uppercase">
                                    <option value="">Year</option>
                                    <option value="2020" @if(old('year',isset($data['year']) ? $data['year'] : '') == '2020') selected @endif>2020</option>
                                    <option value="2021" @if(old('year',isset($data['year']) ? $data['year'] : '') == '2021') selected @endif>2021</option>
                                    <option value="2022" @if(old('year',isset($data['year']) ? $data['year'] : '') == '2022') selected @endif>2022</option>
                                    <option value="2023" @if(old('year',isset($data['year']) ? $data['year'] : '') == '2023') selected @endif>2023</option>
                                    <option value="2024" @if(old('year',isset($data['year']) ? $data['year'] : '') == '2024') selected @endif>2024</option>
                                </select>
                                @if ($errors->has('year'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('year') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line" >
                                        <input type="text" name="conduct" value="Good" class="form-control" style="border: none;" placeholder="Conduct"/>
                                    </div>
                                </div>
                                @if ($errors->has('conduct'))
                                    <span class="text-danger text-danger">
                                        <small>{{ $errors->first('conduct') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                <select name="class" id="classId" class="form-control text-uppercase">
                                    <option value=""> Class</option>
                                    <option value="-" @if(old('class', isset($data['class']) ? $data['class'] : '') == '-') selected @endif>NO CLASS</option>
                                    <option value="FIRST DIVISION" @if(old('class', isset($data['class']) ? $data['class'] : '') == 'FIRST DIVISION') selected @endif>FIRST</option>
                                    <option value="SECOND DIVISION" @if(old('class', isset($data['class']) ? $data['class'] : '') == 'SECOND DIVISION') selected @endif>SECOND</option>
                                    <option value="THIRD DIVISION" @if(old('class', isset($data['class']) ? $data['class'] : '') == 'THIRD DIVISION') selected @endif>THIRD</option>
                                    <option value="DISTINCTION" @if(old('class', isset($data['class']) ? $data['class'] : '') == 'DISTINCTION') selected @endif>DISTINCTION</option>
                                </select>
                                @if ($errors->has('class'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('class') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-8 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="dictinction_text" value="{{ !empty($data['dictinction_text']) ? $data['dictinction_text'] : old('dictinction_text') }}" class="form-control"  style="border: none;" placeholder="DISTINCTION_IN"/>
                                    </div>
                                </div>
                                @if ($errors->has('dictinction_text'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('dictinction_text') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <select class="form-control show-tick" name="leaving_reason">
                                    <option value="">Reason For Leaving</option>
                                    <option value="Has Passed in Diploma in pharmacy Examination" @if(old('leaving_reason', isset($data['leaving_reason']) ? $data['leaving_reason'] : '') == 'Has Passed in Diploma in pharmacy Examination') selected @endif>Has Passed in Diploma in pharmacy Examination</option>
                                    <!-- <option value="Has Passed in Diploma in pharmacy Examination" @if(old('leaving_reason', isset($data['leaving_reason']) ? $data['leaving_reason'] : '') == 'Has Passed in Diploma in pharmacy Examination') selected @endif>Has Passed in Diploma in pharmacy Examination</option> -->
                                    <option value="As Per Guardian Request" @if(old('leaving_reason', isset($data['leaving_reason']) ? $data['leaving_reason'] : '') == 'As Per Guardian Request') selected @endif>As Per Guardian Request</option>
                                    <option value="Due to poor results in academic" @if(old('leaving_reason', isset($data['leaving_reason']) ? $data['leaving_reason'] : '') == 'Due to poor results in academic') selected @endif>Due to poor results in academic</option>
                                    <option value="Terminated from School" @if(old('leaving_reason', isset($data['leaving_reason']) ? $data['leaving_reason'] : '') == 'Terminated from School') selected @endif>Terminated from School</option>
                                </select>
                                @if ($errors->has('leaving_reason'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('leaving_reason') }}</small>
                                    </span>
                                @endif
                            </div>
                            @if(!empty($leavingCertifData->id))
                                <input type="hidden" name="admission_id" value="{{ $leavingCertifData->id }}">
                                <input type="hidden" name="current_class" value="{{ $leavingCertifData->current_class }}">
                            @endif
                            <div class="col-sm-3">
                                <div class="form-group form-float">
                                    <div id="datepicker-popup" class="input-group date datepicker">
                                        <input type="text" name="leaving_date" value="{{ !empty($data['leaving_date']) ? date('d-m-Y', strtotime($data['leaving_date'])) : old('leaving_date') }}" class="datepicker form-control" class="form-control" style="border: none;border-bottom: 1px solid #cbced3;" placeholder="Leaving Date">
                                        <span class="input-group-addon input-group-append border-left">
                                            <span class="icon-calendar input-group-text"></span>
                                        </span>
                                    </div>
                                </div>
                                @if ($errors->has('leaving_date'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('leaving_date') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <select name="status" id="statusId" class="form-control text-uppercase">
                                    <option value="">Leaving Status</option>
                                    <option value="ORIGINAL" @if(old('status', isset($data['status']) ? $data['status'] : '') == 'ORIGINAL') selected @endif>ORIGINAL</option>
                                    <option value="DUPLICATE" @if(old('status', isset($data['status']) ? $data['status'] : '') == 'DUPLICATE') selected @endif>DUPLICATE</option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('status') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" name="saveBtn" value="generate" class="btn btn-info btn-sm" style="margin-top: 12px;">GENERATE</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            @if(!empty($leavingCertifData))
                @if($leavingCertifData->current_class == "KG1" || $leavingCertifData->current_class == "KG2" || $leavingCertifData->current_class == "Nursery")
                    <?php
                        $schoolOrCollege = 'School';
                        $schOrClgName = $leavingCertifData->school->school_name;
                        $udiseno = $leavingCertifData->school->udise_no;
                        $indexno = $leavingCertifData->school->index_no;
                        $recognitionNo = $leavingCertifData->school->recognition_no;
                        $classArray = []; $k = 1; $regular_leaving = null;
                        foreach ($classList as $key => $value) {
                            $classArray[$k] = $value;
                            ++$k;
                        }
                        $arrKey = array_search($leavingCertifData->current_class, $classArray);
                        $cls = $classArray[--$arrKey];
                    ?>
                    @if($leavingCertifData->current_class != $leavingCertifData->admit_to)
                        <?php
                            if($data['month'] == 'Regular Leaving'){
                                $regular_leaving = $cls .' Std '. $data['result'].' Regular Examination '.$data['year'];
                            }else{
                            $regular_leaving =$data['result'] .'  '. $data['month'] .'  '. $data['year'];
                            }
                        ?>
                    @endif    
                @else
                    <?php $curClass = $leavingCertifData->current_class; ?>
                    @if($curClass <= 10)
                        <?php
                            $schoolOrCollege = 'School';
                            $schOrClgName = $leavingCertifData->school->school_name;
                            $udiseno = $leavingCertifData->school->udise_no;
                            $indexno = $leavingCertifData->school->index_no;
                            $recognitionNo = $leavingCertifData->school->recognition_no;
                        ?>
                    @else
                        <?php
                            $schoolOrCollege = 'College';
                            $schOrClgName = $leavingCertifData->school->school_name;
                            $udiseno = $leavingCertifData->school->udise_no;
                            $indexno = $leavingCertifData->school->index_no;
                            $recognitionNo = $leavingCertifData->school->recognition_no;
                        ?>
                    @endif
                @endif
                <?php
                    $classArray = []; $k = 1;
                    foreach ($classList as $key => $value) {
                        $classArray[$k] = $value;
                        ++$k;
                    }
                    $arrKey = array_search($leavingCertifData->current_class, $classArray);
                    // $cls = $classArray[--$arrKey];
                    $regular_leaving = null;
                ?>
                <div class="card">
                    <div class="card-body">
                        <style type="text/css">
                            body{ font-family: arial;}
                            table{font-size:12px;}
                            table td{padding:3px 3px;}
                            .padding-left{padding: 0 0  0 5px !important;}
                            .border-div{border-top: 1.5px solid #000;border-bottom: 1.5px solid #000;}
                            p{ margin: 0;}
                        </style>
                    <!-- Start HTML code from here -->
                        <div class="row clearfix">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-8" style="border:2px solid #000; padding:15px;">
                                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td colspan="2">
                                                        <span>Name of the management: </span>
                                                        <span><b>{{ $leavingCertifData->school->institute_name }}</b></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <span>{{ $schoolOrCollege }} Name: </span>
                                                        <span><b>{{ $schOrClgName }}</b></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4">
                                                        <span>Address: </span>
                                                        <span><b>{{ $leavingCertifData->school->address }}</b></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Taluka: </span>
                                                        <span><b>{{ $leavingCertifData->school->tahsil->tahsil_name }}</b></span>
                                                    </td>
                                                    <!-- <td>
                                                        <span>Dist.: </span>
                                                        <span><b>{{ $leavingCertifData->school->district .', '. $leavingCertifData->school->pin }}</b></span>
                                                    </td> -->
                                                </tr>
                                                <tr>
                                                    <td width="500px" class="padding-left">
                                                        <span>Phone No: </span>
                                                        <span><b>{{ $leavingCertifData->school->phone_no }}</b></span>
                                                    </td>
                                                    <td width="570px">
                                                        <span>Email id: </span>
                                                        <span><b>{{ $leavingCertifData->school->email }}</b></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>Serial No: </span>
                                                        <span><b>{{ $serialNo }}</b></span>
                                                    </td>
                                                    <td>
                                                        <span>General Register No: </span>
                                                        <span><b>{{ $leavingCertifData->gr_number }}</b></span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="top" class="clearfix">
                                            <img src='{{ asset("public/images/logo.png") }}' class="img-responsive pull-right" width="60%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <span>{{ $schoolOrCollege }} Recognition No: </span>
                                            <span><b>{{ $recognitionNo }}</b>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <span>Medium: </span>
                                            <span><b>{{ $leavingCertifData->mediumstream->name }} </b></span>
                                        </td>
                                    </tr>
                                    <tr class="border-div">
                                        <td colspan="4">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <span>UDISE No: </span>
                                                    <span><b>{{ $udiseno }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Board: </span>
                                                    <span><b>{{ $leavingCertifData->school->board }}</b></span>
                                                </td>
                                                <td>
                                                    <span>{{ $schoolOrCollege }} Index No.: </span>
                                                    <span><b>{{ $indexno }}</b></span>
                                                </td>
                                            </tr>
                                            </table>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="text-center">
                                            <span style="font-size:15px;"><b>{{ $schoolOrCollege }} Leaving Certificate</b></span>
                                            <span style="font-size:10px;"> ({{ $data['status'] }})</span>
                                            </td>
                                        </tr>
                                    </table>

                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr class="border-div">
                                                <td>
                                                    <span>Student I.D No.: </span>
                                                    <span><b>{{ $leavingCertifData->student_id }}</b></span>
                                                </td>
                                                <td>
                                                    <span>U. I.D No.(Adhar Card No.): </span>
                                                    <span><b>{{ $leavingCertifData->studentbankinfos->aadhar_no }}</b></span>
                                                </td>
                                            </tr>
                                            </table>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td colspan="5">
                                                    <span>Student First Name (Name): </span>
                                                    <span><b>{{ $leavingCertifData->first_name }}</b></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <span>(Father Name): </span>
                                                    <span><b>{{ $leavingCertifData->guardianinfos->father_first_name }}</b>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <span>(Surname): </span>
                                                    <span><b>{{ $leavingCertifData->guardianinfos->father_last_name }}</b></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <span>Mother Name: </span>
                                                    <span><b>{{ $leavingCertifData->mother_name }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Nationality: </span>
                                                    <span><b>{{ $leavingCertifData->nationality }}</b></span>
                                                </td>
                                                <td colspan="2">
                                                    <span>Mother Tongue: </span>
                                                    <span><b>{{ $leavingCertifData->mother_tounge }} </b></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <span>Religion: </span>
                                                    <span><b>{{ $leavingCertifData->religion }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Caste: </span>
                                                    <span><b>{{ $leavingCertifData->caste }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Sub Caste: </span>
                                                    <span><b></b></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5">
                                                    <span>Place of Birth (Village / City): </span>
                                                    <span><b>{{ $leavingCertifData->place_of_birth }}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <span>Taluka: </span>
                                                    <span><b>{{ $leavingCertifData->guardianinfos->tahsil }}</b></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Dist.: </span>
                                                    <span><b>{{ $leavingCertifData->guardianinfos->district }}</b></span>
                                                </td>
                                                <td>
                                                    <span>State: </span>
                                                    <span><b>{{ $leavingCertifData->guardianinfos->state }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Country: </span>
                                                    <span><b>{{ $leavingCertifData->guardianinfos->country }}</b></span>
                                                </td>
                                            </tr>
                                            <?php
                                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                $dobStr = \Carbon\Carbon::parse($leavingCertifData->dob)->format('d/m/Y');
                                                $dobArr = explode("/",$dobStr);

                                                function numberTowords($num){
                                                    // dd($num);
                                                    $ones = array(
                                                        0 =>"ZERO",
                                                        1 => "ONE",
                                                        2 => "TWO",
                                                        3 => "THREE",
                                                        4 => "FOUR",
                                                        5 => "FIVE",
                                                        6 => "SIX",
                                                        7 => "SEVEN",
                                                        8 => "EIGHT",
                                                        9 => "NINE",
                                                        10 => "TEN",
                                                        11 => "ELEVEN",
                                                        12 => "TWELVE",
                                                        13 => "THIRTEEN",
                                                        14 => "FOURTEEN",
                                                        15 => "FIFTEEN",
                                                        16 => "SIXTEEN",
                                                        17 => "SEVENTEEN",
                                                        18 => "EIGHTEEN",
                                                        19 => "NINETEEN",
                                                        "014" => "FOURTEEN"
                                                    );
                                                    $tens = array(
                                                        0 => "ZERO",
                                                        1 => "TEN",
                                                        2 => "TWENTY",
                                                        3 => "THIRTY",
                                                        4 => "FORTY",
                                                        5 => "FIFTY",
                                                        6 => "SIXTY",
                                                        7 => "SEVENTY",
                                                        8 => "EIGHTY",
                                                        9 => "NINETY"
                                                    );
                                                    $hundreds = array(
                                                        "HUNDRED",
                                                        "THOUSAND",
                                                        "MILLION",
                                                        "BILLION",
                                                        "TRILLION",
                                                        "QUARDRILLION"
                                                    ); //limit t quadrillion
                                                    $num = number_format($num,2,".",",");
                                                    $num_arr = explode(".",$num);
                                                    $wholenum = $num_arr[0];
                                                    $decnum = $num_arr[1];
                                                    $whole_arr = array_reverse(explode(",",$wholenum));
                                                    krsort($whole_arr,1);
                                                    $rettxt = "";
                                                    foreach($whole_arr as $key => $i){
                                                        while(substr($i,0,1)=="0")
                                                        $i=substr($i,1,5);
                                                        if(!empty($i) && $i < 20){
                                                            //echo "getting:".$i;
                                                            $rettxt .= $ones[$i];
                                                        }elseif(!empty($i) && $i < 100){
                                                            if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
                                                            if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
                                                        }else{
                                                            if(!empty($i) && substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
                                                            if(!empty($i) && substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
                                                            if(!empty($i) && substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
                                                        }
                                                        if($key > 0){
                                                            $rettxt .= " ".$hundreds[$key]." ";
                                                        }
                                                    }
                                                    if($decnum > 0){
                                                        $rettxt .= " and ";
                                                        if($decnum < 20){
                                                            $rettxt .= $ones[$decnum];
                                                        }elseif($decnum < 100){
                                                            $rettxt .= $tens[substr($decnum,0,1)];
                                                            $rettxt .= " ".$ones[substr($decnum,1,1)];
                                                        }
                                                    }
                                                    return $rettxt;
                                                }

                                                $year = $dobArr[2];
                                                $month = $dobArr[1];
                                                $day  = $dobArr[0];
                                                $birth_day = numberTowords($day);
                                                $birth_year = numberTowords($year);
                                                $monthNum = $month;
                                                $dateObj = DateTime::createFromFormat('!m', $monthNum);//Convert the number into month name
                                                $monthName = strtoupper($dateObj->format('F'));
                                            ?>
                                            <tr>
                                                <td>
                                                    <span>Date of Birth: </span>
                                                    <span><b>{{ $dobStr }}</b></span>
                                                </td>
                                                <td colspan="4">
                                                    <span>Date of Birth in words: </span>
                                                    <span><b>{{ $birth_day .' '. $monthName .' '. $birth_year }}</b></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <span>Previous School and Class: </span>
                                                    @if(!empty($leavingCertifData->studentgeneralinfos->last_school_attended))
                                                        <span><b>{{ $leavingCertifData->studentgeneralinfos->last_school_attended }} - std {{ $leavingCertifData->studentgeneralinfos->leaving_class }}</b></span>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span>Admission Date: </span>
                                                    <span><b>{{ \Carbon\Carbon::parse($leavingCertifData->admission_date)->format('d/m/Y') }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Class: </span>
                                                    <span><b>{{ $leavingCertifData->admitToClasses->class_name }}</b></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <span>Progress: </span>
                                                    <span><b>{{ $data['progress'] }}</b></span>
                                                </td>
                                                <td>
                                                    <span>Conduct: </span>
                                                    <span><b>{{ $data['conduct'] }}</b></span>
                                                </td>
                                                    <td>
                                                    <span>Leaving Date: </span>
                                                    <span><b>{{ \Carbon\Carbon::parse($data['leaving_date'])->format('d/m/Y') }}</b></span>
                                                </td>
                                            </tr>
                                        
                                            <?php
                                                if($data['month'] == 'Regular Leaving'){
                                                $previousMonthNYear = date("Y");
                                                }else{
                                                    $previousMonthNYear = date("Y",strtotime("-1 year"));
                                                }
                                            ?>
                                            <tr>
                                                <td colspan="4">
                                                    <span>Studying in class & since when (numbers & in words): </span>
                                                    <span><b>{{ 'May '. $previousMonthNYear .' in class '. $leavingCertifData->classes->class_name}} @if($leavingCertifData->admission_in == 'college') science @endif</b></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <span>Reason of Leaving: </span>
                                                    <span><b>{{ $data['leaving_reason'] }}</b></span>
                                                </td>
                                            </tr>

                                            <?php
                                                $distinction_text = null;
                                                if(!empty($data['class']) && $data['class'] == 'DISTINCTION' && !empty($data['dictinction_text'])){
                                                    $distinction_text = ' DISTINCTION IN - '. $data['dictinction_text'];
                                                }elseif (!empty($data['class']) && !empty($data['dictinction_text'])) {
                                                    $distinction_text = 'IN '. $data['class'] . '  DIST. IN - '. $data['dictinction_text'];
                                                }elseif($data['class'] != '-'){
                                                    $distinction_text = 'IN '. $data['class'];
                                                }
                                            ?>

                                            <tr>
                                                <td colspan="4">
                                                    <span>Remark: </span>
                                                    <span><b>{{$regular_leaving}} {{$distinction_text }} </b></span>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                <p>Certified that the above information is in accordance with the School/College General Register No. {{ $leavingCertifData->gr_number }}</p>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="4">
                                                    <span>Date: </span>
                                                    <span><b>{{ date('d', strtotime($data['leaving_date'])) }}</b></span>

                                                    <span>Month: </span>
                                                    <span><b>{{ date('m', strtotime($data['leaving_date'])) }}</b></span>

                                                    <span>Year: </span>
                                                    <span><b>{{ date('Y', strtotime($data['leaving_date'])) }}</b></span>
                                                </td>
                                            </tr>

                                            <tr class="text-center" style="10px;">
                                                <td style="padding-top:20px">
                                                    <p><b>_______________</b></p>
                                                    <span>Class Teacher</span>
                                                </td>
                                                    <td style="padding-top:20px">
                                                    <p><b>_______________</b></p>
                                                    <span>Clerk</span>
                                                </td>
                                                <td style="padding-top:20px">
                                                    <p><b>_______________</b></p>
                                                    <span>Principal</span>
                                                </td>
                                            </tr>

                                            <tr class="text-center">
                                                <td colspan="4" style="padding-top:10px;border-bottom: 1.5px solid #000; font-size:10px;">
                                                    <p><b>Note: </b>Change in school/college leaving certificate libale to involve the imposition of penalty or such as that of restriction and penal offence</p>
                                                </td>
                                            </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            @can('leaving-certificate')  
                                <div class="col-sm-2" style="position: absolute;right: 0px;bottom: 0px;">
                                    <button type="submit" form="leavingCertifFormId" name="saveBtn" class="btn btn-sm btn-success printBtn" value="Submit">SAVE & NEXT</button>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered reporttable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>GR NO.</th>
                                <th>ADMISSION TYPE</th>
                                <th>RESULT</th>
                                <th>MONTH</th>
                                <th>YEAR</th>
                                <th>CLASS</th>
                                <th>STATUS</th>
                                @can('printleaving') 
                                    <th class="text-center">Action_List</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($allgenleaving))
                                @foreach($allgenleaving as $key => $leaving)
                                    <tr>
                                        <td>{{{ ++$key }}}</td>
                                        <td>{{ $leaving->gr_number }}</td>
                                        <td>{{ $leaving->admission_in }}</td>
                                        <td>{{ $leaving->result }}</td>
                                        <td>{{ $leaving->month }}</td>
                                        <td>{{ $leaving->year }}</td>
                                        <td>{{ $leaving->class }}</td>
                                        <td>{{ $leaving->status }}</td>
                                        @can('printleaving')
                                            <td class="text-center">
                                                <a href='{{ url("printleaving/".Crypt::encrypt($leaving->id)) }}' target="_blank">
                                                    <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                        <i class="icon-printer menu-icon"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection