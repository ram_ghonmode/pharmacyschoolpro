@extends('layouts.dash')   
@section('title', 'Attempt List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Attempt Certificate </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" action="{{ route('attempt-list') }}">
                        <div class="row clearfix">
                            <div class="col-sm-5">
                                <div class="demo-radio-button">
                                    <div class="radio-group">
                                        <input name="admission_in" type="radio" id="atcollegeId" value="college" class="with-gap radio-col-indigo" @if(!empty($data['admission_in']) && $data['admission_in'] == 'college') checked @endif />
                                        <label for="atcollegeId">College</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line" >
                                        <input type="text" name="gr_number" value="{{ !empty($data['gr_number']) ? $data['gr_number'] : old('gr_number') }}" style="border: none;" placeholder="G.R No." required  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-sm btn-info">Generate</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            @if(!empty($studentData))
                <form method="POST" action="{{ route('attempt-certificate') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="body"> 
                            @php
                            $instituteName = '';
                            $stamp ='';
                                if(!empty($data['admission_in'])){
                                    if($data['admission_in'] == 'school' || $data['admission_in'] == 'pre-primary'){
                                        $instituteName = $studentData->school->school_name;
                                        $stamp = "Head Master";
                                    }elseif($data['admission_in'] == 'college'){
                                        $instituteName = $studentData->school->school_name;
                                        $stamp = "Principal";
                                    }
                                }
                            @endphp    
                            @php
                                $genderinfo = '';
                                $geninfo = '';
                                    if($studentData['gender'] == 'Female'){
                                        $genderinfo = "She"; $geninfo = "her";
                                    }elseif($studentData['gender'] == 'Male'){
                                        $genderinfo ="He"; $geninfo = "his";
                                    }
                                
                            @endphp
                            @php
                                $month = '';
                                    if($leavingData['month'] == 'Summer'){
                                        $month = "Summer"; 
                                    }
                                    elseif($leavingData['month'] == 'Winter'){
                                        $month = "Winter"; 
                                    }elseif($leavingData['month'] == 'Regular Leaving'){
                                        $month = "Regular Leaving"; 
                                    }
                            @endphp
                            @php
                                $exam = '';
                                    if($leavingData['month'] == 'Summer'){
                                        $exam = "Secondary School Certificate Examination"; 
                                    }
                                    elseif($leavingData['month'] == 'Winter'){
                                        $exam = "Higher Secondary Certificate Examination"; 
                                    }elseif($leavingData['month'] == 'Regular Leaving'){
                                        $exam = "Final Exam"; 
                                    }
                            @endphp
                            @php
                                $grade = '';
                                    if($leavingData['class'] == 'DISTINCTION'){
                                        $grade = "A+"; 
                                    }elseif($leavingData['class'] == 'FIRST DIVISION'){
                                        $grade = "A"; 
                                    }elseif($leavingData['class'] == 'SECOND DIVISION'){
                                        $grade = "B"; 
                                    }elseif($leavingData['class'] == 'THIRD DIVISION'){
                                        $grade = "c"; 
                                    }
                            @endphp    
                        
                                                    
                            <div class="border-class">
                                <div class="heading-div">
                                    <h1 style="margin-bottom: 40px; margin-top: 20px;">{{ $instituteName }}</h1>
                                    <div class="float-div" style="margin-left: 300px;">
                                    <img src='{{ asset("public/images/logo.png") }}'>
                                    <h3 class="text-center" style="padding-top: 10px;"><u>ATTEMPT CERTIFICATE</u></h3>
                                </div>
                                    
                                    <input type="hidden" name="gr_number"  value="{{ !empty($studentData['gr_number']) ? $studentData['gr_number'] : old('gr_number') }}">
                                    <input type="hidden" name="admission_in"  value="{{ !empty($studentData['admission_in']) ? $studentData['admission_in'] : old('admission_in') }}">
                            </div>
                            
                            <!-- <div style="padding-top: 110px; text-align: center;">
                                <h3 class="text-center"><u>ATTEMPT CERTIFICATE</u></h3>
                            </div> -->

                            <div class="clear-both"></div>
                            <div class="header-div" style="padding: 30px  30px; ">
                                <p style="text-indent: 50px; text-align: justify;">This is to certify that, <b>{{ $studentData->first_name.' '.  $studentData->middle_name .' '. $studentData->last_name }}</b>
                                was a student of the college form the session <b>{{ date('d-m-Y',strtotime($studentData->admission_date)) }}</b> has the D-Pharm <b> {{$leavingData->result}}</b> <b>{{$month}} - {{$leavingData->year}} </b> <!-- <b>{{ $instituteName }}</b> from <b>{{ date('d-m-Y',strtotime($studentData->admission_date)) }}</b> -->.</p>
                                <!-- <p style="text-indent: 50px; text-align: justify;">{{ $genderinfo }} appeared for <b>{{ $exam }}</b> in <b>{{$month}}-{{$leavingData->year}}</b> and <b> {{$leavingData->result}}</b> the Examination in <b>April- {{$leavingData->year}} </b> at the <b>{{$leavingData->class}} </b> with grade <b>{{ $grade }}</b> and distinction in subjects <b>{{$leavingData->dictinction_text}}.</b></p>
                                <p style="text-align: justify;">{{ $genderinfo }} was a student of active habits, polite manners and hard working nature.</p> -->
                                <!-- <p style="text-align: justify;">{{ $genderinfo }} has always impressed me as an upright students for her keen-interest in extra curricular activities like _______________________________________</p> -->
                                <!-- <p style="text-align: justify;">To the best of my knowledge, {{ $genderinfo }} bears a good moral character.</p>
                                <p style="text-align: justify;">I wish {{ $geninfo }} very success in all walks of life.</p> -->
                            </div>
                                
                            <div class="footer-div" style="margin-top: 50px;">
                                <div class="float-div">
                                    <p>Place: {{$studentData->school->city->city_name}}</p>
                                    <p>Date: {{ date('d-m-Y') }}</p>
                                </div>
                                    
                                <div class="float-right">
                                    <p class="text-center">{{$stamp}}</p>
                                    <p>{{ $instituteName }}.</p>
                                </div>
                                <div class="clear-both"></div>
                            </div>
                            </div> 
                            @can('attempt-certificate')                  
                                <div class="clearfix">    
                                    <button type="submit" class="btn btn-sm btn-info waves-effect" style="float:right">GENERATE</button> 
                                </div>  
                            @endcan
                        </div>
                    </div>
                </form>    
            @endif
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered reporttable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>GR NO.</th>
                                <th>Admission Type</th>
                                <th>FULL NAME</th>
                                <th>ADMISSION DATE</th>
                                <th>DATE</th>
                                @can('printcharacter') 
                                    <th class="text-center">Action_List</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($AttemptCertificate))
                                @foreach($AttemptCertificate as $key => $attempt)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $attempt->gr_number }}</td>
                                        <td>{{ $attempt->admission_in }}</td>
                                        <td>{{ $attempt->fullname }}</td>
                                        <td>{{ date('d-m-Y',strtotime($attempt->admission_date)) }}</td>
                                        <td>{{ date('d-m-Y',strtotime($attempt->current_date)) }}</td>
                                        @can('printcharacter')
                                            <td class="text-center">
                                                <a href='{{ url("printattempt/".Crypt::encrypt($attempt->id)) }}' target="_blank" title="Print">
                                                    <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                        <i class="icon-printer menu-icon"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection