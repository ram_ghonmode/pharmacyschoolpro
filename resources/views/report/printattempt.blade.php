@extends('layouts.print')

@section('content')

<div class="container-fluid">
	
    <div class="row clearfix">
       @php
            $instituteName = '';
            $instname ='';
            $stamp = '';
            if($data['admission_in'] == 'School' || $data['admission_in'] == 'Pre-Primary'){
                $instituteName = $data->school->school_name;
                $instname ="school"; $stamp = "Head Master";
            }elseif($data['admission_in'] == 'college'){
                $instituteName = $data->school->school_name;
                $instname ="college"; $stamp = "Principal";
            }
         
        @endphp  
        
        @php
            $genderinfo = '';
            $geninfo = '';
                if($data['gender'] == 'Female'){
                    $genderinfo = "She"; $geninfo = "her";
                }elseif($data['gender'] == 'Male'){
                    $genderinfo ="He"; $geninfo = "his";
                }
        @endphp        
        
    
        @php
            $month = '';
                if($attemptData['month'] == 'Summer'){
                    $month = "Summer"; 
                }
                elseif($attemptData['month'] == 'Winter'){
                    $month = "Winter"; 
                }elseif($attemptData['month'] == 'Regular Leaving'){
                    $month = "Regular Leaving"; 
                }
        @endphp
        @php
            $exam = '';
                if($attemptData['month'] == 'Summer'){
                    $exam = "Secondary School Certificate Examination"; 
                }
                elseif($attemptData['month'] == 'Winter'){
                    $exam = "Higher Secondary Certificate Examination"; 
                }elseif($attemptData['month'] == 'Regular Leaving'){
                    $exam = "Final Exam"; 
                }
        @endphp
        @php
            $grade = '';
                if($attemptData['class'] == 'DISTINCTION'){
                    $grade = "A+"; 
                }elseif($attemptData['class'] == 'FIRST DIVISION'){
                    $grade = "A"; 
                }elseif($attemptData['class'] == 'SECOND DIVISION'){
                    $grade = "B"; 
                }elseif($attemptData['class'] == 'THIRD DIVISION'){
                    $grade = "c"; 
                }
        @endphp
                
         <?php
            // $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
            // $dobStr = \Carbon\Carbon::parse($attemptData->dob)->format('d/M/Y');
            // $dobArr = explode("/",$dobStr)
        ?> 
           
    <style>
        .float-div img{width: 100px; margin-left: auto; margin-right: auto;}
        .heading-div{text-align: center;margin-top: 0;}
        h1{text-align: center;margin-top: 0; font-size: 24px;}
        .border-class{border: 1px solid #000;padding: 25px 45px 25px 45px;;}
        .clear-both{clear: both;}
        .float-left{float: left;}
        .float-right{float: right;}
        .text-center{text-align: center;}
        p{margin:.5rem 0 .5rem 0;}
        .footer-div{margin-top: 60px; padding-left:30px}
        .header-div{padding-top: 20px;}
        .header-div p{margin:.5rem 0 .5rem 0; line-height:20px;}
    </style>                                      
            
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card" style="position: relative;">
                    <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top:100px;"> <canvas  class="can" id="demo" style="opacity: 0.03; height: 320px; width: 300px;"></canvas>
                    </div>
                    <div class="body"> 
                        <div class="border-class">
                           
                            <div class="heading-div">
                                <h1 style="margin-bottom: 40px; margin-top: 20px; line-height: 28px;">{{ $instituteName }}</h1>
                                <div class="float-div">
                                    <img src='{{ asset("public/images/logo.png") }}'>
                                </div>
                            </div>
                            <div class="image">
                            <div style="padding-top: 20px; text-align: center;"><h3><u>ATTEMPT CERTIFICATE</u></h3></div>
                            <div class="clear-both"></div>
                            <div class="header-div" style="padding:30px 30px;">
                                <p style="text-indent: 70px; text-align: justify;">This is to certify that, <b>{{ $attemptData->fullname }}</b>
                                 was a student of the college form the session <b>{{ $instituteName }}</b> from <b>{{ \Carbon\Carbon::parse($attemptData->admission_date)->format('d-m-Y') }}</b> has the D-Pharm pass the Examination in <b>{{$month}}-{{$attemptData->year}} </b>.</p>
                                <!-- <p style="text-indent: 70px; text-align: justify;">{{ $genderinfo }} appeared for <b>{{$exam}}</b> in <b>{{$month}}-{{$attemptData->year}}</b> and <b> {{$attemptData->result}}</b> the Examination in <b>April- {{$attemptData->year}} </b> at the <b>{{$attemptData->class}} </b> with grade <b>{{ $grade }}</b> and distinction in subjects <b>{{$attemptData->dictinction_text}}.</b></p>
                                <p style="text-align: justify;">{{ $genderinfo }} was a student of active habits, polite manners and hard working nature.</p>
                                <p style="text-align: justify;">{{ $genderinfo }} has always impressed me as an upright students for her keen-interest in extra curricular activities like ___________________________________</p>
                                <p style="text-align: justify;">To the best of my knowledge, {{ $genderinfo }} bears a good moral character.</p>
                                <p style="text-align: justify;">I wish {{ $geninfo }} very success in all walks of life.</p> -->
                            </div>
                            <div class="footer-div">
                                <div class="float-left">
                                    <p>Place: {{$data->school->city->city_name}}</p>
                                    <p>Date: {{ date('d-m-Y') }}</p>
                                </div>
                                    
                                <div class="float-right">
                                    <p class="text-center">Principal</p>
                                    <p style="padding-right:25px">{{ $data->school->school_name }}.</p>
                                </div>
                                <div class="clear-both"></div>
                            </div>
                        </div>   
                        </div>                
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
        // (B) IMAGES + CANVAS
    var iBack = new Image(),
        iMark = new Image(),
        iLoaded = 0,
        canvas = document.getElementById("demo"),
        ctx = canvas.getContext("2d");
     
    // (C) WATERMARK
    function cmark () { 
        iLoaded++; 
        if (iLoaded==2) {        
          // (C2) ADD WATERMARK
            canvas.width = iBack.naturalWidth;
            canvas.height = iBack.naturalHeight;
            ctx.drawImage(iMark, 0, 0, iMark.naturalWidth, iMark.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack.onload = cmark;
    iMark.onload = cmark;
    iBack.src = '{{ asset("public/images/logo.png") }}';
    iMark.src = '{{ asset("public/images/logo.png") }}';
/*for 2nd water mark*/
            // (B) IMAGES + CANVAS
    var iBack2 = new Image(),
        iMark2 = new Image(),
        iLoaded2 = 0,
        canvas2 = document.getElementById("demo2"),
        ctx2 = canvas2.getContext("2d");
     
    // (C) WATERMARK
    function cmark2 () { 
        iLoaded2++; 
        if (iLoaded2==2) {        
          // (C2) ADD WATERMARK
            canvas2.width = iBack2.naturalWidth;
            canvas2.height = iBack2.naturalHeight;
            ctx2.drawImage(iMark2, 0, 0, iMark2.naturalWidth, iMark2.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack2.onload = cmark2;
    iMark2.onload = cmark2;
    iBack2.src = '{{ asset("public/images/logo.png") }}';
    iMark2.src = '{{ asset("public/images/logo.png") }}';
        
</script> 

@endsection
                                

                            
                           


                              
                               
                        
                    


     
                                      
                            



   