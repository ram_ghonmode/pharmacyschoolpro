@extends('layouts.dash')   
@section('title', 'General Register')
@section('content')
    <style type="text/css">
        body{ font-family: arial;}
        h2{font-size: 22px;text-align: center;}
        p{font-size: 14px;}
        table{font-size: 12px;border-collapse: collapse; text-align: center;width: 100%;}
        table th,table td{padding:5px;height: 20px;}
        .uidno-table td{width: 20px;border-top: none;border-bottom: none; height: 30px;}
        .uidno-table td:first-child{border-left: none;}
        .uidno-table td:last-child{border-right: none;}
        .uidno-table{border: none;}
        .rotate-text{padding: 0;transform: rotate(-45deg);}
    </style>
    <div class="page-header">
        <h3 class="page-title"> General Register </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body" style="padding: 1.2rem 1.875rem;">
                    <form method="POST" action="{{ route('generalregister') }}">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <select name="school" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">School</option>
                                    @foreach($data['schools'] as $skey => $school)
                                        <option value="{{ $skey }}" @if(old('school', isset($inputData['school']) ? $inputData['school'] : '') == $skey) selected @endif >{{ $school }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('school'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('school') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-2">
                                <select name="academic_year" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Academic Year</option>
                                    @foreach($data['years'] as $ykey => $year)
                                        <option value="{{ $ykey }}" @if(old('academic_year', isset($inputData['academic_year']) ? $inputData['academic_year'] : '') == $ykey) selected @endif >{{ $year }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('academic_year'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('academic_year') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-5" style="margin-top: 15px;">
                                <div class="demo-radio-button">
                                    <div class="radio-group">
                                        <input name="admission_type" type="radio" id="atpreprimaryId" value="Pre-Primary" class="with-gap radio-col-indigo" @if(!empty($inputData['admission_type']) && $inputData['admission_type'] == 'Pre-Primary') checked @else checked @endif />
                                        <label for="atpreprimaryId">Pre-Primary</label>
                                    </div>
                                    <div class="radio-group">
                                        <input name="admission_type" type="radio" id="atschoolId" value="School" class="with-gap radio-col-indigo" @if(!empty($inputData['admission_type']) && $inputData['admission_type'] == 'School') checked @endif />
                                        <label for="atschoolId">School</label>
                                    </div>
                                    <div class="radio-group">
                                        <input name="admission_type" type="radio" id="atcollegeId" value="College" class="with-gap radio-col-indigo" @if(!empty($inputData['admission_type']) && $inputData['admission_type'] == 'College') checked @endif />
                                        <label for="atcollegeId">College</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1" style="margin-top: 15px;">
                                <button type="submit" class="btn btn-sm btn-info">Submit</button>
                            </div>
                            @can('printgeneralregister')
                                @if(!empty($allGenRegData))
                                    <div class="col-sm-2 text-right">
                                        <a href='{{ url("printgeneralregister/".$inputData["admission_type"]."/".$inputData["academic_year"]."/".$inputData["school"]) }}' target="_blank"><button type="button" class="btn btn-primary btn-sm" style="position: fixed; right: 5%; z-index: 100;margin-top:1%;">Print</button></a>
                                    </div>
                                @endif
                            @endcan
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div>
                        @php
                            $classNqty = [];    
                            $instituteName = '';
                            $academicYr = '';
                            $stamp = '';
                            if(!empty($inputData['admission_type'])){
                                if($inputData['admission_type'] == 'School' || $inputData['admission_type'] == 'Pre-Primary'){
                                    $instituteName = !empty($instName->school_name) ? $instName->school_name : "";
                                    $stamp ="Head Master";
                                }elseif($inputData['admission_type'] == 'College'){
                                    $instituteName = !empty($instName->school_name) ? $instName->school_name : "";
                                    $stamp ="Principal";
                                }
                            }
                            if(!empty($inputData['academic_year'])){
                                foreach($data['years'] as $ykey => $year){
                                    if($ykey == $inputData['academic_year']){
                                        $academicYr = $year;
                                    }
                                }
                            } 
                        @endphp
                        <h2>GENERAL REGISTER</h2>
                        <p>Name of the School <span style="min-width: 450px;border-bottom: 1px solid #000;display: inline-block;margin: 0 10px;">{{ $instituteName }}</span>For the Year : {{ !empty($academicYr) ? $academicYr : 2015 . ' - ' . 16 }}</p>
                        <p style="text-align: center;">[For Admission & Withdrawal Of Student] (In Chapter 11 Section 11) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; [Vide Rule 83-A.1. Under Chapter Chapter 11 Section 4 APPENDIX EIGHTEEN]</p>
                        <table border="1">
                            <thead>
                                <tr>
                                    <th>General <br> Register <br> No.</th>
                                    <th>Student Name Student- I.D. / U.I.D/</th>
                                    <th class="rotate-text">Nationality</th>
                                    <th class="rotate-text">Mother tongue</th>
                                    <th class="rotate-text">Religion</th>
                                    <th class="rotate-text">Caste</th>
                                    <th class="rotate-text">Sub Caste</th>
                                    <th>Place of <br> Birth</th>
                                    <th>Date of Birth <br>(In Figure <br>/ In Words)</th>
                                    <th>Last School <br>Attended <br>& Class</th>
                                    <th>Date of <br>Admission</th>
                                    <th>From which <br> Class and <br> when was <br> studied</th>
                                    <th>Progress <br> in the <br> study</th>
                                    <th class="rotate-text">Conduct</th>
                                    <th>Date & Sign <br> of Parent <br> from issue <br> the <br> certificate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @for ($i = 1; $i <= 15; $i++)
                                        <td>{{ $i }}</td>
                                    @endfor
                                </tr>
                                @if(!empty($allGenRegData))
                                    @foreach ($allGenRegData as $regisData)
                                        @php 
                                            if(array_key_exists($regisData->admitToClasses->class_name,$classNqty)){
                                                $classNqty[$regisData->admitToClasses->class_name] += 1;
                                            }else{
                                                $classNqty[$regisData->admitToClasses->class_name] = 1;
                                            }
                                        @endphp
                                        <div>
                                            <tr>
                                                <td>{{ $regisData->gr_number }}</td>
                                                <td>{{ $regisData->first_name .' '.  $regisData->middle_name .' '.  $regisData->last_name }}</td>
                                                <td rowspan="3">{{ $regisData->nationality }}</td>
                                                <td rowspan="3">{{ $regisData->mother_tounge }}</td>
                                                <td rowspan="3">{{ $regisData->religion }}</td>
                                                <td rowspan="3">{{ $regisData->caste }}</td>
                                                <td rowspan="3">{{ $regisData->category }}</td>
                                                <td rowspan="3">{{ $regisData->place_of_birth }}</td>
                                                <td>{{ \Carbon\Carbon::parse($regisData->dob)->format('d/m/Y') }}</td>
                                                <td rowspan="3">{{ $regisData->studentgeneralinfos->last_school_attended . ' / '. $regisData->studentgeneralinfos->leaving_class }}</td>
                                                <td>{{ date('d/m/Y',strtotime($regisData->admission_date)) }}</td>
                                                <td>{{ $regisData->admitToClasses->class_name }}</td>
                                                <td>{{ !empty($regisData->leavingcertificates->progress) ? $regisData->leavingcertificates->progress : null }}</td>
                                                <td>{{ !empty($regisData->leavingcertificates->conduct) ? $regisData->leavingcertificates->conduct : null }}</td>
                                                <!-- <td></td> -->
                                                <td rowspan="4"></td>
                                                <!-- <td></td> -->
                                            </tr>
                                            <?php
                                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                $dobStr = \Carbon\Carbon::parse($regisData->dob)->format('d/M/Y');
                                                $dobArr = explode("/",$dobStr);
                                                
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td rowspan="3" width="80px;">{{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Mother's <br> Name</td>
                                                <td>{{ $regisData->mother_name }}</td>
                                                <td></td>
                                                <td>{{ \Carbon\Carbon::parse($regisData->admission_date)->format('d/m/Y') }}</td>
                                                <td></td>
                                                <td></td>
                                                
                                            </tr>
                                            <tr class="uidno-tr">
                                                <td>Student <br> U.I.D No</td>
                                                <td colspan="2" style="padding: 0;">
                                                    <table border="1" class="uidno-table">
                                                        <td style="text-align:left;">{{ $regisData->student_id }}</td>
                                                    </table>
                                                </td>
                                                <td>Adhar <br>Card No.</td>
                                                <td colspan="4" style="padding: 0;">
                                                    <table border="1" class="uidno-table">
                                                        <td style="text-align:left;">{{ $regisData->studentbankinfos->aadhar_no }}</td>
                                                    </table>
                                                </td>
                                                <td>Reason of leaving <br> school</td>
                                                <td colspan="4" style="padding: 0;">
                                                    <table border="1" class="uidno-table">
                                                        <td style="text-align:left;">{{ !empty($regisData->leavingcertificates->leaving_reason) ? $regisData->leavingcertificates->leaving_reason : null }}</td>
                                                    </table>
                                                </td>
                                            </tr>
                                        </div>                                       
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive col-md-12">                       
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th width="10%">Sr.No</th>
                                        <th>Class Name</th>
                                        <th>Admission</th>                                           
                                    </tr>
                                </thead>
                                <tbody>   
                                @if(!empty($allGenRegData))
                                @php 
                                    ksort($classNqty);
                                @endphp
                                    @foreach($classNqty as $key => $value)  
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>                                          
                                            <td> {{ $key }} </td>
                                            <td>{{ $value }}</td>
                                        </tr>     
                                    @endforeach
                                @endif                                                                  
                                </tbody>     
                            </table>        
                        </div>
                    </div>
                </div>
                <div class="footer-div">
                    <div class="float-right">
                        <p class="text-center">{{$stamp}}</p>
                        <p>{{ $instituteName }}.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
