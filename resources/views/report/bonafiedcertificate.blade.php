@extends('layouts.dash')   
@section('title', 'Bonafide List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Bonafide Certificate </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" action="{{ route('bonafied-list') }}" id="bonafiedCertifFormId">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-sm-5">
                                <div class="demo-radio-button">
                                    <div class="radio-group">
                                        <input name="admission_in" type="radio" id="atcollegeId" value="college" class="with-gap radio-col-indigo" @if(!empty($data['admission_in']) && $data['admission_in'] == 'college') checked @endif />
                                        <label for="atcollegeId">College</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 m-b-0">
                                <div class="form-group form-float">
                                    <div class="form-line" >
                                        <input type="text" name="gr_number" value="{{ !empty($data['gr_number']) ? $data['gr_number'] : old('gr_number') }}" style="border: none;" placeholder="G.R No." required  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-sm btn-info">Generate</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            @if(!empty($bonafiedData))
                <form method="POST" action="{{ route('bonafiedcertificate') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            @php
                                $instituteName = '';
                                $instname ='';
                                $stamp = '';
                                if(!empty($data['admission_in'])){
                                    if($data['admission_in'] == 'school' || $data['admission_in'] == 'pre-primary'){
                                        $instituteName = $bonafiedData->school->school_name;
                                        $instname = "school"; $stamp = "Head Master";
                                    }elseif($data['admission_in'] == 'college'){
                                        $instituteName = $bonafiedData->school->school_name;
                                        $instname = "college"; $stamp = "Principal";
                                    }
                                }
                                $genderinfo = '';
                                $geninfo = '';
                                    if($bonafiedData['gender'] == 'Female'){
                                        $genderinfo = "She"; $geninfo = "her";
                                    }elseif($bonafiedData['gender'] == 'Male'){
                                        $genderinfo ="He"; $geninfo = "his";
                                    }
                                
                            @endphp                     
                            <div class="border-class">
                                <div class="float-div">
                                    <img src='{{ asset("public/images/logo.png") }}'>
                                </div>
                                <div class="square"></div>
                                <div class="heading-div">
                                    <h3>{{ $instituteName }}</h3>
                                    <h4><i><b><u>BONAFIDE CERTIFICATE</u></b></i></h4>
                                    <input type="hidden" name="gr_number"  value="{{ !empty($bonafiedData['gr_number']) ? $bonafiedData['gr_number'] : old('gr_number') }}">
                                    <input type="hidden" name="admission_in"  value="{{ !empty($bonafiedData['admission_in']) ? $bonafiedData['admission_in'] : old('admission_in') }}">
                                </div>
                                <div class="clear-both"></div>
                                <?php

                                    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                    $dobStr = \Carbon\Carbon::parse($bonafiedData->dob)->format('d/M/Y');
                                    $dobArr = explode("/",$dobStr);
                                
                                ?>
                                <div class="header-div" style="margin-top: 30px;">
                                    <p style="text-indent: 70px; text-align: justify;">This is to certify that, <b>{{ $bonafiedData->first_name.' '.  $bonafiedData->middle_name .' '. $bonafiedData->last_name }}</b>
                                    is/was a bonafide student of the {{$instname}} studying in class <b>{{ $bonafiedData->classes->class_name }},</b> of this college during the session <b>
                                        {{ $bonafiedData->academicYears->academic_year }}.</b></p> 
                                    <p style="text-indent: 70px; text-align: justify;">He/She bears a goos moral character.</p>
                                    <p style="text-indent: 70px; text-align: justify;">
                                     His/Her date of Birth is- <b>{{ \Carbon\Carbon::parse($bonafiedData->dob)->format('d/m/Y') }}</b> <b>({{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }} .)</b></p>
                                    <!-- <p style="text-indent: 70px; text-align: justify;">As per our {{$instname}} record, {{$geninfo}} Admission Register No. is <b>{{ $bonafiedData->gr_number }}</b> and {{$geninfo}} date of Birth is <b>{{ \Carbon\Carbon::parse($bonafiedData->dob)->format('d-m-Y') }}</b> in words- {{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }} .</p> -->
                                    <!-- <p style="text-indent: 70px; text-align: justify;">{{$genderinfo}} belongs to caste <b>{{ $bonafiedData->caste }}</b> under <b>{{ $bonafiedData->category }}</b> category and {{$geninfo}} Aadhar Card No.is <b>{{ $bonafiedData->studentbankinfos->aadhar_no }}</b> and Student ID is <b>{{$bonafiedData->student_id}}</b>.</p>
                                    <p style="text-indent: 70px; text-align: justify;">{{$genderinfo}} bears a good moral character.</p> -->
                                </div>

                                <div class="footer-div">
                                    <div class="float-div">
                                        <p>Date: {{ date('d/m/Y') }}</p>
                                        <p>Gr. No: {{ $bonafiedData->gr_number }}</p>
                                    </div>
                                    
                                    <div class="float-right">
                                        <p class="text-center">{{$stamp}}</p>
                                        <p><b style="padding-left: 145px">(Mrs Rupali H Tiple)</b></p>
                                        <p>{{ $instituteName }}.</p>
                                    </div>
                                    <div class="clear-both"></div>
                                </div>
                            </div> 
                            @can('bonafiedcertificate')                  
                                <div class="clearfix">    
                                    <button type="submit" class="btn btn-info btn-sm" style="float:right">Generate</button> 
                                </div>    
                            @endcan       
                        </div>
                    </div>
                </form>
            @endif
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered reporttable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>GR No.</th>
                                <!-- <th>Admission Type</th> -->
                                <th>Full Name</th>
                                <th>Academic Year</th>
                                <th>Date</th>
                                @can('printbonafied') 
                                    <th class="text-center">Action_List</th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($BonafiedCertificate))
                                @foreach($BonafiedCertificate as $key => $bonafied)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $bonafied->gr_number }}</td>
                                        <!-- <td>{{ $bonafied->admission_in ?? '' }}</td> -->
                                        <td>{{ $bonafied->fullname }}</td>
                                        <td>{{ $bonafied->current_year }}</td>
                                        <td>{{ date('d-m-Y',strtotime($bonafied->current_date)) }}</td>
                                        @can('printbonafied') 
                                            <td class="text-center">
                                                <a href='{{ url("printbonafied/".Crypt::encrypt($bonafied->id)) }}' target="_blank" title="Print">
                                                    <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                        <i class="icon-printer menu-icon"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection