@extends('layouts.print')

@section('content')
<style>
table td {
    padding: 2px 3px;
}
</style>
<?php 
    function numberTowords($num){
        $ones = array(
            0 =>"ZERO",
            1 => "ONE",
            2 => "TWO",
            3 => "THREE",
            4 => "FOUR",
            5 => "FIVE",
            6 => "SIX",
            7 => "SEVEN",
            8 => "EIGHT",
            9 => "NINE",
            10 => "TEN",
            11 => "ELEVEN",
            12 => "TWELVE",
            13 => "THIRTEEN",
            14 => "FOURTEEN",
            15 => "FIFTEEN",
            16 => "SIXTEEN",
            17 => "SEVENTEEN",
            18 => "EIGHTEEN",
            19 => "NINETEEN",
            "014" => "FOURTEEN"
        );
        $tens = array(
            0 => "ZERO",
            1 => "TEN",
            2 => "TWENTY",
            3 => "THIRTY",
            4 => "FORTY",
            5 => "FIFTY",
            6 => "SIXTY",
            7 => "SEVENTY",
            8 => "EIGHTY",
            9 => "NINETY"
        );
        $hundreds = array(
            "HUNDRED",
            "THOUSAND",
            "MILLION",
            "BILLION",
            "TRILLION",
            "QUARDRILLION"
        ); //limit t quadrillion
        $num = number_format($num,2,".",",");
        $num_arr = explode(".",$num);
        $wholenum = $num_arr[0];
        $decnum = $num_arr[1];
        $whole_arr = array_reverse(explode(",",$wholenum));
        krsort($whole_arr,1);
        $rettxt = "";
        foreach($whole_arr as $key => $i){
            while(substr($i,0,1)=="0")
            $i=substr($i,1,5);
            if(!empty($i) && $i < 20){
                //echo "getting:".$i;
                $rettxt .= $ones[$i];
            }elseif(!empty($i) && $i < 100){
                if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
                if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
            }else{
                if(!empty($i) && substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
                if(!empty($i) && substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
                if(!empty($i) && substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
            }
            if($key > 0){
                $rettxt .= " ".$hundreds[$key]." ";
            }
        }
        if($decnum > 0){
            $rettxt .= " and ";
            if($decnum < 20){
                $rettxt .= $ones[$decnum];
            }elseif($decnum < 100){
                $rettxt .= $tens[substr($decnum,0,1)];
                $rettxt .= " ".$ones[substr($decnum,1,1)];
            }
        }
        return $rettxt;
    }
?>
 <?php
    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
    $dobStr = \Carbon\Carbon::parse($leavingData->admission->dob)->format('d/m/Y');
    $dobArr = explode("/",$dobStr);

    $year = $dobArr[2];
    $month = $dobArr[1];
    $day  = $dobArr[0];
    $birth_day = numberTowords($day);
    $birth_year = numberTowords($year);
    $monthNum = $month;
    $dateObj = DateTime::createFromFormat('!m', $monthNum);//Convert the number into month name
    $monthName = strtoupper($dateObj->format('F'));
?>
<?php
    $distinction_text = null;
    if($leavingData->class == 'DISTINCTION' && !empty($leavingData->dictinction_text)){
        $distinction_text = '  DISTINCTION IN - '. $leavingData->dictinction_text;
    }elseif (!empty($leavingData->class) && !empty($leavingData->dictinction_text)) {
        $distinction_text = 'IN '. $leavingData->class . '  DIST. IN - '. $leavingData->dictinction_text;
    }elseif(!empty($leavingData->class)){
        $distinction_text = 'IN '. $leavingData->class;
    }
?>
<?php
    if($leavingData->month == 'Regular Leaving'){
    $previousMonthNYear = date("Y");
    }else{
        $previousMonthNYear = date("Y",strtotime("-1 year"));
    }
?>

@if($leavingData->admission->current_class == "KG1" || $leavingData->admission->current_class == "KG2" || $leavingData->admission->current_class == "Nursery")
    <?php
        $schoolOrCollege = 'Collage';
        $schOrClgName = $leavingData->admission->school->school_name;
        $udiseno = $leavingData->admission->school->udise_no;
        $indexno = $leavingData->admission->school->index_no;
        $recognitionNo = $leavingData->admission->school->recognition_no;
        $classArray = []; $k = 1;  $regular_leaving = null;
        foreach ($classList as $key => $value) {
            $classArray[$k] = $value;
            ++$k;
        }
        $arrKey = array_search($leavingData->admission->current_class, $classArray);
        $cls = $classArray[--$arrKey];
    ?>
    @if($leavingData->admission->current_class != $leavingData->admission->admit_to)
        <?php
            if($leavingData['month'] == 'Regular Leaving'){
                $regular_leaving = ' Std '. $leavingData['result'].' Regular Examination '.$leavingData['year'];
            }else{
            $regular_leaving =$leavingData['result'] .'  '. $leavingData['month'] .'  '. $leavingData['year'];
            }
        ?>
    @endif    
    @else
    <?php $curClass = substr($leavingData['admission']['current_class'], 0, -2);?>
    @if($curClass <= 10)
        <?php
            $schoolOrCollege = 'College';
            $schOrClgName = !empty($leavingData['admission']['school']['school_name']) ? $leavingData['admission']['school']['school_name'] : '';
            $udiseno = !empty($leavingData['admission']['school']['udise_no']) ? $leavingData['admission']['school']['udise_no'] : '';
            $indexno = !empty($leavingData['admission']['school']['index_no']) ? $leavingData['admission']['school']['index_no'] : '';
            $recognitionNo = !empty($leavingData['admission']['school']['recognition_no']) ? $leavingData['admission']['school']['recognition_no'] : '';
        ?>
    @else
        <?php
            $schoolOrCollege = 'College';
            $schOrClgName = !empty($leavingData['admission']['school']['school_name']) ? $leavingData['admission']['school']['school_name'] : '';
            $udiseno = !empty($leavingData['admission']['school']['udise_no']) ? $leavingData['admission']['school']['udise_no'] : '';
            $indexno = !empty($leavingData['admission']['school']['index_no']) ? $leavingData['admission']['school']['index_no'] : '';
            $recognitionNo = !empty($leavingData['admission']['school']['recognition_no']) ? $leavingData['admission']['school']['recognition_no'] : '';
        ?>
    @endif
        <?php
            $classArray = []; $k = 1;
            foreach ($classList as $key => $value) {
                $classArray[$k] = $value;
                ++$k;
            }
            $arrKey = array_search($leavingData['admission']['current_class'], $classArray);
            // $cls = $classArray[--$arrKey];
            $regular_leaving = null;
        ?>
        @if($leavingData['admission']['current_class'] != $leavingData['admission']['admit_to'])
            <?php
                if($leavingData['month'] == 'Regular Leaving'){
                    $regular_leaving = ' Std '. $leavingData['result'].' Regular Examination '.$leavingData['year'];
                }else{
                $regular_leaving = $leavingData['result'] .'  '. $leavingData['month'] .'  '. $leavingData['year'];
                }
            ?>
        @endif   
    @endif

    <?php 
        $aadhar = str_split($leavingData['admission']['studentbankinfos']['aadhar_no']);
    ?>
<div class="row" style="page-break-after: always;position: relative;">
    <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top: 500px;"> <canvas  class="can" id="demo" style="opacity: 0.1;"></canvas></div>
        <div class="col-sm-12" style="padding:0 35px; font-size: 40px;line-height: 35px;">
            <div class="image">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td width="300">
                                        <span>Name of the Management: </span>
                                    </td>
                                    <td align="left" width="600" >
                                        <span><b>{{ $leavingData['admission']['school']['institute_name']}}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <span>{{ $schoolOrCollege }} Name: </span>
                                    </td>
                                    <td  align="left" style="font-size: 20px;">
                                        <span><b>{{ $leavingData['admission']['school']['school_name'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <span>Address: </span>
                                    </td>
                                    <td  align="left" >
                                        <span><b>{{ $leavingData['admission']['school']['address'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="padding-left" >
                                        <span>Phone No: </span>
                                        <span><b>{{ $leavingData['admission']['school']['phone_no'] }}</b></span>
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <span>Email id: </span>
                                        <span><b>{{ $leavingData['admission']['school']['email'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <span>Serial No: </span>
                                        <span><b>{{ $leavingData->serial_no }}</b></span>
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <span>General Register No: </span>
                                        <span><b>{{ $leavingData->gr_number }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td >
                                        <span>Collage Recognition No.: </span>
                                        <span><b>{{ $leavingData['admission']['school']['recognition_no'] }}</b></span>
                                    </td>
                                    <td style="padding-left: 5px;">
                                        <span>Medium:</span>
                                        <span><b>{{ $leavingData['admission']['mediumstream']['name'] }}</b></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" class="" style="padding-top: 0px;" width="150px">
                            <img src='{{ asset("public/images/logo.png") }}' class="img-responsive pull-right" width="115px" style="padding: 0 10px;">
                        </td>
                    </tr>
                </table>
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr class="">
                        <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="width: 29.5%;">
                                        <span>UDISE No: </span>
                                        <span><b>{{ $udiseno }}</b></span>
                                    </td>
                                    <td>
                                        <span>Board: </span>
                                        <span><b>{{ $leavingData['admission']['school']['board'] }}</b></span>
                                    </td>
                                    <td>
                                        </span>{{ $schoolOrCollege }} Index No.: </span>
                                        <span><b>{{ $indexno }}</b></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-top:1px solid #000;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="text-center" colspan="4">
                                    <span style="font-size:20px;"><b>{{ $schoolOrCollege }} Leaving Certificate</b></span>
                                    <span  style="font-size: 13px;">( Govt. Recognised )</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="border-top:1px solid #000;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td >
                                        <span>Student I.D No.: </span>
                                    </td>
                                    <td >
                                        <span><b>{{ $leavingData['admission']['student_id']}}</b></span>
                                    </td>
                                </tr>
                                <tr >
                                    <td>
                                        <span>U. I.D No.(Adhar Card No.): </span>
                                    </td>
                                    <td >
                                        <table border="1" width="250" height="15">
                                            <tr>
                                                @if(count($aadhar) >= 12)
                                                    @foreach($aadhar as $key => $num)
                                                        <td>{{ $num }}</td>
                                                    @endforeach
                                                @else
                                                    <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="border-div">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td colspan="5">
                                    <span>Student First Name (Name): </span>
                                    <span><b>{{ $leavingData['admission']['first_name'] }}</b></span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <span>(Father Name): </span>
                                        <span><b>{{ $leavingData['admission']['guardianinfos']['father_first_name'] }}</b>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <span>(Surname): </span>
                                        <span><b>{{ $leavingData['admission']['guardianinfos']['father_last_name'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <span>Mother Name: </span>
                                        <span><b>{{ $leavingData['admission']['mother_name'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <span>Nationality: </span>
                                        <span><b>{{ $leavingData['admission']['nationality'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <span>Mother Tongue: </span>
                                        <span><b>{{ $leavingData['admission']['mother_tounge'] }} </b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>Religion: </span>
                                        <span><b>{{ $leavingData['admission']['religion'] }}</b></span>
                                    </td>
                                    <td>
                                        <span>Caste: </span>
                                        <span><b>{{ $leavingData['admission']['caste'] }}</b></span>
                                    </td>
                                    <td>
                                        <span>Sub Caste: </span>
                                        <span><b></b></span>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <span>Place of Birth (Village / City): </span>
                                        <span><b>{{ $leavingData['admission']['place_of_birth'] }}</b></span>
                                    </td>
                                    <td colspan="2">
                                        <span>Taluka: </span>
                                        <span><b>{{ $leavingData['admission']['guardianinfos']['tahsil']}}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>District: </span>
                                        <span><b>{{ $leavingData['admission']['guardianinfos']['district'] }}</b></span>
                                    </td>
                                    <td>
                                        <span>State: </span>
                                        <span><b>{{ $leavingData['admission']['guardianinfos']['state'] }}</b></span>
                                    </td>
                                    <td>
                                        <span>Country: </span>
                                        <span><b>{{ $leavingData['admission']['guardianinfos']['country'] }}</b></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <span>Date of Birth according to christain era: </span>
                                        <tr>
                                            <td></td>
                                            <td colspan="3">
                                                <span>In Figure: </span>
                                                <span><b>{{ $dobStr }}</b></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="3">
                                                <span>In Words: </span>
                                                <span><b>{{ $birth_day .' '. $monthName .' '. $birth_year }}</b></span>
                                            </td>
                                        </tr>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <span>Last school/Collage attended: </span>
                                        @if(!empty($leavingData['admission']['studentgeneralinfos']['last_school_attended']))
                                            <span><b>{{ $leavingData['admission']['studentgeneralinfos']['last_school_attended'] }} - std {{ $leavingData['admission']['studentgeneralinfos']['leaving_class'] }}</b></span>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span>Date of Admission in the Collage: </span>
                            <span><b>{{ \Carbon\Carbon::parse($leavingData['admission']['admission_date'])->format('d/m/Y') }}</b></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span>In which Class: </span>
                            <span><b>{{ $leavingData['admission']['admitToClasses']['class_name'] }}</b></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span>Progress in Studies: </span>
                            <span><b>{{ $leavingData['progress'] }}</b></span>
                        </td>
                        <td colspan="2">
                            <span>All over Behavior/Conduct: </span>
                            <span><b>{{ $leavingData['conduct'] }}</b></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span>Date of Leaving Collage: </span>
                            <span><b>{{ \Carbon\Carbon::parse($leavingData->leaving_date)->format('d/m/Y') }}</b></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <span>Standard in which studying and since when: </span>
                            <span><b>{{ 'May '. $previousMonthNYear .' in class '. $leavingData['admission']['current_class'] }} @if($leavingData['admission']['admission_in'] == 'College') science @endif </b></span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                        <span>Reason for Leaving Collage: </span>
                        <span><b>{{ $leavingData->leaving_reason }}</b></span>
                        </td>
                    </tr>
                        
                    <tr style="border-bottom: 1px solid #000;">
                        <td colspan="4">
                        <span>Remarks: </span>
                        <span><b>{{$regular_leaving}} {{$distinction_text }}</b></span>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                        <p>Above Collage Certificate is have by issuing as per the information given and registered in College General Register No. {{ $leavingData->gr_number }}</p>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                        <span>Date: </span>
                        <span><b>{{ date('d', strtotime($leavingData->leaving_date)) }}</b></span>

                        <span>Month: </span>
                        <span><b>{{ date('m', strtotime($leavingData->leaving_date)) }}</b></span>

                        <span>Year: </span>
                        <span><b>{{ date('Y', strtotime($leavingData->leaving_date)) }}</b></span>
                        </td>
                    </tr>

                    <tr class="text-center">
                        <td width="33%" style="text-align: left;">
                            <p><b>_______________</b></p>
                            <span style="padding-left: 10px;">Class Teacher</span>
                        </td>
                        <td width="33%" style="text-align: center;">
                            <p><b>_______________</b></p>
                            <span>Clerk</span>
                        </td>
                        <td width="33%" style="padding-top:0px; text-align: right;">
                            <p style="text-align: center; padding-left: 60%;"><b>_______________</b></p>
                            <span>Signature of the Head Mistress</span><br>
                            <span style="padding-right: 65px;">(Seal)</span>
                        </td>
                    </tr>

                    <tr class="text-center">
                        <td colspan="4" style="padding-top:0px; font-size:12px;">
                        <p><b>Note: </b>The legal action has been initialed, if any unauthorized change/ Changer made in the Collage Leaving Certificate.</p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

<!-- 2nd page of certificate -->
<div class="row" >
    <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top: 500px;"> <canvas  class="can" id="demo2" style="opacity: 0.1; z-index: 1;"></canvas></div>
    <div class="col-sm-12" style="padding:0 35px; font-size: 40px;line-height: 35px;">
        <div class="image">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="300">
                                    <span>Name of the Management: </span>
                                </td>
                                <td align="left" width="600" >
                                    <span><b>{{ $leavingData['admission']['school']['institute_name']}}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span>{{ $schoolOrCollege }} Name: </span>
                                </td>
                                <td  align="left" style="font-size: 20px;">
                                    <span><b>{{ $leavingData['admission']['school']['school_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span>Address: </span>
                                </td>
                                <td  align="left" >
                                    <span><b>{{ $leavingData['admission']['school']['address'] }}</b></span>
                                </td>
                            </tr>
                             <tr>
                                <td  class="padding-left" >
                                    <span>Phone No: </span>
                                    <span><b>{{ $leavingData['admission']['school']['phone_no'] }}</b></span>
                                </td>
                                <td style="padding-left: 5px;">
                                    <span>Email id: </span>
                                    <span><b>{{ $leavingData['admission']['school']['email'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span>Serial No: </span>
                                    <span><b>{{ $leavingData->serial_no }}</b></span>
                                </td>
                                <td style="padding-left: 5px;">
                                    <span>General Register No: </span>
                                    <span><b>{{ $leavingData->gr_number }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <span>Collage Recognition No.: </span>
                                    <span><b>{{ $leavingData['admission']['school']['recognition_no'] }}</b></span>
                                </td>
                                <td style="padding-left: 5px;">
                                    <span>Medium:</span>
                                    <span><b>{{ $leavingData['admission']['mediumstream']['name'] }}</b></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" class="" style="padding-top: 0px;" width="150px">
                        <img src='{{ asset("public/images/logo.png") }}' class="img-responsive pull-right" width="115px" style="padding: 0 10px;">
                    </td>
                </tr>
            </table>
            <table border="0" cellspacing="0" cellpadding="0" width="100%">
                <tr class="">
                    <td colspan="4">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="width: 29.5%;">
                                    <span>UDISE No: </span>
                                    <span><b>{{ $udiseno }}</b></span>
                                </td>
                                <td>
                                    <span>Board: </span>
                                    <span><b>{{ $leavingData['admission']['school']['board'] }}</b></span>
                                </td>
                                <td>
                                    <span>{{ $schoolOrCollege }} Index No.: </span>
                                    <span><b>{{ $indexno }}</b></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="border-top:1px solid #000;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="text-center" colspan="4">
                                <span style="font-size:20px;"><b>{{ $schoolOrCollege }} Leaving Certificate</b></span>
                                <!-- <span style="font-size:10px;"> ({{ $leavingData->status }})</span> -->
                                <span  style="font-size: 13px;">( Govt. Recognised )</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="border-top:1px solid #000;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td >
                                    <span>Student I.D No.: </span>
                                </td>
                                <td >
                                    <span><b>{{ $leavingData['admission']['student_id'] }}</b></span>
                                </td>
                            </tr>
                            <tr >
                                <td>
                                    <span>U. I.D No.(Adhar Card No.): </span>
                                </td>
                                <td >
                                    <table border="1" width="250" height="15">
                                        <tr>
                                            @if(count($aadhar) >= 12)
                                                @foreach($aadhar as $key => $num)
                                                    <td>{{ $num }}</td>
                                                @endforeach
                                            @else
                                                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                            @endif
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="border-div">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="5">
                                <span>Student First Name (Name): </span>
                                <span><b>{{ $leavingData['admission']['first_name'] }}</b></span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                    <span>(Father Name): </span>
                                    <span><b>{{ $leavingData['admission']['guardianinfos']['father_first_name'] }}</b>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <span>(Surname): </span>
                                    <span><b>{{ $leavingData['admission']['guardianinfos']['father_last_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <span>Mother Name: </span>
                                    <span><b>{{ $leavingData['admission']['mother_name'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <span>Nationality: </span>
                                    <span><b>{{ $leavingData['admission']['nationality'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <span>Mother Tongue: </span>
                                    <span><b>{{ $leavingData['admission']['mother_tounge'] }} </b></span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <span>Religion: </span>
                                    <span><b>{{ $leavingData['admission']['religion'] }}</b></span>
                                </td>
                                <td>
                                    <span>Caste: </span>
                                    <span><b>{{ $leavingData['admission']['caste'] }}</b></span>
                                </td>
                                <td>
                                    <span>Sub Caste: </span>
                                    <span><b></b></span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <span>Place of Birth (Village / City): </span>
                                    <span><b>{{ $leavingData['admission']['place_of_birth'] }}</b></span>
                                </td>
                                <td colspan="2">
                                    <span>Taluka: </span>
                                    <span><b>{{ $leavingData['admission']['guardianinfos']['tahsil'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span>District: </span>
                                    <span><b>{{ $leavingData['admission']['guardianinfos']['district'] }}</b></span>
                                </td>
                                <td>
                                    <span>State: </span>
                                    <span><b>{{ $leavingData['admission']['guardianinfos']['state'] }}</b></span>
                                </td>
                                <td>
                                    <span>Country: </span>
                                    <span><b>{{ $leavingData['admission']['guardianinfos']['country'] }}</b></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <span>Date of Birth according to christain era: </span>
                                    <tr>
                                        <td></td>
                                        <td colspan="3">
                                            <span>In Figure: </span>
                                            <span><b>{{ $dobStr }}</b></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="3">
                                            <span>In Words: </span>
                                            <span><b>{{ $birth_day .' '. $monthName .' '. $birth_year }}</b></span>
                                        </td>
                                    </tr>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <span>Last school/Collage attended: </span>
                                    @if(!empty($leavingData['admission']['studentgeneralinfos']['last_school_attended']))
                                        <span><b>{{ $leavingData['admission']['studentgeneralinfos']['last_school_attended'] }} - std {{ $leavingData['admission']['studentgeneralinfos']['leaving_class'] }}</b></span>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span>Date of Admission in the Collage: </span>
                        <span><b>{{ \Carbon\Carbon::parse($leavingData['admission']['admission_date'])->format('d/m/Y') }}</b></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span>In which Class: </span>
                        <span><b>{{ $leavingData['admission']['admitToClasses']['class_name'] }}</b></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <span>Progress in Studies: </span>
                        <span><b>{{ $leavingData['progress'] }}</b></span>
                    </td>
                    <td colspan="2">
                        <span>All over Behavior/Conduct: </span>
                        <span><b>{{ $leavingData['conduct'] }}</b></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span>Date of Leaving Collage: </span>
                        <span><b>{{ \Carbon\Carbon::parse($leavingData['leaving_date'])->format('d/m/Y') }}</b></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span>Standard in which studying and since when: </span>
                        <span><b>{{ 'May '. $previousMonthNYear .' in class '. $leavingData['admission']['current_class'] }} @if($leavingData['admission']['admission_in'] == 'College') science @endif </b></span>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                    <span>Reason for Leaving Collage: </span>
                    <span><b>{{ $leavingData->leaving_reason }}</b></span>
                    </td>
                </tr>
                    
                <tr style="border-bottom: 1px solid #000;">
                    <td colspan="4">
                    <span>Remarks: </span>
                    <span><b>{{$regular_leaving}} {{$distinction_text }}</b></span>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                    <p>Above Collage Certificate is have by issuing as per the information given and registered in College General Register No. {{ $leavingData->gr_number }}</p>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                    <span>Date: </span>
                    <span><b>{{ date('d', strtotime($leavingData->leaving_date)) }}</b></span>

                    <span>Month: </span>
                    <span><b>{{ date('m', strtotime($leavingData->leaving_date)) }}</b></span>

                    <span>Year: </span>
                    <span><b>{{ date('Y', strtotime($leavingData->leaving_date)) }}</b></span>
                    </td>
                </tr>

                <tr class="text-center">
                    <td width="33%" style="text-align: left;">
                        <p><b>_______________</b></p>
                        <span style="padding-left: 10px;">Class Teacher</span>
                    </td>
                    <td width="33%" style="text-align: center;">
                        <p><b>_______________</b></p>
                        <span>Clerk</span>
                    </td>
                    <td width="33%" style="padding-top:0px; text-align: right;">
                        <p style="text-align: center; padding-left: 60%;"><b>_______________</b></p>
                        <span>Signature of the Head Mistress</span><br>
                        <span style="padding-right: 65px;">(Seal)</span>
                    </td>
                </tr>

                <tr class="text-center">
                    <td colspan="4" style="padding-top:10px; font-size:12px;">
                    <p><b>Note: </b>The legal action has been initialed, if any unauthorized change/ Changer made in the Collage Leaving Certificate.</p>
                    </td>
                </tr>
            </table>
        </div>
        
    </div>
</div>
<script>
        // (B) IMAGES + CANVAS
    var iBack = new Image(),
        iMark = new Image(),
        iLoaded = 0,
        canvas = document.getElementById("demo"),
        ctx = canvas.getContext("2d");
     
    // (C) WATERMARK
    function cmark () { 
        iLoaded++; 
        if (iLoaded==2) {        
          // (C2) ADD WATERMARK
            canvas.width = iBack.naturalWidth;
            canvas.height = iBack.naturalHeight;
            ctx.drawImage(iMark, 0, 0, iMark.naturalWidth, iMark.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack.onload = cmark;
    iMark.onload = cmark;
    iBack.src = '{{ asset("public/images/logo.png") }}';
    iMark.src = '{{ asset("public/images/logo.png") }}';
/*for 2nd water mark*/
            // (B) IMAGES + CANVAS
    var iBack2 = new Image(),
        iMark2 = new Image(),
        iLoaded2 = 0,
        canvas2 = document.getElementById("demo2"),
        ctx2 = canvas2.getContext("2d");
     
    // (C) WATERMARK
    function cmark2 () { 
        iLoaded2++; 
        if (iLoaded2==2) {        
          // (C2) ADD WATERMARK
            canvas2.width = iBack2.naturalWidth;
            canvas2.height = iBack2.naturalHeight;
            ctx2.drawImage(iMark2, 0, 0, iMark2.naturalWidth, iMark2.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack2.onload = cmark2;
    iMark2.onload = cmark2;
    iBack2.src = '{{ asset("public/images/logo.png") }}';
    iMark2.src = '{{ asset("public/images/logo.png") }}';
        
</script>        
@endsection

