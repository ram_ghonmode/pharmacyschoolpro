@extends('layouts.print')

@section('content')

<div class="container-fluid">
	
    <div class="row clearfix">
        @php
            $instituteName = '';
            $instname ='';
            $stamp = '';
                if($data['admission_in'] == 'School' || $data['admission_in'] == 'Pre-Primary'){
                    $instituteName = $data->school->school_name;
                    $instname ="school"; $stamp ="Head Master";
                }elseif($data['admission_in'] == 'College'){
                    $instituteName = $data->school->school_name;
                    $instname ="college"; $stamp ="Principal";
                }
        @endphp  
         
        @php
            $genderinfo = '';
            $geninfo = '';
                if($data['gender'] == 'Female'){
                    $genderinfo = "she"; $geninfo = "her";
                }elseif($data['gender'] == 'Male'){
                    $genderinfo ="he"; $geninfo = "his";
                }
        @endphp        
            
        <?php
            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
            $dobStr = \Carbon\Carbon::parse($characterData->dob)->format('d/M/Y');
            $dobArr = explode("/",$dobStr);
        ?> 
           
        <style>
            .float-div img{width: 100px; margin-left: auto; margin-right: auto;display: block;}
            .heading-div{text-align: center;margin-top: 0;}
            h1{text-align: center;margin-top: 0; font-size: 24px;}
            .border-class{border: 1px solid #000;padding: 25px 45px 25px 45px; }
            .clear-both{clear: both;}
            .float-left{float: left;}
            .float-right{float: right;}
            .text-center{text-align: center;}
            p{margin:.5rem 0 .5rem 0;}
            .footer-div{margin-top: 70px; padding-left:30px}
            .header-div{padding-top: 20px;}
            .header-div p{margin:.5rem 0 .5rem 0; line-height: 26px;}
        </style> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card" style="position: relative;">
                    <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top:200px;"> <canvas  class="can" id="demo" style="opacity: 0.03; height: 320px; width: 300px;"></canvas>
                    </div>
                    <div class="body"> 
                        <div class="border-class">
                            <div class="heading-div">
                                <h1 style="margin-bottom: 50px; margin-top: 20px;line-height: 28px;">{{ $instituteName }}</h1>
                                <div class="float-div">
                                    <img src='{{ asset("public/images/logo.png") }}'>
                                </div>
                                <input type="hidden" name="gr_number"  value="{{ !empty($characterData['gr_number']) ? $characterData['gr_number'] : old('gr_number') }}">
                            </div>
                            <div class="image">
                           
                            <div style="padding-top: 50px; text-align: center;"><h3><u>CHARACTER CERTIFICATE</u></h3></div>
                            <div class="clear-both"></div>
                            <div class="header-div" style="padding:30px 30px;">
                                <p style="text-indent: 70px; text-align: justify;">This is to certify that, <b>{{ $characterData->fullname }}</b> was a student of this {{$instname}}, from<b> {{ date('d-m-Y',strtotime($characterData->admission_date)) }}</b> to <b>{{ date('d-m-Y',strtotime($characterData->last_date)) }},</b> and was studing in standard <b>{{ !empty($data->classes->class_name) ? $data->classes->class_name : '' }} </b>.</p>
                                <p style="text-align: justify;">As per our {{$instname}} record register {{$geninfo}} date of birth is <b>{{ date('d-m-Y',strtotime($characterData->dob)) }}</b> (in words) {{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }}.</p><br>
                                <!-- <p style="text-align: justify;">So far as known to us, {{$genderinfo}} bears a good moral character and conduct.</p> -->
                                <p style="text-align: justify;">As far as my knowledge is concerned he/she posseses a good moral charcter. I wish him every success in his/her future life.</p>
                            </div>
                            <div class="footer-div">
                                <div class="float-left">
                                    <p>Place: {{ $data->school->city->city_name }}</p>
                                    <p>Date: {{ date('d-m-Y') }}</p>
                                </div>
                                <div class="float-right">
                                    <p class="text-center">{{$stamp}}</p>
                                    <p style="padding-right:25px">{{ $instituteName }}.</p>
                               </div>
                               <div class="clear-both"></div>
                            </div>
                        </div>
                       </div>                  
                   </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // (B) IMAGES + CANVAS
    var iBack = new Image(),
        iMark = new Image(),
        iLoaded = 0,
        canvas = document.getElementById("demo"),
        ctx = canvas.getContext("2d");
     
    // (C) WATERMARK
    function cmark () { 
        iLoaded++; 
        if (iLoaded==2) {        
          // (C2) ADD WATERMARK
            canvas.width = iBack.naturalWidth;
            canvas.height = iBack.naturalHeight;
            ctx.drawImage(iMark, 0, 0, iMark.naturalWidth, iMark.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack.onload = cmark;
    iMark.onload = cmark;
    iBack.src = '{{ asset("public/images/logo.png") }}';
    iMark.src = '{{ asset("public/images/logo.png") }}';
/*for 2nd water mark*/
            // (B) IMAGES + CANVAS
    var iBack2 = new Image(),
        iMark2 = new Image(),
        iLoaded2 = 0,
        canvas2 = document.getElementById("demo2"),
        ctx2 = canvas2.getContext("2d");
     
    // (C) WATERMARK
    function cmark2 () { 
        iLoaded2++; 
        if (iLoaded2==2) {        
          // (C2) ADD WATERMARK
            canvas2.width = iBack2.naturalWidth;
            canvas2.height = iBack2.naturalHeight;
            ctx2.drawImage(iMark2, 0, 0, iMark2.naturalWidth, iMark2.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack2.onload = cmark2;
    iMark2.onload = cmark2;
    iBack2.src = '{{ asset("public/images/logo.png") }}';
    iMark2.src = '{{ asset("public/images/logo.png") }}';
        
</script>                                  
@endsection

                                    
    
                            


                            
                           
                                

                              
                               
        
                                 
       
                        
                        
                       
   
        
            
