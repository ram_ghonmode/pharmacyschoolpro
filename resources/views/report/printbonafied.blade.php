@extends('layouts.print')
@section('content')

    <div class="container-fluid">
        @if(!empty($bonafiedData))
            @php
                $instituteName = '';
                $instname ='';
                $stamp = '';
                if(!empty($data['admission_in'])){
                    if($data['admission_in'] == 'School' || $data['admission_in'] == 'Pre-Primary'){
                        $instituteName = $data->school->school_name;
                        $instname = "School"; $stamp = "Head Master";
                    }elseif($data['admission_in'] == 'College'){
                        $instituteName = $data->school->school_name;
                        $instname = "college"; $stamp = "Principal";
                    }
                }
                $genderinfo = '';
                $geninfo = '';
                    if($data['gender'] == 'Female'){
                        $genderinfo = "She"; $geninfo = "her";
                    }elseif($data['gender'] == 'Male'){
                        $genderinfo ="He"; $geninfo = "his";
                    }
                
            @endphp  
                
            <?php
                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                $dobStr = \Carbon\Carbon::parse($bonafiedData->dob)->format('d/M/Y');
                $dobArr = explode("/",$dobStr);
            ?>
            
            <style>
                .float-div img{width: 100px;margin-top: 30px;}
                .float-div{float: left;}
                .heading-div{margin-top: 0;}
                .heading-div h1{text-align: center;margin-top: 0; font-size: 22px; line-height: 28px;}
                .heading-div h2{font-size: 20px;text-align: center;}
                .border-class{border: 1px solid #000;padding: 25px 35px 25px 35px;}
                .clear-both{clear: both;}
                .float-right{float: right;}
                .text-center{text-align: center;}
                p{margin:.5rem 0 .5rem 0;}
                .footer-div{padding-top: 70px;}
                .header-div{padding-top: 30px;}
                .square {height: 90px;width: 90px;border: 1px solid;float: right; margin-top: 30px;}
            </style>
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                    <div class="card" style="position: relative;" >
                        <div style="position: absolute;display: flex;justify-content: center;width: 100%;padding-top:100px;"> <canvas  class="can" id="demo" style="opacity: 0.03; height: 320px; width: 300px;"></canvas></div>
                        <div class="body" >
                            
                            <div class="border-class">
                                <div class="bona">  
                                    <div class="float-div">
                                        <img src='{{ asset("public/images/logo.png") }}'>
                                    </div>
                                    <div class="square"></div>
                                    <div class="heading-div">
                                        <h1>{{ $instituteName }}</h1>
                                        <h2><b><u>BONAFIDE CERTIFICATE</u></b></h2>
                                    
                                        <input type="hidden" name="gr_number"  value="{{ !empty($bonafiedData['gr_number']) ? $bonafiedData['gr_number'] : old('gr_number') }}">
                                    </div>
                                    <div class="clear-both"></div>
                                    <div class="header-div">
                                        <p style="text-indent: 70px; text-align: justify;">This is to certify that,  <b>{{ $bonafiedData->fullname }}</b> is a bonafide student of the {{$instname}} studying in class <b>{{ !empty($data->classes->class_name) ? $data->classes->class_name : '' }} ,</b> of this college during the session <b>{{ !empty($data->school->academicYears->academic_year) ? $data->school->academicYears->academic_year : '' }}.</b> </p>
                                        <p style="text-indent: 70px; text-align: justify;">He/She bears a goos moral character.</p>
                                        <p style="text-indent: 70px; text-align: justify;">
                                     His/Her date of Birth is- <b>{{ \Carbon\Carbon::parse($bonafiedData->dob)->format('d/m/Y') }}</b> <b>{{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }} .</b></p>
                                        <!-- <p style="text-indent: 70px; text-align: justify;">As per our {{$instname}} record, {{$geninfo}} Admission Register No. is <b>{{ $bonafiedData->gr_number }}</b> and {{$geninfo}} date of Birth is <b>{{ \Carbon\Carbon::parse($bonafiedData->dob)->format('d-m-Y') }}</b> (in words- {{ ucfirst($f->format($dobArr[0])) .' '. $dobArr[1] .' '. ucfirst($f->format($dobArr[2])) }}) .</p> -->
                                        <!-- <p style="text-indent: 70px; text-align: justify;">{{$genderinfo}} belongs to caste <b>{{ $bonafiedData->caste }}</b> under <b>{{ $bonafiedData->category }}</b> category and 
                                        {{$geninfo}} Aadhar Card No. is <b>{{ $bonafiedData->aadhar_no }}</b> and Student ID is <b>{{$bonafiedData->student_id}}</b>.</p>
                                        <p style="text-indent: 70px; text-align: justify;">{{ $genderinfo }} bears a good moral character.</p> -->
                                    </div>
                                    
                                    <div class="footer-div">
                                        <div class="float-div">
                                            <p>Date: {{ date('d/m/Y') }}</p>
                                            <p>Gr. No: {{ $bonafiedData->gr_number }}</p>
                                        </div>
                                        <div class="float-right">
                                            <p class="text-center">Principal</p>
                                            <p ><b style="padding-left: 170px">(Mrs Rupali H Tiple)</b></p>
                                            <p>{{ $data->school->school_name }}.</p>
                                        </div>
                                        <div class="clear-both"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    <script>
        // (B) IMAGES + CANVAS
    var iBack = new Image(),
        iMark = new Image(),
        iLoaded = 0,
        canvas = document.getElementById("demo"),
        ctx = canvas.getContext("2d");
     
    // (C) WATERMARK
    function cmark () { 
        iLoaded++; 
        if (iLoaded==2) {        
          // (C2) ADD WATERMARK
            canvas.width = iBack.naturalWidth;
            canvas.height = iBack.naturalHeight;
            ctx.drawImage(iMark, 0, 0, iMark.naturalWidth, iMark.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack.onload = cmark;
    iMark.onload = cmark;
    iBack.src = '{{ asset("public/images/logo.png") }}';
    iMark.src = '{{ asset("public/images/logo.png") }}';
/*for 2nd water mark*/
            // (B) IMAGES + CANVAS
    var iBack2 = new Image(),
        iMark2 = new Image(),
        iLoaded2 = 0,
        canvas2 = document.getElementById("demo2"),
        ctx2 = canvas2.getContext("2d");
     
    // (C) WATERMARK
    function cmark2 () { 
        iLoaded2++; 
        if (iLoaded2==2) {        
          // (C2) ADD WATERMARK
            canvas2.width = iBack2.naturalWidth;
            canvas2.height = iBack2.naturalHeight;
            ctx2.drawImage(iMark2, 0, 0, iMark2.naturalWidth, iMark2.naturalHeight);
     
      
        }
    }
     
    // (D) GO - PROCEED ONLY WHEN IMAGES ARE LOADED
    iBack2.onload = cmark2;
    iMark2.onload = cmark2;
    iBack2.src = '{{ asset("public/images/logo.png") }}';
    iMark2.src = '{{ asset("public/images/logo.png") }}';
        
</script>
@endsection

    
                                
                        
                            
        
      
                            
                     
                        
                        
    

                                 




        
       

 
                               
