@extends('layouts.dash')    
@section('title', 'My Profile')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> My Profile </h3>
        <nav aria-label="breadcrumb">
            <!-- <a href=""><button type="button" class="btn btn-primary custom-btn">Back</button></a> -->
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('my-profile') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter name" name="name" value="{{ old('name', isset($profile->name) ? $profile->name : '' ) }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Email <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter email" name="email" value="{{ old('email', isset($profile->email) ? $profile->email : '' ) }}">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('email') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Mobile No. <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter mobile no." name="mobile_no" value="{{ old('mobile_no', isset($profile->mobile_no) ? $profile->mobile_no : '' ) }}">
                                @if ($errors->has('mobile_no'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('mobile_no') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Profile Image <?php echo $reqLabel; ?></label>
                                <input type="file" class="form-control" name="user_img_url" value="{{ old('user_img_url', isset($profile->user_img_url) ? $profile->user_img_url : '' ) }}">
                                @if ($errors->has('user_img_url'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('user_img_url') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection