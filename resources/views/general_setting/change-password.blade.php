@extends('layouts.dash')    
@section('title', 'Change Password')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Change Password </h3>
        <nav aria-label="breadcrumb">
            <!-- <a href=""><button type="button" class="btn btn-primary custom-btn">Back</button></a> -->
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('change-password') }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Old Password <?php echo $reqLabel; ?></label>
                                <input type="password" class="form-control" placeholder="Enter old password" name="old_password" value="{{ old('old_password') }}">
                                @if ($errors->has('old_password'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('old_password') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">New Password <?php echo $reqLabel; ?></label>
                                <input type="password" class="form-control" placeholder="Enternew password" name="new_password" value="{{ old('new_password') }}">
                                @if ($errors->has('new_password'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('new_password') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Confirm New Password <?php echo $reqLabel; ?></label>
                                <input type="password" class="form-control" placeholder="Enter confirm new password" name="confirm_password" value="{{ old('confirm_password') }}">
                                @if ($errors->has('confirm_password'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('confirm_password') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection