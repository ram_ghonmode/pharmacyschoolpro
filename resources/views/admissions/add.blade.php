@extends('layouts.dash')    
@section('title', 'Admission')
@section('content')   
<?php use App\Http\Controllers\Controller;$feesType = ['Internal','External']; $categories = ['Open', 'OBC', 'SC', 'ST', 'NT', 'SBC', 'VJ', 'NT-B', 'NT-C', 'NT-D'];  $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Admission </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('admission-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('admission-edit', ['id' => isset($admission->id) ? Controller::cryptString($admission->id, 'e') : '']) }}" method="POST" autocomplete="off" enctype="multipart/form-data" id="admissionDetailFormId">
                    @csrf
                        <div class="row clearfix">
                            <div class="col-sm-3 upload-img-div text-center">
                            <?php  ?> 
                                <?php $scr = isset($admission->profile_image) ? $admission->profile_image : asset("public/Stellar-master/images/brokanupload-img.png");?>
                                <img class="upload-img" id="uploadimgId" onerror="this.src='{{ $scr }}';" src="" alt="your image" style="width:200px;height:200px" />
                                <br>
                                <input type='file' name="profile_image" id="computerupload" onchange="readURL(this);"  value="{{ old('profile_image', isset($admission->profile_image) ? $admission->profile_image : '' ) }}"/>
                                <label class="btn btn-primary" for="computerupload" style="padding: 0.5rem 1rem; margin-top:8px;" >Upload</label>
                                <button class="btn btn-secondary" id="resetupload" type="button" style="padding: 0.5rem 1rem;">Reset</button><br>
                                @if ($errors->has('profile_image'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('profile_image') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-9">
                                <div class="row clearfix">
                                    <div class="col-sm-3 form-group">
                                        <label for="">College <?php echo $reqLabel; ?></label>
                                        <select class="form-control" id="" name="school_id">
                                            <option value="">Select college</option>
                                            @foreach($data['schools'] as $id => $name)
                                                <option value="{{ $id  }}" @if(old('school_id', isset($admission->school_id) ? $admission->school_id : '') == $id ) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('school_id'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('school_id') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="">Admission Type <?php echo $reqLabel; ?></label>
                                        <select name="admission_type_id" class="form-control">
                                            <option value="">Select Admission Type</option>
                                            @foreach($data['admissionTypes']  as $id => $adType)
                                                <option value="{{ $id }}" @if(old('admission_type_id', isset($admission->admission_type_id) ? $admission->admission_type_id : '') == $id) selected @endif>{{ $adType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Admission In <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" name="admission_in" value="College" readOnly>
                                        <!-- <select class="form-control" id="admissionInId" name="admission_type" readOnly>
                                            <option value="College" @if(old('admission_in', isset($admission->admission_in) ? $admission->admission_in : '') == "College" ) selected @endif>College</option>
                                        </select>
                                        @if ($errors->has('admission_type'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('admission_type') }}</small>
                                            </span>
                                        @endif -->
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">College Stream</label>
                                        <select class="form-control" id="StreamId" name="college_stream">
                                            <option value="">Select college stream</option>
                                            @foreach($data['streams'] as $id => $name)
                                                <option value="{{ $id  }}" @if(old('college_stream', isset($admission->college_stream) ? $admission->college_stream : '') == $id ) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-3 form-group">
                                        <label for="">General Register No. <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter general register no." name="gr_number" value="{{ old('gr_number', isset($admission->gr_number) ? $admission->gr_number : '' ) }}">
                                        @if ($errors->has('gr_number'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('gr_number') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Student Id <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter student Id" name="student_id" value="{{ old('student_id', isset($admission->student_id) ? $admission->student_id : '' ) }}">
                                        @if ($errors->has('student_id'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('student_id') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Scholarship Type <?php echo $reqLabel; ?></label>
                                        <select name="scholarship_type_id" class="form-control">
                                            <option value="">Select Scholarship Type</option>
                                            @foreach($data['scholarshipTypes']  as $id => $scType)
                                                <option value="{{ $id }}" @if(old('scholarship_type_id', isset($admission->scholarship_type_id) ? $admission->scholarship_type_id : '') == $id) selected @endif>{{ $scType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Admission Status <?php echo $reqLabel; ?></label>
                                        <select class="form-control" id="" name="admission_status">
                                            <option value="Pursuing" @if(old('admission_status', isset($admission->admission_status) ? $admission->admission_status : '') == "Pursuing") selected @endif>Pursuing</option>
                                            <option value="Passout" @if(old('admission_status', isset($admission->admission_status) ? $admission->admission_status : '') == "Passout" ) selected @endif>Passout</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-3 form-group">
                                        <label for="">Student First Name <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter first name" name="first_name" value="{{ old('first_name', isset($admission->first_name) ? $admission->first_name : '' ) }}">
                                        @if ($errors->has('first_name'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('first_name') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Middle Name <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter middle name" name="middle_name" value="{{ old('middle_name', isset($admission->middle_name) ? $admission->middle_name : '' ) }}">
                                        @if ($errors->has('middle_name'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('middle_name') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3 form-group">
                                        <label for="">Last Name <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter last name" name="last_name" value="{{ old('last_name', isset($admission->last_name) ? $admission->last_name : '' ) }}">
                                        @if ($errors->has('last_name'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('last_name') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="">विद्यार्थी का नाम</label>
                                        <input type="text" class="form-control" id="" placeholder="विद्यार्थी का नाम" name="fullname_in_hindi" value="{{ old('fullname_in_hindi', isset($admission->fullname_in_hindi) ? $admission->fullname_in_hindi : '' ) }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                                <label for="">Father First Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter father first name" name="father_first_name" value="{{ old('father_first_name', isset($admission->guardianinfos->father_first_name) ? $admission->guardianinfos->father_first_name : '' ) }}">
                                @if ($errors->has('father_first_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('father_first_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Father Middle Name</label>
                                <input type="text" class="form-control" id="" placeholder="Enter father middle name" name="father_middle_name" value="{{ old('father_middle_name', isset($admission->guardianinfos->father_middle_name) ? $admission->guardianinfos->father_middle_name : '' ) }}">
                                @if ($errors->has('father_middle_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('father_middle_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Father Last Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter father last name" name="father_last_name" value="{{ old('father_last_name', isset($admission->guardianinfos->father_last_name) ? $admission->guardianinfos->father_last_name : '' ) }}">
                                @if ($errors->has('father_last_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('father_last_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Mother First Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter mother first name" name="mother_name" value="{{ old('mother_name', isset($admission->mother_name) ? $admission->mother_name : '' ) }}">
                                @if ($errors->has('mother_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('mother_name') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Admission Date <?php echo $reqLabel; ?></label>
                                <input type="date" class="form-control" id="" name="admission_date"  value="{{ old('admission_date', isset($admission->admission_date) ? $admission->admission_date : '' ) }}">
                                @if ($errors->has('admission_date'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('admission_date') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Academic Year <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="academic_year">
                                    <option value="">Select academic year</option>
                                    @foreach($data['years'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('academic_year', isset($admission->academic_year) ? $admission->academic_year : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('academic_year'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('academic_year') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Admit To <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="admit_to">
                                    <option value="">Select admit to</option>
                                    @foreach($data['classes'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('admit_to', isset($admission->admit_to) ? $admission->admit_to : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('admit_to'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('admit_to') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Current Class <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="current_class">
                                    <option value="">Select admit to</option>
                                    @foreach($data['classes'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('current_class', isset($admission->current_class) ? $admission->current_class : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('current_class'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('current_class') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Class Section <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="section">
                                    <option value="">Select section</option>
                                    <option value="A" @if(old('section', isset($admission->section) ? $admission->section : '') == 'A') selected @endif>A</option>
                                    <option value="B" @if(old('section', isset($admission->section) ? $admission->section : '') == 'B') selected @endif>B</option>
                                    <option value="C" @if(old('section', isset($admission->section) ? $admission->section : '') == 'C') selected @endif>C</option>
                                    <option value="D" @if(old('section', isset($admission->section) ? $admission->section : '') == 'D') selected @endif>D</option>
                                    <option value="E" @if(old('section', isset($admission->section) ? $admission->section : '') == 'E') selected @endif>E</option>
                                </select>
                                @if ($errors->has('section'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('section') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Medium <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="medium">
                                    <option value="">Select medium</option>
                                    @foreach($data['mediums'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('medium', isset($admission->medium) ? $admission->medium : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('medium'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('medium') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Gender <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="gender">
                                    <option value="">Select gender</option>
                                    <option value="Male" @if(old('gender', isset($admission->gender) ? $admission->gender : '') == 'Male') selected @endif>Male</option>
                                    <option value="Female" @if(old('gender', isset($admission->gender) ? $admission->gender : '') == 'Female') selected @endif>Female</option>
                                </select>
                                @if ($errors->has('gender'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('gender') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">DOB <?php echo $reqLabel; ?></label>
                                <input type="date" class="form-control" id=""  name="dob" value="{{ old('dob', isset($admission->dob) ? $admission->dob : '' ) }}">
                                @if ($errors->has('dob'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('dob') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Age</label>
                                <input type="text" class="form-control" id="" placeholder="Enter age" name="age" value="{{ old('age', isset($admission->age) ? $admission->age : '' ) }}">
                                @if ($errors->has('age'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('age') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Place Of Birth </label>
                                <input type="text" class="form-control" id="" placeholder="Enter place of birth" name="place_of_birth" value="{{ old('place_of_birth', isset($admission->place_of_birth) ? $admission->place_of_birth : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">State Of Domicile</label>
                                <input type="text" class="form-control" id="" placeholder="Enter state of domicile" name="domicile" value="{{ old('domicile', isset($admission->domicile) ? $admission->domicile : '' ) }}">
                                @if ($errors->has('domicile'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('domicile') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Mothertounge</label>
                                <input type="text" class="form-control" id="" placeholder="Enter mothertounge" name="mother_tounge" value="{{ old('mother_tounge', isset($admission->mother_tounge) ? $admission->mother_tounge : '' ) }}">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Country <?php echo $reqLabel; ?> </label>
                                <input type="text" class="form-control" id="" placeholder="Enter country" name="country" value="{{ old('country', isset($admission->guardianinfos->country) ? $admission->guardianinfos->country : '' ) }}">
                                @if ($errors->has('country'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('country') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">State <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter state" name="state" value="{{ old('state', isset($admission->guardianinfos->state) ? $admission->guardianinfos->state : '' ) }}">
                                @if ($errors->has('state'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('state') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">District <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter district" name="district" value="{{ old('district', isset($admission->guardianinfos->district) ? $admission->guardianinfos->district : '' ) }}">
                                @if ($errors->has('district'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('district') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Tahsil <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter tahsil" name="tahsil" value="{{ old('tahsil', isset($admission->guardianinfos->tahsil) ? $admission->guardianinfos->tahsil : '' ) }}">
                                @if ($errors->has('tahsil'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('tahsil') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Caste <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter caste" name="caste" value="{{ old('caste', isset($admission->caste) ? $admission->caste : '' ) }}">
                                @if ($errors->has('caste'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('caste') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Category <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="category_id">
                                    <option value="">Select category</option>
                                    @foreach($data['categories']  as $id => $cat)
                                        <option value="{{ $id }}" @if(old('category_id', isset($admission->category_id) ? $admission->category_id : '') == $id) selected @endif>{{ $cat }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Religion</label>
                                <input type="text" class="form-control" id="" placeholder="Enter religion" name="religion" value="{{ old('religion', isset($admission->religion) ? $admission->religion : '' ) }}">
                                @if ($errors->has('religion'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('religion') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Blood Group</label>
                                <input type="text" class="form-control" id="" placeholder="Enter blood group" name="blood_group" value="{{ old('blood_group', isset($admission->studentgeneralinfos->blood_group) ? $admission->studentgeneralinfos->blood_group : '' ) }}">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Height (Feet) </label>
                                <input type="text" class="form-control" id="" placeholder="Enter height (feet)" name="height_feet" value="{{ old('height_feet', isset($admission->studentgeneralinfos->height_feet) ? $admission->studentgeneralinfos->height_feet : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Height (Inch) </label>
                                <input type="text" class="form-control" id="" placeholder="Enter height (inch)" name="height_inch" value="{{ old('height_inch', isset($admission->studentgeneralinfos->height_inch) ? $admission->studentgeneralinfos->height_inch : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Personal Identification Mark-1 </label>
                                <input type="text" class="form-control" id="" placeholder="Enter mark" name="pim_1" value="{{ old('pim_1', isset($admission->studentgeneralinfos->pim_1) ? $admission->studentgeneralinfos->pim_1 : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Personal Identification Mark-1 </label>
                                <input type="text" class="form-control" id="" placeholder="Enter mark" name="pim_2" value="{{ old('pim_2', isset($admission->studentgeneralinfos->pim_2) ? $admission->studentgeneralinfos->pim_2 : '' ) }}">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Weight</label>
                                <input type="text" class="form-control" id="" placeholder="Enter weight" name="weight" value="{{ old('weight', isset($admission->weight) ? $admission->weight : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Physically Handicap</label>
                                <select class="form-control" id="" name="handicapped">
                                    <option value="No"  @if(old('handicapped', isset($admission->handicapped) ? $admission->handicapped : '') == "No") selected @endif>No</option>
                                    <option value="Yes"  @if(old('handicapped', isset($admission->handicapped) ? $admission->handicapped : '') == "Yes") selected @endif>Yes</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Under BPL</label>
                                <select class="form-control" id="" name="under_bpl">
                                    <option value="No"  @if(old('under_bpl', isset($admission->under_bpl) ? $admission->under_bpl : '') == "No") selected @endif>No</option>
                                    <option value="Yes"  @if(old('under_bpl', isset($admission->under_bpl) ? $admission->under_bpl : '') == "Yes") selected @endif>Yes</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Student is From</label>
                                <select class="form-control" id="" name="locality">
                                    <option value="Urban"  @if(old('locality', isset($admission->locality) ? $admission->locality : '') == "Urban") selected @endif>Urban</option>
                                    <option value="Rural"  @if(old('locality', isset($admission->locality) ? $admission->locality : '') == "Rural") selected @endif>Rural</option>
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Father Qualification</label>
                                <input type="text" class="form-control" id="" placeholder="Enter father qualification" name="qualification" value="{{ old('qualification', isset($admission->guardianinfos->qualification) ? $admission->guardianinfos->qualification : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Occupation</label>
                                <input type="text" class="form-control" id="" placeholder="Enter occupation" name="occupation" value="{{ old('occupation', isset($admission->guardianinfos->occupation) ? $admission->guardianinfos->occupation : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Annual Income</label>
                                <input type="text" class="form-control" id="" placeholder="Enter annual income" name="income" value="{{ old('income', isset($admission->guardianinfos->income) ? $admission->guardianinfos->income : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Contact No. <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter contatc no." name="mobile_no" value="{{ old('mobile_no', isset($admission->guardianinfos->mobile_no) ? $admission->guardianinfos->mobile_no : '' ) }}">
                                @if ($errors->has('mobile_no'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('mobile_no') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-6">
                                <label for="">Permanent Address</label>
                                <input type="text" class="form-control" id="" placeholder="Enter permanent address" name="perm_address" value="{{ old('perm_address', isset($admission->guardianinfos->perm_address) ? $admission->guardianinfos->perm_address : '' ) }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="">Local Address</label>
                                <input type="text" class="form-control" id="" placeholder="Enter local address" name="local_address" value="{{ old('local_address', isset($admission->guardianinfos->local_address) ? $admission->guardianinfos->local_address : '' ) }}">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Last School Attended</label>
                                <input type="text" class="form-control" id="" placeholder="Enter last school" name="last_school_attended" value="{{ old('last_school_attended', isset($admission->studentgeneralinfos->last_school_attended) ? $admission->studentgeneralinfos->last_school_attended : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Class</label>
                                <input type="text" class="form-control" id="" placeholder="Enter class" name="leaving_class" value="{{ old('leaving_class', isset($admission->studentgeneralinfos->leaving_class) ? $admission->studentgeneralinfos->leaving_class : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Leaving Date</label>
                                <input type="date" class="form-control" id=""  name="leaving_date" value="{{ old('leaving_date', isset($admission->studentgeneralinfos->leaving_date) ? $admission->studentgeneralinfos->leaving_date : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Reason For Leaving</label>
                                <select class="form-control" id="" name="reason_of_leaving">
                                    <option value="">Select reason</option>
                                    <option value="Has Successfully Completed examination in Higher Class"  @if(old('reason_of_leaving', isset($admission->studentgeneralinfos->reason_of_leaving) ? $admission->studentgeneralinfos->reason_of_leaving : '') == 'Has Successfully Completed examination in Higher Class') selected @endif>Has Successfully Completed examination in Higher Class</option>
                                    <option value="As Per Guardian Request"  @if(old('reason_of_leaving', isset($admission->studentgeneralinfos->reason_of_leaving) ? $admission->studentgeneralinfos->reason_of_leaving : '') == 'As Per Guardian Request') selected @endif>As Per Guardian Request</option>
                                    <option value="Due to poor results in academic"  @if(old('reason_of_leaving', isset($admission->studentgeneralinfos->reason_of_leaving) ? $admission->studentgeneralinfos->reason_of_leaving : '') == 'Due to poor results in academic') selected @endif>Due to poor results in academic</option>
                                    <option value="Terminated from School"  @if(old('reason_of_leaving', isset($admission->studentgeneralinfos->reason_of_leaving) ? $admission->studentgeneralinfos->reason_of_leaving : '') == 'Terminated from School') selected @endif>Terminated from School</option>
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Aadhar No.</label>
                                <input type="text" class="form-control" id="" placeholder="Enter aadhar no." name="aadhar_no" value="{{ old('aadhar_no', isset($admission->studentbankinfos->aadhar_no) ? $admission->studentbankinfos->aadhar_no : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Bank Account No.</label>
                                <input type="text" class="form-control" id="" placeholder="Enter bank account no." name="account_no" value="{{ old('account_no', isset($admission->studentbankinfos->account_no) ? $admission->studentbankinfos->account_no : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Bank Name</label>
                                <input type="text" class="form-control" id="" placeholder="Enter bank name" name="bank_name" value="{{ old('bank_name', isset($admission->studentbankinfos->bank_name) ? $admission->studentbankinfos->bank_name : '' ) }}">
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">IFSC Code</label>
                                <input type="text" class="form-control" id="" placeholder="Enter ifsc code" name="ifsc" value="{{ old('ifsc', isset($admission->studentbankinfos->ifsc) ? $admission->studentbankinfos->ifsc : '' ) }}">
                            </div>
                        </div>
                        @php
                            $docDbArr = [];
                            if(isset($admission['document'])){
                                $docDbArr = explode(', ', trim($admission['document']));
                            }
                        @endphp
                        <div class="row clearfix">
                            <div class="col-12">
                                <div class="form-group">
                                    <label style="padding-left: 10px;">Documents</label>
                                    <div class="checkbox checkbox-type">
                                        @foreach($data['documents'] as $id => $doc)
                                            <div class="col-md-6">
                                                <div class="form-check checkbox-box">
                                                    <input class="form-check-input" name="document[{{$id}}]" type="checkbox" value="{{$doc}}" id="defaultCheck{{$id}}" @if(in_array($doc,$docDbArr)) checked @endif>
                                                    <label class="form-check-label" for="defaultCheck{{$id}}">
                                                        {{ $doc }}
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection