@extends('layouts.dash')   
@section('title', 'Admission List')
@section('content') 
<style>
    container--default .select2-selection--single select.select2-search__field, select.typeahead, select.tt-query, select.tt-hint
    {
        padding: -3.5625rem 0.75rem !important;
    }
</style> 
@php
    use App\Http\Controllers\Controller;
    $style = NULL;
    $inputArr = ['standard_id','course_id','subject_id','chapter_id','difficulty_level_id','question'];
    foreach($inputArr as $nameOfinput){
        if(!empty(Request::get($nameOfinput))){
            $style= 'in';
        }
    }
@endphp
    <div class="page-header">
        <h3 class="page-title"> Admission </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('admission-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table id="student-listing" class="table table-bordered table-responsive" width="100%">
                        <thead>
                            <tr>
                                <th width="5%">Sr. No.</th>
                                <th>Gr. No.</th>
                                <!-- <th>Student ID</th> -->
                                <th>Student Name</th>
                                <th>Admission Type</th>
                                <th>Admit To</th>
                                <th>Current Class</th>
                                <th>Academic Year</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($admissions) && count($admissions) > 0)
                                @foreach($admissions as $key => $stud)
                                    <?php 
                                        $encyId = Controller::cryptString($stud->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $stud->gr_number }}</td>
                                        <td>{{ $stud->first_name.' '.$stud->middle_name.' '.$stud->last_name }}</td>
                                        <td>{{ $stud->admissionTypes->admission_type ?? ''}}</td>
                                        <td>{{ $stud->admitToClasses->class_name ?? ''}}</td>
                                        <td>{{ $stud->classes->class_name ?? ''}}</td>
                                        <td>{{ $stud->academicYears->academic_year ?? '' }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('admission-edit', ['id' => $encyId]) }}">Edit</a>
                                                   &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$stud->id.'/admissions')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection