@extends('layouts.dash')   
@section('title', 'Role List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Role </h3>
        @can('user-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('role-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">Sr. No.</th>
                                <th>Name</th>
                                @if(Gate::check('user-edit') || Gate::check('user-delete'))
                                    <th width="10%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($allroles) && count($allroles) > 0)
                                <?php  $count = $allroles->firstItem(); ?>
                                @foreach($allroles as $id => $role)
                                    @php $encyId = Controller::cryptString($role->id, 'e') @endphp
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $role->name }}</td>
                                        @if(Gate::check('user-edit') || Gate::check('user-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('user-edit')
                                                        <a href="{{ route('role-edit', ['id' => $encyId]) }}">Edit</a>  
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('user-delete') 
                                                        <a href="{{ url('/dashboard/'.$role->id.'/roles')}}" onclick="return confirm('Are you sure?')">Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif  
                                    </tr> 
                                @endforeach
                            @endif  
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $allroles->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection