@extends('layouts.dash')   
@section('title', 'Document')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Document </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('document-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Document Name </th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($documents) && count($documents) > 0)
                            <?php  $count = $documents->firstItem(); ?>
                                @foreach($documents as $key => $document)
                                    <?php 
                                        $encyId = Controller::cryptString($document->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $document->document_name }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('document-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$document->id.'/documents')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $documents->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection