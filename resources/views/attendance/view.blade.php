@extends('layouts.dash')   
@section('title', 'Student List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Student List </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('student-attendance-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($allStudents) && count($allStudents) > 0)
                                @foreach($allStudents as $key => $student)
                                    @php
                                        if($student->is_present == 1){
                                            $status = 'Present';
                                        }else{
                                            $status = 'Absent';
                                        }
                                    @endphp
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $student->admissions['first_name'] }} {{ $student->admissions['last_name'] }}</td>
                                        <td>{{ $status }}</td>
                                        <td>
                                            @if($student->is_present == 1)
                                                <a href="{{ route('attendance-status', ['id' => $student->id]) }}" title="Present"><button class="btn btn-primary btn-xs">Present</button></a>
                                            @else
                                                <a href="{{ route('attendance-status', ['id' => $student->id]) }}" title="Absent"><button class="btn btn-warning btn-xs">Absent</button></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @endif 
                        </tbody>
                    </table>
                    <br>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection