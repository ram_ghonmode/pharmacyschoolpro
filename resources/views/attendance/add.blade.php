@extends('layouts.dash')    
@section('title', 'Student Attendance')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Student Attendance </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('student-attendance-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('student-attendance-add') }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                                <label for="">Date<?php echo $reqLabel; ?></label>
                                <div id="datepicker-popup" class="input-group date datepicker" style="padding:0">
                                    <input type="text" name="date" value="{{ old('date') }}" class="datepicker form-control" placeholder="Enter date">
                                    <span class="input-group-addon input-group-append border-left">
                                        <span class="icon-calendar input-group-text"></span>
                                    </span>
                                </div>
                                @if ($errors->has('date'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('date') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix subdiv">
                            <table id="datatable-checkbox" class="table table-bordered bulk_action testmanager">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" class="parentCheckBox" onclick="checkAllChild(this);" style="width:17px;height:17px"></th>
                                        <th>Sr No.</th>
                                        <th>Student Name</th>
                                        <th>Class</th>
                                        <th>Section</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(!empty($students))                                
                                        @foreach($students as $key => $student) 
                                            <tr>                                                                         
                                                <th><input type="checkbox" class="childCheckBox" onclick="checkForParent(this);" name="students[{{ $student->id }}]" style="width:17px;height:17px" value="1"></th>
                                                <td>{{ ++$key }}</td>
                                                <td>{{ $student->first_name . ' ' . $student->last_name}}</td>
                                                <td>{{ $student->classes->class_name }}</td>
                                                <td>{{ $student->section }}</td>
                                                <input type="hidden" name="allstudents[{{ $student->id }}]" value="0">                                        
                                                <input type="hidden" name="school_id" value="{{ $student->school_id}}">                                        
                                                <input type="hidden" name="class_id" value="{{ $student->current_class }}">                                        
                                                <input type="hidden" name="section" value="{{ $student->section }}">                                        
                                            </tr>
                                        @endforeach                                 
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection