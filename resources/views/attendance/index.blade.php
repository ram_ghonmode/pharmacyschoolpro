@extends('layouts.dash')   
@section('title', 'Student Attendance List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Student Attendance List </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('student-attendance-add') }}"><button type="button" class="btn btn-primary custom-btn">New Attendance</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Date</th>
                                <th>Total</th>
                                <th>Present</th>
                                <th>Absent</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classAttd) && count($classAttd) > 0)
                            <?php  $count = $classAttd->firstItem(); ?>
                                @foreach($classAttd as $key => $class)
                                    <?php 
                                        $encyId = Controller::cryptString($class->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td><a href="{{ route('view-attendance', $class->id) }}">{{ date('d-m-Y', strtotime($class->date)) }}</a></td>
                                        <td>{{ $class->total }}</td>
                                        <td>{{ $class->present }}</td>
                                        <td>{{ $class->total - $class->present }}</td>
                                        <td><a href="{{ route('delete-attendance', $class->id) }}"><button id="delbutton" data-id="{{ $class->id }}" onclick="return confirm('Are you sure?')" class="btn btn-danger btn-xs aclass">Delete</button></a></td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <br>
                    <div class="text-right">
                        {{ $classAttd->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection