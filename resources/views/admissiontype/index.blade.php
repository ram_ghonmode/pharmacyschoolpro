@extends('layouts.dash')   
@section('title', 'Admission Type')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Admission Type </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('admissiontype-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Admission Type </th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($admissionTypes) && count($admissionTypes) > 0)
                            <?php  $count = $admissionTypes->firstItem(); ?>
                                @foreach($admissionTypes as $key => $admissionType)
                                    <?php 
                                        $encyId = Controller::cryptString($admissionType->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $admissionType->admission_type }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('admissiontype-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$admissionType->id.'/admission_types')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $admissionTypes->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection