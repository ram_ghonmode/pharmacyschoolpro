@extends('layouts.dash')    
@section('title', 'Admission Type')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Admission Type </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('admissiontype-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('admissiontype-edit', ['id' => isset($admissionType->id) ? Controller::cryptString($admissionType->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Admission Type <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter Admission Type" name="admission_type" value="{{ old('admissionType', isset($admissionType->admission_type) ? $admissionType->admission_type : '' ) }}">
                                @if ($errors->has('admission_type'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('admission_type') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection