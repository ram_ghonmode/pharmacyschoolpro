@extends('layouts.dash')   
@section('title', 'Scholarship Type')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Scholarship Type </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('scholarshiptype-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Scholarship Type </th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($scholarshipTypes) && count($scholarshipTypes) > 0)
                            <?php  $count = $scholarshipTypes->firstItem(); ?>
                                @foreach($scholarshipTypes as $key => $scholarshipType)
                                    <?php 
                                        $encyId = Controller::cryptString($scholarshipType->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $scholarshipType->scholarship_type }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('scholarshiptype-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$scholarshipType->id.'/scholarship_types')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $scholarshipTypes->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection