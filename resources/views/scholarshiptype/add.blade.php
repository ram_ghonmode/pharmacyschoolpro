@extends('layouts.dash')    
@section('title', 'Scholarship Type')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Scholarship Type </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('scholarshiptype-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('scholarshiptype-edit', ['id' => isset($scholarshipType->id) ? Controller::cryptString($scholarshipType->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Scholarship Type <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter Scholarship Type" name="scholarship_type" value="{{ old('scholarshipType', isset($scholarshipType->scholarship_type) ? $scholarshipType->scholarship_type : '' ) }}">
                                @if ($errors->has('scholarship_type'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('scholarship_type') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection