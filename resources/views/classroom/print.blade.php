<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('public/Stellar-master/css/style.css') }}">
        
    </head>
    <body>
        <style type="text/css">
            body{font-family: arial;}
            table{font-size:12px;}
            table th{padding:6px;}
            table td{padding:6px;}
            .padding-left{padding: 0 0  0 5px !important;}
            p{margin: 0;}
            
            .image {
                    background :url('{{ asset("public/images/logo.png")}}') no-repeat center !important;
                    width: 100%;
                    height: 100%;
                    position:relative;
                }
            .bona {
                    background :url('{{ asset("public/images/logo.png")}}') no-repeat center !important;
                    background-size: 50%;
                    position:relative;
                } 
            .border-class img{width: 100px !important;}
            @media print
                { 
                    .scaled{zoom: 91%;}
                    #print-sport{zoom: 88%;}
                    .border-class img{width: 100px !important;}
                }
        </style>
    <body>
        <div class="heading-div">
            <center><h3><u>Class Room List</u></h2></center>
        </div>
        <div class="container-fluid">
            <div class="col-12 grid-margin stretch-card">
                <div class="card" id="printableArea">
                    <div class="card-body">
                        <h3>School: {{ !empty($data['school']['school_name']) ? $data['school']['school_name'] : '' }}</h3>
                        <h4>Class: {{ !empty($data['current_class']['class_name']) ? $data['current_class']['class_name'] : '' }}</h4>
                        <table border="1"  cellspacing="0" cellpadding="3" width="100%">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>GR No.</th>
                                    <th>Student Name</th>
                                    <th>Student Id</th>
                                    <th>Mother Name</th>
                                    <th>Academic Year</th>
                                    <th>Admission Date</th>
                                    <th>DOB</th>
                                    <th>Gender</th>
                                    <th>Caste</th>
                                    <th>Category</th>
                                    <th>Aadhar No</th>
                                    <th>Mobile No</th>
                                    <th>Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($clsData))
                                    @foreach($clsData as $key => $data)
                                        <tr>
                                            <td>{{ ++$key  }}</td>
                                            <td>{{ $data->gr_number }}</td>
                                            <td>{{ $data->first_name.' '.$data->middle_name.' '.$data->last_name }}</td>
                                            <td>{{ $data->student_id ?? '' }}</td>
                                            <td>{{ $data->mother_name ?? '' }}</td>
                                            <td>{{ $data->academicYears->academic_year ?? '' }}</td>
                                            <td>{{ !empty($data->admission_date) ? date('d-m-Y',strtotime($data->admission_date)) : '' }}</td>
                                            <td>{{ !empty($data->dob) ? date('d-m-Y',strtotime($data->dob)) : '' }}</td>
                                            <td>{{ $data->gender }}</td>
                                            <td>{{ $data->caste }}</td>
                                            <td>{{ $data->categories->category_name }}</td>
                                            <td>{{ isset($data->studentbankinfos->aadhar_no) ? $data->studentbankinfos->aadhar_no : '' }}</td>
                                            <td>{{ isset($data->guardianinfos->mobile_no) ? $data->guardianinfos->mobile_no : '' }}</td>
                                            <td>{{ isset($data->guardianinfos->perm_address) ? $data->guardianinfos->perm_address : '' }}</td>
                                        </tr>
                                    @endforeach
                                @endif    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery Core Js -->
        <script src="{{ asset('public/js/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap Core Js -->
        <script src="{{ asset('public/js/bootstrap.js') }}"></script>
        <script src="{{ asset('public/js/krutidevToMangal.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                //window.print();
                PrintElem('#printableArea');
               function PrintElem(elem) {
                Popup($(elem).html());
            }

            function Popup(data) {
                var mywindow = window.open('', 'printDiv', 'height=500,width=700');mywindow.document.write(data);
                setTimeout(function () {
                    mywindow.print();
                    mywindow.document.close(); // necessary for IE >= 10
                    mywindow.focus(); // necessary for IE >= 10
                    mywindow.close();
                }, 1000);
                return true;
            }
        });
        </script>
    </body>
</html>