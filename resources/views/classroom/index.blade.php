@extends('layouts.dash')   
@section('title', 'Class Room List')
@section('content')  
<?php $sectionArr = ['A', 'B', 'C', 'D']; ?> 
<?php $scholarshipArr = ['Regular','RTE']; ?> 
    <div class="page-header">
        <h3 class="page-title"> Class Room List </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" id="classRoomFormId" action="{{ route('class-room-list') }}">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <select name="school_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">School</option>
                                    @foreach($data['schools'] as $skey => $value)
                                        <option value="{{ $skey }}" @if(Request::get('school_id') == $skey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <select name="current_class" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Class</option>
                                    @foreach($data['classes'] as $ckey => $value)
                                        <option value="{{ $ckey }}" @if(Request::get('current_class') == $ckey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary btn-sm waves-effect" >GET</button>
                            </div>
                            @if(!empty($clsData))
                                @php 
                                    $school = !empty($upcond["school_id"]) ? $upcond["school_id"] : '';
                                    $class = !empty($upcond["current_class"]) ? $upcond["current_class"] : '';
                                    $section = !empty($upcond["section"]) ? $upcond["section"] : '';
                                    $scholarship = !empty($upcond["scholarship"]) ? $upcond["scholarship"] : '';
                                @endphp
                                <div class="col-sm-1">
                                <a href="{{ route('class-room-excel',['school_id'=>$school,'current_class'=> $class,'section'=> $section]) }}" class="btn btn-success btn-sm waves-effect" target="_blank" title="excel">
                                    Excel
                                </a>
                                    <!-- <button type="button" class="btn btn-success btn-sm waves-effect" id="classRoomListId">Excel</button> -->
                                </div>
                                <!-- <a href='{{ url(preg_replace('#/+#','/',"printclasslist/".$school."/".$class."/".$section)) }}' target="_blank"><button type="button" class="btn btn-info btn-sm pull-right">Print</button></a> -->
                                <a href='{{ url("printclasslist/".$school."/".$class."/".$section) }}' target="_blank"><button type="button" class="btn btn-info btn-sm pull-right">Print</button></a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card" id="printableArea">
                <div class="card-body">
                    <h6>School: {{ !empty($data['school']['school_name']) ? $data['school']['school_name'] : '' }}</h6>
                    <h6>Class: {{ !empty($data['current_class']['class_name']) ? $data['current_class']['class_name'] : '' }}</h6>
                    <table id="student-listing" class="table table-bordered table-responsive classRoomListTable">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>GR No.</th>
                                <th>Student Name</th>
                                <th>Student Id</th>
                                <th>Mother Name</th>
                                <th>Academic Year</th>
                                <th>Admission Date</th>
                                <th>DOB</th>
                                <th>Gender</th>
                                <th>Caste</th>
                                <th>Category</th>
                                <th>Aadhar No</th>
                                <th>Mobile No</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($clsData))
                                @foreach($clsData as $key => $data)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $data->gr_number }}</td>
                                        <td>{{ $data->first_name.' '.$data->middle_name.' '.$data->last_name }}</td>
                                        <td>{{ $data->student_id ?? '' }}</td>
                                        <td>{{ $data->mother_name ?? '' }}</td>
                                        <td>{{ $data->academicYears->academic_year ?? '' }}</td>
                                        <td>{{ !empty($data->admission_date) ? date('d-m-Y',strtotime($data->admission_date)) : '' }}</td>
                                        <td>{{ !empty($data->dob) ? date('d-m-Y',strtotime($data->dob)) : '' }}</td>
                                        <td>{{ $data->gender }}</td>
                                        <td>{{ $data->caste }}</td>
                                        <td>{{ $data->categories->category_name }}</td>
                                        <td>{{ isset($data->studentbankinfos->aadhar_no) ? $data->studentbankinfos->aadhar_no : '' }}</td>
                                        <td>{{ isset($data->guardianinfos->mobile_no) ? $data->guardianinfos->mobile_no : '' }}</td>
                                        <td>{{ isset($data->guardianinfos->perm_address) ? $data->guardianinfos->perm_address : '' }}</td>
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection