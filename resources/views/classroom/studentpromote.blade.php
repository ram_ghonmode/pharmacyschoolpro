@extends('layouts.dash')   
@section('title', 'Class Room List')
@section('content')  
<?php $sectionArr = ['A', 'B', 'C', 'D']; ?> 
    <div class="page-header">
        <h3 class="page-title"> Student Promote </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" action="{{ route('student-class-list') }}">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <select name="school_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">School</option>
                                    @foreach($data['schools'] as $skey => $value)
                                        <option value="{{ $skey }}" @if(Request::get('school_id') == $skey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="current_class" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Class</option>
                                    @foreach($data['classes'] as $ckey => $value)
                                        <option value="{{ $ckey }}" @if(Request::get('current_class') == $ckey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3 m-b-0">
                                <select name="section" id="sectionId" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Class-Section</option>
                                    @foreach($sectionArr as $section)
                                        <option value="{{ $section }}" @if(Request::get('section') == $section) selected @endif>{{ $section }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary btn-sm waves-effect" >GET</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card" id="printableArea">
                <div class="card-body">
                    <h6>School: {{ !empty($data['school']['school_name']) ? $data['school']['school_name'] : '' }}</h6>
                    <h6>Class: {{ !empty($data['current_class']['class_name']) ? $data['current_class']['class_name'] : '' }}</h6>
                    <form method="POST" action="{{ route('student-class-list') }}">      
                    @csrf         
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>GR No.</th>
                                    <th>Full Name</th>
                                    <th>Class</th>
                                    <th>Section</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($clsData))
                                    @foreach($clsData as $key => $data)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ $data->gr_number }}</td>
                                            <td>{{ $data->first_name.' '.$data->middle_name.' '.$data->last_name }}</td>
                                            <td>{{ $data->classes->class_name }}</td>
                                            <td>{{ $data->section }}</td>
                                            <td>
                                                <select name="section[{{ $data->id }}]" class="form-control show-tick">
                                                    <option value="1">PASS</option>
                                                    <option value="0">FAIL</option>
                                                </select>   
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif    
                            </tbody>
                        </table>
                        <br>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection