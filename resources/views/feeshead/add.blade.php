@extends('layouts.dash')    
@section('title', 'Class')
@section('content')   
<?php use App\Http\Controllers\Controller;$feesType = ['Internal','External','Old']; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Fees Head </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('fees-head-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('fees-head-edit', ['id' => isset($feesHead->id) ? Controller::cryptString($feesHead->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Fees Head Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter fees head name" name="fees_head_name" value="{{ old('fees_head_name', isset($feesHead->fees_head_name) ? $feesHead->fees_head_name : '' ) }}">
                                @if ($errors->has('fees_head_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('fees_head_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-2" style="margin-top: 30px;">
                                <input type="checkbox" name="is_admission" @if(!empty($feesHead)) @if($feesHead->is_admission) checked @endif @endif/>
                                <label for="">Is New Admission?</label>
                            </div>
                            <div class="form-group col-sm-2" style="margin-top: 30px;">
                                <input type="checkbox" name="is_management" @if(!empty($feesHead)) @if($feesHead->is_management) checked @endif @endif/>
                                <label for="">Is Management?</label>
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection