@extends('layouts.dash')   
@section('title', 'Class')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Class </h3>
        @can('fees-head-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('fees-head-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Fees Head Name </th>
                                <th>Is Admission</th>
                                <th>Is Management</th>
                                <th>Fees Type</th>
                                @if(Gate::check('fees-head-edit') || Gate::check('fees-head-delete'))
                                    <th width="18%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($feesHeads) && count($feesHeads) > 0)
                            <?php  $count = $feesHeads->firstItem(); ?>
                                @foreach($feesHeads as $key => $feesHead)
                                    <?php 
                                        $encyId = Controller::cryptString($feesHead->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $feesHead->fees_head_name }}</td>
                                        <td>{{ ($feesHead->is_admission == 1) ? 'Yes' : 'No' }}</td>
                                        <td>{{ ($feesHead->is_management == 1) ? 'Yes' : 'No' }}</td>
                                        <td>{{ $feesHead->fees_type ?? '' }}</td>
                                        @if(Gate::check('fees-head-edit') || Gate::check('fees-head-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('fees-head-edit')
                                                        <a href="{{ route('fees-head-edit', ['id' => $encyId]) }}">Edit</a>
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('fees-head-delete')
                                                        <a href="{{ url('/dashboard/'.$feesHead->id.'/fees_heads')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif       
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $feesHeads->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection