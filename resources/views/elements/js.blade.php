<!-- plugins:js -->
<script src="{{ asset('public/Stellar-master/vendors/js/vendor.bundle.base.js') }}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="http://www.bootstrapdash.com/demo/stellar-admin/jquery/template/assets/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="http://www.bootstrapdash.com/demo/stellar-admin/jquery/template/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{ asset('public/Stellar-master/vendors/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('public/Stellar-master/vendors/moment/moment.min.js') }}"></script>
<script src="{{ asset('public/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('public/js/admissionform.js') }}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{ asset('public/Stellar-master/js/off-canvas.js') }}"></script>
<script src="{{ asset('public/Stellar-master/js/select2.js') }}"></script>
<script src="{{ asset('public/Stellar-master/vendors/select2/select2.min.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page -->
<script src="{{ asset('public/js/role.js') }}"></script>
<script src="{{ asset('public/js/print.js') }}"></script>
<script src="{{ asset('public/js/schoolteacher.js') }}"></script>
<!-- End custom js for this page -->
<script src="{{ asset('public/Stellar-master/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('public/Stellar-master/vendors/daterangepicker/daterangepicker.js') }}"></script>
<script src="https://cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>
<script>
    $('.select2').select2();
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#uploadimgId').attr('src', e.target.result);
                $('#dynaImg').val($('#computerupload').val());
            };
            reader.readAsDataURL(input.files[0]);
        }
    }


    $("#resetupload").click(function () {
        $('#uploadimgId').attr('src', '');
    });

    setTimeout(function(){
        $('.alert').fadeOut();
    }, 5000);

    $('#student-listing').DataTable();
    $('.reporttable').DataTable();

    $('.datepicker').datepicker({
        todayHighlight : true,
        autoclose: true,
        format: 'dd-mm-yyyy',
        orientation : "bottom",
    });

  
    function getTotalFees() {
        var total = 0; 
        $("[id^='pay_amount']").each(function() { 
            var val = $(this).val();
            total += isNaN(val) || $.trim(val)=="" ? 0 : parseFloat(val); 
        });
        $("#grandAmtTotId").text(total); 
    }
        // get feeshead 
    function getfeeshead(data){
        $('input[name=amount]').val('');
        $('input[name=amount]').val(data.amount);
        $('select[name=fees_head_id]').val(data.fees_head_id);
        $('select[name=fees_head_id]').trigger("change");
        $('select[name=status]').val(data.status);
        $('select[name=status]').trigger("change");
        $('input[name=update_id]').val(data.id);
    }
    //account field
    var state = $('#pay_mode');province = $('#cheque');amount= $('#amount');bankName = $('#bank');

    state.change(function (){
        if (state.val() == 'Cheque' || state.val() == 'DD') {
            province.removeAttr('disabled');
            bankName.removeAttr('disabled');
        } 
        else if (state.val() == 'Cash' || state.val() == 'select'){
            province.attr('disabled', 'disabled').val('');
            bankName.attr('disabled', 'disabled').val('');
        }
    }).trigger('change');

    $(document).ready(function () {
        $('#classFeesFormId').validate({ // initialize the plugin
            errorClass: 'errors',
            rules: {
                school_id: {
                    required: true,
                },
                class_id: {
                    required: true,
                },
                academic_year: {
                    required: true,
                },
            },
            messages: {
                school_id: {
                    required: "School is required."
                },
                class_id: {
                    required: "Class is required."
                },
                academic_year: {
                    required: "Academic Year is required."
                },
            }
        });

        $('#dailyFeesFormId').validate({ // initialize the plugin
            errorClass: 'errors',
            rules: {
                school_id: {
                    required: true,
                },
                academic_year: {
                    required: true,
                },
                date: {
                    required: true,
                },
            },
            messages: {
                school_id: {
                    required: "School is required."
                },
                academic_year: {
                    required: "Academic Year is required."
                },
                date: {
                    required: "Fees date is required."
                },
            }
        });

        $('#classRoomFormId').validate({ // initialize the plugin
            errorClass: 'errors',
            rules: {
                school_id: {
                    required: true,
                },
                current_class: {
                    required: true,
                },
            },
            messages: {
                school_id: {
                    required: "School is required."
                },
                current_class: {
                    required: "Class is required."
                },
            }
        });
    });
</script>