<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
            <div class="profile-image">
                <img class="img-xs rounded-circle" @if(!empty(Auth::user()->user_img_url)) src="{{ Auth::user()->user_img_url }}" @else src="{{ asset('public/Stellar-master/images/faces/photo-512.png') }}" @endif>    
                <div class="dot-indicator bg-success"></div>
            </div>
            <div class="text-wrapper">
                <p class="profile-name">{{ ucfirst(Auth::user()->name) }}</p>
                <p class="designation">{{ !empty(Auth::user()->roles[0]['name']) ? Auth::user()->roles[0]['name'] : '' }}</p>
            </div>
            </a>
        </li>
        @if(!(Auth::user()->inRole('admin')))
            <li>
                <a class="nav-link"  href="">
                    <span class="menu-title">{{ !empty(Auth::user()->getSchoolName()) ? Auth::user()->getSchoolName() : '' }}</span>
                    <i class="icon-screen-university menu-icon"></i>
                </a>
            </li>
        @endif
        <li class="nav-item">
            <a class="nav-link" href="{{ route('home') }}">
                <span class="menu-title">Dashboard</span>
                <i class="icon-screen-desktop menu-icon"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('fees-dashboard') }}">
                <span class="menu-title">Fees Dashboard</span>
                <i class="icon-screen-desktop menu-icon"></i>
            </a>
        </li>
        @php 
            $parentLiClassGeneral  = '';
            $ulChildOfLiGeneral    = '';
            $GeneralArr = ['my-profile','change-password'];
            if(in_array(Route::currentRouteName(), $GeneralArr)){
                $parentLiClassGeneral  = 'active';
                $ulChildOfLiGeneral    = 'show';
            }
        @endphp
        <li class="nav-item {{ $parentLiClassGeneral }}">
            <a class="nav-link" data-toggle="collapse" href="#ui-general" aria-expanded="false" aria-controls="ui-general">
                <span class="menu-title">General Setting</span>
                <i class="icon-user menu-icon"></i>
            </a>

            <div class="collapse {{ $ulChildOfLiGeneral }}" id="ui-general">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['my-profile']))  active  @endif" href="{{ route('my-profile') }}" >Profile</a></li>
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['change-password']))  active  @endif" href="{{ route('change-password') }}" >Change Password</a></li>
                </ul>
            </div>
        </li>
        @php 
            $parentLiClassSystem  = '';
            $ulChildOfLiSystem    = '';
            $SystemArr = ['user-list', 'user-add', 'user-edit', 'role-list', 'role-add', 'role-edit'];
            if(in_array(Route::currentRouteName(), $SystemArr)){
                $parentLiClassSystem  = 'active';
                $ulChildOfLiSystem    = 'show';
            }
        @endphp
        @if(Gate::check('user-list') || Gate::check('role-list'))
            <li class="nav-item {{ $parentLiClassSystem }}">
                <a class="nav-link" data-toggle="collapse" href="#ui-system" aria-expanded="false" aria-controls="ui-system">
                    <span class="menu-title">System Setting</span>
                    <i class="icon-settings menu-icon"></i>
                </a>

                <div class="collapse {{ $ulChildOfLiSystem }}" id="ui-system">
                    <ul class="nav flex-column sub-menu">
                        @can('user-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['user-list', 'user-add', 'user-edit']))  active  @endif" href="{{ route('user-list') }}" >User</a></li>
                        @endcan
                        @can('role-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['role-list', 'role-add', 'role-edit']))  active  @endif" href="{{ route('role-list') }}" >Role</a></li>
                        @endcan
                    </ul>
                </div>
            </li>
        @endif
        @php 
            $parentLiClassMaster  = '';
            $ulChildOfLiMaster    = '';
            $MasterArr = ['school-list', 'school-add', 'medium-edit', 'medium-list', 'medium-add', 'medium-edit', 'academic-year-list', 'academic-year-add', 'academic-year-edit', 'class-list', 'class-add', 'class-edit','fees-head-list', 'fees-head-add', 'fees-head-edit','class-master-list', 'class-master-add', 'class-master-edit','document-list', 'document-add', 'document-edit','category-list', 'category-add', 'category-edit','admissiontype-list', 'admissiontype-add', 'admissiontype-edit','scholarshiptype-list', 'scholarshiptype-add', 'scholarshiptype-edit'];
            if(in_array(Route::currentRouteName(), $MasterArr)){
                $parentLiClassMaster  = 'active';
                $ulChildOfLiMaster    = 'show';
            }
        @endphp
        @if(Gate::check('school-list') || Gate::check('medium-list') || Gate::check('academic-year-list') || Gate::check('class-list'))
            <li class="nav-item {{ $parentLiClassMaster }}">
                <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                    <span class="menu-title">Master</span>
                    <i class="icon-layers menu-icon"></i>
                </a>

                <div class="collapse {{ $ulChildOfLiMaster }}" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        @can('school-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['school-list', 'school-add', 'school-edit']))  active  @endif" href="{{ route('school-list') }}" >College</a></li>
                        @endcan
                        @can('medium-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['medium-list', 'medium-add', 'medium-edit']))  active  @endif" href="{{ route('medium-list') }}" >Medium / College Stream</a></li>
                        @endcan
                        @can('academic-year-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['academic-year-list', 'academic-year-add', 'academic-year-edit']))  active  @endif" href="{{ route('academic-year-list') }}" >Academic Year</a></li>
                        @endcan
                        @can('class-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['class-list', 'class-add', 'class-edit']))  active  @endif" href="{{ route('class-list') }}">Class</a></li>
                        @endcan
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['class-master-list', 'class-master-add', 'class-master-edit']))  active  @endif" href="{{ route('class-master-list') }}">Class Master</a></li>
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['designation-list', 'designation-add', 'designation-edit']))  active  @endif" href="{{ route('designation-list') }}">Designation</a></li>
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['document-list', 'document-add', 'document-edit']))  active  @endif" href="{{ route('document-list') }}">Document</a></li>
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['category-list', 'category-add', 'category-edit']))  active  @endif" href="{{ route('category-list') }}">Category</a></li>
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['admissiontype-list', 'admissiontype-add', 'admissiontype-edit']))  active  @endif" href="{{ route('admissiontype-list') }}">Admission Type</a></li>
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['scholarshiptype-list', 'scholarshiptype-add', 'scholarshiptype-edit']))  active  @endif" href="{{ route('scholarshiptype-list') }}">Scholarship Type</a></li>
                        <li class="nav-item"> <a class="nav-link" href="">Allowance</a></li>
                        <li class="nav-item"> <a class="nav-link" href="">Deduction</a></li>
                        <li class="nav-item"> <a class="nav-link" href="">Account</a></li>
                        <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['fees-head-list', 'fees-head-add', 'fees-head-edit']))  active  @endif" href="{{ route('fees-head-list') }}">Fees Head</a></li>
                        <li class="nav-item"> <a class="nav-link" href="">Leave Type</a></li>
                    </ul>
                </div>
            </li>
        @endif
        @php 
            $parentLiClassFees  = '';
            $ulChildOfLiFees    = '';
            $FeesArr = ['classfees-list','classfees-add','classfees-edit','student-list','feescollection'];
            if(in_array(Route::currentRouteName(), $FeesArr)){
                $parentLiClassFees  = 'active';
                $ulChildOfLiFees    = 'show';
            }
        @endphp
        <li class="nav-item {{ $parentLiClassFees }}">
            <a class="nav-link" data-toggle="collapse" href="#fees-ui" aria-expanded="false" aria-controls="fees-ui">
                <span class="menu-title">Fees Management</span>
                <i class="icon-home menu-icon"></i>
            </a>
            <div class="collapse {{ $ulChildOfLiFees }}" id="fees-ui">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['classfees-list','classfees-add','classfees-edit']))  active  @endif" href="{{ route('classfees-list') }}" >Class Fees</a></li>
                    <li class="nav-item">
                        <a class="nav-link">
                            <span class="menu-title">Fees Collection</span>
                        </a>
                        <!-- this id value are school id -->
                        <div class="collapse show">
                            <ul class="nav flex-column sub-menu">
                                <!-- <li class="nav-item"> <a class="nav-link" href="{{ route('student-list',['school_id' => 4]) }}" style="margin-left: 30%;">Pre Primary</a></li>
                                <li class="nav-item"> <a class="nav-link" href="{{ route('student-list',['school_id' => 1]) }}" style="margin-left: 30%;">Primary</a></li>
                                <li class="nav-item"> <a class="nav-link" href="{{ route('student-list',['school_id' => 2]) }}" style="margin-left: 30%;">Secondary</a></li> -->
                                <li class="nav-item"> <a class="nav-link" href="{{ route('student-list',['school_id' => 1]) }}" style="margin-left: 30%;">College</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
        @php 
            $parentLiClassFees  = '';
            $ulChildOfLiFees    = '';
            $feesArr = ['class-fees-report','daily-fees-report'];
            if(in_array(Route::currentRouteName(), $feesArr)){
                $parentLiClassFees  = 'active';
                $ulChildOfLiFees    = 'show';
            }
        @endphp
        <li class="nav-item {{ $parentLiClassFees }}">
            <a class="nav-link" data-toggle="collapse" href="#fReport-ui" aria-expanded="false" aria-controls="fReport-ui">
                <span class="menu-title">Fees Report</span>
                <i class="icon-wallet menu-icon"></i>
            </a>
            <div class="collapse {{ $ulChildOfLiFees }}" id="fReport-ui">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['class-fees-report']))  active  @endif" href="{{ route('class-fees-report') }}" >Class Fees</a></li>
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['daily-fees-report']))  active  @endif" href="{{ route('daily-fees-report') }}" >Daily Fees</a></li>
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['total-fees-report']))  active  @endif" href="{{ route('total-fees-report') }}" >Total Monthly Fees</a></li>
                </ul>
            </div>
        </li>
        @php 
            $parentLiClassAdmission  = '';
            $ulChildOfLiAdmission    = '';
            $StudArr = ['admission-list', 'admission-add', 'admission-edit'];
            if(in_array(Route::currentRouteName(), $StudArr)){
                $parentLiClassAdmission  = 'active';
                $ulChildOfLiAdmission    = 'show';
            }
        @endphp
        @if(Gate::check('admission-list'))
            <li class="nav-item {{ $parentLiClassAdmission }}">
                <a class="nav-link" data-toggle="collapse" href="#adm-ui" aria-expanded="false" aria-controls="adm-ui">
                    <span class="menu-title">Student Management</span>
                    <i class="icon-graduation menu-icon"></i>
                </a>

                <div class="collapse {{ $ulChildOfLiAdmission }}" id="adm-ui">
                    <ul class="nav flex-column sub-menu">
                        @can('admission-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['admission-list', 'admission-add', 'admission-edit']))  active  @endif" href="{{ route('admission-list') }}" >Admission</a></li>
                        @endcan
                    </ul>
                </div>
            </li>
        @endif
        @php 
            $parentLiClassRoom  = '';
            $ulChildOfLiClass    = '';
            $ClassArr = ['class-room-list','student-class-list'];
            if(in_array(Route::currentRouteName(), $ClassArr)){
                $parentLiClassRoom  = 'active';
                $ulChildOfLiClass    = 'show';
            }
        @endphp
        <li class="nav-item {{ $parentLiClassRoom }}">
            <a class="nav-link" data-toggle="collapse" href="#cls-ui" aria-expanded="false" aria-controls="cls-ui">
                <span class="menu-title">Class Room Management</span>
                <i class="icon-people menu-icon"></i>
            </a>
            <div class="collapse {{ $ulChildOfLiClass }}" id="cls-ui">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['student-class-list']))  active  @endif" href="{{ route('student-class-list') }}" >Student Promote</a></li>
                    <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['class-room-list']))  active  @endif" href="{{ route('class-room-list') }}" >Class List</a></li>
                    <!-- <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['class-teacher-list','class-teacher-add','class-teacher-edit']))  active  @endif" href="{{ route('class-teacher-list') }}" >Class Teacher Allotment</a></li> -->
                </ul>
            </div>
        </li>
        @php 
            $parentLiClassAttendance   = '';
            $ulChildOfLiAttendance    = '';
            $AttenArr = ['student-attendance-list', 'student-attendance-add', 'view-attendance'];
            if(in_array(Route::currentRouteName(), $AttenArr)){
                $parentLiClassAttendance   = 'active';
                $ulChildOfLiAttendance    = 'show';
            }
        @endphp
        @if(Gate::check('student-attendance-list'))
            <li class="nav-item {{ $parentLiClassAttendance  }}">
                <a class="nav-link" data-toggle="collapse" href="#atten-ui" aria-expanded="false" aria-controls="atten-ui">
                    <span class="menu-title">Attendance Management</span>
                    <i class="icon-doc menu-icon"></i>
                </a>
                <div class="collapse {{ $ulChildOfLiAttendance }}" id="atten-ui">
                    <ul class="nav flex-column sub-menu">
                        @can('student-attendance-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['student-attendance-list', 'student-attendance-add', 'view-attendance']))  active  @endif" href="{{ route('student-attendance-list') }}" >Attendance</a></li>
                        @endcan
                    </ul>
                </div>
            </li>
        @endif
        @php 
            $parentLiClassExam  = '';
            $ulChildOfLiExam    = '';
            $ExamArr = ['subject-list','subject-add','subject-edit','test-list','test-add','test-edit'];
            if(in_array(Route::currentRouteName(), $ExamArr)){
                $parentLiClassExam  = 'active';
                $ulChildOfLiExam    = 'show';
            }
        @endphp
        @if(Gate::check('subject-list') || Gate::check('test-list') || Gate::check('class-test-subject'))
            <li class="nav-item {{ $parentLiClassExam }}">
                <a class="nav-link" data-toggle="collapse" href="#exm-ui" aria-expanded="false" aria-controls="exm-ui">
                    <span class="menu-title">Exam Management</span>
                    <i class="icon-pencil menu-icon"></i>
                </a>

                <div class="collapse {{ $ulChildOfLiExam }}" id="exm-ui">
                    <ul class="nav flex-column sub-menu">
                        @can('subject-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['subject-list','subject-add','subject-edit']))  active  @endif" href="{{ route('subject-list') }}" >Subject</a></li>
                        @endcan
                        @can('test-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['test-list','test-add','test-edit']))  active  @endif" href="{{ route('test-list') }}" >Test</a></li>
                        @endcan
                        @can('class-test-subject')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['class-test-subject','assign-class-subject','assign-class-test']))  active  @endif" href="{{ route('class-test-subject') }}" >Class Test</a></li>
                        @endcan
                    </ul>
                </div>
            </li>
        @endif
        @php 
            $parentLiClassReport  = '';
            $ulChildOfLiReport    = '';
            $CertArr = ['generalregister', 'bonafied-list', 'bonafiedcertificate','leaving-certificate','character-list','character-certificate','attempt-list','attempt-certificate'];
            if(in_array(Route::currentRouteName(), $CertArr)){
                $parentLiClassReport  = 'active';
                $ulChildOfLiReport    = 'show';
            }
        @endphp
        @if(Gate::check('generalregister') || Gate::check('leaving-certificate') || Gate::check('bonafied-list') || Gate::check('character-list') || Gate::check('attempt-list'))
            <li class="nav-item {{ $parentLiClassReport }}">
                <a class="nav-link" data-toggle="collapse" href="#report-ui" aria-expanded="false" aria-controls="report-ui">
                    <span class="menu-title">Student Report</span>
                    <i class=" icon-book-open menu-icon"></i>
                </a>

                <div class="collapse {{ $ulChildOfLiReport }}" id="report-ui">
                    <ul class="nav flex-column sub-menu">
                        @can('generalregister')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['generalregister'])) active @endif" href="{{ route('generalregister') }}" >General Register</a></li>
                        @endcan
                        @can('leaving-certificate')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['leaving-certificate'])) active @endif" href="{{ route('leaving-certificate') }}" >Leaving Certificate</a></li>
                        @endcan
                        @can('bonafied-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['bonafied-list','bonafiedcertificate'])) active @endif" href="{{ route('bonafied-list') }}" >Bonafide Certificate</a></li>
                        @endcan
                        @can('character-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['character-list','character-certificate'])) active @endif" href="{{ route('character-list') }}">Character Certificate</a></li>
                        @endcan
                        @can('attempt-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['attempt-list','attempt-certificate'])) active @endif" href="{{ route('attempt-list') }}">Attempt Certificate</a></li>
                        @endcan
                    </ul>
                </div>
            </li>
        @endif
        @php 
            $parentLiStaff  = '';
            $ulChildOfLiStaff    = '';
            $EmployeeArr = ['employee-list','employee-add','employee-edit'];
            if(in_array(Route::currentRouteName(), $EmployeeArr)){
                $parentLiStaff  = 'active';
                $ulChildOfLiStaff    = 'show';
            }
        @endphp
        @if(Gate::check('employee-list'))
            <li class="nav-item {{ $parentLiStaff }}">
                <a class="nav-link" data-toggle="collapse" href="#staff-ui" aria-expanded="false" aria-controls="staff-ui">
                    <span class="menu-title">Staff Management</span>
                    <i class="icon-note menu-icon"></i>
                </a>
                <div class="collapse {{ $ulChildOfLiStaff }}" id="staff-ui">
                    <ul class="nav flex-column sub-menu">
                        @can('employee-list')
                            <li class="nav-item"> <a class="nav-link @if(in_array(Route::currentRouteName(), ['employee-list','subject-add','subject-edit']))  active  @endif" href="{{ route('employee-list') }}" >Staff Details</a></li>
                        @endcan
                    </ul>
                </div>
            </li>
        @endif
    </ul>
</nav>
<!-- partial -->