<!-- plugins:css -->
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/simple-line-icons/css/simple-line-icons.css') }}">
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/flag-icon-css/css/flag-icon.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/css/vendor.bundle.base.css') }}">
<!-- endinject -->
<!-- Plugin css for this page -->
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/chartist/chartist.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
<!-- End plugin css for this page -->
<!-- Layout styles -->
<link rel="stylesheet" href="{{ asset('public/Stellar-master/css/style.css') }}">
<!-- End layout styles -->
<link rel="shortcut icon" href="{{ asset('public/Stellar-master/favicon.png') }}" />
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/select2/select2.min.css') }}">

<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/select2-bootstrap-theme/select2-bootstrap.min.css') }}">
<style> 
    .content-wrapper {
        padding: 0.75rem 1.5rem 0 !important;
    }
    .page-header {margin: 0px !important}
    .custom-btn{padding: 0.5rem 1.0rem !important;}

    /***** Example custom styling *****/
    #computerupload {
        /* visibility: hidden etc. wont work */
        width: 0.1px;
        height: 0.1px;
        opacity: 0;
        overflow: hidden;
        position: absolute;
        z-index: -1;
    }
    .footer {
        position: fixed;
        bottom: 0;
        width: 100%;
        text-align: center;
    }
    .table th, .table td {
        padding: 0.7rem;
    }

    .bg-blue {
        background-color:#343a40 !important;
    }

    .radio-group{float: left;width: 30%;}
    .radio-group label{margin-left: 10px;}
    .admission-rdio .demo-radio-button .label-radio{ min-width: auto;  float: left;}

    .form-group .form-line {
    width: 100%;
    position: relative;
    border-bottom: 1px solid #ddd;
}
.border-class{border: 1px solid #000;padding: 20px 40px; margin:10px 90px 10px 90px; }

.float-div img{width: 100px;}
.float-div{float: left;}
.heading-div{text-align: center;}
.heading-div h1{text-align: center;margin-top: 0; font-size: 22px;}
.heading-div h2{font-size: 20px;}
.border-class{border: 1px solid #000;padding: 20px 40px; margin:10px 90px 10px 90px; }
.clear-both{clear: both;}
.float-right{float: right;}
.text-center{text-align: center;}
.header-div p{margin:.5rem 0 .5rem 0; line-height: 26px;}
.footer-div{padding-top: 70px;}
.header-div{padding-top: 30px;}
.square {
height: 80px;
width: 80px;
border: 1px solid;
float: right;
}
 
.certificate p{line-height: 15px; margin-top: 0px; margin-bottom: 0px;}
.certificate{font-size: 11px;margin-top: 5px;}
.eli-table td{padding-left: 10px; padding-right: 10px;}
.eli-table1 table tr td{border: 1px solid black;}
label.errors{color: red;}
select.form-control{color: black;}
</style>