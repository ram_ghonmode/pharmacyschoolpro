<!-- partial:partials/_navbar.html -->
<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="navbar-brand-wrapper d-flex align-items-center">
        <h3 style="color:#ffff;">SBGCP ERP</h3>
        <!-- <a class="navbar-brand brand-logo" href="index.html">
        <img src="{{ asset('public/Stellar-master/images/logo.svg') }}" alt="logo" class="logo-dark" />
        </a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{ asset('public/Stellar-master/images/logo-mini.svg') }}" alt="logo" /></a> -->
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center flex-grow-1">
        <h5 class="mb-0 font-weight-medium d-none d-lg-flex">Welcome SBGCP ERP dashboard!</h5>
        <ul class="navbar-nav navbar-nav-right ml-auto">
        <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
            <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            <img class="img-xs rounded-circle ml-2" @if(!empty(Auth::user()->user_img_url)) src="{{ Auth::user()->user_img_url }}" @else src="{{ asset('public/Stellar-master/images/faces/photo-512.png') }}" @endif alt="Profile image"> <span class="font-weight-normal"> {{ ucfirst(Auth::user()->name) }} </span></a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                    <img class="img-md rounded-circle" src="{{ asset('public/Stellar-master/images/faces/photo-512.png') }}" width="40px"alt="Profile image">
                    <p class="mb-1 mt-3">{{ !empty(Auth::user()->name) ? ucfirst(Auth::user()->name) : '' }}</p>
                    <p class="font-weight-light text-muted mb-0">{{ !empty(Auth::user()->email) ? (Auth::user()->email) : '' }}</p>
                </div>
                <a class="dropdown-item" href="{{ route('my-profile') }}"><i class="dropdown-item-icon icon-user text-primary"></i> My Profile</a>
                <a class="dropdown-item" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="dropdown-item-icon icon-power text-primary"></i>Sign Out</a>
                
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="icon-menu"></span>
        </button>
    </div>
</nav>
<!-- partial -->