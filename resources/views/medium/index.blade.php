@extends('layouts.dash')   
@section('title', 'Medium / College Stream List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Medium / College Stream </h3>
        @can('medium-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('medium-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Medium / College Stream </th>
                                <th>Type</th>
                                @if(Gate::check('medium-edit') || Gate::check('medium-delete'))
                                    <th width="18%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($mediums) && count($mediums) > 0)
                            <?php  $count = $mediums->firstItem(); ?>
                                @foreach($mediums as $key => $medium)
                                    <?php 
                                        $encyId = Controller::cryptString($medium->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $medium->name }}</td>
                                        <td>{{ $medium->type ?? '' }}</td>
                                        @if(Gate::check('medium-edit') || Gate::check('medium-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('medium-edit')
                                                        <a href="{{ route('medium-edit', ['id' => $encyId]) }}">Edit</a>
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('medium-delete')
                                                        <a href="{{ url('/dashboard/'.$medium->id.'/medium_streams')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif       
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $mediums->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection