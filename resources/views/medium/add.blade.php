@extends('layouts.dash')    
@section('title', 'Medium / College Stream')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Medium / College Stream </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('medium-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('medium-edit', ['id' => isset($medium->id) ? Controller::cryptString($medium->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id=""  placeholder="Enter name" name="name" value="{{ old('name', isset($medium->name) ? $medium->name : '' ) }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Type <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="type">
                                    <option value="">Select type</option>
                                    <option value="Medium" @if(old('type', isset($medium->type) ? $medium->type : '') == 'Medium') selected @endif>Medium</option>
                                    <option value="Stream" @if(old('type', isset($medium->type) ? $medium->type : '') == 'Stream') selected @endif>Stream</option>
                                </select>
                                @if ($errors->has('type'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('type') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection