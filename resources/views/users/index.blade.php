@extends('layouts.dash')   
@section('title', 'User List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> User </h3>
        @can('user-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('user-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">Sr. No.</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email/Login Id</th>
                                <th>Schhol</th>
                                <th>Role</th>
                                <th>Status</th>
                                @if(Gate::check('user-edit') || Gate::check('user-delete'))
                                    <th width="10%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($users) && count($users) > 0)
                                <?php  $count = $users->firstItem(); ?>
                                @foreach($users as $key => $user)
                                    <?php 
                                        $roleName = '';
                                        $encyId = Controller::cryptString($user->id, 'e'); 
                                        if(!empty($user->roles) && count($user->roles) > 0){
                                            $roleName = $user->roles[0]->name;
                                        } 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->mobile_no }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ !empty($school) ? $school : '' }}</td>
                                        <td>{{ $roleName }}</td>
                                        <td>{{ $user->is_active ? 'Active' : 'Inactive' }}</td>
                                        @if(Gate::check('user-edit') || Gate::check('user-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('user-edit')
                                                        <a href="{{ route('user-edit', ['id' => $encyId]) }}">Edit</a>  
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('user-delete')   
                                                        <a href="{{ url('/dashboard/'.$user->id.'/users')}}" onclick="return confirm('Are you sure?')">Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif  
                                    </tr> 
                                @endforeach
                            @endif  
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $users->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection