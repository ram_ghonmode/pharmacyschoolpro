@extends('layouts.dash')    
@section('title', 'User')
@section('content')   
<?php use App\Http\Controllers\Controller; 
    $reqLabel = '<sup class="text-danger">*</sup>'; 
    $prevRoleId = !empty($user->roles[0]) ? $user->roles[0]->id : NULL;
    $prevSchoolId = !empty($user->schools[0]) ? $user->schools[0]->id : NULL;
?> 
    <div class="page-header">
        <h3 class="page-title"> User </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('user-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('user-edit', ['id' => isset($user->id) ? Controller::cryptString($user->id, 'e') : '']) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter name" name="name" value="{{ old('name', isset($user->name) ? $user->name : '' ) }}">
                                @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Email <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter email" name="email" value="{{ old('email', isset($user->email) ? $user->email : '' ) }}">
                                @if ($errors->has('email'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('email') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Mobile No. <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter mobile no." name="mobile_no" value="{{ old('mobile_no', isset($user->mobile_no) ? $user->mobile_no : '' ) }}">
                                @if ($errors->has('mobile_no'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('mobile_no') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Role <?php echo $reqLabel; ?></label>
                                <select name="role_id" id="" class="form-control">
                                    <option value="">Select role</option>
                                    @foreach($roles as $id => $name)
                                        <option value="{{ $id }}" @if(old('role_id', $prevRoleId) == $id) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('role_id'))
                                    <span class="text-danger" role="alert">
                                        <small>{{ $errors->first('role_id') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Password <?php echo $reqLabel; ?></label>
                                <input type="password" class="form-control" placeholder="Enternew password" name="password" value="{{ old('password') }}">
                                @if ($errors->has('password'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('password') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Confirm New Password <?php echo $reqLabel; ?></label>
                                <input type="password" class="form-control" placeholder="Enter confirm password" name="confirm_password" value="{{ old('confirm_password') }}">
                                @if ($errors->has('confirm_password'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('confirm_password') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="storeId">Select School <?php echo $reqLabel; ?></label>
                                <select name="school_id" class="form-control" style="width: 100%;">
                                    <option value="">Select school</option>
                                    @foreach($schools as $id => $name)
                                        <option value="{{ $id }}" @if(old('school_id', $prevSchoolId) == $id) Selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('school_id'))
                                    <span class="text-danger" role="alert">
                                        <small>{{ $errors->first('school_id') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_active" @if(!empty($user)) @if($user->is_active) checked @endif @else empty($user) checked @endif> <strong>Is Active ?</strong>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection