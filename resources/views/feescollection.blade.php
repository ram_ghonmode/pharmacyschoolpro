@extends('layouts.dash')
@section('title', 'Dashboard')
@section('content')
    @if(!empty($data['schools']) && count($data['schools']) > 0)
        @foreach($data['schools'] as $key => $val)
            <div class="row">
                <div class="col-md-12 grid-margin">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-sm-flex align-items-baseline report-summary-header">
                                        <h5 class="font-weight-semibold">{{$val->school_name}}</h5> <span class="ml-auto">Updated Report</span> <button class="btn btn-icons border-0 p-2"><i class="icon-refresh"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="row report-inner-cards-wrapper">
                                <div class=" col-md -6 col-xl report-inner-card">
                                    <div class="inner-card-text">
                                        <span class="report-title">Total Student</span>
                                        <h4>{{ $data['totStudent'][$val->id] }}</h4>
                                    </div>
                                    <div class="inner-card-icon bg-success">
                                        <i class="icon-people"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl report-inner-card">
                                    <div class="inner-card-text">
                                        <span class="report-title">Total Amount</span>
                                        <h4>₹ {{ $data['totalFees'][$val->id] }}</h4>
                                    </div>
                                    <div class="inner-card-icon bg-danger">
                                        <i class="icon-wallet"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl report-inner-card">
                                    <div class="inner-card-text">
                                        <span class="report-title">Fees Collected</span>
                                        <h4>₹ {{ $data['collectedFees'][$val->id] }}</h4>
                                    </div>
                                    <div class="inner-card-icon bg-warning">
                                        <i class="icon-wallet"></i>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl report-inner-card">
                                    <div class="inner-card-text">
                                        <span class="report-title">Expected Fees To Be Recover</span>
                                        <h4>₹ {{ $data['totalFees'][$val->id] - $data['collectedFees'][$val->id] }}</h4>
                                    </div>
                                    <div class="inner-card-icon bg-primary">
                                        <i class="icon-wallet"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
@endsection
