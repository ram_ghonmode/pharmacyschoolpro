@extends('layouts.dash')   
@section('title', 'Class')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Class Master </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('class-master-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Class Name </th>
                                <!-- <th>College Name </th> -->
                                <th>Academic Year </th>
                                <!-- <th>College Stream</th> -->
                                <!-- <th>Category</th> -->
                                <th>Admission Type</th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classes) && count($classes) > 0)
                            <?php  $count = $classes->firstItem(); ?>
                                @foreach($classes as $key => $class)
                                    <?php 
                                        $encyId = Controller::cryptString($class->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $class->classes->class_name }}</td>
                                        <!-- <td>{{ $class->school->school_name ?? '' }}</td> -->
                                        <td>{{ $class->academicYears->academic_year }}</td>
                                        <!-- <td>{{ $class->stream->name ?? '' }}</td> -->
                                        <!-- <td>{{ $class->categories->category_name ?? '' }}</td> -->
                                        <td>{{ $class->admissionType->admission_type ?? '' }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('class-master-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$class->id.'/class_masters')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $classes->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection