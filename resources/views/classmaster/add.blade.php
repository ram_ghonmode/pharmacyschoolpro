@extends('layouts.dash')    
@section('title', 'Class')
@section('content')   
<?php use App\Http\Controllers\Controller; $categories = ['Open', 'OBC', 'SC', 'ST', 'NT', 'SBC', 'VJ', 'NT-B', 'NT-C', 'NT-D']; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 
    <div class="page-header">
        <h3 class="page-title"> Class </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('class-master-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('class-master-edit', ['id' => isset($classMaster->id) ? Controller::cryptString($classMaster->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-4">
                                <label for="">Class <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="class_id">
                                    <option value="">Select class</option>
                                    @foreach($data['classes'] as $id => $name)
                                        <option value="{{$id}}" @if(old('class_id', isset($classMaster->class_id) ? $classMaster->class_id : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('class_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('class_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="">College <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="school_id">
                                    <!-- <option value="">Select school</option> -->
                                    @foreach($data['schools'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('school_id', isset($classMaster->school_id) ? $classMaster->school_id : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('school_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('school_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Academic Year <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="academic_year">
                                    <option value="">Select academic year</option>
                                        @foreach($data['years'] as $id => $name)
                                            <option value="{{ $id }}" @if(old('academic_year', isset($classMaster->academic_year) ? $classMaster->academic_year : '') == $id ) selected @endif>{{ $name }}</option>
                                        @endforeach
                                </select>
                                {{ $school->academicYears->academic_year ?? '' }}
                                @if ($errors->has('academic_year'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('academic_year') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-4 form-group">
                                <label for="">Addmission Type <?php echo $reqLabel; ?></label>
                                <select name="admission_type_id" class="form-control">
                                    <option value="">Select Addmission-Type</option>
                                    @foreach($data['admissionType']  as $id => $scType)
                                        <option value="{{ $id }}" @if(old('admission_type_id', isset($classMaster->admission_type_id) ? $classMaster->admission_type_id : '') == $id) selected @endif>{{ $scType }}</option>
                                     @endforeach
                                </select>
                                @if ($errors->has('school_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('school_id') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection