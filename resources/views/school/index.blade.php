@extends('layouts.dash')   
@section('title', 'College List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> College </h3>
        @can('school-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('school-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="10%">Sr. No.</th>
                                <th>College Name</th>
                                <th>Phone No.</th>
                                <th>Academic Year</th>
                                @if(Gate::check('school-edit') || Gate::check('school-delete'))
                                    <th width="18%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($schools) && count($schools) > 0)
                            <?php  $count = $schools->firstItem(); ?>
                                @foreach($schools as $key => $school)
                                    <?php 
                                        $roleName = '';
                                        $encyId = Controller::cryptString($school->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $school->school_name ?? '' }}</td>
                                        <td>{{ $school->phone_no ?? '' }}</td>
                                        <td>{{ $school->academicYears->academic_year ?? '' }}</td>
                                        @if(Gate::check('school-edit') || Gate::check('school-delete'))
                                            <td>
                                                <div class="btn-group">
                                                @can('school-edit')
                                                    <a href="{{ route('school-edit', ['id' => $encyId]) }}">Edit</a>
                                                    &nbsp; | &nbsp;
                                                @endcan
                                                @can('school-delete')
                                                    <a href="{{ url('/dashboard/'.$school->id.'/schools')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                                @endcan
                                                </div>
                                            </td>
                                        @endif
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $schools->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection