@extends('layouts.dash')    
@section('title', 'College')
@section('content')   
<?php use App\Http\Controllers\Controller; $years = ['2018-19', '2019-20', '2020-21', '2021-22', '2022-23', '2023-24', '2024-25'];  $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> College </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('school-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('school-edit', ['id' => isset($school->id) ? Controller::cryptString($school->id, 'e') : '']) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                        <div class="row clearfix">
                            <div class="col-sm-3 upload-img-div text-center">
                            <?php  ?> 
                                <?php $scr = isset($school->logo) ? $school->logo : asset("public/Stellar-master/images/brokanupload-img.png");?>
                                <img class="upload-img" id="uploadimgId" onerror="this.src='{{ $scr }}';" src="" alt="your image" style="width:200px;height:200px" />
                                <br>
                                <input type='file' name="logo" id="computerupload" onchange="readURL(this);"  value="{{ old('logo', isset($school->logo) ? $school->logo : '' ) }}"/>
                                <label class="btn btn-primary" for="computerupload" style="padding: 0.5rem 1rem; margin-top:8px;" >Upload</label>
                                <button class="btn btn-secondary" id="resetupload" type="button" style="padding: 0.5rem 1rem;">Reset</button><br>
                                @if ($errors->has('logo'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('logo') }}</small>
                                    </span>
                                @endif
                            </div>
                            <!-- <input type='hidden' name="logo" id="dynaImg"  value=""/> -->
                            <div class="col-sm-9">
                                <div class="row clearfix">
                                    <div class="col-sm-6 form-group">
                                        <label for="">Institute / Management Name <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter institute / management name" name="institute_name" value="{{ old('institute_name', isset($school->institute_name) ? $school->institute_name : '' ) }}">
                                        
                                        @if ($errors->has('institute_name'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('institute_name') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-6 form-group">
                                        <label for="">College Name <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" id="" placeholder="Enter college name" name="school_name" value="{{ old('school_name', isset($school->school_name) ? $school->school_name : '' ) }}">
                                        @if ($errors->has('school_name'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('school_name') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-6 form-group">
                                        <label for="">College Name In Marathi</label>
                                        <input type="text" class="form-control" id="" placeholder="Enter college name in marathi" name="school_name_marathi" value="{{ old('school_name_marathi', isset($school->school_name_marathi) ? $school->school_name_marathi : '' ) }}">
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="">Academic Year <?php echo $reqLabel; ?></label>
                                        <select class="form-control" id="" name="academic_year">
                                            <option value="">Select academic year</option>
                                            @foreach($data['years'] as $id => $name)
                                                <option value="{{ $id  }}" @if(old('academic_year', isset($school->academic_year) ? $school->academic_year : '') == $id ) selected @endif>{{ $name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('academic_year'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('academic_year') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <label for="">Session <?php echo $reqLabel; ?></label>
                                        <select class="form-control" id="" name="session">
                                            <option value="">Select session</option>
                                            <option value="Winter" @if(old('session', isset($school->session) ? $school->session : '') == 'Winter') selected @endif>Winter</option>
                                            <option value="Summer" @if(old('session', isset($school->session) ? $school->session : '') == 'Summer') selected @endif>Summer</option>
                                        </select>
                                        @if ($errors->has('session'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('session') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="form-group col-sm-12">
                                        <label for="">Address <?php echo $reqLabel; ?></label>
                                        <input type="text" class="form-control" placeholder="Enter address" name="address"  value="{{ old('address', isset($school->address) ? $school->address : '' ) }}"> 
                                        @if ($errors->has('address'))
                                            <span class="text-danger">
                                                <small>{{ $errors->first('address') }}</small>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Country <?php echo $reqLabel; ?></label>
                                <select class="form-control" id="" name="country_id">
                                    <option value="">Select country</option>
                                    @foreach($data['countries'] as $k => $country)
                                        <option value="{{ $k }}" @if(old('country_id', isset($school->country_id) ? $school->country_id : '') == $k) selected @endif>{{ $country }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('country_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">State <?php echo $reqLabel; ?></label>
                                {{--<select class="form-control" id="" name="state_id">
                                    <option value="">Select state</option>
                                    @foreach($data['states'] as $k => $state)
                                        <option value="{{ $state->id }}"  @if(old('state_id', isset($school->state_id) ? $school->state_id : '') == $state->id) selected @endif>{{ $state->state_name }}</option>
                                    @endforeach
                                </select>--}}
                                <input type="text" class="form-control" placeholder="Enter State" name="state_id"  value="{{ old('state_id', isset($school->state_id) ? $school->state_id : '' ) }}"> 
                                @if ($errors->has('state_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('state_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">City <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" placeholder="Enter City" name="city_id"  value="{{ old('city_id', isset($school->city_id) ? $school->city_id : '' ) }}"> 
                                @if ($errors->has('city_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('city_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Tahsil </label>
                                <input type="text" class="form-control" placeholder="Enter Tahsil" name="tahsil_id"  value="{{ old('tahsil_id', isset($school->tahsil_id) ? $school->tahsil_id : '' ) }}"> 
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Pincode </label>
                                <input type="text" class="form-control" placeholder="Enter pincode" name="pin_code" value="{{ old('pin_code', isset($school->pin_code) ? $school->pin_code : '' ) }}"> 
                                @if ($errors->has('pin_code'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('pin_code') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Phone No. <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter phone no." name="phone_no" value="{{ old('phone_no', isset($school->phone_no) ? $school->phone_no : '' ) }}">
                                @if ($errors->has('phone_no'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('phone_no') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Fax No.</label>
                                <input type="text" class="form-control" id="" placeholder="Enter fax no." name="fax_no"  value="{{ old('fax_no', isset($school->fax_no) ? $school->fax_no : '' ) }}">
                                
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Recognition No.</label>
                                <input type="text" class="form-control" id="" placeholder="Enter recognition no." name="recognition_no"  value="{{ old('recognition_no', isset($school->recognition_no) ? $school->recognition_no : '' ) }}">
                                
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                                <label for="">Index No.</label>
                                <input type="text" class="form-control" id="" placeholder="Enter index no." name="index_no" value="{{ old('index_no', isset($school->index_no) ? $school->index_no : '' ) }}">
                               
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Website Url</label>
                                <input type="text" class="form-control" id="" placeholder="Enter website url" name="website_url" value="{{ old('website_url', isset($school->website_url) ? $school->website_url : '' ) }}">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Email</label>
                                <input type="email" class="form-control" id="" placeholder="Enter email" name="email" value="{{ old('email', isset($school->email) ? $school->email : '' ) }}">
                               
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">UDISE No.</label>
                                <input type="text" class="form-control" id="" placeholder="Enter UDISE No." name="udise_no" value="{{ old('udise_no', isset($school->udise_no) ? $school->udise_no : '' ) }}">
                                
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                                <label for="">Board</label>
                                <input type="text" class="form-control" id="" placeholder="Enter board" name="board" value="{{ old('board', isset($school->board) ? $school->board : '' ) }}">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Establishment Year</label>
                                <input type="text" class="form-control" id="" placeholder="Enter establishment year" name="establishment_year" value="{{ old('establishment_year', isset($school->establishment_year) ? $school->establishment_year : '' ) }}">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Organisation granted From Date</label>
                                <input type="date" class="form-control" id=""  name="granted_from_date" value="{{ old('granted_from_date', isset($school->granted_from_date) ? $school->granted_from_date : '' ) }}">
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Organisation granted End Date</label>
                                <input type="date" class="form-control" id=""  name="granted_end_date" value="{{ old('granted_end_date', isset($school->granted_end_date) ? $school->granted_end_date : '' ) }}">
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3 form-group">
                                <label for="">Principal Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter principal name" name="principal_name" value="{{ old('principal_name', isset($school->principal_name) ? $school->principal_name : '' ) }}">
                                @if ($errors->has('principal_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('principal_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Principal Mobile No. <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id=""  placeholder="Enter principal mobile no." name="principal_mob_no" value="{{ old('principal_mob_no', isset($school->principal_mob_no) ? $school->principal_mob_no : '' ) }}">
                                @if ($errors->has('principal_mob_no'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('principal_mob_no') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Sport Teacher Name <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id="" placeholder="Enter sport teacher name" name="sport_teacher_name" value="{{ old('sport_teacher_name', isset($school->sport_teacher_name) ? $school->sport_teacher_name : '' ) }}">
                                @if ($errors->has('sport_teacher_name'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('sport_teacher_name') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Sport Teacher Mobile No. <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id=""  placeholder="Enter sport teacher mobile no." name="sport_teacher_mob_no" value="{{ old('sport_teacher_mob_no', isset($school->sport_teacher_mob_no) ? $school->sport_teacher_mob_no : '' ) }}">
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection