@extends('layouts.dash')   
@section('title', 'Total Fess Report')
@section('content')  
    <div class="page-header">
        <h3 class="page-title"> Total Fess Report </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" id="totalFeesFormId" action="{{ route('total-fees-report') }}">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <p>School<select name="school_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">School</option>
                                    @foreach($data['schools'] as $skey => $value)
                                        <option value="{{ $skey }}" @if(Request::get('school_id') == $skey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select></p>
                            </div>
                            <div class="col-sm-3">
                                <p>Academic year<select name="academic_year" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Academic Year</option>
                                    @foreach($data['years'] as $ckey => $value)
                                        <option value="{{ $ckey }}" @if(Request::get('academic_year') == $ckey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select></p>
                            </div>
                            <div class="col-sm-3">
                                <p>From:-<input type="date" name="from_date" value="{{ old('date',!empty(Request::get('date')) ? date('Y-m-d',strtotime(Request::get('date'))) : '') }}" class="form-control" style="border: none;border-bottom: 1px solid #cbced3;"/></p>
                            </div>
                            <div class="col-sm-3">
                                <p>To:-<input type="date" name="to_date" value="{{ old('date',!empty(Request::get('date')) ? date('Y-m-d',strtotime(Request::get('date'))) : '') }}" class="form-control" style="border: none;border-bottom: 1px solid #cbced3;"/></p>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary btn-sm waves-effect" >GET</button>
                            </div>
                            @if(!empty($feesRecord))
                                <a onclick="PrintElem('#printableArea');" target="_blank"><button type="button" class="btn btn-info btn-sm pull-right">Print</button></a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card" id="printableArea">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-div">
                               <img src='{{ asset("public/images/logo.png") }}'style="width: 120px; height:150px;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <center>
                                <h3>Total Monthly Fees</h3>
                                <h5>Sudam Shikshan Prasarak Mandal</h5>
                            </center>  
                        </div>
                    </div>
                    <center>
                        <h4>{{ !empty($data['school']['school_name']) ? $data['school']['school_name'] : '' }}</h4>
                        <br>
                        <h6>From Date:-{{ !empty($data['school']['school_name']) ?date('d-m-Y',strtotime($upcond['from_date']))  : '' }} &nbsp;&nbsp;  To Date:- &nbsp;{{ !empty($data['school']['school_name']) ?date('d-m-Y',strtotime($upcond['to_date']))  : '' }}</h6>

                        
                    </center>
                    <div class="table-responsive">
                        <table class="table table-bordered classRoomListTable"border="1"  cellspacing="0" cellpadding="3" width="100%" >
                            <thead>
                                <tr>
                                    <th>Sr No.</th>
                                    <th>Fees Date</th>
                                    <th>GR No.</th>
                                    <th>Receipt No.</th>
                                    <th>Student Name</th>
                                    <th>Class</th>
                                    <!-- <th>Building Fund</th> -->
                                    <th>Development Fund</th>
                                    <!-- <th>Donation</th> -->
                                    <th>Basic Amount</th>
                                    <th>Total Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($feesRecord))
                                    @foreach($feesRecord as $key => $data)
                                   <?php 
                                     $devlopmentFee = 0;
                                     $buildingFee = 0;
                                     $donation = 0;
                                   ?>
                                    @foreach($data->studentYearlyFeesPaid as $feeKey => $feeVal)
                                        <?php 
                                           
                                            if($feeVal->head_id == 2) {
                                                $devlopmentFee = $feeVal->amount;
                                            }
                                            if($feeVal->head_id == 3) {
                                                if (!empty($data['building_fund']) && ($data['building_fund']) != 0) {
                                                    $buildingFee = $data['building_fund'];
                                                }else{
                                                    $buildingFee = $feeVal->amount;
                                                }
                                                
                                            }
                                            if($feeVal->head_id == 10) {
                                                $donation += $feeVal->amount;
                                            }
                                            if($feeVal->head_id == 11) {
                                                $donation += $feeVal->amount;
                                            }
                                            if($feeVal->head_id == 12) {
                                                $donation += $feeVal->amount;
                                            }
                                        ?>
                                    @endforeach
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td>{{ date('d-m-Y',strtotime($data['date'])) }}</td>
                                            <td>{{ $data->gr_number }}</td>
                                            <td>{{ $data['receipt_no'] }}</td>
                                            <td>{{ $data['admission']['first_name'].' '.$data['admission']['middle_name'].' '.$data['admission']['last_name'] }}</td>
                                            <td>{{ $data['admission']['classes']['class_name']}}</td>
                                            <!-- <td>{{ $buildingFee ?? 0 }}</td> -->
                                            <td>{{ $devlopmentFee ?? 0  }}</td>
                                            <!-- <td>{{ $donation ?? 0 }}</td> -->
                                            <td>{{ $data['amount']-$buildingFee-$devlopmentFee-$donation }}</td>
                                            <td>{{ $data['amount'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif    
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="6">Total</th>
                                    <!-- <th>{{ $totBuildingFee }}</th> -->
                                    <th>{{ $totDevlopmentFee }}</th>
                                    <!-- <th>{{ $totDonation }}</th> -->
                                    <th>{{ $totPayAmt-$totBuildingFee-$totDevlopmentFee-$totDonation }}</th>
                                    <th>{{ $totPayAmt }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection