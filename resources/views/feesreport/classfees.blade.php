@extends('layouts.dash')   
@section('title', 'Class Fees Report')
@section('content')  
    <div class="page-header">
        <h3 class="page-title"> Class Fees Report </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="GET" id="classFeesFormId" action="{{ route('class-fees-report') }}">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <select name="school_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">School</option>
                                    @foreach($data['schools'] as $skey => $value)
                                        <option value="{{ $skey }}" @if(Request::get('school_id') == $skey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="class_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Class</option>
                                    @foreach($data['classes'] as $ckey => $value)
                                        <option value="{{ $ckey }}" @if(Request::get('class_id') == $ckey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <select name="academic_year" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="">Academic Year</option>
                                    @foreach($data['years'] as $ckey => $value)
                                        <option value="{{ $ckey }}" @if(Request::get('academic_year') == $ckey) selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary btn-sm waves-effect" >GET</button>
                            </div>
                            @if(!empty($studList))
                                <a onclick="PrintElem('#printableArea');" target="_blank"><button type="button" class="btn btn-info btn-sm pull-right">Print</button></a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card" id="printableArea">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="float-div">
                               <img src='{{ asset("public/images/logo.png") }}'style="width: 120px; height:150px;">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <center>
                                <h3>Total Class Fees</h3>
                                <h5>Sudam Shikshan Prasarak Mandal</h5>
                            </center>  
                        </div>
                    </div>
                    <center>
                        <h5>{{ !empty($data['school']['school_name']) ? $data['school']['school_name'] : '' }}</h5>
                    </center>
                    <table class="table table-bordered" border="1"  cellspacing="0" cellpadding="3" width="100%">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>GR No.</th>
                                <th>Student Name</th>
                                <th>Class</th>
                                <th>Academic Year</th>
                                <th>Total Fees</th>
                                <th>Paid Fees</th>
                                <th>Remaining Fees</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($studList))
                                @foreach($studList as $key => $data)
                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $data['gr_number'] }}</td>
                                        <td>{{ $data['first_name'].' '.$data['middle_name'].' '.$data['last_name'] }}</td>
                                        <td>{{ $data['current_class'] ?? '' }}</td>
                                        <td>{{ $data['academic_year'] ?? '' }}</td>
                                        <td>{{ $data['amount'] ?? '' }}</td>
                                        <td>{{ $data['payfee'] ?? '' }}</td>
                                        <td>{{ $data['remainingpayfee'] ?? '' }}</td>
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection