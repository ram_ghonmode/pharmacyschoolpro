@extends('layouts.dash')   
@section('title', 'Class')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Class </h3>
        @can('class-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('class-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Class Name </th>
                                @if(Gate::check('class-edit') || Gate::check('class-delete'))
                                    <th width="18%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classes) && count($classes) > 0)
                            <?php  $count = $classes->firstItem(); ?>
                                @foreach($classes as $key => $class)
                                    <?php 
                                        $encyId = Controller::cryptString($class->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $class->class_name }}</td>
                                        @if(Gate::check('class-edit') || Gate::check('class-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('class-edit')
                                                        <a href="{{ route('class-edit', ['id' => $encyId]) }}">Edit</a>
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('class-delete')
                                                        <a href="{{ url('/dashboard/'.$class->id.'/class_lists')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif       
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $classes->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection