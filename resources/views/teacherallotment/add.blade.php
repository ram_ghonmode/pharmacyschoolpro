@extends('layouts.dash')    
@section('title', 'Class Teacher Allotment')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 
<style>
    .hidden{display:none;}
</style>
    <div class="page-header">
        <h3 class="page-title"> Class Teacher Allotment </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('class-teacher-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('class-teacher-edit', ['id' => isset($teacher->id) ? Controller::cryptString($teacher->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="col-sm-6 form-group">
                                <label for="">School <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="school_id" id="SchoolId">
                                    <option value="">Select school</option>
                                    @foreach($data['schools'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('school_id', isset($teacher->school_id) ? $teacher->school_id : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('school_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('school_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Class <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="class_id">
                                    <option value="">Select school</option>
                                    @foreach($data['classes'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('class_id', isset($teacher->class_id) ? $teacher->class_id : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('class_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('class_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="">Class Section <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="section">
                                    <option value="">Select section</option>
                                    <option value="A" @if(old('section', isset($teacher->section) ? $teacher->section : '') == 'A') selected @endif>A</option>
                                    <option value="B" @if(old('section', isset($teacher->section) ? $teacher->section : '') == 'B') selected @endif>B</option>
                                    <option value="C" @if(old('section', isset($teacher->section) ? $teacher->section : '') == 'C') selected @endif>C</option>
                                    <option value="D" @if(old('section', isset($teacher->section) ? $teacher->section : '') == 'D') selected @endif>D</option>
                                    <option value="E" @if(old('section', isset($teacher->section) ? $teacher->section : '') == 'E') selected @endif>E</option>
                                </select>
                                @if ($errors->has('section'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('section') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix"> 
                            <div class="col-sm-3 form-group">
                                <label for="">Teacher <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="teacher_id" id="TeacherId">
                                    <option value="">Select Teacher</option>
                                    @foreach($empTeacher as $id => $val)
                                        <option class="hidden" data-school="{{ $val->school_id }}" @if( old('teacher_id', isset($teacher->teacher_id) ? $teacher->teacher_id : '') == $val->empid) Selected @endif value="{{ $val->empid }}">{{ $val->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('teacher_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('teacher_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3 form-group">
                                <label for="">Academic Year <?php echo $reqLabel; ?></label>
                                <select class="form-control" name="academic_year">
                                    <option value="">Select academic year</option>
                                    @foreach($data['years'] as $id => $name)
                                        <option value="{{ $id  }}" @if(old('academic_year', isset($teacher->academic_year) ? $teacher->academic_year : '') == $id ) selected @endif>{{ $name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('academic_year'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('academic_year') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection