@extends('layouts.dash')   
@section('title', 'Class Teacher Allotment')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Class Teacher Allotment </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('class-teacher-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            <a href="{{ route('print-teacher-list') }}" target="_blank"><button type="button" class="btn btn-info custom-btn">Print</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sr.No.</th>
                                <th>Teacher</th>
                                <th>Class</th>
                                <th>Section</th>
                                <th>Academic Year</th>
                                <th width="18%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($clsTeachers) && count($clsTeachers) > 0)
                            <?php  $count = $clsTeachers->firstItem(); ?>
                                @foreach($clsTeachers as $key => $teacher)
                                    <?php 
                                        $encyId = Controller::cryptString($teacher->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $teacher->employees->name ?? ''}}</td>
                                        <td>{{ $teacher->classes->class_name }}</td>
                                        <td>{{ $teacher->section }}</td>
                                        <td>{{ $teacher->academicYears->academic_year }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="{{ route('class-teacher-edit', ['id' => $encyId]) }}">Edit</a>
                                                &nbsp; | &nbsp;
                                                <a href="{{ url('/dashboard/'.$teacher->id.'/teacher_allotments')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                            </div>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $clsTeachers->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection