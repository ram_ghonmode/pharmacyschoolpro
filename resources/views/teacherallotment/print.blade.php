@extends('layouts.print')

@section('content')

<div class="container-fluid">
	<div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    <div class="table-responsive col-md-12" style="margin-top: 5px;">
                        <table class="table table-bordered">
                            <thead style="font-size: 14px;">
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Class</th>
                                    <th>Section</th>
                                    <th>Academic Year</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($allotmentData as $key => $value)
                                    <tr>
                                        <td>{{{++$key}}}</td>
                                        <td>{{ $value->employees->name ?? ''}}</td>
                                        <td>{{ $value->classes->class_name }}</td>
                                        <td>{{ $value->section }}</td>
                                        <td>{{ $value->academic_year }}</td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
                                        