@extends('layouts.dash')    
@section('title', 'Academic Year')
@section('content')   
<?php use App\Http\Controllers\Controller; $reqLabel = '<sup class="text-danger">*</sup>'; ?> 

    <div class="page-header">
        <h3 class="page-title"> Academic Year </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('academic-year-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-md-6 grid-margin stretch-card">
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                   <form role="form"  class="form-horizontal form-label-left" action="{{ route('academic-year-edit', ['id' => isset($year->id) ? Controller::cryptString($year->id, 'e') : '']) }}" method="POST" autocomplete="off">
                    @csrf
                        <div class="row clearfix">
                            <div class="form-group col-sm-3">
                                <label for="">Academic Year <?php echo $reqLabel; ?></label>
                                <input type="text" class="form-control" id=""  placeholder="Enter academic year" name="academic_year" value="{{ old('academic_year', isset($year->academic_year) ? $year->academic_year : '' ) }}">
                                @if ($errors->has('academic_year'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('academic_year') }}</small>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            <button class="btn btn-light" type="reset">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection