@extends('layouts.dash')   
@section('title', 'Academic Year')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Academic Year </h3>
        @can('academic-year-add')
            <nav aria-label="breadcrumb">
                <a href="{{ route('academic-year-add') }}"><button type="button" class="btn btn-primary custom-btn">Add</button></a>
            </nav>
        @endcan
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th width="8%">Sr. No.</th>
                                <th>Academic Year </th>
                                @if(Gate::check('academic-year-edit') || Gate::check('academic-year-delete'))
                                    <th width="18%">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($years) && count($years) > 0)
                            <?php  $count = $years->firstItem(); ?>
                                @foreach($years as $key => $year)
                                    <?php 
                                        $encyId = Controller::cryptString($year->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $year->academic_year }}</td>
                                        @if(Gate::check('academic-year-edit') || Gate::check('academic-year-delete'))
                                            <td>
                                                <div class="btn-group">
                                                    @can('academic-year-edit')
                                                        <a href="{{ route('academic-year-edit', ['id' => $encyId]) }}">Edit</a>
                                                        &nbsp; | &nbsp;
                                                    @endcan
                                                    @can('academic-year-delete')
                                                        <a href="{{ url('/dashboard/'.$year->id.'/academic_years')}}" onclick="return confirm('Are you sure?')" >Delete</a>
                                                    @endcan
                                                </div>
                                            </td>
                                        @endif       
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $years->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection