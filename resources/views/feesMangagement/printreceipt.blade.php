@extends('layouts.print')
@section('content')
    <style>
        .well{padding: 15px 19px 10px; margin: 10px;font-size:25px;}
        table{
            table-layout: fixed;
            width: 600px;  
        }
        th,td{
            word-wrap: break-word;
            white-space: pre-line !important;
        }
    </style>
    <div style="width: 50%;">
        <div class="row" style="page-break-after: always;">
            <div class="well col-xs-12 col-sm-12 col-md-12" style="border: 0.5px solid;">
                <center>
                    <h4><b>{{$studDetail->school->institute_name}}</b></h4>
                    <h5><b>{{$studDetail->school->school_name}}</b></h5>
                    <h6>(Student Copy)</h6>
                </center>
                <table class="table first-table">
                    <tr>
                        <tr>
                            <td colspan="2" style="font-size:25px;" class="text-left"><b>Receipt No :</b> {{ $feePay['receipt_no'] }}</td>
                            <td colspan="2" style="font-size:25px;" style="min-width: 120px;" class="text-right"><b>Date :</b> {{ date('d-m-Y', strtotime($feePay['date'])) }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="d-flex justify-content-between">
                                    <span style="font-size:25px;"><b>Name :</b> {{ $studDetail['first_name'].' '.$studDetail['last_name'] }}</span>
                                    <span style="font-size:25px;"><b>Class: </b>{{ $studDetail['classes']['class_name'].' '.$studDetail['section'] }}</span>
                                </div>
                            </td>
                        </tr>                            
                    </tr>
                </table>
                <table class="table table-bordered fontSize">
                    <thead>
                        <tr>
                            <th style="padding:2px 2px 2px 4px;">Fee Title</th>
                            <th style="padding:2px 2px 2px 4px;" class="text-center" colspan="2">Rs.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $payAmt = 0; ?>
                        @foreach($feepayData as $fPayk => $feePayval)
                            <tr>
                                <td style="padding:2px 2px 2px 6px;font-size: 18px;" class="col-md-9 text-left">{{ $feePayval['head_name'] }}</h4></td>
                                <td style="padding:2px 2px 2px 6px;font-size: 18px;" class="col-md-1 text-center" colspan="2">{{ $feePayval['amount'] }}</td>
                                @php $payAmt = $payAmt + $feePayval['amount']; @endphp
                            </tr>
                        @endforeach
                        <tr>
                            <td style="padding:0px 4px 0px 4px;font-size: 20px;"><strong>Total: </strong></td>
                            <td class="text-danger" colspan="2" style="padding:0px 4px 0px 4px;"><h6 class="text-center"><strong>{{ $payAmt }}</strong></h6></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding:0px 4px 0px 4px;font-size: 20px;">
                                <span><b>Fee Rupees (₹) :</b> {{ $feePayInWord }}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="d-flex mt-4">
                    <div>
                        <h6>___________________</h6>
                        <h5>Class Teacher/Clerk</h5>
                    </div>
                </div>
            </div>
             <!--For management fees receipt  -->
            @if(count($feeMngData) > 0)
            <div class="well col-xs-12 col-sm-12 col-md-12" style="border: 0.5px solid;">
                <div class="text-center">
                    <h4><b>{{$studDetail->school->institute_name}}</b></h4>
                    <h5><b>{{$studDetail->school->school_name}}</b></h5>
                    <h6>(Student Copy)</h6>
                </div>
                <table class="table first-table" style="margin:0px; font-size:9px;">
                    <tr>
                        <tr>
                            <td colspan="2" style="font-size:25px;" class="text-left"><b>Receipt No :</b> {{ $feePay['receipt_no'] }}</td>
                            <td colspan="2" style="font-size:25px;" style="min-width: 120px;" class="text-right"><b>Date :</b> {{ date('d-m-Y', strtotime($feePay['date'])) }}</td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div class="d-flex justify-content-between">
                                    <span style="font-size:25px;"><b>Name :</b> {{ $studDetail['first_name'].' '.$studDetail['last_name'] }}</span>
                                    <span style="font-size:25px;"><b>Class: </b>{{ $studDetail['classes']['class_name'].' '.$studDetail['section'] }}</span>
                                </div>
                            </td>
                        </tr>                               
                    </tr>
                </table>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="padding:2px 2px 2px 4px;">Fee Title</th>
                            <th style="padding:2px 2px 2px 4px;" class="text-center" colspan="2">Rs.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $payAmt = 0; ?>
                        @foreach($feeMngData as $fPayk => $feePayval)
                            <tr>
                                <td style="padding:2px 2px 2px 6px;font-size: 18px;" class="col-md-9 text-left">{{ $feePayval['head_name'] }}</h4></td>
                                <td style="padding:2px 2px 2px 6px;font-size: 18px;" class="col-md-1 text-center" colspan="2">{{ $feePayval['amount'] }}</td>
                                @php $payAmt = $payAmt + $feePayval['amount']; @endphp
                            </tr>
                        @endforeach
                        <tr>
                            <td style="padding:0px 4px 0px 4px;font-size: 20px;"><strong>Total: </strong></td>
                            <td class="text-danger" colspan="2" style="padding:0px 4px 0px 4px;"><h6 class="text-center"><strong>{{ $payAmt }}</strong></h6></td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding:0px 4px 0px 4px;font-size: 20px;">
                                <span><b>Fee Rupees (₹) :</b> {{ $feeMngInWord }}</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="d-flex mt-4">
                    <div>
                        <h6>___________________</h6>
                        <h5>Class Teacher/Clerk</h5>
                    </div>
                </div>
            </div> 
            @endif
        </div>   
    </div>
@endsection