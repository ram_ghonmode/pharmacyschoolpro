@extends('layouts.dash')   
@section('title', 'Class')
@section('content')  
<?php $payTypeArr = ['Cash', 'Cheque', 'DD']; ?> 
    <div class="page-header">
        <h3 class="page-title"> Fees Collection</h3>
        <!-- <nav aria-label="breadcrumb">
            <a href="{{ route('student-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav> -->
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4>Students Detail</h4>
                    <form method="" action="" id="leavingCertifFormId">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Name</label>
                                        <input type="text" name="stud_name" value="{{  $studFeeDetail['first_name'] .' '.   $studFeeDetail['middle_name'] .' '.  $studFeeDetail['last_name']}}" class="form-control"  disabled="true" style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">GR Number</label>
                                        <input type="text" value="{{$studFeeDetail['gr_number']}}" class="form-control" disabled="true" style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Standard</label>
                                        <input type="text" name="stud_class" value="{{$studFeeDetail['class_name']}}" class="form-control" disabled="true" style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Section</label>
                                        <input type="text" name="stud_sec" value="{{$studFeeDetail['section']}}" class="form-control"  disabled="true"  style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Total Fees</label>
                                        <input type="text" name="tot_fees" value="₹{{ $studFeeDetail['amount'] }}" class="form-control" disabled="true"  style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Paid Fees</label>
                                        <input type="text" name="pay_fees" value="₹{{ isset($studFeeDetail['payfee']) ? $studFeeDetail['payfee'] : 0 }}" class="form-control" disabled="true"  style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Balance</label>
                                        <input type="text" name="fee_bal" value="₹{{ isset($studFeeDetail['payfee']) ? ($studFeeDetail['amount']-$studFeeDetail['payfee']) : $studFeeDetail['amount'] }}" class="form-control" disabled="true"  style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4>Previous Fees Transaction</h4>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Date</th>
                                <th>Recipt No.</th>
                                <th>Pay Mode</th>
                                <th>Academic Year</th>
                                <th>Amount</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($feeRecord))
                                @foreach($feeRecord as $key => $value)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ date('d-m-Y', strtotime($value->date)) ?? '' }}</td>
                                        <td>{{ $value->receipt_no ?? '' }}</td>
                                        <td>{{ $value->pay_type ?? '' }}</td>
                                        <td>{{ $value->academicYears->academic_year ?? '' }}</td>
                                        <td>{{ $value->amount ?? '' }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('printpayrecipt',['id' => $value->id]) }}" target="_blank" title="Print">
                                                <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                    <i class="icon-printer menu-icon"></i>
                                                </button>
                                            </a>
                                            <a href="{{ route('deleteReceipt',['id' => $value->id]) }}" onclick="return confirm('Are you sure?')" title="Delete">
                                                <button type="button" class="btn btn-danger btn-xs waves-effect">
                                                    <i class=" icon-trash menu-icon"></i>            
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4>Fees Details</h4>
                    <form method="post" action="{{ route('feescollection', ['id' => $studFeeDetail['stud_id']]) }}">
                        @csrf
                        <input type="hidden" name="student_id" value="{{$studFeeDetail['stud_id']}}" class="form-control"/>
                        <input type="hidden" name="total_amount" value="{{$studFeeDetail['amount']}}" class="form-control"/>
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Date</label>
                                        <input type="date" name="date" value="{{ old('date') }}" class="form-control" style="border: none;background: white;"/>
                                    </div>
                                    @if ($errors->has('date'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('date') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Academic Year</label>
                                        <select name="academic_year" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                            <option value="">Select Academic Year</option>
                                            @foreach($data['years'] as $key => $value)
                                                <option value="{{ $key }}" @if((Request::get('academic_year') == $key) || ($schooldetail['academic_year']) == $key) selected @endif>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('academic_year'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('academic_year') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Pay Type</label>
                                        <select class="form-control show-tick" name="pay_type" id="pay_mode" style="outline: none;border-bottom: 1px solid #cbced3;" />
                                            <option value="">Select Pay Type</option>
                                            @foreach($payTypeArr as $payType)
                                                <option value="{{ $payType }}" @if(Request::get('section') == $payType) selected @endif>{{ $payType }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('pay_type'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('pay_type') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>                            
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">CC/DD No.</label>
                                        <input type="text" name="draft_no" placeholder="Enter CC/DD No." value="{{ old('draft_no') }}" class="form-control" id="cheque" disabled style="border: none;background: white;"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Bank Name</label>
                                        <input type="text" name="bank_name" value="{{ old('bank_name') }}" placeholder="Enter Bank Name" class="form-control" disabled style="border: none;background: white;" id="bank"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">School</label>
                                        <select name="school_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                            <option value="">Select School</option>
                                            @foreach($data['schools'] as $skey => $value)
                                                <option value="{{ $skey }}" @if((Request::get('school_id') == $skey) || ($studFeeDetail['school_id']) == $skey) selected @endif>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('school_id'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('school_id') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Receipt No</label>
                                        <input type="text" name="receipt_no" value="{{ old('receipt_no',isset($feeData['receiptNo']) ? $feeData['receiptNo'] : '') }}" placeholder="Enter Receipt No" class="form-control" style="border: none;background: white;"/>
                                    </div>
                                    @if ($errors->has('receipt_no'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('receipt_no') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label class="form-label">Concession Remark</label>
                                        <input type="text" name="remark" value="{{ old('remark',isset($feeData['ConcRemark']['remark']) ? $feeData['ConcRemark']['remark'] : '') }}" placeholder="Enter Remark" class="form-control" @if(isset($feeData['ConcRemark']['remark'])) disabled @endif style="border: none;background: white;"/>
                                    </div>
                                    @if ($errors->has('remark'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('remark') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Sr. No.</th>
                                    <th>Fees Head</th>
                                    <th>Fees</th>
                                    <th>Paid</th>
                                    <th>Balance</th>
                                    <th width="25%">Pay</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($studFeesHeadData))
                                    <?php $i = 0;$totalBalance = 0;$totalPaid = 0;$studFeeDetail['amount'] = 0;?>
                                    @foreach($studFeesHeadData as $key => $val)
                                        @php
                                            $readonly = '';
                                            $balance  = $val['total_amount'] - $val['paid_amount'];
                                            $studFeeDetail['amount']  += $val['total_amount'];
                                            $totalPaid  += $val['paid_amount'];
                                            $totalBalance  += $balance;
                                            if($balance <= 0){
                                                $readonly = 'readonly';
                                            }
                                        @endphp
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $val['head_name'] }}</td>
                                            <td>{{ $val['total_amount'] }}</td>
                                            <td>{{ $val['paid_amount'] }}</td>
                                            <td>{{ $balance }}</td>
                                            <td width="25%"><input type="number" min="1" class="form-control" {{ $readonly }} name="pay_amount[{{ $val['head_id'] }}]" id="pay_amount[{{$val['head_id']}}]" onChange='getTotalFees({{$val['head_id']}})'></td>
                                        </tr> 
                                    @endforeach
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>Concession</td>
                                        <td colspan="3">{{ isset($feeData['ConcRemark']['amount']) ? $feeData['ConcRemark']['amount'] : 0 }}</td>
                                        <td width="25%">
                                            <input type="number" min="1" class="form-control" name="amount" @if(isset($feeData['ConcRemark']['amount'])) readonly @endif>
                                            @if ($errors->has('amount'))
                                                <span class="text-danger">
                                                    <small>{{ $errors->first('amount') }}</small>
                                                </span>
                                            @endif
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td colspan="2"></td>
                                        <td>{{ $studFeeDetail['amount'] }}</td>
                                        <td>{{ $totalPaid }}</td>
                                        <td>{{ $totalBalance }}</td>
                                        <td id="grandAmtTotId"></td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <br>
                        <div class="row clearfix">    
                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                        </div>
                    </form>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection