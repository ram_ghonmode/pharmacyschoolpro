@extends('layouts.dash')   
@section('title', 'Students List')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Students List </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered" id="student-listing">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>GR. No.</th>
                                <th>Name</th>
                                <th>Current Class</th>
                                <!-- <th>Category</th> -->
                                <th>Admission Type</th>
                                <th>Admission Year</th>
                                <th>Total Fees</th>
                                <th>Paid Fees</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($studFeeData))
                                <?php $i=0; ?>
                                @foreach ($studFeeData as $key => $student)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $student['gr_number'] }}</td>
                                        <td>{{ $student['first_name'].' '.$student['middle_name'].' '.$student['last_name'] }}</td>
                                        <td>{{ $student['current_class'] }}</td>
                                        <!-- @if(!empty($student['category_id']))
                                            @foreach($catData as $key=> $value)
                                                @if($key == $student['category_id'])
                                                    <td>{{ $value }}</td>
                                                @endif
                                            @endforeach
                                        @else
                                            <td>{{ "Null" }}</td>
                                        @endif -->
                                        @if(!empty($student['admission_type_id']))
                                            @foreach($admTypId as $key=> $value)
                                                @if($key == $student['admission_type_id'])
                                                    <td>{{ $value }}</td>
                                                @endif
                                            @endforeach
                                        @else
                                            <td>{{ "Null" }}</td>
                                        @endif
                                        <td>{{ $student['academic_year'] }}</td>
                                        <td>{{ $student['amount'] }}</td>
                                        <td>{{ $student['payfee'] }}</td>
                                        <td>
                                            <a href="{{ route('feescollection', ['id' => $student['id']]) }}" title="Pay Fees">
                                                <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                    <i class="icon-wallet menu-icon"></i>
                                                </button>
                                            </a>    
                                            <button title="Send SMS" type="button" class="btn btn-info btn-xs waves-effect" data-toggle="modal" data-target="#defaultModal"> <i class="icon-envelope-open menu-icon"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif    
                        </tbody>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection