@extends('layouts.dash')   
@section('title', 'Class')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Standard: {{ $data['classMt']['classes']['class_name'] ?? '' }} &nbsp;&nbsp;&nbsp;&nbsp; College Stream: {{ $data['classMt']['stream']['name'] ?? '' }} &nbsp;&nbsp;&nbsp;&nbsp; Academic Year: {{ $data['classMt']['school']['academicYears']['academic_year'] ?? '' }} </h3>
        <nav aria-label="breadcrumb">
            <a href="{{ route('classfees-list') }}"><button type="button" class="btn btn-primary custom-btn">Back</button></a>
        </nav>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('classfees-edit',['id' => $data['classMt']['id']]) }}">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-sm-2">
                                <input type="hidden" name="update_id" value="" >
                                <select name="fees_head_id" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;" required>
                                    <option value="">Select Fees Head</option>
                                    @foreach($data['feesHeads'] as $skey => $feesHead)
                                        <option value="{{ $skey }}" @if(old('fees_head_id') == $skey) selected @endif >{{ $feesHead }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('fees_head_id'))
                                    <span class="text-danger">
                                        <small>{{ $errors->first('fees_head_id') }}</small>
                                    </span>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" name="amount" value="{{ old('amount') }}" class="form-control" placeholder="Please enter amount" style="border: none;" required/>  
                                    </div>
                                    @if ($errors->has('amount'))
                                        <span class="text-danger">
                                            <small>{{ $errors->first('amount') }}</small>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- <div class="col-sm-3">
                                <select name="status" class="form-control" style="outline: none;border-bottom: 1px solid #cbced3;">
                                    <option value="1" selected>Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div> -->
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-sm btn-info">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Fees Name </th>
                                <th>Academic Year </th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $oldFees=0; $newAdmFees = 0; $oldAdmFees = 0; $oldStdFees = 0;  $totamt = 0;?>
                            @if(!empty($classfees) && count($classfees) > 0)
                                @foreach($classfees as $key => $class)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $class->feesHead->fees_head_name }}</td>
                                        <td>{{ $class->classMas->school->academicYears->academic_year ?? '' }}</td>
                                        <td>{{ $class->amount }}</td>
                                        <?php 
                                       
                                        $totamt+=$class->amount;
                                            // if($class->feesHead->is_admission == 0 && $class->feesHead->fees_type == null){
                                            //     $oldFees += $class->amount;
                                            // }
                                            // if($class->feesHead->fees_type == 'Old'){
                                            //     $oldStdFees += $class->amount;
                                            // }
                                            // if((($class->feesHead->is_admission == 1) && ($class->feesHead->fees_type == null)) || (($class->feesHead->is_admission == 1) && ($class->feesHead->fees_type == "Internal"))){
                                            //     $oldAdmFees += $class->amount;
                                            // }
                                            // if((($class->feesHead->is_admission == 1) && ($class->feesHead->fees_type == null)) || (($class->feesHead->is_admission == 1) && ($class->feesHead->fees_type == "External"))){
                                            //     $newAdmFees = $newAdmFees + $class->amount;
                                            // }

                                        ?>
                                        
                                        <td>
                                            <button type="button" onClick="getfeeshead({{$class }});" class="btn btn-primary btn-xs waves-effect">
                                                <i class="icon-wallet menu-icon"></i>
                                            </button>
                                            <a href="{{ route('classfees-delete',['id' => $class->id]) }}" onclick="return confirm('Are you sure?')" title="Delete">
                                                <button type="button" class="btn btn-danger btn-xs waves-effect">
                                                    <i class=" icon-trash menu-icon"></i>            
                                                </button>
                                            </a>
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                        <tfoot>
                            <tr>
                               <td></td>
                               <td></td>
                               <td><strong>Total New Fees:</strong></td>
                                <td colspan="2"><strong> ₹ {{ $totamt }}</strong></td>
                                
                            </tr>
                        </tfoot>
                    </table>
                    <div class="text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection