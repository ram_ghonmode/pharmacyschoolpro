@extends('layouts.dash')   
@section('title', 'Class')
@section('content')  
<?php use App\Http\Controllers\Controller; ?>
    <div class="page-header">
        <h3 class="page-title"> Class Fees Details </h3>
    </div>
    <div class="row">
        <div class="col-12 grid-margin stretch-card"></div>
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Class Name </th>
                                <!-- <th>College Stream</th> -->
                                <!-- <th style="max-width:380px;">School Name </th> -->
                                <th>Academic Year </th>
                                <!-- <th>Category</th> -->
                                <th>Admission Type</th>
                                <th>Total Fees</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($classes) && count($classes) > 0)
                            <?php  $count = $classes->firstItem(); ?>
                                @foreach($classes as $key => $class)
                                    <?php $oldFees=0; $newAdmFees = 0; $oldAdmFees = 0; $oldStdFees = 0;                                    
                                            // if($amtVal->feesHead->is_admission == 0 && $amtVal->feesHead->fees_type == null){
                                            //     $oldFees += $amtVal->amount;
                                            // }
                                            // if($amtVal->feesHead->fees_type == 'Old'){
                                            //     $oldStdFees += $amtVal->amount;
                                            // }
                                            // if((($amtVal->feesHead->is_admission == 1) && ($amtVal->feesHead->fees_type == null)) || (($amtVal->feesHead->is_admission == 1) && ($amtVal->feesHead->fees_type == "Internal"))){
                                            //     $oldAdmFees += $amtVal->amount;
                                            // }
                                            // if((($amtVal->feesHead->is_admission == 1) && ($amtVal->feesHead->fees_type == null)) || (($amtVal->feesHead->is_admission == 1) && ($amtVal->feesHead->fees_type == "External"))){
                                            //     $newAdmFees += $amtVal->amount;
                                            // }
                                        $encyId = Controller::cryptString($class->id, 'e'); 
                                    ?>
                                    <tr>
                                        <td>{{ $count++ }}</td>
                                        <td>{{ $class->classes->class_name }}</td>
                                        <!-- <td>{{ $class->stream->name ?? '' }}</td> -->
                                       <!--  <td style="max-width:380px;overflow:hidden;">{{ $class->school->school_name }}</td> -->
                                        <td>{{ $class->academicYears->academic_year }}</td>
                                        <!-- <td>{{$class->categories->category_name ?? ''}}</td> -->
                                        <td>{{$class->admissionType->admission_type ?? ''}}</td>
                                        
                                        <td>{{ $class->ammount }}</td>
                                        <td>
                                            <a href="{{ route('classfees-add', ['id' => $class->id]) }}" title="Fees Details">
                                                <button type="button" class="btn btn-primary btn-xs waves-effect">
                                                    <i class="icon-wallet menu-icon"></i>
                                                </button>
                                            </a> 
                                        </td>
                                    </tr> 
                                @endforeach
                            @endif       
                        </tbody>
                    </table>
                    <div class="text-right">
                        {{ $classes->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection