<!DOCTYPE html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>SBGCP ERP | Login </title>
        <link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/simple-line-icons/css/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/flag-icon-css/css/flag-icon.min.css') }}">
        <link rel="stylesheet" href="{{ asset('public/Stellar-master/vendors/css/vendor.bundle.base.css') }}">
        <link rel="stylesheet" href="{{ asset('public/Stellar-master/css/style.css') }}"> <!-- End layout styles -->
        <link rel="shortcut icon" href="{{ asset('public/Stellar-master/favicon.png') }}" />
    </head>
    <body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            @yield('content')
        </div>
        @include('elements.js')
    </body>
</html>