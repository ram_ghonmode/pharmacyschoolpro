<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SBGCP ERP | @yield('title') </title>
        @include('elements.css')
  </head>
  <body>
    <div class="container-scroller">
        @include('elements.navbar')
        <div class="container-fluid page-body-wrapper">
            @include('elements.sidebar')
            <!-- page content -->
            <div class="main-panel">
                <div class="content-wrapper">
                    @include('elements.flash')
                    @yield('content')
                    @include('elements.footer')
                </div>
            </div>
        </div>
        @include('elements.js')
        @yield('custom_script')
    </body>
</html>