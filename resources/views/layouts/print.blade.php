<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @if(Route::currentRouteName() == 'printgeneralregister')
            <title>General Register</title>
        @elseif( Route::currentRouteName() == 'printleaving' )
            <title>Leaving Certificate</title>
        @elseif( Route::currentRouteName() == 'printbonafied' )
            <!-- <span><title>Bonafied Certificate</title></span> -->
        @elseif( Route::currentRouteName() == 'printcharacter' )
            <span><title>Character Certificate</title></span>
        @elseif( Route::currentRouteName() == 'printattempt' )
            <span><title>Attempt Certificate</title></span>
        @elseif( Route::currentRouteName() == 'printteacherassign' )
            <span><title>Teacher List</title></span>
        @elseif( Route::currentRouteName() == 'printsportentry' )
            <span><title>Sport Entry Form</title></span>
        @elseif( Route::currentRouteName() == 'printagegroupsport' )
            <span><title>Sport Entry Form</title></span>
        @elseif( Route::currentRouteName() == 'printeligibility' )
            <span><title>Eligibility Form</title></span>
        @elseif( Route::currentRouteName() == 'printpaycertificate' )
            <span><title>Pay Certificate</title></span>
        @endif
        <link href="{{ asset('public/Stellar-master/css/style.css') }}" rel="stylesheet">
        
    </head>
    <body>
        <style type="text/css">
            body{font-family: arial;}
            table{font-size:12px;}
            table td{padding:3px 3px;}
            .padding-left{padding: 0 0  0 5px !important;}
            .border-div{border-top: 1.5px solid #000; border-bottom: 1.5px solid #000;}
            p{margin: 0;}
            
            .image {
                background :url('{{ asset("public/images/logo3.png")}}') no-repeat center !important;
                width: 100%;
                height: 100%;
                position:relative;
                }
            .bona {
                background :url('{{ asset("public/images/logo3.png")}}') no-repeat center !important;
                background-size: 50%;
                position:relative;
                } 
            .border-class img{width: 100px !important;}
            @media print
                { 
                .scaled{zoom: 91%;}
                #print-sport{zoom: 88%;}
                .border-class img{width: 100px !important;}
               
                }
        </style>

        @if(Route::currentRouteName() == 'printpayrecipt')
            <style type="text/css">
                body{font-family: initial;}
                .padding-left{padding: 0 0  0 5px !important;}
                .border-div{border-top: 1.5px solid #000; border-bottom: 1.5px solid #000;}
                p{margin: 0; font-size: 14px}
                
                .image {
                    background :url('{{ asset("public/images/logo3.png")}}') no-repeat center !important;
                    width: 100%;
                    height: 100%;
                    position:relative;
                    }
                .bona {
                    background :url('{{ asset("public/images/logo3.png")}}') no-repeat center !important;
                    background-size: 50%;
                    position:relative;
                    } 
                .border-class img{width: 100px !important;}
                @media print
                { 
                    @page {
                    margin:0;
                    }
                    .scaled{zoom: 91%;}
                    #print-sport{zoom: 88%;}
                    .border-class img{width: 100px !important;}
                }
            </style>
        @endif            
           
        @if(Route::currentRouteName() == 'printgeneralregister')
            <style type="text/css">
                body{
                    font-family: arial;
                }
                h2{
                    font-size: 22px;text-align: center;
                }
                p{
                    font-size: 14px;
                }
                table{
                    font-size: 12px;border-collapse: collapse; text-align: center;width: 100%;
                }
                table th,table td{
                    padding:5px;height: 20px;
                }
                .uidno-table td{width: 20px;border-top: none;border-bottom: none; height: 30px;
                }
                .uidno-table td:first-child{
                    border-left: none;
                }
                .uidno-table td:last-child{
                    border-right: none;
                }
                .uidno-table{border: none;}
                .rotate-text{padding: 0;transform: rotate(-45deg);}
            </style>
        @endif
    <body>
        <div class="container-fluid">
            @yield('content')
        </div>
        <!-- Jquery Core Js -->
        <script src="{{ asset('public/js/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap Core Js -->
        <script src="{{ asset('public/js/bootstrap.js') }}"></script>
        <script src="{{ asset('public/js/krutidevToMangal.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                window.print();
                //var mywindow = window.open('content');
                //var mywindow = window.open();
                //mywindow.print(); 
                //mywindow.document.close(); // necessary for IE >= 10
                //mywindow.focus(); // necessary for IE >= 10
                //mywindow.close();       
            });
        </script>
    </body>
</html>