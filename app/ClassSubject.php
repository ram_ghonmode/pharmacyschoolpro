<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassSubject extends Model
{
    protected $fillable = ['subject_id', 'class_id'];

    public function subjects(){
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    public function getAllClassSubjectsList(){
        return ClassSubject::where(['status' => 1])->get();
    }

    public function getAllClassSubjects($classId){
        return ClassSubject::where(['status' => 1, 'class_id' => $classId])->get();
    }

    public function assignSubject(ClassSubject $subject, $data){
        $saveResult = false;
        foreach($data['subject_id'] as $key => $subId){
            if(!empty($subId)){
                $cond = [
                    'class_id' => $data['class_id'],
                    'subject_id' => $subId,
                    'status' => 1
                ];
                $cond = array_filter($cond);
                $details = ClassSubject::where($cond)->get()->first();
                if(!empty($details['id'])){
                    $members['class_id']  = $data['class_id'];
                    $members['subject_id'] = $subId;
                    $saveResult = ClassSubject::updateOrCreate(['id' => isset($details['id']) ? $details['id'] : 0], $members);
                }else{
                    $members['class_id']  = $data['class_id'];
                    $members['subject_id'] = $subId;
                    $saveResult = ClassSubject::create($members);
                }
            }
        }
        return $saveResult;
    }
}
