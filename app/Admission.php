<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Config;
use File;
use Auth;

class Admission extends Model
{
    protected $fillable = ['gr_number', 'student_id', 'scholarship_type_id','admission_type_id', 'school_id', 'first_name', 'middle_name', 'last_name', 'fullaname_in_hindi', 'profile_image', 'mother_name', 'gender', 'dob', 'age', 'place_of_birth', 'admission_date', 'academic_year', 'admit_to', 'current_class', 'section', 'admission_in', 'college_stream', 'medium', 'nationality', 'domicile', 'mother_tounge', 'caste', 'category_id','document', 'religion', 'admission_status',];

    public function guardianinfos(){
    	return $this->hasOne('App\StudentGuardianInfo');
    }

    public function studentbankinfos(){
    	return $this->hasOne('App\StudentBankInfo');
    }

    public function studentgeneralinfos(){
    	return $this->hasOne('App\StudentGeneralInfo');
    }

    public function classes(){
    	return $this->belongsTo('App\ClassList', 'current_class');
    }

    public function school(){
    	return $this->belongsTo('App\School', 'school_id');
    }

    public function mediumstream(){
    	return $this->belongsTo('App\MediumStream', 'medium');
    }

    public function admitToClasses(){
    	return $this->belongsTo('App\ClassList', 'admit_to');
    }

    public function academicYears(){
    	return $this->belongsTo('App\AcademicYear', 'academic_year');
    }

    public function leavingcertificates(){
    	return $this->hasOne('App\Leavingcertificate');
    }

    public function admissionTypes(){
    	return $this->belongsTo('App\AdmissionType', 'admission_type_id');
    }

    public function scholarshipTypes(){
    	return $this->belongsTo('App\ScholarshipType', 'scholarship_type_id');
    }

    public function categories(){
    	return $this->belongsTo('App\Category', 'category_id');
    }

    public function getAllAdmissions(){
        $cond = ['status' => '1', 'school_id' => session()->get('school_id'), 'admission_status' => 'Pursuing'];
        $upcond = array_filter($cond);
        return Admission::orderBy('id', 'ASC')->where($upcond)->get();
    }

    public function getClsStudents(){
        $cond = [
            'status'        => '1', 
            'school_id'     => session()->get('school_id'), 
            'current_class' => session()->get('class_id'), 
            'section'       => session()->get('section'),
            'admission_status' => 'Pursuing'
        ];
        $upcond = array_filter($cond);
        return Admission::orderBy('id', 'ASC')->where($upcond)->get();
    }

    public function getCountOFTotStudForFirstSch($id){
        return Admission::where(['status' => 1, 'school_id' => $id])->count();
    }

    public function getCountOfRegularStud($id){
        // return Admission::where(['status' => 1, 'school_id' => $id, 'scholarship_type' => 'Regular', 'admission_status' => 'Pursuing'])->count();
        return Admission::where(['status' => 1, 'school_id' => $id, 'admission_status' => 'Pursuing'])->count();
    }

    public function getCountOfRTEStud($id){
        // return Admission::where(['status' => 1, 'school_id' => $id, 'scholarship_type' => 'RTE', 'admission_status' => 'Pursuing'])->count();
        return Admission::where(['status' => 1, 'school_id' => $id,  'admission_status' => 'Passout'])->count();
    }

    public function getCountOfCurStud($school){
        $arr  = [];
        $data = Admission::where(['status' => 1, 'school_id' => $school->id, 'admission_status' => 'Pursuing'])->get();
        foreach ($data as $key => $value) {
            if($value->academic_year == $school->academic_year){
                $arr[$value->id] = $value->id;
            }
          
        }
        return count($arr);
    }

    public function saveAdmission(Admission $admission, $data){
        $saveResult = false;
        $profileImg = null;
        $admissionDetail = $this->getAdmissionDetail($admission);
        if(!empty($admissionDetail)){
            $profileImg = $admissionDetail->profile_image;
            $oldProfile = substr($admissionDetail->profile_image, strrpos($admissionDetail->profile_image, '/') + 1);
            if(!empty($data['profile_image'])){
                File::delete(public_path('images/students/'. $oldProfile)); // Delete previous image from folder
            }
        }
        $data['document'] = (isset($data['document']) && !empty($data['document'])) ? implode(", ", ($data['document'])) : (isset($admission->document) && !empty($admission->document) ? $admission->document : NULL);
        $data['profile_image']   = !empty($data['profile_image']) ? Common::imageUpload($data['profile_image'], 'students') : $profileImg;
        $data['dob']   = !empty($data['dob']) ?  date('Y-m-d', strtotime($data['dob'])) : null;
        $data['admission_date']   = !empty($data['admission_date']) ?  date('Y-m-d', strtotime($data['admission_date'])) : null;
        $data['profile_image']   = !empty($data['profile_image']) ? Common::imageUpload($data['profile_image'], 'students') : $profileImg;
        $saveResult = Admission::updateOrCreate(['id' => isset($admission->id) ? $admission->id : 0], $data);
        if($saveResult){
            $data['admission_id'] = $saveResult->id;
            StudentGuardianInfo::updateOrCreate(['admission_id' => isset($admission->id) ? $admission->id : 0], $data);
            StudentBankInfo::updateOrCreate(['admission_id' => isset($admission->id) ? $admission->id : 0], $data);
            StudentGeneralInfo::updateOrCreate(['admission_id' => isset($admission->id) ? $admission->id : 0], $data);
        }
        return $saveResult;
    }

    public function getAdmissionDetail(Admission $admission){
        $admissionDetail = false;
        $admissionDetail = Admission::where('id', isset($admission->id) ? $admission->id : 0)->first();
        return $admissionDetail;
    }

    public function getAllClassList($cond)
    {
        $cond['status'] = 1;
        $cond['admission_status'] = 'Pursuing';
        return Admission::orderBy('gr_number', 'ASC')
                        ->with(['classes','school','academicYears','studentbankinfos','studentgeneralinfos','guardianinfos'])
                        ->select(['id', 'gr_number','school_id', 'student_id','current_class','first_name', 'middle_name', 'last_name', 'admission_date','category_id','admission_in','academic_year','mother_name','dob','gender','caste'])
                        ->where($cond)
                        ->get();
    }

    public function getAllStdentList($cond, Request $request)
    {
        $cond['status'] = 1;
        $cond['admission_status'] = 'Pursuing';
        return Admission::orderBy('gr_number', 'ASC')
                        ->with(['classes','school'])
                        ->select(['id', 'gr_number','school_id','current_class','first_name', 'middle_name', 'last_name', 'admission_date','category_id','admission_in','section'])
                        ->where($cond)
                        ->get();
    }
}
