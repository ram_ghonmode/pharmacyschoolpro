<?php

namespace App;
use Config;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $fillable = ['subject_name'];

    public function getAllSubjects(){
        return Subject::orderBy('id', 'DESC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getSubjectList(){
        return Subject::where('status',1)->pluck('subject_name', 'id');
    }

    public function saveSubject(Subject $subject, $data){
        $saveResult = false;
        $saveResult = Subject::updateOrCreate(['id' => isset($subject->id) ? $subject->id : 0], $data);
        return $saveResult;
    }

    public function getSubjectDetail(Subject $subject){
        $subjectDetail = false;
        $subjectDetail = Subject::where('id', isset($subject->id) ? $subject->id : 0)->first();
        return $subjectDetail;
    }
}
