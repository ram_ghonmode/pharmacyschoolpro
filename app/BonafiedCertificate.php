<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class BonafiedCertificate extends Model
{
    protected $fillable = ['gr_number','admission_in','fullname','current_date','current_class','current_year','dob','caste','category','aadhar_no','section','student_id','status'];

    public function admission(){
    	return $this->belongsTo('App\Admission', 'admission_id');
    }

    public function getAllBonafiedCertificates(){
        return BonafiedCertificate::select('id', 'gr_number','fullname','current_date','current_year')->orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));;
    }

    public function saveBonafied($data)
    {
        $getBonafied = Admission::with('studentbankinfos')->where($data)->first();
        $fullname = $getBonafied['first_name'].' '.$getBonafied['middle_name'].' '.$getBonafied['last_name'];
        $saveData = [
            'gr_number'         => $getBonafied['gr_number'],
            'fullname'          => $fullname,
            'current_date'      => date("Y-m-d"),
            'current_class'     => $getBonafied['current_class'],
            'current_year'      => $this->getacademicyear(),
            'dob'               => $getBonafied['dob'],
            'caste'             => $getBonafied['caste'],
            'category'          => $getBonafied['category'],
            'section'           => $getBonafied['section'],
            'student_id'        => $getBonafied['student_id'],
            'aadhar_no'         => $getBonafied['studentbankinfos']['aadhar_no'],
            'admission_in'      => $getBonafied['admission_in'],
        ];
        $saveResult = false;
        DB::beginTransaction();
        $saveResult = BonafiedCertificate::create($saveData);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    protected function getacademicyear(){
        $year =  School::select('academic_year')->with('academicYears')->first();
        return $year->academicYears->academic_year;
    }
}
