<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolUser extends Model
{
    protected $fillable = [ 'school_id','user_id'];

    public function school(){
        return $this->belongsTo('App\School','school_id');
    }
}
