<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;
use Image;

class Common extends Model
{
    public static function imageUpload($selectedImage, $folder){
        $imgpath = null;
        $image = $selectedImage;
        $filename = $image->getClientOriginalName();
        $input['imagename'] = time().$filename;
        $destinationPath = public_path('/images/'.$folder);
        if(!File::isDirectory($destinationPath)){
            File::makeDirectory($destinationPath,0777, true, true);
        }
        $img = Image::make($image->getRealPath());
        $img->save($destinationPath.'/'.$input['imagename']);
        $imgpath = url('/public/images/'.$folder)."/".$input['imagename'];
        return $imgpath;
    }

    public static function deletePreviousImgFromFolder($folder, $image){
        $result = false;
        $oldImg = substr($image, strrpos($image, '/') + 1);
        $result = File::delete(public_path('images/'.$folder.'/'. $oldImg)); // Delete previous image from folder
        return $result;
    }
}