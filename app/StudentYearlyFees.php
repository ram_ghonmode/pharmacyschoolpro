<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class StudentYearlyFees extends Model
{
    protected $fillable = ['admission_id','gr_number','receipt_no','current_class','section','school_id','fees_type','total_amount','amount','date','academic_year','admission_in','pay_type','draft_no','bank_name','status'];

    public function academicYears(){
    	return $this->belongsTo('App\AcademicYear', 'academic_year');
    }

    public function admission(){
    	return $this->belongsTo('App\Admission', 'admission_id');
    }

    public function studentYearlyFeesPaid(){
        return $this->hasMany('App\StudentFeesPaidHead', 'fees_id')->where('status', 1);;
    }

    public function getSumOfTotalFees($data){
        return StudentYearlyFees::where(['status' => 1, 'school_id' => $data->id, 'academic_year' => $data->academic_year])->sum('total_amount');
    }

    public function getSumOfCollectedFees($data){
        return StudentYearlyFees::where(['status' => 1, 'school_id' => $data->id, 'academic_year' => $data->academic_year])->sum('amount');
    }

    public function getDailyFeesReport($data){
        $data['status'] = 1;
        return StudentYearlyFees::where($data)->with('admission.classes')->get();
    }

    public function getTotalFeesReport($data){
        $data['status'] = 1;
        $from_date = $data['from_date'];
        $to_date = $data['to_date'];
        $accYar = $data['academic_year'];
        $record = StudentYearlyFees::where('academic_year', $accYar)->whereBetween('date',[$from_date,$to_date])->get();
        //$record = StudentYearlyFees::whereBetween('date',[$from_date , $to_date])->get();
        return $record;
    }

    public function saveFees(StudentYearlyFees $studentFees, $data){        
        $saveResult = false;
        $totPayAmt = 0;
        foreach($data['pay_amount'] as $key => $val){
            $totPayAmt +=  $val;
        }
        $studDetail = Admission::where('id',$data['student_id'])->with('classes')->get()->first();
        if(!empty($data['remark'])){
            $concData = [
                'remark'            => $data['remark'],
                'student_id'        => $studDetail['id'],
                'gr_number'         => $studDetail['gr_number'],
                'current_class'     => $studDetail['current_class'],
                'section'           => $studDetail['section'],
                'school_id'         => $data['school_id'],
                'amount'            => $data['amount'],
                'date'              => date('Y-m-d', strtotime($data['date'])),
                'academic_year'     => $data['academic_year'],
                'admission_in'      => $studDetail['admission_in'],
            ];
            $saveResult = FeesConcession::create($concData);
        }
        if($totPayAmt > 0){
            $saveData = [
                'admission_id'      => $studDetail['id'],
                'gr_number'         => $studDetail['gr_number'],
                'current_class'     => $studDetail['current_class'],
                'section'           => $studDetail['section'],
                'fees_type'         => $studDetail['fees_type'],
                'amount'            => $totPayAmt,
                'total_amount'      => $data['total_amount'],
                'date'              => date('Y-m-d', strtotime($data['date'])),
                'academic_year'     => $data['academic_year'],
                'admission_in'      => $studDetail['admission_in'],
                'pay_type'          => $data['pay_type'],
                'school_id'         => $data['school_id'],
                'draft_no'          => !empty($data['draft_no']) ?  $data['draft_no'] : null,
                'bank_name'         => !empty($data['bank_name']) ?  $data['bank_name'] : null,
                'receipt_no'        => $data['receipt_no'] 
            ];
            $saveResult = StudentYearlyFees::updateOrCreate(['id' => isset($studentFees->id) ? $studentFees->id : 0], $saveData);
            if($saveResult){
                foreach($data['pay_amount'] as $key => $val){
                    $payheads = new StudentFeesPaidHead;
                    $payheads->fees_id = $saveResult->id;
                    $payheads->head_id = $key;
                    $payheads->amount = $val;
                    $payheads->save();
                }
            }
        }
        return $saveResult;
    }

    public function getFeesReceiptNo($data){
        $cond = [
            'status'            => 1,
            'admission_in'      => $data['admission_in'], 
            'school_id'         => $data['school_id'],
            'academic_year'     => $this->getSchholAcademicYr($data)
        ];
        $receiptRecord = StudentYearlyFees::where($cond)->orderBy('receipt_no', 'DESC')->first();
        $no = '001';
        if(!empty($receiptRecord)){
            $no = str_pad($receiptRecord->receipt_no + 1, 3, 0, STR_PAD_LEFT);
            // $no = $receiptRecord->receipt_no;
        }
        return $no;
    }

    public function getRemark($data){
        $cond = [
            'status'            => 1, 
            'gr_number'         => $data['gr_number'], 
            'admission_in'    => $data['admission_in'], 
            'current_class'     => $data['current_class'], 
            'school_id'         => $data['school_id'],
            'academic_year'     => $this->getSchholAcademicYr($data)
        ];
        $remark= FeesConcession::where($cond)->orderBy('id', 'DESC')->first();
        return $remark;
    }

    public function getSchholAcademicYr($data){
        $schAcYr = School::where('id',$data['school_id'])->pluck('academic_year')->first();
        return $schAcYr;
    }
}