<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;
use DB;
use Hash;

class Employee extends Model
{
    protected $fillable = ['school_id','name','email_id','number','alt_num','dob','designation_id','p_handicap','handicap','class','gender','marital_status','address','city','pin_code','state','nationality','caste','category','religion','working_status','aadhar','pan_num','pay_band','doj','dor'];

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_users','user_id');
    }

    public function schools()
    {
        return $this->belongsToMany('App\School','school_users','user_id');
    }

    public function users(){
    	return $this->hasOne('App\User','employee_id');
    }

    public function designations()
    {
        return $this->belongsTo('App\Designation','designation_id');
    }

    public function bankdetail(){
    	return $this->hasOne('App\BankDetail','employee_id');
    }

    public function getAllEmployees(){
        return Employee::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function saveEmployee(Employee $employee, $data){
        $saveResult = false;
        DB::beginTransaction();
        $saveResult = Employee::updateOrCreate(['id' => isset($employee->id) ? $employee->id : 0], $data);
        $bankId = !empty($employee->id) ? BankDetail::where('employee_id', isset($employee->id) ? $employee->id : 0)->pluck('id')->first() : '';
        $userId = !empty($employee->id) ? User::where('employee_id', isset($employee->id) ? $employee->id : 0)->pluck('id')->first() : '';
        $employeeData = Employee::find($employee->id);
        $empId = $saveResult->id;
        $bankData = [
            'employee_id' =>  $empId,
            'bank_name'   => !empty($data['bank_name']) ? $data['bank_name'] : null,
            'ac_num'      => !empty($data['ac_num']) ? $data['ac_num'] : null,
            'ac_name'     => !empty($data['ac_name']) ? $data['ac_name'] : null,
            'ifsc_code'   => !empty($data['ifsc_code']) ? $data['ifsc_code'] : null,
            'select_pf'   => !empty($data['select_pf']) ? $data['select_pf'] : null,
            'pf_acc'      => !empty($data['pf_acc']) ? $data['pf_acc'] : null
        ];
        $saveResult = BankDetail::updateOrCreate(['id' => isset($bankId) ? $bankId : 0], $bankData);
        $userData = [
            'employee_id'    => $empId,
            'name'           => !empty($data['name']) ? $data['name'] : null,
            'email'          => !empty($data['email']) ? $data['email'] : null,
            'password'       => Hash::make('12345678'),
        ];
        if(!empty($data['user_img_url'])){
            $userData['user_img_url']   = !empty($data['user_img_url']) ? Common::imageUpload($data['user_img_url'], 'SystemUsers') :  $employeeData->users->user_img_url;
        }
        $saveResult = User::updateOrCreate(['id' => isset($userId) ? $userId : 0], $userData);
        if($userId != 0){
            $employee->roles()->sync($data['role_id']);
            $employee->schools()->sync($data['school_id']);
        }
        $saveResult->roles()->attach($data['role_id']);
        $saveResult->schools()->attach($data['school_id']);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    public function getEmployeeDetail(Employee $employee){
        $employeeDetail = false;
        $employeeDetail = Employee::where('id', isset($employee->id) ? $employee->id : 0)->with('users')->first();
        return $employeeDetail;
    }
}


