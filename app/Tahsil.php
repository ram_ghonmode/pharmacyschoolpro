<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tahsil extends Model
{
    protected $fillable = ['tahsil_name', 'country_id', 'state_id', 'city_id'];

    public function getAllTahsils(){
        return Tahsil::where('status', 1)->get();
    }
}
