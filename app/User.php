<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Config;
use DB;
use File;
use Hash;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile_no','user_img_url', 'is_active','employee_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role','role_users');
    }

    public function schools()
    {
        return $this->belongsToMany('App\School','school_users');
    }

    public function employees()
    {
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function hasAccess(array $permissions){
        foreach ($this->roles as $role) {
            if($role->hasAccess($permissions)){
                return true;
            }
        }
        return false;
    }

    public function inRole($roleSlug){
        return $this->roles()->where('slug',$roleSlug)->count() >= 1;
    }

    public function getAllUsers(){
        return User::orderBy('id', 'ASC')->with('roles')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getTeacherEmp(){
        return User::join('employees', 'employees.id', '=', 'users.employee_id')
                    ->join('role_users', 'role_users.user_id', '=', 'users.id')
                    ->where(['employees.status' => 1,'role_users.role_id' => 5])
                    ->select('users.id','employees.name','school_id','employees.id AS empid')
                    ->get();
    }

    public function saveProfile(User $user, $data){
        $saveResult = false;
        DB::beginTransaction();
        $userData = User::findOrFail($user->id);
        $user_img = substr($userData->user_img_url, strrpos($userData->user_img_url, '/') + 1);
        if(!empty($data['user_img_url'])){
            File::delete(public_path('images/SystemUsers/'. $user_img)); // Delete previous image from folder
        }
        $data['user_img_url']   = !empty($data['user_img_url']) ? Common::imageUpload($data['user_img_url'], 'SystemUsers') :  $userData->user_img_url;
        $saveResult = User::updateOrCreate(['id' => isset($user->id) ? $user->id : 0], $data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    public function getUserDetail(User $user){
        $userDetail = false;
        $userDetail = User::where('id', isset($user->id) ? $user->id : 0)->first();
        return $userDetail;
    }

    public function changePassword(User $user, $request){
        $saveResult = false;
        $saveResult = User::where('id', Auth::user()->id)->update(['password' => Hash::make($request['new_password'])]);
        return $saveResult;
    }

    public function saveUser(User $user, $data){
        $saveResult = false;
        $saveResult = User::updateOrCreate(['id' => isset($user->id) ? $user->id : 0], $data);
        return $saveResult;
    }

    public function getSchoolName(){
        return !empty(session()->get('school_id')) ? School::where('status', 1)->where('id', session()->get('school_id'))->select('id','school_name')->first()->school_name : '';
    }
}
