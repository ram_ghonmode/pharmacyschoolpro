<?php

namespace App;
use Config;
use Auth;
use Illuminate\Database\Eloquent\Model;

class AdmissionType extends Model
{
    protected $fillable = ['admission_type'];

    public function getAllAdmissionTypes(){
        return AdmissionType::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllAdmissionTypeList(){
        return AdmissionType::where('status',1)->pluck('admission_type', 'id');
    }

    public function saveAdmissionType(AdmissionType $admissionType, $data){
        $saveResult = false;
        $saveResult = AdmissionType::updateOrCreate(['id' => isset($admissionType->id) ? $admissionType->id : 0], $data);
        return $saveResult;
    }

    public function getAdmissionTypeDetail(AdmissionType $admissionType){
        $admissionTypeDetail = false;
        $admissionTypeDetail = AdmissionType::where('id', isset($admissionType->id) ? $admissionType->id : 0)->first();
        return $admissionTypeDetail;
    }
}
