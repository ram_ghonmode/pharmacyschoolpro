<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['state_name', 'country_id'];

    public function getAllStates(){
        return State::where('status', 1)->get();
    }
}
