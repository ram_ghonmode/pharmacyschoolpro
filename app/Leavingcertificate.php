<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Leavingcertificate extends Model
{
    protected $fillable = ['gr_number','admission_id','admission_in','serial_no','result','month','year','class','dictinction_text','conduct','progress','leaving_reason','leaving_date','leaving_type','status'];

    public function admission(){
    	return $this->belongsTo('App\Admission', 'admission_id');
    }

    public function saveLeaving($data)
    {  
        $saveResult = false;
        DB::beginTransaction();
        $saveResult = Leavingcertificate::create($data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    public function getLeavingDetail($id){
        $leavingDetail = false;
        $leavingDetail = Leavingcertificate::where('id', isset($id) ? $id : 0)
                                            ->with(['admission.school','admission.guardianinfos','admission.studentbankinfos','admission.studentgeneralinfos','admission.categories'])->first();
        return $leavingDetail;
    }
}