<?php

namespace App\Exports;

use App\Admission;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Http\Request;

class ClassRoomExport implements FromCollection,WithHeadings
{
    use Exportable;
    protected $request;

    public function __construct($req)
    {
        $this->request = $req; 
    }

    public function collection()
    {
        $cond = [
            'school_id'      => $this->request['school_id'], 
            'current_class'  => $this->request['current_class'], 
            'section'        => $this->request['section'],
            'admission_status' => 'Pursuing'
        ];
        $upcond = array_filter($cond);
        $upcond['status'] = 1;
        $reportData = Admission::orderBy('gr_number', 'ASC')
                        ->with(['classes','school','academicYears','studentbankinfos','studentgeneralinfos','guardianinfos'])
                        ->select(['id', 'gr_number','school_id', 'student_id','current_class','first_name', 'middle_name', 'last_name', 'admission_date','category_id','admission_in','academic_year','mother_name','dob','gender','caste'])
                        ->where($upcond)
                        ->get();
        
        $assets_array = array();
        
        if(!$reportData->isEmpty()){
            foreach($reportData as $k => $data){
                $assets_array[] = array(
                    'sr_no' => ++$k,
                    'gr_number' => $data->gr_number,
                    'student_name' => ($data->first_name ?? '').' '.($data->middle_name ?? '').' '.($data->last_name ?? ''),
                    'student_id' => ' '.$data->student_id,
                    'mother_name' => $data->mother_name,
                    'academic_year' => $data->academicYears->academic_year,
                    'admission_date' => !empty($data->admission_date) ? date('d-m-Y',strtotime($data->admission_date)) : '',
                    'dob' => !empty($data->dob) ? date('d-m-Y',strtotime($data->dob)) : '',
                    'gender' => $data->gender,
                    'caste' => $data->caste,
                    'category_id' => $data->category_id,
                    'aadhar_no' => isset($data->studentbankinfos->aadhar_no) ? ' '.$data->studentbankinfos->aadhar_no : '',
                    'mobile_no' => isset($data->guardianinfos->mobile_no) ? $data->guardianinfos->mobile_no : '',
                    'perm_address' => isset($data->guardianinfos->perm_address) ? $data->guardianinfos->perm_address : '',
                );
            }
        }
        return collect($assets_array);
    }

    public function headings(): array{
        return [
            'Sr No.',
            'GR No.',
            'Student Name',
            'Student Id',
            'Mother Name',
            'Academic Year',
            'Admission Date',
            'DOB',
            'Gender',
            'Caste',
            'Category',
            'Aadhar No',
            'Mobile No',
            'Address'
        ];
    }
}
