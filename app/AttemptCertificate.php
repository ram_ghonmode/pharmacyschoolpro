<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class AttemptCertificate extends Model
{
    protected $fillable = ['gr_number','admission_in','fullname','dictinction_text','current_date','admission_date','result','month','class','year'];

    public function admission(){
    	return $this->belongsTo('App\Admission', 'admission_id');
    }

    public function getAllAttemptCertificates(){
        return AttemptCertificate::orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));;
    }

    public function saveAttempt($data)
    {
        $getAttempt = Admission::with('leavingcertificates')->where($data)->first();
        $fullname = $getAttempt['first_name'].' '.$getAttempt['middle_name'].' '.$getAttempt['last_name'];
        $saveData = [
            'gr_number'         => $getAttempt['gr_number'],
            'admission_in'    => $getAttempt['admission_in'],
            'fullname'          => $fullname,
            'dictinction_text'  => $getAttempt['leavingcertificates']['dictinction_text'],
            'current_date'      => date("Y-m-d"),
            'admission_date'    => $getAttempt['admission_date'],
            'result'            => $getAttempt['leavingcertificates']['result'],
            'month'             => $getAttempt['leavingcertificates']['month'],
            'class'             => $getAttempt['leavingcertificates']['class'],
            'year'              => $getAttempt['leavingcertificates']['year'],
        ];
        $saveResult = false;
        DB::beginTransaction();
        $saveResult = AttemptCertificate::create($saveData);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }
}

