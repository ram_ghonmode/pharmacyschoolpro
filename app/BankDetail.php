<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    protected $fillable = ['employee_id','bank_name','ac_num','ac_name','ifsc_code','select_pf','pf_acc'];
}