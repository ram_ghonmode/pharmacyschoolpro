<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class ClassYearlyFees extends Model
{
    protected $fillable = ['class_master_id','fees_head_id','amount','status', 'category_id'];

    public function feesHead(){
    	return $this->belongsTo('App\FeesHead', 'fees_head_id');
    }

    public function classMas(){
    	return $this->belongsTo('App\ClassMaster', 'class_master_id');
    }

    public function getClassYearlyFees($cMasId){
        return ClassYearlyFees::orderBy('id', 'ASC')->where('class_master_id', $cMasId)->with(['feesHead','classMas.school'])->get();
    }

    public function getClassFeesHeads($id){
        return ClassYearlyFees::where('class_master_id',$id)->get();
        
    }

    public function getClassFeesHeadsSum($id){
        return ClassYearlyFees::where('class_master_id',$id)->sum('amount');
    }

    public function saveClassYearlyFees(ClassYearlyFees $classFees, $data){
        $saveResult = false;
        $saveResult = ClassYearlyFees::updateOrCreate(['id' => isset($classFees->id) ? $classFees->id : 0], $data);
        return $saveResult;
    }

    public function getClassYearlyFeesDetail(ClassYearlyFees $classFees){
        $classFeesDetail = false;
        $classFeesDetail = ClassYearlyFees::where('id', isset($classFees->id) ? $classFees->id : 0)->first();
        return $classFeesDetail;
    }
    // public function CYfees($cMasId){
    //     $cMasId = $this->getClassYearlyFees($cMasId);
    //     foreach ($cMasId as $key => $value) {
    //         $record = ClassYearlyFees::where('class_master_id'== $value);
    //     }
    //     dd($record);
    // }
}
