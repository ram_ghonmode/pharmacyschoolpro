<?php

namespace App;
use Config;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['designation_name'];

    public function getAllDesignations(){
        return Designation::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllDesignationList(){
        return Designation::where('status',1)->pluck('designation_name', 'id');
    }

    public function saveDesignation(Designation $designation, $data){
        $saveResult = false;
        $saveResult = Designation::updateOrCreate(['id' => isset($designation->id) ? $designation->id : 0], $data);
        return $saveResult;
    }

    public function getDesignationDetail(Designation $designation){
        $designationDetail = false;
        $designationDetail = Designation::where('id', isset($designation->id) ? $designation->id : 0)->first();
        return $designationDetail;
    }
}
