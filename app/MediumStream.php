<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class MediumStream extends Model
{
    protected $fillable = ['name', 'type'];

    public function getAllMediumsAndStreams(){
        return MediumStream::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllMediumList(){
        return MediumStream::where(['status'=> 1, 'type' => 'Medium'])->pluck('name', 'id');
    }

    public function getAllStreamsList(){
        return MediumStream::where(['status'=> 1, 'type' => 'Stream'])->pluck('name', 'id');
    }

    public function saveMediumStream(MediumStream $medium, $data){
        $saveResult = false;
        $saveResult = MediumStream::updateOrCreate(['id' => isset($medium->id) ? $medium->id : 0], $data);
        return $saveResult;
    }

    public function getMediumStreamDetail(MediumStream $medium){
        $mediumDetail = false;
        $mediumDetail = MediumStream::where('id', isset($medium->id) ? $medium->id : 0)->first();
        return $mediumDetail;
    }
}
