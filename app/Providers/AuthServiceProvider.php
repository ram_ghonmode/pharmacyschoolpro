<?php

namespace App\Providers;

use App\Policies\SystemPolicy;
use App\Policies\MasterPolicy;
use App\Policies\StudentManagementPolicy;
use App\Policies\ReportPolicy;
use App\Policies\ClassRoomManagementPolicy;
use App\Policies\ExamManagementPolicy;
use App\Policies\StaffManagementPolicy;
use App\Policies\StudentAttendancePolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        SystemPolicy::userPolicies();
        SystemPolicy::rolePolicies();
        MasterPolicy::schoolPolicies();
        MasterPolicy::mediumPolicies();
        MasterPolicy::academicYearPolicies();
        MasterPolicy::classPolicies();
        MasterPolicy::designationPolicies();
        MasterPolicy::accountPolicies();
        MasterPolicy::allowancePolicies();
        MasterPolicy::deductionPolicies();
        MasterPolicy::feesHeadPolicies();
        MasterPolicy::leaveTypePolicies();
        StudentManagementPolicy::admissionPolicies();
        ReportPolicy::generalRegisterPolicies();
        ReportPolicy::bonafiedPolicies();
        ReportPolicy::leavingPolicies();
        ReportPolicy::characterPolicies();
        ReportPolicy::attemptPolicies();
        ClassRoomManagementPolicy::classRoomPolicies();
        ClassRoomManagementPolicy::classTeacherPolicies();
        ExamManagementPolicy::subjectPolicies();
        ExamManagementPolicy::testPolicies();
        ExamManagementPolicy::classTestPolicies();
        StaffManagementPolicy::employeePolicies();
        StudentAttendancePolicy::studentAttendancePolicies();
    }
}
