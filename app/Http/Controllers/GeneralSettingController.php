<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;

class GeneralSettingController extends Controller
{
    public function __construct(User $user){
        $this->middleware('auth');
        $this->exceptionroute = 'home';
        $this->user = $user;
    }

    public function profile(Request $request){
        try{
            $this->user->id = Auth::user()->id;
            if($request->isMethod('post')){
                $validator = $this->getValidatoruserProfile($this->user, $request->all());
                if($validator->fails()){
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('name','email', 'mobile_no','user_img_url');
                if($this->user->saveProfile($this->user, $data)){
                    Session::flash('success', 'Profile Updated Successfully!!!');
                    return redirect()->route('my-profile');
                }else{
                    Session::flash('danger', 'Unable to Update Profile..! Try Again');
                }
            }
            $profile = $this->user->getUserDetail($this->user);
            return view('general_setting.my-profile',compact('profile'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function changePassword(Request $request){
        try {
            if($request->isMethod('post')){
                if($request->old_password == null){
                    $validator = Validator::make($request->all(), []);
                    $validator->getMessageBag()->add('old_password', 'Old password is required.');
                    $validator->getMessageBag()->add('new_password', 'New password is required.');
                    $validator->getMessageBag()->add('confirm_password', 'Confirm new password is required.');
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if(Hash::check($request->old_password, Auth::user()->password)){
                    if($request->old_password == $request->new_password){
                        $validator = Validator::make($request->all(), []);
                        $validator->getMessageBag()->add('new_password', 'New password should be other than the old password.');
                        return redirect()->back()->withErrors($validator)->withInput();
                    }else{
                        $validator = $this->getValidateChangePassword($request->all());
                        if ($validator->fails()){
                            return redirect()->back()->withErrors($validator)->withInput();
                        }
                    }
                    
                    if($this->user->changePassword($this->user, $request)){
                        Session::flash('success', 'Password changed Successfully!!!');
                        return redirect()->route('change-password');
                    }else{
                        Session::flash('danger', 'Unable to Change password..! Try Again');
                    }
                }else{
                    $validator = Validator::make($request->all(), []);
                    $validator->getMessageBag()->add('old_password', 'Old password is incorrect.');
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
            return view('general_setting.change-password');
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidatoruserProfile(user $user, $data){
        $rules = [
            'name'          =>  'required|max:80|regex:/^[a-zA-Z ]*$/',
            'mobile_no'     =>  'required|regex:/^[6-9]\d{9}$/',
            'email'         =>  'required|email'
        ];
        $errmsg = [
            'name.required'         =>  'Name is required.',
            'name.regex'            =>  'Please enter only characters.',
            'name.max'              =>  'Name can be up to 80 characters long.',
            'mobile_no.required'    =>  'Mobile no. is required.',
            'email.required'        =>  'Email address is required.',
            'email.email'           =>  'Email address must be a valid email address.',
            'mobile_no.regex'       =>  'Please enter valid mobile no.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }

    protected function getValidateChangePassword($data){
        $rules = [
            'old_password'      => 'required|min:6',
            'new_password'      => 'required|required_with:confirm_password|same:confirm_password|min:6',
            'confirm_password'  => 'required|min:6',
        ];
        $errmsgs = [
            'old_password.required'     => 'Old password is required.',
            'new_password.required'     => 'New password is required.',
            'confirm_password.required' => 'Conform new password is required.',
            'new_password.same'         => 'New password and confirm new password must match.',
            'new_password.min'          => 'New password must be at least 6 characters.',
            'confirm_password.min'      => 'Conform new password must be at least 6 characters.'
        ];
        return Validator::make($data, $rules, $errmsgs);
    }
}
