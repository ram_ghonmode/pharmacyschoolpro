<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\AdmissionType;
use Session;

class AdmissionTypeController extends Controller
{
    public $exceptionRoute;
    public $admissionType;

    public function __construct(AdmissionType $admissionType){
        $this->middleware('auth');
        $this->exceptionRoute    = 'home';
        $this->admissionType     = $admissionType;
    }

    public function index(){
        try{
            $admissionTypes = $this->admissionType->getAllAdmissionTypes();
            return view('admissiontype.index', compact('admissionTypes'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->admissionType->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->admissionType->id)){
                $sucMsg  = 'Admission Type Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Admission Type..! Try Again';
            }else{
                $sucMsg  = 'Admission Type Successfully Created !!!';
                $dangMsg = 'Unable to Saved Admission Type..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateAdmissionType($this->admissionType, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('admission_type');
                if($this->admissionType->saveAdmissionType($this->admissionType, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('admissiontype-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $admissionType = $this->admissionType->getAdmissionTypeDetail($this->admissionType);
            return view('admissiontype.add', compact('admissionType'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateAdmissionType(AdmissionType $admissionType, $data){
        $rules = [
            'admission_type' => ['required',
                Rule::unique($admissionType->getTable())->ignore($admissionType->id)->where(function($query) {
                $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'admission_type.required' => 'Admission Type is required.',
            'admission_type.unique'   => 'Admission Type has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
