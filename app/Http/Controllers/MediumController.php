<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\MediumStream;
use Session;

class MediumController extends Controller
{
    public $exceptionRoute;
    public $medium;

    public function __construct(MediumStream $medium){
        $this->middleware('auth');
        $this->exceptionRoute   = 'home';
        $this->medium           = $medium;
    }

    public function index(){
        try{
            $mediums = $this->medium->getAllMediumsAndStreams();
            return view('medium.index', compact('mediums'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->medium->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->medium->id)){
                $sucMsg  = 'Medium / College Stream Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Medium / College Stream..! Try Again';
            }else{
                $sucMsg  = 'Medium / College Stream Successfully Created !!!';
                $dangMsg = 'Unable to Saved Medium / College Stream..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateMedium($this->medium, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('name', 'type');
                if($this->medium->saveMediumStream($this->medium, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('medium-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $medium = $this->medium->getMediumStreamDetail($this->medium);
            return view('medium.add', compact('medium'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateMedium(MediumStream $medium, $data){
        $rules = [
            'name' => ['required',
                Rule::unique($medium->getTable())->ignore($medium->id)->where(function($query) {
                $query->where('status', '=',  1);
                }),
            ],
            'type'             => 'required',
        ];
        $errmsg = [
            'name.required'     => 'Name is required.',
            'name.unique'       => 'Name has been already taken.',
            'type.required'     => 'Type is required.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
