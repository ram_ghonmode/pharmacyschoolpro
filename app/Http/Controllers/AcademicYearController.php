<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\AcademicYear;
use Session;

class AcademicYearController extends Controller
{
    public $exceptionRoute;
    public $year;

    public function __construct(AcademicYear $year){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->year           = $year;
    }

    public function index(){
        try{
            $years = $this->year->getAllAcademicYears();
            return view('academic_years.index', compact('years'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->year->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->year->id)){
                $sucMsg  = 'Academic Year Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Academic Year..! Try Again';
            }else{
                $sucMsg  = 'Academic Year Successfully Created !!!';
                $dangMsg = 'Unable to Saved Academic Year..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateAcademicYear($this->year, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('academic_year');
                if($this->year->saveAcademicYear($this->year, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('academic-year-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $year = $this->year->getAcademicYearDetail($this->year);
            return view('academic_years.add', compact('year'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateAcademicYear(AcademicYear $year, $data){
        $rules = [
            'academic_year' => ['required',
                Rule::unique($year->getTable())->ignore($year->id)->where(function($query) {
                $query->where('status', '=',  1);
                }),
            ]
        ];
        $errmsg = [
            'academic_year.required' => 'Academic year is required.',
            'academic_year.unique'   => 'Academic year has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
