<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\ClassYearlyFees;
use App\AdmissionType;
use App\ClassMaster;
use App\FeesHead;
use App\School;
use App\Admission;
use App\Category;
use App\ScholarshipType;
use App\StudentYearlyFees;
use App\AccountMaster;
use App\AcademicYear;
use App\StudentFeesPaidHead;
use Session;

class ClassYearlyFeesController extends Controller
{
    public $exceptionRoute;
    public $classYearlyFees;
    public $classMaster;
    public $feesHead;
    public $year;
    public $school;
    public $studentYearlyFees;
    public $category;
    public $scholarshipType;
    public $admissionType;

    public function __construct(StudentYearlyFees $studentYearlyFees, ClassYearlyFees $classYearlyFees, ClassMaster $classMaster, FeesHead $feesHead, AcademicYear $year, School $school, Category $category, ScholarshipType $scholarshipType, AdmissionType $admissionType){
        $this->middleware('auth');
        $this->exceptionRoute    = 'home';
        $this->classYearlyFees   = $classYearlyFees;
        $this->classMaster       = $classMaster;
        $this->feesHead          = $feesHead;
        $this->year              = $year;
        $this->school            = $school;
        $this->studentYearlyFees = $studentYearlyFees;
        $this->category          = $category;
        $this->scholarshipType   = $scholarshipType;
        $this->admissionType     = $admissionType;


    }

    protected function getMasterData(){
        $data['schools']   = $this->school->getAllSchoolsList();
        $data['years']     = $this->year->getAllAcademicYearsList();
        return $data;
    }

    public function index(Request $request){
        try{
            $classes = $this->classMaster->getAllClasses();
            $admissionType = $this->admissionType->getAllAdmissionTypeList();
            //$classfees = $this->classYearlyFees->getClassYearlyFees($this->classYearlyFees);
            //$cFeesDetail =$this->getClassYearlyFeesDetail();
            //foreach ($cFeesDetail as $key => $value) {
            $classfees = $this->classYearlyFees->get();
            $cmkey = $this->classMaster->get();
            $k = 0;
            $allclassamt=[]; $amtot = 0; $clsarray['amtot'] = 0;
            
            foreach ($classes as $key => $cmk) {
                $amtot = 0;
                foreach ($classfees as $clf => $cfv) {
                    if($cmk->id == $cfv->class_master_id){
                        $amtot += $cfv->amount;
                    }
                        $clsarray['clsmaid'] = $cmk->id;
                    
                }
                $classes[$key]['ammount'] = $amtot;
                $k++;
            }
            return view('feesMangagement.index', compact('classes','admissionType','classfees'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $cMasId = $request->id;
            $this->classYearlyFees->id = $request->update_id;            
            if($request->isMethod('post')){
                // $validator = $this->getValidateClassYearlyFees($request->all());
                // if ($validator->fails()) {
                //     return redirect()->back()->withErrors($validator)->withInput();
                // }
                $saveData = $request->only('fees_head_id','amount','status');
                $saveData['class_master_id'] = $cMasId;
                if($this->classYearlyFees->saveClassYearlyFees($this->classYearlyFees, $saveData)){
                    if(!empty($this->classYearlyFees->id)){
                        $sucMsg  = 'Class Fees Head Successfully Updated !!!';
                    }else{
                        $sucMsg  = 'Class Fees Head Successfully Created !!!';
                    }
                    Session::flash('success', $sucMsg);
                    return redirect()->back();
                }else{
                    $dangMsg = 'Unable to Updated Class Fees Head..! Try Again';
                    Session::flash('danger', $dangMsg);
                }
            }
            $cFeesDetail = $this->classMaster->getClassDetail($this->classMaster);
            $classfees = $this->classYearlyFees->getClassYearlyFees($cMasId);
            $data['classMt'] = $this->classMaster->getClass($cMasId);
            $data['feesHeads'] = $this->feesHead->getAllFeesHeadList();
            // $data['categories'] = $this->category->getAllCategoryList();
            return view('feesMangagement.add', compact('data','classfees','cFeesDetail'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function deleteFeeHead($id){
        $saveResult = ClassYearlyFees::where('id',$id)->delete();
        return redirect()->back()->with('success', 'Class Fees Head Deleted Suceesfully..');
    }

    public function studentList(Request $request){
        try{
            $schoolId = $request->school_id;
            $studList = $this->classMaster->getAllStudentClassList($schoolId);
            $studFee = $this->classMaster->fees($schoolId);
            $catData= $this->category->getAllCategoryList();
            $admTypId = $this->admissionType->getAllAdmissionTypeList();
            $studFeeData = []; $k=1;
            $schooldetail = School::where('id',$schoolId)->first();
            foreach($studList as $key => $val){
                foreach($studFee as $fkey => $fval){
                    if($val->id == $fval['stud_id']){
                        $studFeeData[$k] = [
                            'gr_number' => $val->gr_number,
                            'id' => $val->id,
                            'first_name' => $val->first_name,
                            'middle_name' => $val->middle_name,
                            'last_name' => $val->last_name,
                            'category_id'=>$val->category_id,
                            'admission_type_id'=>$val->admission_type_id,
                            'current_class' => $val->classes->class_name,
                            'academic_year' => $val->academicYears->academic_year,
                            'amount' => $fval['amount'],
                            'payfee' => $fval['payfee'],
                            'school_year' => $schooldetail['academic_year']
                        ];
                        $k++;
                    }
                }
            }
            return view('feesMangagement.studfeeslist', compact('studFeeData','catData','admTypId'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function feesCollection(Request $request){
        $data = $this->getMasterData();
        $studFeeDetail = $this->classMaster->studentFees($request->id);
        $schooldetail = School::where('id',$studFeeDetail['school_id'],)->first();
        $studFeesHeadData = $this->feesHead->getFeesHeadGrid($studFeeDetail);
        $feeRecord = StudentYearlyFees::where(['admission_id' => $studFeeDetail['stud_id'], 'admission_in' =>  $studFeeDetail['admission_in'], 'current_class' =>  $studFeeDetail['current_class'], 'status' => 1])->get();
        $feeData['receiptNo'] = $this->studentYearlyFees->getFeesReceiptNo($studFeeDetail);
        $feeData['ConcRemark'] = $this->studentYearlyFees->getRemark($studFeeDetail);
        if($request->isMethod('post')){
            $validator = $this->getValidateFeesData($this->studentYearlyFees,$request->all());
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            $saveData = $request->all();
            if($this->studentYearlyFees->saveFees($this->studentYearlyFees, $saveData)){
                Session::flash('success', 'Fees Data Saved Successfully!!!');
                return redirect()->route('feescollection', ['id' => $request->id]);
            }else{
                Session::flash('danger', 'Unable To Save Fees Data!!!...Try Again');
            }
        }
        return view('feesMangagement.feescollection',compact('data','studFeeDetail','schooldetail','studFeesHeadData','feeData','feeRecord'));
    }

    public function printFeeRecipt($id){
        $feePay = StudentYearlyFees::findOrFail($id);
        $studDetail = Admission::where(['id' => $feePay['admission_id'], 'admission_in' => $feePay['admission_in']])->with('classes')->first();
        $feepayHeads = StudentFeesPaidHead::where('fees_id', $id)->with('fessHead')->get(); 
        $feepayData = [];
        $feeMngData = [];     
        $totFeePay = 0;
        $totMngPay = 0;   
        foreach($feepayHeads as $fPayk => $feePayval){
            if($feePayval->fessHead->is_management == 0){
                $totFeePay += $feePayval->amount;
                $feepayData[$fPayk] = [
                    'head_name' => $feePayval->fessHead->fees_head_name,
                    'amount'    => $feePayval->amount
                ];
            }
        }
        foreach($feepayHeads as $fPayk => $feePayval){
            if(($feePayval->fessHead->is_management == 1) && ($feePayval->amount != null)){
                $totMngPay += $feePayval->amount;
                $feeMngData[$fPayk] = [
                    'head_name' => $feePayval->fessHead->fees_head_name,
                    'amount'    => $feePayval->amount
                ];
            }
        }        
        $schooldetail = School::where('id',$studDetail['school_id'])->first();
        $feeMngInWord = $this->amtTowords($totMngPay);
        $feePayInWord = $this->amtTowords($totFeePay);
        return view('feesMangagement.printreceipt',compact('studDetail','schooldetail','feepayData','feeMngData','feePay','feeMngInWord','feePayInWord')); 
    }

    public function deleteFeeRecipt($id){
        $saveResult = StudentYearlyFees::updateOrCreate(['id' => isset($id) ? $id : 0], ['status' => 0]);
        if($saveResult){
            foreach ($saveResult['studentYearlyFeesPaid'] as $key => $value) {
               StudentFeesPaidHead::where('id', $value->id)->update(['status' => 0]);
            }
        }
        return redirect()->back()->with('success', 'Receipt Deleted Suceesfully..');
    }

    public function amtTowords($num = null){
        $number = $num;
        $no = round($number);
        $point = round($number - $no, 2) * 100;
        $hundred = null;
        $digits_1 = strlen($no);
        $i = 0;
        $str = array();
        $words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
        $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
        while ($i < $digits_1) {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
        }
        $str = array_reverse($str);
        $result = implode('', $str);
        $points = ($point) ?
        "." . $words[$point / 10] . " " . 
              $words[$point = $point % 10] : '';
        $inWord = $result . "Rupees";
        return $inWord;
    }

    protected function feePaid($feeRecord){
        $tot = 0;
        foreach ($feeRecord as $key => $feeval) {
            $tot = $tot + $feeval['amount'];
        }
        return $tot; 
    }

    protected function getValidateFeesData(StudentYearlyFees $studentYearlyFees, $data){
        $schAcYr = School::where('id',isset($data['school_id']) ? $data['school_id'] : '')->pluck('academic_year')->first();
        $schAcYrFess = StudentYearlyFees::where(['status' => 1,'school_id' => $data['school_id'], 'academic_year' => $schAcYr])->count();
        $rules = [
            'date'              => 'required',
            'school_id'         => 'required',
            // 'pay_type'          => 'required',
            'amount'            => !empty($data['remark']) ? 'required' : 'nullable',
            'academic_year'     => 'required',
            'receipt_no'        => (isset($data['school_id']) && ($schAcYr == $data['academic_year']) && ($schAcYrFess > 0)) ? ['required','max:50',
                                        Rule::unique($studentYearlyFees->getTable())->ignore($studentYearlyFees->id)->where(function($query) use ($data , $schAcYr){
                                            $query->where(['status'=> 1, 'school_id' => $data['school_id'], 'academic_year' => $schAcYr]);
                                        }),
                                    ] : 'required',
            // 'receipt_no'        => (isset($data['school_id']) && ($schAcYr == $data['academic_year']) && ($schAcYrFess > 0)) ? 'required|unique:student_yearly_fees,receipt_no,'.$studentYearlyFees->receipt_no : 'required'
        ];
        $errmsg = [
            'date.required'             => 'Date is required.',
            'school_id.required'        => 'School is required.',
            // 'pay_type'               => 'required',
            'amount.required'           => 'Concession amount is required.',
            'academic_year.required'    => 'Academic Year is required.',
            'receipt_no.required'       => 'Receipt No. is required.',
        ];
        return Validator::make($data, $rules, $errmsg);
    }

    protected function getValidateClassYearlyFees($data){
        $rules = [
            'class_master_id'       => 'required',
            'fees_head_id'          => 'required',
            'amount'                => 'required|numeric',
        ];
        $errmsg = [
            'class_master_id.required'  => 'Class name is required.',
            'fees_head_id.required'     => 'Fees head is required.',
            'amount.required'           => 'Amount is required.',
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
