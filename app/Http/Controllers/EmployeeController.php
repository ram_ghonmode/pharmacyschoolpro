<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Employee;
use App\BankDetail;
use App\School;
use App\Designation;
use App\Role;
use Session;

class EmployeeController extends Controller
{
    public $exceptionRoute;
    public $employee;
    public $bankDetail;
    public $school;
    public $designation;
    public $role;

    public function __construct(Employee $employee, BankDetail $bankDetail, School $school, Designation $designation,Role $role){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->employee          = $employee;
        $this->bankDetail        = $bankDetail;
        $this->school            = $school;
        $this->designation       = $designation;
        $this->role              = $role;
    }

    public function index(){
        try{
            $employees = $this->employee->getAllEmployees();
            return view('staffManagement.employees.index', compact('employees'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->employee->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->employee->id)){
                $sucMsg  = 'Employee Details Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Employee Details..! Try Again';
            }else{
                $sucMsg  = 'Employee Details Successfully Created !!!';
                $dangMsg = 'Unable to Saved Employee Details..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateEmployee($this->employee, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('school_id','name','email_id','number','alt_num','dob','designation_id','user_img_url','p_handicap','handicap','class','gender','marital_status','address','city','pin_code','state','nationality','caste','category','religion','working_status','aadhar','pan_num','pay_band','doj','dor','bank_name','ac_num','ac_name','ifsc_code','select_pf','pf_acc','email','role_id');
                if($this->employee->saveEmployee($this->employee, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('employee-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $employee = $this->employee->getEmployeeDetail($this->employee);
            $data['schools']        = $this->school->getAllSchoolsList();
            $data['designations']   = $this->designation->getAllDesignationList();
            $data['roles']          = $this->role->getRolesList();
            return view('staffManagement.employees.add', compact('employee','data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateEmployee(Employee $employee, $data){
        $rules = [
            'school_id'        => 'required',
            'name'             => 'required',
            'email_id'         => 'required',
            'number'           => 'required|regex:/^[6-9]\d{9}$/',
            'alt_num'          => 'nullable|regex:/^[6-9]\d{9}$/',
            'dob'              => 'required',
            'p_handicap'       => 'required',
            'designation_id'   => 'required',
            'doj'              => 'required',
            'email'            => 'required',
            'role_id'          => 'required',
        ];
        $errmsg = [
            'school_id.required'       => 'School is required.',
            'name.required'            => 'Name is required.',
            'email_id.required'        => 'Email is required.',
            'number.required'          => 'Number is required.',
            'number.regex'             => 'Enter correct number.',
            'alt_num.regex'            => 'Enter correct number.',
            'dob.required'             => 'Enter date of birth.',
            'p_handicap.required'      => 'Enter physically handicapped.',
            'designation_id.required'  => 'Enter designation.',
            'doj.required'             => 'Enter joining date.',
            'email.required'           => 'Email is required.',
            'role_id.required'         => 'Enter role.',
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
