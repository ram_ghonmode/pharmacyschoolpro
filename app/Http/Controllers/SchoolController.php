<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\School;
use App\Country;
use App\State;
use App\City;
use App\Tahsil;
use App\AcademicYear;
use Session;

class SchoolController extends Controller
{
    public $exceptionRoute;
    public $school;
    public $country;
    public $state;
    public $city;
    public $tahsil;
    public $year;

    public function __construct(School $school, Country $country, State $state, City $city, Tahsil $tahsil, AcademicYear $year){
        $this->middleware('auth');
        $this->exceptionRoute   = 'home';
        $this->school           = $school;
        $this->country          = $country;
        $this->state            = $state;
        $this->city             = $city;
        $this->tahsil           = $tahsil;
        $this->year             = $year;
    }

    protected function getMasterData(){
        $data['countries'] = $this->country->getAllCountries();
        $data['states']    = $this->state->getAllStates();
        $data['cities']    = $this->city->getAllCities();
        $data['tahsils']   = $this->tahsil->getAllTAhsils();
        $data['years']     = $this->year->getAllAcademicYearsList();
        return $data;
    }

    public function index(){
        try{
            $schools = $this->school->getAllSchools();
            return view('school.index', compact('schools'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->school->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->school->id)){
                $sucMsg  = 'School Successfully Updated !!!';
                $dangMsg = 'Unable to Updated School..! Try Again';
            }else{
                $sucMsg  = 'School Successfully Created !!!';
                $dangMsg = 'Unable to Saved School..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateSchool($this->school, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $reqData = $request->only('school_name', 'institute_name', 'school_name_marathi', 'academic_year', 'session', 'country_id', 'state_id', 'city_id', 'tahsil_id', 'address', 'pin_code', 'fax_no', 'recognition_no', 'index_no', 'udise_no', 'website_url', 'email', 'phone_no', 'board', 'establishment_year', 'granted_from_date', 'granted_end_date', 'logo', 'principal_name', 'principal_mob_no', 'sport_teacher_name', 'sport_teacher_mob_no');
                
                if($this->school->saveSchool($this->school, $reqData)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('school-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $data = $this->getMasterData();
            $school = $this->school->getSchoolDetail($this->school);
            return view('school.add', compact('school', 'data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateSchool(School $school, $reqdata){
        $rules = [
            'logo'              => !empty($school->id) && empty($reqdata['logo']) ? '' : 'required',
            'institute_name'    => 'required',
            'school_name'       => 'required',
            'academic_year'     => 'required',
            'session'           => 'required',
            'address'           => 'required',
            'country_id'        => 'required',
            'state_id'          => 'required',
            'city_id'           => 'required',
            'tahsil_id'         => 'nullable',
            'phone_no'          => 'required',
            'pin_code'          => 'nullable|digits:6',
            'email'             => 'nullable|email',
            'principal_name'    => 'required',
            'sport_teacher_name'    => 'required',
            'principal_mob_no'      => 'required|regex:/^[6-9]\d{9}$/',
        ];
        $errmsg = [
            'logo.required'             => 'Logo is required.',
            'institute_name.required'   => 'Institute / Management name is required.',
            'school_name.required'      => 'School name is required.',
            'academic_year.required'    => 'Academic year is required.',
            'session.required'          => 'Session is required.',
            'address.required'          => 'Address is required.',
            'country_id.required'       => 'Country is required.',
            'state_id.required'         => 'State is required.',
            'city_id.required'          => 'City is required.',
            'tahsil_id.required'        => 'Tahsil is required.',
            'phone_no.required'         => 'Phone no. is required.',
            'pin_code.digits'           => 'Pincode must be 6 digits.',
            'email.email'               => 'Please enter valid email.',
            'principal_name.required'   => 'Principal name is required.',
            'sport_teacher_name.required'   => 'Sport teacher name is required.',
            'principal_mob_no.required'     => 'Mobile no. is required.',
            'principal_mob_no.regex'        => 'Please enter valid mobile no.',
        ];
        return Validator::make($reqdata, $rules, $errmsg);
    }
}
