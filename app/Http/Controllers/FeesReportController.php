<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Admission;
use App\ClassList;
use App\StudentYearlyFees;
use App\School;
use App\AcademicYear;
use App\ClassMaster;

class FeesReportController extends Controller
{
    public $exceptionRoute;
    public $admission;
    public $school;
    public $class;
    public $year;
    public $classMaster;
    public $studentYearlyFees;

    public function __construct(ClassList $class, Admission $admission, School $school, AcademicYear $year, ClassMaster $classMaster, StudentYearlyFees $studentYearlyFees){
        $this->middleware('auth');
        $this->exceptionRoute   = 'home';
        $this->admission        = $admission;
        $this->school           = $school;
        $this->class            = $class;
        $this->year             = $year;
        $this->classMaster      = $classMaster;
        $this->studentYearlyFees           = $studentYearlyFees;
    }

    public function index(Request $request){
        try{
            $cond = $request->only('school_id', 'class_id', 'academic_year');
            $upcond = array_filter($cond);
            $studList = !empty($upcond) ? $this->classMaster->getStudentFeesList($upcond) : NULL;
            $data = $this->getMasterData();
            $data['school'] = !empty($cond['school_id']) ? School::select('id','school_name')->where('id',$cond['school_id'])->first() : '';
            return view('feesreport.classfees', compact('studList','data'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function dailyFeesReport(Request $request){
        try{
            $cond = $request->only('school_id', 'date', 'academic_year');
            $upcond = array_filter($cond);
            $feesRecord = !empty($upcond) ? $this->studentYearlyFees->getDailyFeesReport($upcond) : NULL;
            $totPayAmt = 0;
            if($feesRecord != null){
                foreach($feesRecord as $key => $val){
                    $totPayAmt +=  $val['amount'];
                }
            }
            $data = $this->getMasterData();
            $data['school'] = !empty($cond['school_id']) ? School::select('id','school_name')->where('id',$cond['school_id'])->first() : '';
            return view('feesreport.dailyfees', compact('feesRecord','data','totPayAmt'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function total_fees_report(Request $request){
        try{
            $cond = $request->only('school_id', 'from_date','to_date','academic_year');
            $upcond = array_filter($cond);
            $feesRecord = !empty($upcond) ? $this->studentYearlyFees->getTotalFeesReport($upcond) : NULL;
            $totPayAmt = 0;
            $totDevlopmentFee = 0;
            $totBuildingFee = 0;
            $totDonation = 0;
            if($feesRecord != null){
                foreach($feesRecord as $key => $val){
                    $totPayAmt +=  $val['amount'];
                    foreach($val->studentYearlyFeesPaid as $feeKey => $feeVal){
                        if($feeVal->head_id == 2) {
                            $totDevlopmentFee += $feeVal->amount;
                        }
                        if($feeVal->head_id == 3) {
                            $totBuildingFee += $feeVal->amount;
                        }
                        if($feeVal->head_id == 10 || $feeVal->head_id == 11 || $feeVal->head_id == 12) {
                            $totDonation += $feeVal->amount;
                        }
                    }
                }
            }
            $data = $this->getMasterData();
            $data['school'] = !empty($cond['school_id']) ? School::select('id','school_name')->where('id',$cond['school_id'])->first() : '';
            $arr=$data['schools']->keys();
            return view('feesreport.totalmonthlyfees', compact('feesRecord','data','totPayAmt', 'totDevlopmentFee', 'totBuildingFee', 'totDonation','arr','upcond'));
        }catch (\Exception $e) {
             return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    protected function getMasterData(){
        $data['schools']   = $this->school->getAllSchoolsList();
        $data['classes']   = $this->class->getAllClassList();
        $data['years']     = $this->year->getAllAcademicYearsList();
        return $data;
    }
}
