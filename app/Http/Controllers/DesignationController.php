<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Designation;
use Session;

class DesignationController extends Controller
{
    public $exceptionRoute;
    public $designation;

    public function __construct(Designation $designation){
        $this->middleware('auth');
        $this->exceptionRoute    = 'home';
        $this->designation       = $designation;
    }

    public function index(){
        try{
            $designations = $this->designation->getAllDesignations();
            return view('designation.index', compact('designations'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->designation->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->designation->id)){
                $sucMsg  = 'Designation Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Designation..! Try Again';
            }else{
                $sucMsg  = 'Designation Successfully Created !!!';
                $dangMsg = 'Unable to Saved Designation..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateDesignation($this->designation, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('designation_name');
                if($this->designation->saveDesignation($this->designation, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('designation-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $designation = $this->designation->getDesignationDetail($this->designation);
            return view('designation.add', compact('designation'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateDesignation(Designation $designation, $data){
        $rules = [
            'designation_name' => ['required',
                Rule::unique($designation->getTable())->ignore($designation->id)->where(function($query) {
                $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'designation_name.required' => 'Designation name is required.',
            'designation_name.unique'   => 'Designation name has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
