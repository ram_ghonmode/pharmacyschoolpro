<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\BonafiedCertificate;
use App\Admission;
use App\AcademicYear;
use App\School;
use App\ClassList;
use App\Leavingcertificate;
use App\CharacterCertificate;
use App\AttemptCertificate;
use Session;

class ReportController extends Controller
{
    public $exceptionRoute;

    public function __construct()
    {
        $this->middleware('auth');
        $this->exceptionRoute   = 'home';
        $this->year = new AcademicYear;
        $this->school = new School;
        $this->classList = new ClassList;
        $this->bonafiedcertificate = new BonafiedCertificate;
        $this->leavingcertificate = new Leavingcertificate;
        $this->charactercertificate = new CharacterCertificate;
        $this->attemptcertificate = new AttemptCertificate;
    }

    public function index(Request $request){
        try {
            $BonafiedCertificate = $this->bonafiedcertificate->getAllBonafiedCertificates();
            if($request->isMethod('get')){
                $data = $request->input();
                if(!empty($data)){
                    $cond = [ 
                        'gr_number'      => $data['gr_number'], 
                        'status'         => 1
                    ];
                    $bonafiedData = Admission::with(['school','academicYears',
                                                    'studentbankinfos' => function($query){
                                                        $query->select('id','aadhar_no','admission_id');
                                                    }
                                                ])
                                                ->where($cond)
                                                ->first();
                    if(!empty($bonafiedData)){
                        return view('report.bonafiedcertificate', compact('BonafiedCertificate','bonafiedData','data'));
                    }else{
                        Session::flash('warning', 'Record not found for GR Number'." " .$data['gr_number'] .'..!!');
                        return redirect()->back();
                    }
                }
            }
            return view('report.bonafiedcertificate', compact('BonafiedCertificate'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function bonafiedAdd(Request $request){
        try {
            if($request->isMethod('post')){
                $data = $request->only('gr_number');
                if($this->bonafiedcertificate->saveBonafied($data)){
                    Session::flash('success', "Bonafied Certificate is submitted.");
                    return redirect()->back();
                } else{
                    Session::flash('danger', "Bonafied Certificate is not submitted.");
                    return redirect()->back();
                }
            }
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function printBonafied($id, Request $request){
        try {
            $decId = Crypt::decrypt($id);
            $bonafiedData = BonafiedCertificate::findOrFail($decId);
            $cond = [
                'gr_number' => $bonafiedData->gr_number,
                'status'         => 1
            ];
            $data = Admission::select('gender','school_id','current_class')->with('school.academicYears')->where($cond)->get()->last();
            return view('report.printbonafied',compact('bonafiedData','data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function generalRegister(Request $request){
        try {
            $data['years']     = $this->year->getAllAcademicYearsList();
            $data['schools']   = $this->school->getAllSchoolsList();
            if($request->isMethod('post')){
                $validator = $this->getValideGeneralRegRequest($request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $inputData = $request->input();
                $cond = [
                    'school_id'      => $inputData['school'], 
                    'academic_year'  => $inputData['academic_year'],
                    // 'admission_type' => $inputData['admission_type'],
                    'status'         => 1
                ];
                $allGenRegData = Admission::orderBy('gr_number','ASC')
                                            ->with(['studentbankinfos' => function($query){
                                                    $query->select('id','aadhar_no','admission_id');
                                                },'studentgeneralinfos' => function($query){
                                                    $query->select('id','last_school_attended','admission_id','leaving_class');
                                                },'leavingcertificates' => function($query){
                                                    $query->select('id','admission_id','progress','conduct','leaving_reason');
                                                }
                                            ])
                                            ->where($cond)
                                            ->get();

                $instName = School::select('id','school_name')->where('id', $inputData['school'])->first();
                if($allGenRegData->count() > 0){
                    Session::forget('warning');  
                    return view('report.generalregister', compact('allGenRegData','inputData','instName','data'));
                }else{
                    Session::flash('warning', 'Record not found..!!');
                    return view('report.generalregister',compact('data','inputData'));
                }
            }
            return view('report.generalregister',compact('data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValideGeneralRegRequest($reqdata){
        $rules = [
            'school'         => 'required',
            'academic_year'  => 'required'
        ];
        $errmsg = [
            'school.required'            => 'School is required.',
            'academic_year.required'     => 'Academic year is required.',
        ];
        return Validator::make($reqdata, $rules, $errmsg);
    }

    public function printGeneralRegister($type,$year,$school){
        try {
            $cond = [
                'academic_year'  => $year, 
                // 'admission_type' => $type,
                'school_id'      => $school, 
                'status'         => 1
            ];
            $allGenRegData = Admission::orderBy('gr_number','ASC')
                                        ->with(['studentbankinfos' => function($query){
                                                $query->select('id','aadhar_no','admission_id');
                                            },'studentgeneralinfos' => function($query){
                                                $query->select('id','last_school_attended','admission_id','leaving_class');
                                            },'leavingcertificates' => function($query){
                                                $query->select('id','admission_id','progress','conduct','leaving_reason');
                                            }
                                        ])
                                        ->where($cond)
                                        ->get();
            $instName = School::select('id','school_name')->where('id',$school)->first();
            $academicYr = AcademicYear::select('id','academic_year')->where('id',$year)->first();
            $data['instName'] = $instName;
            $data['type'] = $type;
            $data['year'] = $academicYr;
            if(!empty($allGenRegData)){
                return view('report.printgeneralregister', compact('allGenRegData','data'));
            }else{
                return view('report.printgeneralregister');
            }
        }catch (\Exception $e) {
            return redirect()->route('generalregister')->with('warning', $e->getMessage());
        }
    }

    public function leavingAdd(Request $request){
        try {
            $allgenleaving = Leavingcertificate::orderBy('id','DESC')->get();
            $classList = $this->classList->getAllClassList();
            if($request->isMethod('post')){
                $validator = $this->getValidateLeavingSearch($request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('gr_number','admission_id','admission_in','serial_no','result','month','year','class','dictinction_text','conduct','progress','leaving_reason','leaving_date','leaving_type','status','saveBtn');
                $leavingCertifData = Admission::with(['school','studentbankinfos','guardianinfos','studentgeneralinfos','categories'])
                                            ->where(['gr_number' => $data['gr_number'], 'admission_in' => $data['admission_in'],'status' => 1])
                                            ->get()
                                            ->first();
                if(($data['saveBtn'] == 'generate')){    //this else generate the laeaving view
                    if(!empty($leavingCertifData)){
                                            // dd($leavingCertifData);
                        $serialNoData = $this->generateLeavingSerialNo($leavingCertifData->current_class, $data['leaving_date']);
                        $serialNo = $serialNoData['srno'];
                        return view('report.leavingcertificate2',compact('leavingCertifData','data','serialNo','allgenleaving','classList'));
                    }else{
                        $msg = Session::flash('warning', 'Record not found for GR Number'." " .$data['gr_number'] .'..!!');
                        return view('report.leavingcertificate2',compact('data','allgenleaving','classList'))->with($msg);
                    }
                }else{
                    $serialNoData = $this->generateLeavingSerialNo($leavingCertifData->current_class, $data['leaving_date']);
                    $serialNo = $serialNoData['srno'];
                    $data['serial_no']      = $serialNoData['srno'];
                    $data['type']           = $serialNoData['type'];
                    $data['leaving_date']   = isset($input['leaving_date']) ? date('Y-m-d', strtotime($input['leaving_date'])) : date('Y-m-d');
                    if($data['status'] == 'ORIGINAL'){
                        $leavdata = Leavingcertificate::where(['gr_number'=>$data['gr_number']],['status'=> 'ORIGINAL'])->first();
                        if (!empty($leavdata)) {
                                if ($leavdata->status == 'ORIGINAL') {
                                    $msg = Session::flash('warning','ORIGINAL Leaving Certificate for GR Number ('. $leavdata->gr_number .') is already saved.');
                                    return view('report.leavingcertificate2',compact('data','allgenleaving','classList'))->with($msg);
                                }

                        }else{
                            if($this->leavingcertificate->saveLeaving($data)){
                                $admissionstatus = Admission::findOrFail($data['admission_id']);
                                $admissionstatus->admission_status = 'Passout';
                                if($admissionstatus->save()){
                                    $allgenleaving = Leavingcertificate::orderBy('id','DESC')->get();
                                    $msg = Session::flash('success', 'Leaving successfully generated..!!');
                                    return view('report.leavingcertificate2',compact('allgenleaving','classList') )->with($msg);
                                }
                            }
                        }
                    }
                    if($data['status'] == 'DUPLICATE'){
                        if($this->leavingcertificate->saveLeaving($data)){
                            $admissionstatus = Admission::findOrFail($data['admission_id']);
                            $admissionstatus->admission_status = 'Passout';
                            if($admissionstatus->save()){
                                $allgenleaving = Leavingcertificate::orderBy('id','DESC')->get();
                                $msg = Session::flash('success', 'Leaving successfully generated..!!');
                                return view('report.leavingcertificate2',compact('allgenleaving','classList') )->with($msg);
                            }
                        }                           
                    }
                }
            }
            return view('report.leavingcertificate2', compact('allgenleaving', 'classList'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function generateLeavingSerialNo($currentClass, $leavingDate){

        $type = 'College';
        //$curClass = substr($currentClass, 0, -2);
        $result = [];
        // if($curClass <= 10){
        //     $type = 'College';
        // }else{
        //     $type = 'College';
        // }
        $result['srno'] = '001';
        $result['type'] = $type;
        $lastLeaving = Leavingcertificate::select('id','serial_no','leaving_type','leaving_date')
                                    ->where('leaving_type', $type)
                                    ->orderBy('id', 'DESC')->first();
        if(!empty($lastLeaving)){
            $leavingserialNo = sprintf("%'.03d\n", ++$lastLeaving->serial_no);
            $leavingserialNo = preg_replace('/\s+/', '', $leavingserialNo);
            $result['srno'] = $leavingserialNo;
        }
        return $result;
    }

    public function printLeaving($id, Request $request){
        try {
            $leaveId = Crypt::decrypt($id);
            $leavingData = $this->leavingcertificate->getLeavingDetail($leaveId);
            $classList = $this->classList->getAllClassList();
            return view('report.printleaving2', compact(['leavingData', 'classList']));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }
    
    public function characterList(Request $request){
        try {
            $CharacterCertificate = $this->charactercertificate->getAllCharacterCertificates();
            if($request->isMethod('get')){
                $data = $request->only('gr_number','admission_in');
                if(!empty($data)){
                    $characterData = Admission::with(['school',
                                                    'studentbankinfos' => function($query){
                                                        $query->select('id','aadhar_no','admission_id');
                                                    }
                                                ])
                                                ->where($data)
                                                ->first();
            //dd($characterData->school->school_name);
                    $leavingData = Leavingcertificate::where($data)->first();   
                    if(!empty($characterData ) && !empty($leavingData )){
                        return view('report.charactercertificate', compact('CharacterCertificate','characterData','data','leavingData'));
                    }else{
                        Session::flash('warning', 'Please, Generate Leaving for GR Number Or Check Admission Type for '." " .$data['gr_number'] .'..!!');
                        return redirect()->back();
                    }
                }
            }
            return view('report.charactercertificate', compact('CharacterCertificate'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function characterAdd(Request $request){
        try {
            if($request->isMethod('post')){
                $data = $request->only('gr_number','admission_in');
                if($this->charactercertificate->saveCharacter($data)){
                    Session::flash('success', "Character Certificate is submitted.");
                    return redirect()->back();
                } else{
                    Session::flash('danger', "Character Certificate is not submitted.");
                    return redirect()->back();
                }
            }
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function printCharacter($id,Request $request){
        try{   
            $decId = Crypt::decrypt($id);
            $characterData = CharacterCertificate::findOrFail($decId);
            $data = Admission::select('id','admission_in', 'gender','school_id','current_class')->with('school')->where(['gr_number' => $characterData->gr_number, 'admission_in' => $characterData->admission_in])->get()->last();
            return view('report.printcharacter',compact('characterData','data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function attemptList(Request $request){
        try {
            $AttemptCertificate = $this->attemptcertificate->getAllAttemptCertificates();
            if($request->isMethod('get')){
                $data = $request->only('gr_number','admission_in');

                if(!empty($data)){
                    $studentData = Admission::with(['school', 'studentbankinfos'])
                                                ->where($data)
                                                ->first();
                    $leavingData = Leavingcertificate::where($data)->first();   
                    if(!empty($studentData ) && !empty($leavingData )){
                        return view('report.attemptcertificate', compact('AttemptCertificate','studentData','data','leavingData'));
                    }else{
                        Session::flash('warning', 'Please, Generate Leaving for GR Number Or Check Admission Type for '." " .$data['gr_number'] .'..!!');
                        return redirect()->back();
                    }
                }
            }
            return view('report.attemptcertificate', compact('AttemptCertificate'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function attemptAdd(Request $request){
        try {
            if($request->isMethod('post')){
                $data = $request->only('gr_number','admission_in');
                if($this->attemptcertificate->saveAttempt($data)){
                    Session::flash('success', "Attempt Certificate is submitted.");
                    return redirect()->back();
                } else{
                    Session::flash('danger', "Attempt Certificate is not submitted.");
                    return redirect()->back();
                }
            }
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function printAttempt($id,Request $request){
        try{   
            $decId = Crypt::decrypt($id);
            $attemptData = AttemptCertificate::findOrFail($decId);
            $data = Admission::select('id','admission_in', 'gender','school_id')->with('school')->where(['gr_number' => $attemptData->gr_number, 'admission_in' => $attemptData->admission_in])->get()->last();
            return view('report.printattempt',compact('attemptData','data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateLeavingSearch($data){
        $rules = [
            // 'school_id'      => 'required',
            'admission_in' => 'required',
            'gr_number'      => 'required',
            'status'         => 'required',
            'month'          => 'required',
            'year'           => 'required',
            'progress'       => 'required',
            'conduct'        => 'required',
            'class'          => 'required',
            'leaving_reason' => 'required',
            'leaving_date'   => 'required'
        ];
        $errmsg = [
            // 'school_id.required'        => 'School is required.',
            'admission_in.required'   => 'Admission type is required.',
            'gr_number.required'        => 'GR no. is required.',
            'status.required'           => 'Status type is required.',
            'month.required'            => 'Month is required.',
            'year.required'             => 'Year is required.',
            'progress.required'         => 'Progress is required.',
            'conduct.required'          => 'Conduct is required.',
            'class.required'            => 'Class is required.',
            'leaving_reason.required'   => 'Leaving reason is required.',
            'leaving_date.required'     => 'Leaving date is required.',
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
