<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\FeesHead;
use Session;

class FeesHeadController extends Controller
{
    public $exceptionRoute;
    public $feesHead;

    public function __construct(FeesHead $feesHead){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->feesHead          = $feesHead;
    }

    public function index(){
        try{
            $feesHeads = $this->feesHead->getAllFeesHeads();
            return view('feeshead.index', compact('feesHeads'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->feesHead->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->feesHead->id)){
                $sucMsg  = 'Fees Head Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Fees Head..! Try Again';
            }else{
                $sucMsg  = 'Fees Head Successfully Created !!!';
                $dangMsg = 'Unable to Saved Fees Head..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateFeesHead($this->feesHead, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('fees_head_name','is_admission','is_management');
                $data['is_admission']  = !empty($data['is_admission']) ? true : false;
                $data['is_management']  = !empty($data['is_management']) ? true : false;
                if($this->feesHead->saveFeesHead($this->feesHead, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('fees-head-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $feesHead = $this->feesHead->getFeesHeadDetail($this->feesHead);
            return view('feeshead.add', compact('feesHead'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateFeesHead(FeesHead $feesHead, $data){
        $rules = [
            'fees_head_name' => ['required',
                Rule::unique($feesHead->getTable())->ignore($feesHead->id)->where(function($query) {
                    $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'fees_head_name.required' => 'Fees Head name is required.',
            'fees_head_name.unique'   => 'Fees Head name has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
