<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\ClassMaster;
use App\AcademicYear;
use App\School;
use App\ClassList;
use App\MediumStream;
use App\Category;
use App\ScholarshipType;
use App\AdmissionType;
use Session;

class ClassMasterController extends Controller
{
    public $exceptionRoute;
    public $classMaster;
    public $school;
    public $year;
    public $class;
    public $mediumStream;
    public $category;
    public $scholarshipType;
    public $admissionType;

    public function __construct(ClassMaster $classMaster, School $school, AcademicYear $year, ClassList $class, MediumStream $mediumStream, Category $category, ScholarshipType $scholarshipType,AdmissionType $admissionType){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->classMaster    = $classMaster;
        $this->school         = $school;
        $this->year           = $year;
        $this->class          = $class;
        $this->mediumStream     = $mediumStream;
        $this->category         = $category;
        $this->scholarshipType  = $scholarshipType;
        $this->admissionType  = $admissionType;
    }

    public function index(){
        try{
            $classes = $this->classMaster->getAllClasses();
            return view('classmaster.index', compact('classes'));
        }catch (\Exception $e) {
             return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->classMaster->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->classMaster->id)){
                $sucMsg  = 'Class Master Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Class Master..! Try Again';
            }else{
                $sucMsg  = 'Class Master Successfully Created !!!';
                $dangMsg = 'Unable to Saved Class Master..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateClass($request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('class_id','school_id','academic_year','admission_type_id');
                if($this->classMaster->saveClass($this->classMaster, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('class-master-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $data['schools']   = $this->school->getAllSchoolsList();
            $data['years']     = $this->year->getAllAcademicYearsList();
            $data['classes']   = $this->class->getAllClassList();
            $data['stream']    = $this->mediumStream->getAllStreamsList();
            $data['categories'] = $this->category->getAllCategoryList();
            $data['admissionType'] = $this->admissionType->getAllAdmissionTypeList();
            $classMaster = $this->classMaster->getClassDetail($this->classMaster);
            return view('classmaster.add', compact('classMaster','data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateClass($data){
        $rules = [
            'class_id'             => 'required',
            'school_id'             => 'required',
            'academic_year'         => 'required',
        ];
        $errmsg = [
            'class_id.required' => 'Class name is required.',
            'school_id.required' => 'School name is required.',
            'academic_year.required' => 'Academic year is required.',
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
