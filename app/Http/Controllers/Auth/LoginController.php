<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Route;
use App\School;
use App\User;
use App\SchoolUser;
use App\TeacherAllotment;

class LoginController extends Controller
{
    public $school;
    public $user;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(School $school, User $user)
    {
        $this->school = $school;
        $this->user = $user;
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        // $schools = $this->school->schoolListWhileLogin();
        return view('auth.login');
    }

    public function login(Request $request){
        if($request->isMethod('post')){
            $validator = $this->getValidateUserLogin($this->user, $request->all());
            if ($validator->fails()){
                return redirect()->back()->withErrors($validator)->withInput();
            }
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 1])){
                if(Auth::user()->inRole('admin')){
                    $condtion = [
                        'school_id' => $request->school_id,
                        'user_id'   => Auth::id()
                    ];
                    if($request->school_id == 0){
                        unset($condtion['school_id']);
                    }
                    $schooluser = SchoolUser::where($condtion)->first();
                    if(!empty($schooluser) || Auth::user()->inRole('admin')){
                        session()->put('school_id', $request->school_id);
                        return redirect()->intended('home');
                    }else{
                        $validator->getMessageBag()->add('school_id', 'Not authorised for this school');
                    }
                }elseif(Auth::user()->inRole('teacher')){
                    if( ($request->school_id != 0) ){
                        $condtion = [
                            'school_id' => $request->school_id,
                            'user_id' => Auth::id()
                        ];
                        $schooluser = SchoolUser::where($condtion)->first();
                        $empId = User::where('id',Auth::id())->pluck('employee_id')->first();
                        $clsData = TeacherAllotment::where(['teacher_id' => $empId,'school_id' => $request->school_id])->first();
                        if(!empty($schooluser)){
                            session()->put('school_id', $request->school_id);
                            session()->put('teacher_id', $clsData->teacher_id);
                            session()->put('class_id', $clsData->class_id);
                            session()->put('section', $clsData->section);
                            return redirect()->intended('home');
                        }else{
                            $validator->getMessageBag()->add('school_id', 'Not authorised for this school');
                        }
                    }else{
                        $validator->getMessageBag()->add('school_id', 'Not authorised for this school');
                    }  
                }else{
                    if( ($request->school_id != 0) ){
                        $condtion = [
                            'school_id' => $request->school_id,
                            'user_id' => Auth::id()
                        ];
                        $schooluser = SchoolUser::where($condtion)->first();
                        if(!empty($schooluser)){
                            session()->put('school_id', $request->school_id);
                            return redirect()->intended('home');
                        }else{
                            $validator->getMessageBag()->add('school_id', 'Not authorised for this school');
                        }
                    }else{
                        $validator->getMessageBag()->add('school_id', 'Not authorised for this school');
                    }  
                }
                Auth::logout();
                return redirect()->back()->withErrors($validator)->withInput();
            }
            return redirect()->back()->withInput($request->only('email','password', 'school_id'))->withErrors(['email' => 'This credentials does not matched with our records']);
        }
    }

    protected function getValidateUserLogin(User $user, $data){
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required|string|min:6'
            ];
        $errmsg = [
            'email.required'        => 'Username is required.',
            'email.email'           => 'Please enter valid username.',
            'password.required'     => 'Password is required.',
            'password.min'          => 'Incorrect password.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
