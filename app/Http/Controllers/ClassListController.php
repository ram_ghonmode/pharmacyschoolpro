<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\ClassList;
use Session;

class ClassListController extends Controller
{
    public $exceptionRoute;
    public $class;

    public function __construct(ClassList $class){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->class          = $class;
    }

    public function index(){
        try{
            $classes = $this->class->getAllClasses();
            return view('classes.index', compact('classes'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->class->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->class->id)){
                $sucMsg  = 'Class Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Class..! Try Again';
            }else{
                $sucMsg  = 'Class Successfully Created !!!';
                $dangMsg = 'Unable to Saved Class..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateClass($this->class, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('class_name');
                if($this->class->saveClass($this->class, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('class-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $class = $this->class->getClassDetail($this->class);
            return view('classes.add', compact('class'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateClass(ClassList $class, $data){
        $rules = [
            'class_name' => ['required',
                Rule::unique($class->getTable())->ignore($class->id)->where(function($query) {
                $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'class_name.required' => 'Class name is required.',
            'class_name.unique'   => 'Class name has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
