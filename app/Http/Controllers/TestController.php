<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Test;
use Session;

class TestController extends Controller
{
    public $exceptionRoute;
    public $test;

    public function __construct(Test $test){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->test          = $test;
    }

    public function index(){
        try{
            $tests = $this->test->getAllTests();
            return view('examManagement.test.index', compact('tests'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->test->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->test->id)){
                $sucMsg  = 'Test Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Test..! Try Again';
            }else{
                $sucMsg  = 'Test Successfully Created !!!';
                $dangMsg = 'Unable to Saved Test..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateTest($this->test, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('test_name');
                if($this->test->saveTest($this->test, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('test-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $test = $this->test->getTestDetail($this->test);
            return view('examManagement.test.add', compact('test'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateTest(Test $test, $data){
        $rules = [
            'test_name' => ['required',
                Rule::unique($test->getTable())->ignore($test->id)->where(function($query) {
                $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'test_name.required' => 'Test name is required.',
            'test_name.unique'   => 'Test name has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
