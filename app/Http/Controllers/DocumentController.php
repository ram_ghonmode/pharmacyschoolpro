<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Document;
use Session;

class DocumentController extends Controller
{
    public $exceptionRoute;
    public $document;

    public function __construct(Document $document){
        $this->middleware('auth');
        $this->exceptionRoute    = 'home';
        $this->document          = $document;
    }

    public function index()
    {
        try{
            $documents = $this->document->getAllDocuments();
            return view('document.index', compact('documents'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->document->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->document->id)){
                $sucMsg  = 'Document Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Document..! Try Again';
            }else{
                $sucMsg  = 'Document Successfully Created !!!';
                $dangMsg = 'Unable to Saved Document..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateDocument($this->document, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('document_name');
                if($this->document->saveDocument($this->document, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('document-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $document = $this->document->getDocumentDetail($this->document);
            return view('document.add', compact('document'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateDocument(Document $document, $data){
        $rules = [
            'document_name' => ['required',
                Rule::unique($document->getTable())->ignore($document->id)->where(function($query) {
                $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'document_name.required' => 'Document name is required.',
            'document_name.unique'   => 'Document name has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
