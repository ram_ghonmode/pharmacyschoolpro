<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\ClassSubject;
use App\ClassTest;
use App\ClassList;
use App\Subject;
use App\Test;

class ClassTestController extends Controller
{
    public $classSubject;
    public $classTest;
    public $classList;
    public $subject;
    public $test;

    public function __construct(){
        $this->classSubject = new ClassSubject();
        $this->classTest    = new ClassTest();
        $this->classList    = new ClassList();
        $this->subject      = new Subject();
        $this->test         = new Test();
    }


    public function index(){
        try{
            $classes = $this->classList->getAllClasses();
            $assignSub = $this->classSubject->getAllClassSubjectsList();
            $assignTest = $this->classTest->getAllClassTestsList();
            return view('examManagement.classtest.index', compact('classes', 'assignSub', 'assignTest'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function subjectAssign(Request $request){
        try{
            $successMsg  = 'Subject Successfully Assign !!!';
            $dangMsg = 'Unable to Assign Subject..! Try Again';
            if($request->isMethod('post')){
                $data = $request->only('subject_id', 'class_id');
                if($this->classSubject->assignSubject($this->classSubject, $data)){
                    return redirect()->back()->with('success', $successMsg);
                }else{
                    return redirect()->back()->with('danger', $dangMsg);
                }
            }
            $subjects = $this->subject->getSubjectList();
            $className = ClassList::findOrFail($request->route()->parameter('classId'));
            $classSubjects = $this->classSubject->getAllClassSubjects($request->route()->parameter('classId'));
            return view('examManagement.classtest.subject_assign', compact('subjects', 'classSubjects', 'className'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function testAssign(Request $request){
        // try{
            $successMsg  = 'Test Successfully Assign !!!';
            $dangMsg = 'Unable to Assign Test..! Try Again';
            if($request->isMethod('post')){
                $data = $request->only('test_id', 'class_id');
                if($this->classTest->assignTest($this->classTest, $data)){
                    return redirect()->back()->with('success', $successMsg);
                }else{
                    return redirect()->back()->with('danger', $dangMsg);
                }
            }
            $tests = $this->test->getTestList();
            $className = ClassList::findOrFail($request->route()->parameter('classId'));
            $classTests  = $this->classTest->getAllClassTests($request->route()->parameter('classId'));
            return view('examManagement.classtest.test_assign', compact('tests', 'classTests', 'className'));
        // }catch (\Exception $e) {
        //     return redirect()->back()->with('warning', $e->getMessage());
        // }
    }
}
