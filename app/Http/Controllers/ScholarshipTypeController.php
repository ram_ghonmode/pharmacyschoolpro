<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\ScholarshipType;
use Session;

class ScholarshipTypeController extends Controller
{
    public $exceptionRoute;
    public $scholarshipType;

    public function __construct(ScholarshipType $scholarshipType){
        $this->middleware('auth');
        $this->exceptionRoute    = 'home';
        $this->scholarshipType   = $scholarshipType;
    }

    public function index(){
        try{
            $scholarshipTypes = $this->scholarshipType->getAllScholarshipTypes();
            return view('scholarshiptype.index', compact('scholarshipTypes'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->scholarshipType->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->scholarshipType->id)){
                $sucMsg  = 'Scholarship Type Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Scholarship Type..! Try Again';
            }else{
                $sucMsg  = 'Scholarship Type Successfully Created !!!';
                $dangMsg = 'Unable to Saved Scholarship Type..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateScholarshipType($this->scholarshipType, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('scholarship_type');
                if($this->scholarshipType->saveScholarshipType($this->scholarshipType, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('scholarshiptype-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $scholarshipType = $this->scholarshipType->getScholarshipTypeDetail($this->scholarshipType);
            return view('scholarshiptype.add', compact('scholarshipType'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateScholarshipType(ScholarshipType $scholarshipType, $data){
        $rules = [
            'scholarship_type' => ['required',
                Rule::unique($scholarshipType->getTable())->ignore($scholarshipType->id)->where(function($query) {
                $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'scholarship_type.required' => 'Scholarship Type is required.',
            'scholarship_type.unique'   => 'Scholarship Type has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
