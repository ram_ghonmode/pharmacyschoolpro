<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\Admission;
use App\ClassMaster;

class HomeController extends Controller
{
    public $admission;
    public $school;

    public function __construct(Admission $admission, School $school, ClassMaster $classMaster)
    {
        $this->admission      = $admission;
        $this->school         = $school;
        $this->classMaster    = $classMaster;
        $this->middleware('auth');
    }

    public function index()
    {   
        $data['schools'] = $this->school->getTotSchools();
        foreach ($data['schools'] as $key => $value) {
            $data['totStudent'][$value->id] = $this->admission->getCountOFTotStudForFirstSch($value->id);
            $data['regularStudent'][$value->id] = $this->admission->getCountOfRegularStud($value->id);
            $data['rteStudent'][$value->id] = $this->admission->getCountOfRTEStud($value->id);
            $data['currrentAdmission'][$value->id] = $this->admission->getCountOfCurStud($value);
        }
        return view('home', compact('data'));
    }

    public function feesCollection()
    {   
        $data['schools'] = $this->school->getTotSchools();
        foreach ($data['schools'] as $key => $value) {
            $totalFees = 0;
            $collectedFees = 0;
            $studList = $this->classMaster->getAllClassList($value->id);
            $feesData = $this->classMaster->fees($value->id);
            foreach($studList as $key => $val){
                foreach($feesData as $fkey => $fval){
                    if($val->id == $fval['stud_id']){
                        $totalFees += $fval['amount'];
                        $collectedFees += $fval['payfee'];
                    }
                }
            }
            $data['totStudent'][$value->id] = $this->admission->getCountOFTotStudForFirstSch($value->id);
            $data['totalFees'][$value->id] = $totalFees;
            $data['collectedFees'][$value->id] = $collectedFees;
        }
        return view('feescollection', compact('data'));
    }
}
