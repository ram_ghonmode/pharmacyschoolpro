<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\StudentAttendance;
use App\Admission;
use Session;
use Auth;
use DB;

class StudentAttendanceController extends Controller
{
    public function __construct(StudentAttendance $studentAttendance, Admission $admission){
        $this->middleware('auth');
        $this->exceptionroute = 'home';
        $this->studentAttendance = $studentAttendance;
        $this->admission = $admission;
    }

    public function index(){
        try {
            $classAttd = $this->studentAttendance->getAllClsAttendance();
            return view('attendance.index', compact('classAttd'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionroute)->with('warning', $e->getMessage());
        }
    }

    public function studentAttendance(Request $request){
        try{           
            if($request->isMethod('post')){
                $validator = $this->getValidateAttendance($this->studentAttendance, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if(isset($request->allstudents) && !empty($request->allstudents) && !empty($request->students)){
                    foreach ($request->allstudents as $id => $status) {
                        $savedata = [
                            'date'         => date('Y-m-d', strtotime($request->date)),
                            'teacher_id'   => Auth::id(),
                            'school_id'    => $request->school_id,
                            'class_id'     => $request->class_id,
                            'section'      => $request->section,
                            'admission_id' => $id,
                            'is_present'   => isset($request->students[$id]) ? $request->students[$id] : $status
                        ];
                        $save = $this->studentAttendance->saveStudentAttendance($this->studentAttendance, $savedata);
                    }
                }
                if(isset($save) && !empty($save)){
                    Session::flash('success', 'Attendance Taken Successfully !');
                    return redirect()->route('student-attendance-list');
                }else{
                    Session::flash('danger', 'Unable to Take Attendance ..! Try Again');
                }
            }
            $students = $this->admission->getClsStudents();
            return view('attendance.add',compact('students'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionroute)->with('warning', $e->getMessage());
        }
    }

    protected function viewAttendance($atten){
        try {
            $current = $this->studentAttendance::find($atten);
            $condition = [
                'date'          => $current['date'],
                'teacher_id'    => $current['teacher_id'],
                'school_id'     => $current['school_id'],
                'class_id'      => $current['class_id'],
                'section'       => $current['section'],
            ];
            $allStudents = $this->studentAttendance->getListOfAllStudent($condition);
            return view('attendance.view',compact('allStudents'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionroute)->with('warning', $e->getMessage());
        }
    }  
    
    public function attendanceChangeStatus($id){
        if(!empty($id)){
            $studentAttendance = StudentAttendance::findOrFail($id);
            $astatus = $studentAttendance['is_present'];
            if ($astatus == 0) {
                if ($studentAttendance->update(['is_present' => 1,'is_edited' => 1])) {
                    Session::flash('success', 'Attendance Updated Successfully');
                }
            }if ($astatus == 1) {
                if ($studentAttendance->update(['is_present' => 0,'is_edited' => 1])) {
                    Session::flash('success', 'Attendance Updated Successfully');
                }
            }
        }
        return redirect()->back();
    }

    protected function deleteAttendance($atten){
        try {
            $current = $this->studentAttendance::find($atten);
            $condition = [
                'date'          => $current['date'],
                'teacher_id'    => $current['teacher_id'],
                'school_id'     => $current['school_id'],
                'class_id'      => $current['class_id'],
                'section'       => $current['section'],
            ];
            $delAttenId = StudentAttendance::where($condition)->pluck('id');
            $delAttenData = DB::table('student_attendances')->whereIn('id', $delAttenId)->delete();
            if ($delAttenData) {
                Session::flash('success', 'Attendance Deleted Successfully');
                return redirect()->back();
            }
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionroute)->with('warning', $e->getMessage());
        }
    }  

    protected function getValidateAttendance(StudentAttendance $studentAttendance,$data){
        $condition = [
            'teacher_id'    => Auth::id(),
            'school_id'     => $data['school_id'],
            'class_id'      => $data['class_id'],
            'section'       => $data['section'],
            'date'          => date('Y-m-d', strtotime($data['date'])),
        ];
        $rules = [
            'date'    =>  ['required',
                                Rule::unique('student_attendances')->where(function ($query) use($data,$condition){
                                    $query->where($condition);
                            })
                        ]
        ];
        $errmsg = [
            'date.unique'    => "Attendance already taken for this date.",
            'date.required'  => "Date is required.",
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
