<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Exports\ClassRoomExport;
use App\Admission;
use App\ClassList;
use App\School;
use App\TeacherAllotment;
use App\AcademicYear;
use App\User;
use App\Employee;
use Session;
use Excel;

class ClassRoomController extends Controller
{
    public $exceptionRoute;
    public $admission;
    public $school;
    public $class;
    public $teacher;
    public $year;
    public $user;
    public $employee;

    public function __construct(ClassList $class, Admission $admission, School $school, TeacherAllotment $teacher, AcademicYear $year,User $user,Employee $employee){
        $this->middleware('auth');
        $this->exceptionRoute   = 'home';
        $this->admission        = $admission;
        $this->school           = $school;
        $this->class            = $class;
        $this->teacher          = $teacher;
        $this->year             = $year;
        $this->user             = $user;
        $this->employee         = $employee;
    }

    protected function getMasterData(){
        $data['schools']   = $this->school->getAllSchoolsList();
        $data['classes']   = $this->class->getAllClassList();
        $data['years']     = $this->year->getAllAcademicYearsList();
        return $data;
    }

    public function index(Request $request){
        try{
            $cond = $request->only('school_id', 'current_class');
            $upcond = array_filter($cond);
            $clsData = !empty($upcond) ? $this->admission->getAllClassList($upcond) : NULL;
            $data = $this->getMasterData();
            $data['school'] = !empty($cond['school_id']) ? School::select('id','school_name')->where('id',$cond['school_id'])->first() : '';
            $data['current_class'] = !empty($cond['current_class']) ? ClassList::select('id','class_name')->where('id',$cond['current_class'])->first() : '';
            return view('classroom.index', compact('clsData','data','upcond'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function teacherList(){
        try{
            $clsTeachers = $this->teacher->getAllClassTeachers();
            return view('teacherallotment.index', compact('clsTeachers'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function teacherAdd(Request $request){
        try{
            $this->teacher->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->teacher->id)){
                $sucMsg  = 'Class Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Class..! Try Again';
            }else{
                $sucMsg  = 'Class Successfully Created !!!';
                $dangMsg = 'Unable to Saved Class..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateClassTeacher($this->teacher, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('school_id','class_id','section','teacher_id','academic_year');
                if($this->teacher->saveClassTeacher($this->teacher, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('class-teacher-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $teacher = $this->teacher->getClassTeacherDetail($this->teacher);
            $empTeacher = $this->user->getTeacherEmp();
            $data = $this->getMasterData();
            return view('teacherallotment.add', compact('teacher','data','empTeacher'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    public function printClassList($school = 0,$class = 0,$section = 0){
        try {
            $cond = [
                'school_id'      => $school, 
                'current_class'  => $class, 
                'section'        => $section,
                'admission_status' => 'Pursuing'
            ];
            $upcond = array_filter($cond);
            $upcond['status'] = 1;
            $clsData = !empty($upcond) ? $this->admission->getAllClassList($upcond) : NULL;
            $data['school'] = !empty($school) ? School::select('id','school_name')->where('id',$school)->first() : '';
            $data['current_class'] = !empty($class) ? ClassList::select('id','class_name')->where('id',$class)->first() : '';
            if(!empty($clsData)){
                return view('classroom.print', compact('clsData','data'));
            }else{
                return view('classroom.print',compact('data'));
            }
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function printClassTeacher(){
        $allotmentData = TeacherAllotment::with('employees')->select('id','teacher_id','class_id','section','academic_year')->where('status',1)->get();
        return view('teacherallotment.print',compact('allotmentData'));
    }  

    public function studentPromote(Request $request){
        try{
            $msg = 'Students Promoted Sucessfully.';
            $cond = $request->only('school_id', 'current_class', 'section');
            $upcond = array_filter($cond);
            $clsData = !empty($upcond) ? $this->admission->getAllStdentList($upcond, $request) : NULL;
            $data = $this->getMasterData();
            $data['school'] = !empty($cond['school_id']) ? School::select('id','school_name')->where('id',$cond['school_id'])->first() : '';
            $data['current_class'] = !empty($cond['current_class']) ? ClassList::select('id','class_name')->where('id',$cond['current_class'])->first() : '';
            $arraySchool = $data['classes']->toArray();
            $input = $request->all();
            if($request->isMethod('post')){          
                $studData = $input['section'];
                foreach ($studData as $key => $value) {
                    if($value == 1){
                        $admission = Admission::findOrFail($key);
                        $classArr = ["KG 2", "7th", "10th", "12th"];
                        if(in_array($arraySchool[$admission->current_class], $classArr)){
                            $admission->admission_status = 'Passout';
                        }else{
                            $admission->current_class = ++$admission->current_class;
                        }
                        $saveAdmission = $admission->save();
                        if($saveAdmission){
                            $msg = 'Students Promoted Sucessfully.';
                        }else{
                            $msg = 'Students Promoted Unsucessful.';
                        }
                    }
                } 
                Session::flash('success', $msg);
                return redirect()->route('student-class-list');
            }
            return view('classroom.studentpromote', compact('clsData','data','upcond'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function classRoomExcelExport(Request $request) {
        $req = $request->all();
        $headers = array('Content-Type' => 'text/csv');
        return Excel::download(new ClassRoomExport($req), 'class-room-list.xlsx');
    }

    protected function getValidateClassTeacher(TeacherAllotment $teacher, $data){
        $rules = [
            'school_id'   => 'required',
            'class_id'    => 'required',
            'section'     => 'required',
            'teacher_id'  => 'required',
            'academic_year'  => 'required',
        ];
        $errmsg = [
            'school_id.required'     => 'School name is required.',
            'class_id.required'      => 'Class name is required.',
            'section.required'       => 'Section is required.',
            'teacher_id.required'    => 'Teacher name is required.',
            'academic_year.required' => 'Academic Year is required.',
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
