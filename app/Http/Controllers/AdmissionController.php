<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\AcademicYear;
use App\MediumStream;
use App\Admission;
use App\School;
use App\Country;
use App\State;
use App\City;
use App\Tahsil;
use App\ClassList;
use App\Category;
use App\AdmissionType;
use App\ScholarshipType;
use App\Document;
use Session;

class AdmissionController extends Controller
{
    public $exceptionRoute;
    public $admission;
    public $medium;
    public $school;
    public $country;
    public $tahsil;
    public $state;
    public $city;
    public $year;
    public $class;
    public $category;
    public $admissionType;
    public $scholarshipType;
    public $document;

    public function __construct(ClassList $class, AcademicYear $year, MediumStream $medium, Admission $admission, School $school, Country $country, State $state, City $city, Tahsil $tahsil, Category $category, AdmissionType $admissionType, ScholarshipType $scholarshipType, Document $document){
        $this->middleware('auth');
        $this->exceptionRoute   = 'home';
        $this->admission        = $admission;
        $this->medium           = $medium;
        $this->school           = $school;
        $this->country          = $country;
        $this->state            = $state;
        $this->city             = $city;
        $this->tahsil           = $tahsil;
        $this->year             = $year;
        $this->class            = $class;
        $this->category         = $category;
        $this->admissionType    = $admissionType;
        $this->scholarshipType  = $scholarshipType;
        $this->document         = $document;
    }

    protected function getMasterData(){
        $data['countries'] = $this->country->getAllCountries();
        $data['states']    = $this->state->getAllStates();
        $data['cities']    = $this->city->getAllCities();
        $data['tahsils']   = $this->tahsil->getAllTAhsils();
        $data['schools']   = $this->school->getAllSchoolsList();
        $data['mediums']   = $this->medium->getAllMediumList();
        $data['streams']   = $this->medium->getAllStreamsList();
        $data['years']     = $this->year->getAllAcademicYearsList();
        $data['classes']   = $this->class->getAllClassList();
        $data['categories'] = $this->category->getAllCategoryList();
        $data['admissionTypes'] = $this->admissionType->getAllAdmissionTypeList();
        $data['scholarshipTypes'] = $this->scholarshipType->getAllScholarshipTypeList();
        $data['documents'] = $this->document->getAllDocumentList();
        
        return $data;
    }

    public function index(){
        try{
            $admissions = $this->admission->getAllAdmissions();
            return view('admissions.index', compact('admissions'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->admission->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->admission->id)){
                $sucMsg  = 'Admission Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Admission..! Try Again';
            }else{
                $sucMsg  = 'Admission Successfully Created !!!';
                $dangMsg = 'Unable to Saved Admission..! Try Again';
            }
            if($request->isMethod('post')){
                // $validator = $this->getValidateAdmission($this->admission, $request->all());
                // if ($validator->fails()) {
                //     return redirect()->back()->withErrors($validator)->withInput();
                // }
                if($this->admission->saveAdmission($this->admission, $request->all())){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('admission-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $data = $this->getMasterData();
            $admission = $this->admission->getAdmissionDetail($this->admission);
            return view('admissions.add', compact('admission', 'data'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateAdmission(Admission $admission, $reqdata){
        $rules = [
            'school_id'             => 'required',
            // 'admission_type'        => 'required',
            'gr_number'             => 'required',
            // 'student_id'            => 'required',
            // 'first_name'            => 'required',
            // 'middle_name'           => 'required',
            // 'last_name'             => 'required',
            // 'father_first_name'     => 'required',
            // 'father_last_name'      => 'required',
            // 'mother_name'           => 'required',
            // 'gender'                => 'required',
            // 'admission_date'        => 'required',
            // 'academic_year'         => 'required',
            // 'admit_to'              => 'required',
            // 'current_class'         => 'nullable',
            // 'section'               => 'required',
            // 'medium'                => 'required',
            // 'dob'                   => 'required',
            // 'age'                   => 'nullable|numeric',
            // 'country'               => 'required',
            // 'state'                 => 'required',
            // 'district'              => 'required',
            // 'tahsil'                => 'required',
            // 'mobile_no'             => 'required|numeric'
        ];
        $errmsg = [
            'school_id.required'            => 'School is required.',
            // 'admission_type.required'       => 'Admission in is required.',
            'gr_number.required'            => 'General no. is required.',
            // 'student_id.required'           => 'Student Id is required.',
            // 'first_name.required'           => 'First name is required.',
            // 'middle_name.required'          => 'Middle name is required.',
            // 'last_name.required'            => 'Last name is required.',
            // 'father_first_name.required'    => 'First name is required.',
            // 'father_last_name.required'     => 'Last name is required.',
            // 'mother_name.required'          => 'Mother name is required.',
            // 'gender.required'               => 'Gender is required.',
            // 'admission_date.required'       => 'Admission date is required.',
            // 'academic_year.required'        => 'Academic year is required.',
            // 'admit_to.required'             => 'Admit to is required.',
            // 'current_class.required'        => 'Current class is required.',
            // 'section.required'              => 'Class section is required.',
            // 'medium.required'               => 'Medium is required.',
            // 'dob.required'                  => 'DOB is required.',
            // 'age.numeric'                   => 'Age must be digits.',
            // 'country.required'              => 'Country is required.',
            // 'state.required'                => 'State is required.',
            // 'district.required'             => 'District is required.',
            // 'tahsil.required'               => 'Tahsil is required.',
            // 'mobile_no.required'            => 'Contact no. is required.',
            // 'mobile_no.numeric'             => 'Contact no. must be digits.'
        ];
        return Validator::make($reqdata, $rules, $errmsg);
    }
}
