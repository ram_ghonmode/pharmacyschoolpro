<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\Subject;
use Session;

class SubjectController extends Controller
{
    public $exceptionRoute;
    public $subject;

    public function __construct(Subject $subject){
        $this->middleware('auth');
        $this->exceptionRoute = 'home';
        $this->subject        = $subject;
    }

    public function index(){
        try{
            $subjects = $this->subject->getAllSubjects();
            return view('examManagement.subject.index', compact('subjects'));
        }catch (\Exception $e) {
            return redirect()->route($this->exceptionRoute)->with('warning', $e->getMessage());
        }
    }

    public function add(Request $request){
        try{
            $this->subject->id = $this->cryptString($request->route()->parameter('id'), "d");
            if(!empty($this->subject->id)){
                $sucMsg  = 'Subject Successfully Updated !!!';
                $dangMsg = 'Unable to Updated Subject..! Try Again';
            }else{
                $sucMsg  = 'Subject Successfully Created !!!';
                $dangMsg = 'Unable to Saved Subject..! Try Again';
            }
            if($request->isMethod('post')){
                $validator = $this->getValidateSubject($this->subject, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                $data = $request->only('subject_name');
                if($this->subject->saveSubject($this->subject, $data)){
                    Session::flash('success', $sucMsg);
                    return redirect()->route('subject-list');
                }else{
                    Session::flash('danger', $dangMsg);
                }
            }
            $subject = $this->subject->getSubjectDetail($this->subject);
            return view('examManagement.subject.add', compact('subject'));
        }catch (\Exception $e) {
            return redirect()->back()->with('warning', $e->getMessage());
        }
    }

    protected function getValidateSubject(Subject $subject, $data){
        $rules = [
            'subject_name' => ['required',
                    Rule::unique($subject->getTable())->ignore($subject->id)->where(function($query) {
                    $query->where('status', 1);
                }),
            ]
        ];
        $errmsg = [
            'subject_name.required' => 'Subject name is required.',
            'subject_name.unique'   => 'Subject name has been already taken.'
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
