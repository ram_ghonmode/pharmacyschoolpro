<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class FeesHead extends Model
{
    protected $fillable = ['fees_head_name','is_admission','is_management','fees_type'];

    public function getAllFeesHeads(){
        return FeesHead::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllFeesHeadList(){
        return FeesHead::where('status',1)->pluck('fees_head_name','id');
    }

    public function getClassFeesHeadId(){    	
        return FeesHead::select('id','fees_head_name', 'is_admission','fees_type')->get();
      }

    public function saveFeesHead(FeesHead $feesHead, $data){
        $saveResult = false;
        $saveResult = FeesHead::updateOrCreate(['id' => isset($feesHead->id) ? $feesHead->id : 0], $data);
        return $saveResult;
    }

    public function getFeesHeadDetail(FeesHead $feesHead){
        $feesHeadDetail = false;
        $feesHeadDetail = FeesHead::where('id', isset($feesHead->id) ? $feesHead->id : 0)->first();
        return $feesHeadDetail;
    }

    public function getFeesHeadGrid($stdData){
        $acYear = $stdData['academic_year'];
        $schAcYear = School::where('id',$stdData['school_id'])->pluck('academic_year')->first();
        $feeRecord = StudentYearlyFees::where(['status' => 1,'admission_id' => $stdData['stud_id'],'current_class' =>  $stdData['current_class'], 'academic_year' => $schAcYear])->get();
        $claMasId = ClassMaster::select('id', 'class_id')->where(['class_id'=> $stdData['current_class'], 'admission_type_id' => $stdData['admission_type_id']])->pluck('id')->first();
        $clsYearlyFees = ClassYearlyFees::where('class_master_id',$claMasId,)->with('feesHead')->get();
        $newArr = [];
        $outAssoArray = [];
        if(!empty($feeRecord) && count($feeRecord) > 0) {
            foreach ($feeRecord as $key => $value) {
                foreach ($value->studentYearlyFeesPaid as $k => $val) {
                    if(isset($outAssoArray[$val->head_id])){
                        $outAssoArray[$val->head_id] = $outAssoArray[$val->head_id] + (isset($val->amount) ? $val->amount : 0);
                    }else{
                        $outAssoArray[$val->head_id] = isset($val->amount) ? $val->amount : 0;
                    }
                }
            }
            if(!empty($outAssoArray)){
                foreach($outAssoArray as $k => $val){
                    foreach($clsYearlyFees as $key => $clsFees){
                        if($k == $clsFees->fees_head_id){
                            if($clsFees->feesHead->is_admission == 0 /*&& $clsFees->feesHead->fees_type == null*/){
                                $newArr[$key] = [
                                    'head_id'       => $clsFees['feesHead']['id'],
                                    'head_name'     => $clsFees['feesHead']['fees_head_name'],
                                    'total_amount'  => $clsFees['amount'],
                                    'paid_amount'   => $val,
                                ];
                            }
                            elseif(/*$clsFees->feesHead->fees_type == 'Old'  &&*/ $acYear != $schAcYear){
                                $newArr[$key] = [
                                    'head_id'       => $clsFees['feesHead']['id'],
                                    'head_name'     => $clsFees['feesHead']['fees_head_name'],
                                    'total_amount'  => $clsFees['amount'],
                                    'paid_amount'   => $val,
                                ];
                            }
                            elseif(($acYear == $schAcYear) && ($clsFees->feesHead->is_admission == 1)){
                                //if($clsFees->feesHead->fees_type == null){
                                    $newArr[$key] = [
                                        'head_id'       => $clsFees['feesHead']['id'],
                                        'head_name'     => $clsFees['feesHead']['fees_head_name'],
                                        'total_amount'  => $clsFees['amount'],
                                        'paid_amount'   => $val,
                                    ];
                                //}
                                //else{
                                    // if($stdData['fees_type'] == $clsFees->feesHead->fees_type){
                                    //     $newArr[$key] = [
                                    //         'head_id'       => $clsFees['feesHead']['id'],
                                    //         'head_name'     => $clsFees['feesHead']['fees_head_name'],
                                    //         'total_amount'  => $clsFees['amount'],
                                    //         'paid_amount'   => $val,
                                    //     ];
                                    //     //internal or external fees is there if not then 0
                                    // }else{
                                    //     $newArr[$key] = [
                                    //         'head_id'       => $clsFees['feesHead']['id'],
                                    //         'head_name'     => $clsFees['feesHead']['fees_head_name'],
                                    //         'total_amount'  => 0,
                                    //         'paid_amount'   => 0,
                                    //     ];
                                    // }
                            }
                            else{
                                $newArr[$key] = [
                                    'head_id'       => $clsFees['feesHead']['id'],
                                    'head_name'     => $clsFees['feesHead']['fees_head_name'],
                                    'total_amount'  => 0,
                                    'paid_amount'   => $val,
                                ];
                            }
                        }
                    }
                }
            }
        }
        else{
            foreach($clsYearlyFees as $key => $clsFees){
                if($clsFees->feesHead->is_admission == 0 /*&& $clsFees->feesHead->fees_type == null*/){
                    $newArr[$key] = [
                        'head_id'       => $clsFees['feesHead']['id'],
                        'head_name'     => $clsFees['feesHead']['fees_head_name'],
                        'total_amount'  => $clsFees['amount'],
                        'paid_amount'   => 0,
                    ];
                }
        // elseif($clsFees->feesHead->fees_type == 'Old'  && $acYear != $schAcYear){
        //             $newArr[$key] = [
        //                 'head_id'       => $clsFees['feesHead']['id'],
        //                 'head_name'     => $clsFees['feesHead']['fees_head_name'],
        //                 'total_amount'  => $clsFees['amount'],
        //                 'paid_amount'   => 0,
        //             ];
        //         }elseif(($acYear == $schAcYear) && ($clsFees->feesHead->is_admission == 1)){
        //             if($clsFees->feesHead->fees_type == null){
        //                 $newArr[$key] = [
        //                     'head_id'       => $clsFees['feesHead']['id'],
        //                     'head_name'     => $clsFees['feesHead']['fees_head_name'],
        //                     'total_amount'  => $clsFees['amount'],
        //                     'paid_amount'   => 0,
        //                 ];
        //             }else{
        //                 if($stdData['fees_type'] == $clsFees->feesHead->fees_type){
        //                     $newArr[$key] = [
        //                         'head_id'       => $clsFees['feesHead']['id'],
        //                         'head_name'     => $clsFees['feesHead']['fees_head_name'],
        //                         'total_amount'  => $clsFees['amount'],
        //                         'paid_amount'   => 0,
        //                     ];
        //                 }else{
        //                     $newArr[$key] = [
        //                         'head_id'       => $clsFees['feesHead']['id'],
        //                         'head_name'     => $clsFees['feesHead']['fees_head_name'],
        //                         'total_amount'  => 0,
        //                         'paid_amount'   => 0,
        //                     ];
        //                 }
        //             }
        //         }
                else{
                    $newArr[$key] = [
                        'head_id'       => $clsFees['feesHead']['id'],
                        'head_name'     => $clsFees['feesHead']['fees_head_name'],
                        'total_amount'  => 0,
                        'paid_amount'   => 0,
                    ];
                }
            }
        }
        return $newArr;
    }
}