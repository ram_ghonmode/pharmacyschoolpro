<?php

namespace App;
use Config;
use Auth;
use Illuminate\Database\Eloquent\Model;

class ScholarshipType extends Model
{
    protected $fillable = ['scholarship_type'];

    public function getAllScholarshipTypes(){
        return ScholarshipType::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllScholarshipTypeList(){
        return ScholarshipType::where('status',1)->pluck('scholarship_type', 'id');
    }

    public function saveScholarshipType(ScholarshipType $scholarshipType, $data){
        $saveResult = false;
        $saveResult = ScholarshipType::updateOrCreate(['id' => isset($scholarshipType->id) ? $scholarshipType->id : 0], $data);
        return $saveResult;
    }

    public function getScholarshipTypeDetail(ScholarshipType $scholarshipType){
        $scholarshipTypeDetail = false;
        $scholarshipTypeDetail = ScholarshipType::where('id', isset($scholarshipType->id) ? $scholarshipType->id : 0)->first();
        return $scholarshipTypeDetail;
    }
}
