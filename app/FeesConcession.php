<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeesConcession extends Model
{
    protected $fillable = ['remark','student_id','gr_number','current_class','section','school_id','amount','date','academic_year','admission_type'];
}