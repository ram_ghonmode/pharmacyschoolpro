<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;
    
class ClassMaster extends Model
{
    protected $fillable = ['class_id','school_id','academic_year','college_stream','category_id','admission_type_id'];

    public function classes(){
        return $this->belongsTo('App\ClassList', 'class_id');
    }

    public function school(){
        return $this->belongsTo('App\School', 'school_id');
    }

    public function academicYears(){
        return $this->belongsTo('App\AcademicYear', 'academic_year');
    }

    public function stream(){
        return $this->belongsTo('App\MediumStream', 'college_stream');
    }

    public function admissionType(){
        return $this->belongsTo('App\AdmissionType', 'admission_type_id');
    }

    public function categories(){
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function classfees(){
        return $this->hasMany('App\ClassYearlyFees','class_master_id','App\FeesHead','id');
    }

    public function classfeesamt(){
        return $this->hasMany('App\ClassYearlyFees','class_master_id');
    }

    public function getAllClasses(){
        $data = ClassMaster::orderBy('id', 'ASC')->where('status', 1)->with('classes','school','academicYears','stream','categories','admissionType','classfeesamt.feesHead','classfees.feesHead')->paginate(Config::get('constant.datalength'));
        return $data;
    }

    public function getAllClassMasterList(){
        return ClassMaster::where('status',1)->pluck('class_id','school_id','academic_year','college_stream','id','category_id','scholarship_type_id');
    }

    public function getClassList($class, $ay){
        return ClassMaster::select('id', 'class_id')->where('class_id', $class)->first();
    }
    
    public function getStudentFeesList($cond){
        $allstudents = Admission::select(['id','gr_number','student_id','first_name','middle_name','last_name','fullname_in_hindi','gender','dob','admission_date','academic_year','admit_to','admission_in','current_class', 'college_stream', 'category_id','admission_type_id'])
                        ->orderBy('id','ASC')
                        ->where(['admission_status' => 'Pursuing'/*, 'scholarship_type_id' => 'Regular'*/,'school_id' => $cond['school_id'],'current_class' => $cond['class_id'], 'status' => 1])
                        ->with('academicYears','classes')
                        ->get();
        $classList = ClassMaster::select('id', 'class_id','admission_type_id')->where($cond)->first();
        $school = School::select('academic_year')->where('id',$cond['school_id'])->first();
        $k = 1; $studFee = [];
        if($classList != null){
            foreach ($allstudents as $key => $value) {
                $classList = ClassMaster::select('id', 'class_id')->where(['class_id'=> $value['current_class'], 'admission_type_id' => $value['admission_type_id']])->first();
               // if ($school['academic_year'] == $value['academic_year']) {
                    $FeeAmt = $this->classFeeTotal($classList);
                    $PaidAmt = $this->paidFeesYearlyTotal($value, $cond);
                    if($value->fees_type == "External"){
                        $studFee[$k] = [    
                            'gr_number'     => $value['gr_number'],
                            'first_name'    => $value['first_name'],
                            'middle_name'   => $value['middle_name'],
                            'last_name'     => $value['last_name'],
                            'current_class' => $value['classes']['class_name'],
                            'academic_year' => $school['academicYears']['academic_year'],
                            'amount'        =>  $FeeAmt['newExtAdm'],
                            'payfee'        => $PaidAmt,
                            'remainingpayfee' => $FeeAmt['newExtAdm']-$PaidAmt
                        ];
                    }    
                    else{  
                        $studFee[$k] = [	
                            'gr_number'     => $value['gr_number'],
                            'first_name'    => $value['first_name'],
                            'middle_name'   => $value['middle_name'],
                            'last_name'     => $value['last_name'],
                            'current_class' => $value['classes']['class_name'],
                            'academic_year' => $school['academicYears']['academic_year'],
                            'amount'        => $FeeAmt['newIntAdm'],
                            'payfee'        => $PaidAmt,
                            'remainingpayfee' => $FeeAmt['newIntAdm']-$PaidAmt
                        ];
                    }
                // }
                // else{
                //     $FeeAmt = $this->classFeeTotal($classList);
                //     $PaidAmt = $this->paidFeesYearlyTotal($value, $cond);
                //     $studFee[$k] = [
                //         'gr_number'     => $value['gr_number'],
                //         'first_name'    => $value['first_name'],
                //         'middle_name'   => $value['middle_name'],
                //         'last_name'     => $value['last_name'],
                //         'current_class' => $value['classes']['class_name'],
                //         'academic_year' => $school['academicYears']['academic_year'],
                //         'amount'        => $FeeAmt['old'],
                //         'payfee'        => $PaidAmt,
                //         'remainingpayfee' => $FeeAmt['old']-$PaidAmt
                //     ];
                // }
                $k++;
            }
        }
        return $studFee;
    }

    public function getAllClassList($schoolId){
        $allstudents = Admission::select(['id','gr_number','student_id','first_name','middle_name','last_name','fullname_in_hindi','gender','dob','admission_date','academic_year','admit_to','admission_in','current_class', 'college_stream','category_id'])
                        ->orderBy('id','ASC')
                        ->where(['admission_status' => 'Pursuing','school_id' => $schoolId, 'status' => 1])
                        ->with('academicYears','classes')
                        ->get();  
        $classList = ClassMaster::where('school_id',$schoolId)->get();
        $studList  = [];
        foreach($allstudents as $key => $val){
            foreach($classList as $ckey => $cval){
                if($cval->class_id == $val->current_class){
                    $studList[$val->id] = $val;
                }
            }
        }
        return $studList;
    }

    public function getAllStudentClassList($schoolId){
        $allstudents = Admission::select(['id','gr_number','student_id','school_id','first_name','middle_name','last_name','fullname_in_hindi','gender','dob','admission_date','academic_year','admit_to','admission_in','current_class', 'college_stream','category_id','admission_type_id'])
                        ->orderBy('id','ASC')
                        ->get();        
        $classList = ClassMaster::where('school_id',$schoolId)->get();
        $studList  = [];
        foreach($allstudents as $key => $val){
            foreach($classList as $ckey => $cval){
                if($cval->class_id == $val->current_class){
                    $studList[$val->id] = $val;
                }
            }
        }
        return $studList;
    }
    
    public function fees($schoolId){
        $allstudents = Admission::select(['id','gr_number','student_id','school_id','first_name','middle_name','last_name','fullname_in_hindi','gender','dob','admission_date','academic_year','admit_to','admission_in','current_class', 'college_stream','category_id','admission_type_id'])
                        ->orderBy('id','ASC')
                        ->where(['admission_status' => 'Pursuing', 'status' => 1])
                        ->with('academicYears')
                        ->get();
        $school = School::select('id')->where('id',$schoolId)->first();
        $k = 1; $studFee = [];
        foreach ($allstudents as $key => $value) {
            //if ($school['academic_year'] == $value['academic_year']) {
                $classdata = ClassMaster::select('id', 'class_id')->where(['class_id'=> $value['current_class'], 'admission_type_id' => $value['admission_type_id']])->first();
                $FeeAmt = $this->classFeeTotal($classdata);
                $PaidAmt = $this->paidFeesTotal($value);
                $studFee[$k] = [    
                    'stud_id' => $value['id'],
                    'stud_gr' => $value['gr_number'],
                    'amount'  => $FeeAmt['newIntAdm'],
                    'payfee'  => $PaidAmt
                ];
            //}
            $k++;
        }
        return $studFee;
    }

    
    public function studentFees($stuId){
        $studentData = Admission::where(['id' => $stuId])
                                ->with('academicYears','classes')
                                ->first();
        $school = School::select('academic_year')->where('id',$studentData['school_id'])->first();
        $classdata = ClassMaster::select('id', 'class_id')->where(['class_id'=> $studentData['current_class'], 'admission_type_id' => $studentData['admission_type_id']])->first();
        $FeeAmt = $this->classFeeTotal($classdata);
        $PaidAmt = $this->paidFeesTotal($studentData);
        $studFeesData = [];
        if ($school['academic_year'] == $studentData['academic_year']) {
            if($studentData->fees_type == "External"){
               $studFeesData = [    
                    'stud_id' => $studentData['id'],
                    'school_id' => $studentData['school_id'],
                    'first_name' => $studentData['first_name'],
                    'middle_name' => $studentData['middle_name'],
                    'last_name' => $studentData['last_name'],
                    'section' => $studentData['section'],
                    'college_stream' => $studentData['college_stream'],
                    'academic_year' => $studentData['academic_year'],
                    'admission_in' => $studentData['admission_in'],
                    'current_class' => $studentData['current_class'],
                    //'fees_type' => $studentData['fees_type'],
                    'class_name' => $studentData['classes']['class_name'],
                    'gr_number' => $studentData['gr_number'],
                    'amount'  => $FeeAmt['newExtAdm'],
                    'admission_type_id' => $studentData['admission_type_id'],
                    'payfee'  => $PaidAmt
                ];
            }
            else{

               $studFeesData = [	
                    'stud_id' => $studentData['id'],
                    'school_id' => $studentData['school_id'],
                    'first_name' => $studentData['first_name'],
                    'middle_name' => $studentData['middle_name'],
                    'last_name' => $studentData['last_name'],
                    'section' => $studentData['section'],
                    'college_stream' => $studentData['college_stream'],
                    'academic_year' => $studentData['academic_year'],
                    'admission_in' => $studentData['admission_in'],
                    'current_class' => $studentData['current_class'],
                    //'fees_type' => $studentData['fees_type'],
                    'class_name' => $studentData['classes']['class_name'],
                    'gr_number' => $studentData['gr_number'],
                    'amount'  => $FeeAmt['newIntAdm'],
                    'admission_type_id' => $studentData['admission_type_id'],
                    'payfee'  => $PaidAmt
                ];
            }
        }
        else{
           $studFeesData = [	
                'stud_id' => $studentData['id'],
                'school_id' => $studentData['school_id'],
                'first_name' => $studentData['first_name'],
                'middle_name' => $studentData['middle_name'],
                'last_name' => $studentData['last_name'],
                'section' => $studentData['section'],
                'college_stream' => $studentData['college_stream'],
                'academic_year' => $studentData['academic_year'],
                'admission_in' => $studentData['admission_in'],
                'current_class' => $studentData['current_class'],
                //'fees_type' => $studentData['fees_type'],
                'class_name' => $studentData['classes']['class_name'],
                'gr_number' => $studentData['gr_number'],
                'amount'  => $FeeAmt['old'],
                'admission_type_id' => $studentData['admission_type_id'],
                'payfee'  => $PaidAmt
            ];
        }  
        return $studFeesData;
    }

    public function saveClass(ClassMaster $class, $data){
        $saveResult = false;
        $saveResult = ClassMaster::updateOrCreate(['id' => isset($class->id) ? $class->id : 0], $data);
        return $saveResult;
    }

    public function getClassDetail(ClassMaster $class){
        $classDetail = false;
        $classDetail = ClassMaster::where('id', isset($class->id) ? $class->id : 0)->first();
        return $classDetail;
    }

    public function getClass($id){
        $classDetail = false;
        $classDetail = ClassMaster::where('id', isset($id) ? $id : 0)->with('classes','school','academicYears','stream')->first();
        return $classDetail;
    }

    public function classFeeTotal($classdata){
        $totFee = [
            'old'       => 0,
            'newExtAdm' => 0,
            'newIntAdm' => 0,
        ];
        $oldFees=0; $newAdmFees = 0; $oldAdmFees = 0; $oldStdFees = 0;
        $feeRecord = !empty($classdata) ? ClassYearlyFees::where('class_master_id', $classdata['id'])->with('feesHead')->get() : '';
            if (!empty($feeRecord)) {
                foreach ($feeRecord as $key => $value){
                    if($value->feesHead->is_admission == 0){
                        $oldFees += $value->amount;
                    }
                    if($value->feesHead->fees_type == 'Old'){
                        $oldStdFees += $value->amount;
                    }
                    if((($value->feesHead->is_admission == 1)) || (($value->feesHead->is_admission == 1))){
                        $oldAdmFees += $value->amount;
                    }
                    if((($value->feesHead->is_admission == 1) ) || (($value->feesHead->is_admission == 1))){
                        $newAdmFees += $value->amount;
                    }
                }
            }
        $totFee = [
            'old'       => $oldStdFees + $oldFees,
            'newExtAdm' => $newAdmFees + $oldFees,
            'newIntAdm' => $oldAdmFees + $oldFees,
        ];
        return $totFee;
    }

    public function paidFeesTotal($data){
        $payFee = 0; 
        $schCurAcYr =  $this->getSchholAcademicYr($data); 
		$feeRecord = StudentYearlyFees::where(['admission_id' => $data->id, 'admission_in' => $data->admission_in, 'current_class' =>  $data->current_class,'academic_year' => $schCurAcYr,'status' => 1])->get();
        if (!empty($feeRecord)) {
            foreach ($feeRecord as $key => $value) {
                $payFee = $payFee + $value['amount'];  
            }
            
        }

		return $payFee;

    }

    public function paidFeesYearlyTotal($data, $cond){
        $payFee = 0; 
		$feeRecord = StudentYearlyFees::where(['admission_id' => $data->id, 'admission_in' => $data->admission_in, 'current_class' =>  $data->current_class,'academic_year' => $cond['academic_year'],'status' => 1])->get();
        foreach ($feeRecord as $key => $value) {
            $payFee = $payFee + $value['amount'];
        }
        return $payFee;
    }
    
    public function getSchholAcademicYr($data){
        $schAcYr = School::where('id',$data->school_id)->pluck('academic_year')->first();
        return $schAcYr;
    }
}
