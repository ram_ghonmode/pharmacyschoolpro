<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['country_name'];

    public function getAllCountries(){
        return Country::where('status', 1)->pluck('country_name', 'id');
    }
}
