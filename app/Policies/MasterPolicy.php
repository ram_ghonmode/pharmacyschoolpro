<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class MasterPolicy
{
    use HandlesAuthorization;

    public static function schoolPolicies()
    {
        Gate::define('school-list', function ($user) {
            return $user->hasAccess(['school-list']);
        });

        Gate::define('school-add', function ($user) {
            return $user->hasAccess(['school-add']);
        });

        Gate::define('school-edit', function ($user) {
            return $user->hasAccess(['school-edit']);
        });

        Gate::define('school-delete', function ($user) {
            return $user->hasAccess(['school-delete']);
        });
    }

    public static function mediumPolicies()
    {
        Gate::define('medium-list', function ($user) {
            return $user->hasAccess(['medium-list']);
        });

        Gate::define('medium-add', function ($user) {
            return $user->hasAccess(['medium-add']);
        });

        Gate::define('medium-edit', function ($user) {
            return $user->hasAccess(['medium-edit']);
        });

        Gate::define('medium-delete', function ($user) {
            return $user->hasAccess(['medium-delete']);
        });
    }

    public static function academicYearPolicies()
    {
        Gate::define('academic-year-list', function ($user) {
            return $user->hasAccess(['academic-year-list']);
        });

        Gate::define('academic-year-add', function ($user) {
            return $user->hasAccess(['academic-year-add']);
        });

        Gate::define('academic-year-edit', function ($user) {
            return $user->hasAccess(['academic-year-edit']);
        });

        Gate::define('academic-year-delete', function ($user) {
            return $user->hasAccess(['academic-year-delete']);
        });
    }

    public static function classPolicies()
    {
        Gate::define('class-list', function ($user) {
            return $user->hasAccess(['class-list']);
        });

        Gate::define('class-add', function ($user) {
            return $user->hasAccess(['class-add']);
        });

        Gate::define('class-edit', function ($user) {
            return $user->hasAccess(['class-edit']);
        });

        Gate::define('class-delete', function ($user) {
            return $user->hasAccess(['class-delete']);
        });
    }

    public static function designationPolicies()
    {
        Gate::define('designation-list', function ($user) {
            return $user->hasAccess(['designation-list']);
        });

        Gate::define('designation-add', function ($user) {
            return $user->hasAccess(['designation-add']);
        });

        Gate::define('designation-edit', function ($user) {
            return $user->hasAccess(['designation-edit']);
        });

        Gate::define('designation-delete', function ($user) {
            return $user->hasAccess(['designation-delete']);
        });
    }

    public static function allowancePolicies()
    {
        Gate::define('allowance-list', function ($user) {
            return $user->hasAccess(['allowance-list']);
        });

        Gate::define('allowance-add', function ($user) {
            return $user->hasAccess(['allowance-add']);
        });

        Gate::define('allowance-edit', function ($user) {
            return $user->hasAccess(['allowance-edit']);
        });

        Gate::define('allowance-delete', function ($user) {
            return $user->hasAccess(['allowance-delete']);
        });
    }

    public static function deductionPolicies()
    {
        Gate::define('deduction-list', function ($user) {
            return $user->hasAccess(['deduction-list']);
        });

        Gate::define('deduction-add', function ($user) {
            return $user->hasAccess(['deduction-add']);
        });

        Gate::define('deduction-edit', function ($user) {
            return $user->hasAccess(['deduction-edit']);
        });

        Gate::define('deduction-delete', function ($user) {
            return $user->hasAccess(['deduction-delete']);
        });
    }

    public static function accountPolicies()
    {
        Gate::define('account-list', function ($user) {
            return $user->hasAccess(['account-list']);
        });

        Gate::define('account-add', function ($user) {
            return $user->hasAccess(['account-add']);
        });

        Gate::define('account-edit', function ($user) {
            return $user->hasAccess(['account-edit']);
        });

        Gate::define('account-delete', function ($user) {
            return $user->hasAccess(['account-delete']);
        });
    }

    public static function feesHeadPolicies()
    {
        Gate::define('fees-head-list', function ($user) {
            return $user->hasAccess(['fees-head-list']);
        });

        Gate::define('fees-head-add', function ($user) {
            return $user->hasAccess(['fees-head-add']);
        });

        Gate::define('fees-head-edit', function ($user) {
            return $user->hasAccess(['fees-head-edit']);
        });

        Gate::define('fees-head-delete', function ($user) {
            return $user->hasAccess(['fees-head-delete']);
        });
    }

    public static function leaveTypePolicies()
    {
        Gate::define('leave-type-list', function ($user) {
            return $user->hasAccess(['leave-type-list']);
        });

        Gate::define('leave-type-add', function ($user) {
            return $user->hasAccess(['leave-type-add']);
        });

        Gate::define('leave-type-edit', function ($user) {
            return $user->hasAccess(['leave-type-edit']);
        });

        Gate::define('leave-type-delete', function ($user) {
            return $user->hasAccess(['leave-type-delete']);
        });
    }
}
