<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentAttendancePolicy
{
    use HandlesAuthorization;

    public static function studentAttendancePolicies(){
        Gate::define('student-attendance-list', function ($user) {
            return $user->hasAccess(['student-attendance-list']);
        });

        Gate::define('student-attendance-add', function ($user) {
            return $user->hasAccess(['student-attendance-add']);
        });

        Gate::define('attendance-status', function ($user) {
            return $user->hasAccess(['attendance-status']);
        });

        Gate::define('delete-attendance', function ($user) {
            return $user->hasAccess(['delete-attendance']);
        });
    }
}
