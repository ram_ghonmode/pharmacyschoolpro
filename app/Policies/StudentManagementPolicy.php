<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentManagementPolicy
{
    use HandlesAuthorization;

    public static function admissionPolicies()
    {
        Gate::define('admission-list', function ($user) {
            return $user->hasAccess(['admission-list']);
        });

        Gate::define('admission-add', function ($user) {
            return $user->hasAccess(['admission-add']);
        });

        Gate::define('admission-edit', function ($user) {
            return $user->hasAccess(['admission-edit']);
        });

        Gate::define('admission-delete', function ($user) {
            return $user->hasAccess(['admission-delete']);
        });
    }
}
