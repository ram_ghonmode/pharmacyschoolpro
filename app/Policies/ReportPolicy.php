<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReportPolicy
{
    use HandlesAuthorization;

    public static function generalRegisterPolicies()
    {
        Gate::define('generalregister', function ($user) {
            return $user->hasAccess(['generalregister']);
        });

        Gate::define('printgeneralregister', function ($user) {
            return $user->hasAccess(['printgeneralregister']);
        });
    }

    public static function leavingPolicies()
    {
        Gate::define('leaving-certificate', function ($user) {
            return $user->hasAccess(['leaving-certificate']);
        });

        Gate::define('printleaving', function ($user) {
            return $user->hasAccess(['printleaving']);
        });
    }

    public static function bonafiedPolicies()
    {
        Gate::define('bonafied-list', function ($user) {
            return $user->hasAccess(['bonafied-list']);
        });

        Gate::define('bonafiedcertificate', function ($user) {
            return $user->hasAccess(['bonafiedcertificate']);
        });

        Gate::define('printbonafied', function ($user) {
            return $user->hasAccess(['printbonafied']);
        });
    }


    public static function characterPolicies()
    {
        Gate::define('character-list', function ($user) {
            return $user->hasAccess(['character-list']);
        });

        Gate::define('character-certificate', function ($user) {
            return $user->hasAccess(['character-certificate']);
        });

        Gate::define('printcharacter', function ($user) {
            return $user->hasAccess(['printcharacter']);
        });
    }

    public static function attemptPolicies()
    {
        Gate::define('attempt-list', function ($user) {
            return $user->hasAccess(['attempt-list']);
        });

        Gate::define('attempt-certificate', function ($user) {
            return $user->hasAccess(['attempt-certificate']);
        });

        Gate::define('printattempt', function ($user) {
            return $user->hasAccess(['printattempt']);
        });
    }
}
