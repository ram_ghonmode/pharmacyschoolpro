<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamManagementPolicy
{
    use HandlesAuthorization;

    public static function subjectPolicies(){
        Gate::define('subject-list', function ($user) {
            return $user->hasAccess(['subject-list']);
        });

        Gate::define('subject-add', function ($user) {
            return $user->hasAccess(['subject-add']);
        });

        Gate::define('subject-edit', function ($user) {
            return $user->hasAccess(['subject-edit']);
        });

        Gate::define('subject-delete', function ($user) {
            return $user->hasAccess(['subject-delete']);
        });
    }

    public static function testPolicies(){
        Gate::define('test-list', function ($user) {
            return $user->hasAccess(['test-list']);
        });

        Gate::define('test-add', function ($user) {
            return $user->hasAccess(['test-add']);
        });

        Gate::define('test-edit', function ($user) {
            return $user->hasAccess(['test-edit']);
        });

        Gate::define('test-delete', function ($user) {
            return $user->hasAccess(['test-delete']);
        });
    }

    public static function classTestPolicies(){
        Gate::define('class-test-subject', function ($user) {
            return $user->hasAccess(['class-test-subject']);
        });

        Gate::define('assign-class-subject', function ($user) {
            return $user->hasAccess(['assign-class-subject']);
        });

        Gate::define('assign-class-test', function ($user) {
            return $user->hasAccess(['assign-class-test']);
        });
    }
}
