<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassRoomManagementPolicy
{
    use HandlesAuthorization;

    public static function classRoomPolicies(){
        Gate::define('class-room-list', function ($user) {
            return $user->hasAccess(['class-room-list']);
        });
    }

    public static function classTeacherPolicies(){
        Gate::define('class-teacher-list', function ($user) {
            return $user->hasAccess(['class-teacher-list']);
        });

        Gate::define('class-teacher-add', function ($user) {
            return $user->hasAccess(['class-teacher-add']);
        });

        Gate::define('class-teacher-edit', function ($user) {
            return $user->hasAccess(['class-teacher-edit']);
        });

        Gate::define('class-teacher-delete', function ($user) {
            return $user->hasAccess(['class-teacher-delete']);
        });
    }
}
