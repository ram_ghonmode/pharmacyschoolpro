<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class StaffManagementPolicy
{
    use HandlesAuthorization;

    public static function employeePolicies(){
        Gate::define('employee-list', function ($user) {
            return $user->hasAccess(['employee-list']);
        });

        Gate::define('employee-add', function ($user) {
            return $user->hasAccess(['employee-add']);
        });

        Gate::define('employee-edit', function ($user) {
            return $user->hasAccess(['employee-edit']);
        });

        Gate::define('employee-delete', function ($user) {
            return $user->hasAccess(['employee-delete']);
        });
    }
}
