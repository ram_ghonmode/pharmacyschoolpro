<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Carbon\Carbon;
use DB;

class StudentAttendance extends Model
{
    protected $fillable = ['teacher_id','school_id','class_id','section','admission_id','date','is_present','is_edited'];

    public function admissions()
    {
        return $this->belongsTo('App\Admission', 'admission_id');
    }

    public function getAllClsAttendance(){
        $studAttendance = false;
        $studAttendance = StudentAttendance::groupBy(['date','teacher_id','school_id','class_id','section'])
        									->select('student_attendances.*',DB::raw('sum(is_present) as present'),'is_present', DB::raw('count(*) as total'))
        									->orderBy('date','DESC')
        									->paginate(Config::get('constant.datalength'));
        return $studAttendance;
    }

    public function getListOfAllStudent($condition = 0)
    {
        return StudentAttendance::orderBy('id', 'DESC')->where($condition)->get();
    }

    public function saveStudentAttendance(StudentAttendance $StudentAttendance, $data)
    {
		$saveResult = false;
        DB::beginTransaction();
        $saveResult = StudentAttendance::updateOrCreate(['id' => isset($StudentAttendance->id) ? $StudentAttendance->id : 0], $data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }
}
