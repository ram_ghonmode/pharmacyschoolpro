<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class AcademicYear extends Model
{
    protected $fillable = ['academic_year'];

    public function getAllAcademicYears(){
        return AcademicYear::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllAcademicYearsList(){
        return AcademicYear::orderBy('id', 'DESC')->where('status', 1)->pluck('academic_year', 'id');
    }

    public function saveAcademicYear(AcademicYear $year, $data){
        $saveResult = false;
        $saveResult = AcademicYear::updateOrCreate(['id' => isset($year->id) ? $year->id : 0], $data);
        return $saveResult;
    }

    public function getAcademicYearDetail(AcademicYear $year){
        $yearDetail = false;
        $yearDetail = AcademicYear::where('id', isset($year->id) ? $year->id : 0)->first();
        return $yearDetail;
    }
}
