<?php

namespace App;
use Config;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['document_name'];

    public function getAllDocuments(){
        return Document::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllDocumentList(){
        return Document::where('status',1)->pluck('document_name', 'id');
    }

    public function saveDocument(Document $document, $data){
        $saveResult = false;
        $saveResult = Document::updateOrCreate(['id' => isset($document->id) ? $document->id : 0], $data);
        return $saveResult;
    }

    public function getDocumentDetail(Document $document){
        $documentDetail = false;
        $documentDetail = Document::where('id', isset($document->id) ? $document->id : 0)->first();
        return $documentDetail;
    }
}
