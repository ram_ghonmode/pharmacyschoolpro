<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentBankInfo extends Model
{
    protected $fillable = ['admission_id', 'gr_number', 'student_id', 'aadhar_no', 'bank_name', 'account_no', 'ifsc', 'scholarship_type'];

    public function admissions(){
		return $this->belongsTo('App\Admission', 'admission_id');
    }
}
