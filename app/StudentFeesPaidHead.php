<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentFeesPaidHead extends Model
{
    protected $fillable = ['fees_id','head_id','amount'];

    public function fessHead(){
    	return $this->belongsTo('App\FeesHead', 'head_id');
    }
}
