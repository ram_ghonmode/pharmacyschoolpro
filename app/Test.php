<?php

namespace App;
use Config;
use Auth;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = ['test_name'];

    public function getAllTests(){
        return Test::orderBy('id', 'DESC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getTestList(){
        return Test::where('status',1)->pluck('test_name', 'id');
    }

    public function saveTest(Test $test, $data){
        $saveResult = false;
        $saveResult = Test::updateOrCreate(['id' => isset($test->id) ? $test->id : 0], $data);
        return $saveResult;
    }

    public function getTestDetail(Test $test){
        $testDetail = false;
        $testDetail = Test::where('id', isset($test->id) ? $test->id : 0)->first();
        return $testDetail;
    }
}
