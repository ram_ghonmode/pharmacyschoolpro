<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentGuardianInfo extends Model
{
    protected $fillable = ['admission_id', 'gr_number', 'student_id', 'father_first_name', 'father_middle_name', 'father_last_name', 'qualification', 'occupation', 'income', 'perm_address', 'local_address', 'tahsil', 'district', 'state', 'country', 'mobile_no', 'under_bpl', 'locality'];

    public function admissions(){
		$this->belongsTo('App\Admission', 'admission_id');
    }
}
