<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentGeneralInfo extends Model
{
    protected $fillable = ['admission_id', 'gr_number', 'student_id', 'blood_group', 'height_feet', 'height_inch', 'weight', 'handicapped', 'last_school_attended', 'leaving_date', 'reason_of_leaving', 'leaving_class', 'pim_1', 'pim_2'];

    public function admissions(){
		return $this->belongsTo('App\Admission', 'admission_id');
    }
}
