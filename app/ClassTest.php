<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassTest extends Model
{
    protected $fillable = ['test_id', 'class_id'];

    public function tests(){
        return $this->belongsTo('App\Test', 'test_id');
    }

    public function classes(){
        return $this->belongsTo('App\ClassList', 'class_id');
    }

    public function getAllClassTestsList(){
        return ClassTest::where(['status' => 1])->get();
    }

    public function getAllClassTests($classId){
        return ClassTest::where(['status' => 1, 'class_id' => $classId])->get();
    }

    public function assignTest(ClassTest $test, $data){
        $saveResult = false;
        foreach($data['test_id'] as $key => $testId){
            if(!empty($testId)){
                $cond = [
                    'class_id' => $data['class_id'],
                    'test_id' => $testId,
                    'status' => 1
                ];
                $cond = array_filter($cond);
                $details = ClassTest::where($cond)->get()->first();
                if(!empty($details['id'])){
                    $members['class_id']  = $data['class_id'];
                    $members['test_id'] = $testId;
                    $saveResult = ClassTest::updateOrCreate(['id' => isset($details['id']) ? $details['id'] : 0], $members);
                }else{
                    $members['class_id']  = $data['class_id'];
                    $members['test_id'] = $testId;
                    $saveResult = ClassTest::create($members);
                }
            }
        }
        return $saveResult;
    }
}
