<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class CharacterCertificate extends Model
{
    protected $fillable = ['gr_number','admission_in','fullname','current_class','current_date','admission_date','last_date','dob','section'];

    public function admission(){
    	return $this->belongsTo('App\Admission', 'admission_id');
    }

    public function getAllCharacterCertificates(){
        return CharacterCertificate::orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));;
    }

    public function saveCharacter($data)
    {
        $getCharacter = Admission::with('leavingcertificates')->where($data)->first();
        $fullname = $getCharacter['first_name'].' '.$getCharacter['middle_name'].' '.$getCharacter['last_name'];
        $saveData = [
            'gr_number'         => $getCharacter['gr_number'],
            'admission_in'    => $getCharacter['admission_in'],
            'fullname'          => $fullname,
            'current_date'      => date("Y-m-d"),
            'current_class'     => $getCharacter['current_class'],
            'dob'               => $getCharacter['dob'],
            'admission_date'    => $getCharacter['admission_date'],
            'section'           => $getCharacter['section'],
            'last_date'         => $getCharacter['leavingcertificates']['leaving_date']
        ];
        $saveResult = false;
        DB::beginTransaction();
        $saveResult = CharacterCertificate::create($saveData);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }
}
