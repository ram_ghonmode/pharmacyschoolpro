<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use File;
use Auth;

class School extends Model
{
    protected $fillable = ['school_name', 'institute_name', 'school_name_marathi', 'academic_year', 'session', 'country_id', 'state_id', 'city_id', 'tahsil_id', 'address', 'pin_code', 'fax_no', 'recognition_no', 'index_no', 'udise_no', 'website_url', 'email', 'phone_no', 'board', 'establishment_year', 'granted_from_date', 'granted_end_date', 'logo', 'principal_name', 'principal_mob_no', 'sport_teacher_name', 'sport_teacher_mob_no'];

    public function tahsil(){
    	return $this->belongsTo('App\Tahsil', 'tahsil_id');
    }

    public function city(){
    	return $this->belongsTo('App\City', 'city_id');
    }

    public function academicYears(){
    	return $this->belongsTo('App\AcademicYear', 'academic_year');
    }

    public function getAllSchools(){
        $cond = ['status' => '1', 'id' => session()->get('school_id')];
        if(session()->get('school_id') == 0){
            unset($cond['id']);
        }
        return School::orderBy('id', 'ASC')->where($cond)->paginate(Config::get('constant.datalength'));
    }

    public function getAllSchoolsList(){
        $cond = ['status' => '1', 'id' => session()->get('school_id')];
        if(session()->get('school_id') == 0){
            unset($cond['id']);
        }
        return School::where($cond)->pluck('school_name', 'id', 'academic_year');
    }

    public function getTotSchools(){
        $cond = ['status' => '1', 'id' => session()->get('school_id')];
        if(session()->get('school_id') == 0){
            unset($cond['id']);
        }
        $result =  School::where($cond)->with('academicYears')->select('school_name', 'id', 'academic_year')->get();
        return $result;
    }

    public function schoolListWhileLogin(){
        return School::orderBy('id','DESC')->select('id', 'school_name')->where('status','1')->get();
    }

    public function saveSchool(School $school, $data){
        $saveResult = false;
        $logoImg = null;
        $schoolDetail = $this->getSchoolDetail($school);
        if(!empty($schoolDetail)){
            $logoImg = $schoolDetail->logo;
            $oldLogo = substr($schoolDetail->logo, strrpos($schoolDetail->logo, '/') + 1);
            if(!empty($data['logo'])){
                File::delete(public_path('images/logos/'. $oldLogo)); // Delete previous image from folder
            }
        }
        $data['logo']   = !empty($data['logo']) ? Common::imageUpload($data['logo'], 'logos') : $logoImg;
        $saveResult = School::updateOrCreate(['id' => isset($school->id) ? $school->id : 0], $data);
        return $saveResult;
    }

    public function getSchoolDetail(School $school){
        $schoolDetail = false;
        $schoolDetail = School::where('id', isset($school->id) ? $school->id : 0)->first();
        return $schoolDetail;
    }
}
