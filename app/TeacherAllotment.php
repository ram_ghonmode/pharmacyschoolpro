<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class TeacherAllotment extends Model
{
    protected $fillable = ['school_id','class_id','section','teacher_id','academic_year'];

    public function employees(){
        return $this->belongsTo('App\Employee', 'teacher_id');
    }
  
    public function classes(){
        return $this->belongsTo('App\ClassList', 'class_id');
    }

    public function academicYears(){
    	return $this->belongsTo('App\AcademicYear', 'academic_year');
    }

    public function getAllClassTeachers(){
        return TeacherAllotment::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function saveClassTeacher(TeacherAllotment $teacher, $data){
        $saveResult = false;
        $saveResult = TeacherAllotment::updateOrCreate(['id' => isset($teacher->id) ? $teacher->id : 0], $data);
        return $saveResult;
    }

    public function getClassTeacherDetail(TeacherAllotment $teacher){
        $teacherDetail = false;
        $teacherDetail = TeacherAllotment::where('id', isset($teacher->id) ? $teacher->id : 0)->first();
        return $teacherDetail;
    }
}