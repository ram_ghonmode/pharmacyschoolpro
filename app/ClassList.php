<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;
use Auth;

class ClassList extends Model
{
    protected $fillable = ['class_name'];

    public function getAllClasses(){
        return ClassList::orderBy('id', 'ASC')->where('status', 1)->paginate(Config::get('constant.datalength'));
    }

    public function getAllClassList(){
        return ClassList::where('status',1)->pluck('class_name', 'id');
    }

    public function saveClass(ClassList $class, $data){
        $saveResult = false;
        $saveResult = ClassList::updateOrCreate(['id' => isset($class->id) ? $class->id : 0], $data);
        return $saveResult;
    }

    public function getClassDetail(ClassList $class){
        $classDetail = false;
        $classDetail = ClassList::where('id', isset($class->id) ? $class->id : 0)->first();
        return $classDetail;
    }
}
